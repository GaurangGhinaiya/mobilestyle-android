package com.moremy.style.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Rating {

    @SerializedName("sum_rating")
    @Expose
    private float sum_rating;

    @SerializedName("count_rating")
    @Expose
    private float count_rating;

    @SerializedName("avg_rating")
    @Expose
    private float avg_rating;


    public float getsum_rating() {
        return sum_rating;
    }

    public void setsum_rating(float sum_rating) {
        this.sum_rating = sum_rating;
    }

    public float getcount_rating() {
        return count_rating;
    }

    public void setcount_rating(float count_rating) {
        this.count_rating = count_rating;
    }

    public float getavg_rating() {
        return avg_rating;
    }

    public void setavg_rating(float avg_rating) {
        this.avg_rating = avg_rating;
    }

}