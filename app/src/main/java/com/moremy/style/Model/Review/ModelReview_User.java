
package com.moremy.style.Model.Review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelReview_User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("googe_id")
    @Expose
    private Object googeId;
    @SerializedName("apple_id")
    @Expose
    private Object appleId;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("is_premium")
    @Expose
    private Integer isPremium;
    @SerializedName("stripe_cust_id")
    @Expose
    private String stripeCustId;
    @SerializedName("stripe_user_id")
    @Expose
    private String stripeUserId;
    @SerializedName("stripe_subscription_id")
    @Expose
    private String stripeSubscriptionId;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("salon_name")
    @Expose
    private Object salonName;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("passbase_id")
    @Expose
    private Object passbaseId;
    @SerializedName("contact")
    @Expose
    private Object contact;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("one_line")
    @Expose
    private String oneLine;
    @SerializedName("service_id")
    @Expose
    private Object serviceId;
    @SerializedName("date_of_birth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("user_image")
    @Expose
    private Object userImage;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("website_url")
    @Expose
    private Object websiteUrl;
    @SerializedName("instagram_url")
    @Expose
    private Object instagramUrl;
    @SerializedName("telephone")
    @Expose
    private Object telephone;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("keywords")
    @Expose
    private Object keywords;
    @SerializedName("address_line_1")
    @Expose
    private Object addressLine1;
    @SerializedName("street_name")
    @Expose
    private Object streetName;
    @SerializedName("county")
    @Expose
    private Object county;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("selling_status")
    @Expose
    private Integer sellingStatus;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("radius_from_postal_code")
    @Expose
    private Double radiusFromPostalCode;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("is_selfie_verify")
    @Expose
    private Integer isSelfieVerify;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("fcm_token")
    @Expose
    private Object fcmToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Object getGoogeId() {
        return googeId;
    }

    public void setGoogeId(Object googeId) {
        this.googeId = googeId;
    }

    public Object getAppleId() {
        return appleId;
    }

    public void setAppleId(Object appleId) {
        this.appleId = appleId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }

    public String getStripeCustId() {
        return stripeCustId;
    }

    public void setStripeCustId(String stripeCustId) {
        this.stripeCustId = stripeCustId;
    }

    public String getStripeUserId() {
        return stripeUserId;
    }

    public void setStripeUserId(String stripeUserId) {
        this.stripeUserId = stripeUserId;
    }

    public String getStripeSubscriptionId() {
        return stripeSubscriptionId;
    }

    public void setStripeSubscriptionId(String stripeSubscriptionId) {
        this.stripeSubscriptionId = stripeSubscriptionId;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public Object getSalonName() {
        return salonName;
    }

    public void setSalonName(Object salonName) {
        this.salonName = salonName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Object getPassbaseId() {
        return passbaseId;
    }

    public void setPassbaseId(Object passbaseId) {
        this.passbaseId = passbaseId;
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOneLine() {
        return oneLine;
    }

    public void setOneLine(String oneLine) {
        this.oneLine = oneLine;
    }

    public Object getServiceId() {
        return serviceId;
    }

    public void setServiceId(Object serviceId) {
        this.serviceId = serviceId;
    }

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public Object getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(Object websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public Object getInstagramUrl() {
        return instagramUrl;
    }

    public void setInstagramUrl(Object instagramUrl) {
        this.instagramUrl = instagramUrl;
    }

    public Object getTelephone() {
        return telephone;
    }

    public void setTelephone(Object telephone) {
        this.telephone = telephone;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getKeywords() {
        return keywords;
    }

    public void setKeywords(Object keywords) {
        this.keywords = keywords;
    }

    public Object getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(Object addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public Object getStreetName() {
        return streetName;
    }

    public void setStreetName(Object streetName) {
        this.streetName = streetName;
    }

    public Object getCounty() {
        return county;
    }

    public void setCounty(Object county) {
        this.county = county;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Integer getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(Integer sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Double getRadiusFromPostalCode() {
        return radiusFromPostalCode;
    }

    public void setRadiusFromPostalCode(Double radiusFromPostalCode) {
        this.radiusFromPostalCode = radiusFromPostalCode;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getIsSelfieVerify() {
        return isSelfieVerify;
    }

    public void setIsSelfieVerify(Integer isSelfieVerify) {
        this.isSelfieVerify = isSelfieVerify;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Object getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(Object fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
