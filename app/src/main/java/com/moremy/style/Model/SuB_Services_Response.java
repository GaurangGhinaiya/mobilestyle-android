package com.moremy.style.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuB_Services_Response {

@SerializedName("flag")
@Expose
private Boolean flag;
@SerializedName("data")
@Expose
private List<SuB_Services_Data> data = null;
@SerializedName("message")
@Expose
private String message;
@SerializedName("code")
@Expose
private Integer code;

public Boolean getFlag() {
return flag;
}

public void setFlag(Boolean flag) {
this.flag = flag;
}

public List<SuB_Services_Data> getData() {
return data;
}

public void setData(List<SuB_Services_Data> data) {
this.data = data;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

}