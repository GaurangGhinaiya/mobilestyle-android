
package com.moremy.style.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelectModel_StylistSubservice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("subservice_id")
    @Expose
    private Integer subserviceId;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("service")
    @Expose
    private SelectModel_Service service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(Integer subserviceId) {
        this.subserviceId = subserviceId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SelectModel_Service getService() {
        return service;
    }

    public void setService(SelectModel_Service service) {
        this.service = service;
    }

}
