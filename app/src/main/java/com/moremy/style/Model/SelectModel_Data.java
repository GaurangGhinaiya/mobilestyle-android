
package com.moremy.style.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelectModel_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("stylist_subservices")
    @Expose
    private List<SelectModel_StylistSubservice> stylistSubservices = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<SelectModel_StylistSubservice> getStylistSubservices() {
        return stylistSubservices;
    }

    public void setStylistSubservices(List<SelectModel_StylistSubservice> stylistSubservices) {
        this.stylistSubservices = stylistSubservices;
    }


    @SerializedName("other_sub_services")
    @Expose
    private List<SelectModel_OtherSubService> otherSubServices = null;

    public List<SelectModel_OtherSubService> getOtherSubServices() {
        return otherSubServices;
    }

    public void setOtherSubServices(List<SelectModel_OtherSubService> otherSubServices) {
        this.otherSubServices = otherSubServices;
    }

}
