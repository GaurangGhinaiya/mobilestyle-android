
package com.moremy.style.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StripePlan_Data {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("object")
    @Expose
    private String object;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("aggregate_usage")
    @Expose
    private Object aggregateUsage;
    @SerializedName("amount")
    @Expose
    private float amount;
    @SerializedName("amount_decimal")
    @Expose
    private String amountDecimal;
    @SerializedName("billing_scheme")
    @Expose
    private String billingScheme;
    @SerializedName("created")
    @Expose
    private Integer created;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("interval")
    @Expose
    private String interval;
    @SerializedName("interval_count")
    @Expose
    private Integer intervalCount;
    @SerializedName("livemode")
    @Expose
    private Boolean livemode;
    @SerializedName("metadata")
    @Expose
    private List<Object> metadata = null;
    @SerializedName("nickname")
    @Expose
    private Object nickname;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("tiers")
    @Expose
    private Object tiers;
    @SerializedName("tiers_mode")
    @Expose
    private Object tiersMode;
    @SerializedName("transform_usage")
    @Expose
    private Object transformUsage;
    @SerializedName("trial_period_days")
    @Expose
    private Object trialPeriodDays;
    @SerializedName("usage_type")
    @Expose
    private String usageType;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Object getAggregateUsage() {
        return aggregateUsage;
    }

    public void setAggregateUsage(Object aggregateUsage) {
        this.aggregateUsage = aggregateUsage;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getAmountDecimal() {
        return amountDecimal;
    }

    public void setAmountDecimal(String amountDecimal) {
        this.amountDecimal = amountDecimal;
    }

    public String getBillingScheme() {
        return billingScheme;
    }

    public void setBillingScheme(String billingScheme) {
        this.billingScheme = billingScheme;
    }

    public Integer getCreated() {
        return created;
    }

    public void setCreated(Integer created) {
        this.created = created;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public Integer getIntervalCount() {
        return intervalCount;
    }

    public void setIntervalCount(Integer intervalCount) {
        this.intervalCount = intervalCount;
    }

    public Boolean getLivemode() {
        return livemode;
    }

    public void setLivemode(Boolean livemode) {
        this.livemode = livemode;
    }

    public List<Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<Object> metadata) {
        this.metadata = metadata;
    }

    public Object getNickname() {
        return nickname;
    }

    public void setNickname(Object nickname) {
        this.nickname = nickname;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Object getTiers() {
        return tiers;
    }

    public void setTiers(Object tiers) {
        this.tiers = tiers;
    }

    public Object getTiersMode() {
        return tiersMode;
    }

    public void setTiersMode(Object tiersMode) {
        this.tiersMode = tiersMode;
    }

    public Object getTransformUsage() {
        return transformUsage;
    }

    public void setTransformUsage(Object transformUsage) {
        this.transformUsage = transformUsage;
    }

    public Object getTrialPeriodDays() {
        return trialPeriodDays;
    }

    public void setTrialPeriodDays(Object trialPeriodDays) {
        this.trialPeriodDays = trialPeriodDays;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
