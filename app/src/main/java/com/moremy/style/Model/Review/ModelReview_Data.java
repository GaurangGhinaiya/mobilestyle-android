
package com.moremy.style.Model.Review;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelReview_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("rating_type")
    @Expose
    private String ratingType;
    @SerializedName("user")
    @Expose
    private ModelReview_User user;
    @SerializedName("customer")
    @Expose
    private ModelReview_Customer customer;
    @SerializedName("order")
    @Expose
    private ModelReview_Order order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }

    public ModelReview_User getUser() {
        return user;
    }

    public void setUser(ModelReview_User user) {
        this.user = user;
    }

    public ModelReview_Customer getCustomer() {
        return customer;
    }

    public void setCustomer(ModelReview_Customer customer) {
        this.customer = customer;
    }

    public ModelReview_Order getOrder() {
        return order;
    }

    public void setOrder(ModelReview_Order order) {
        this.order = order;
    }

}
