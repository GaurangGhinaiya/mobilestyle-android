package com.moremy.style.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.AnalyticsApplication;
import com.moremy.style.CommanActivity.Base1_Activity;

import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.Customer.Customer_MainProfile_Activty;

import com.moremy.style.Salon.Salon_MainProfile_Activty;


import com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty;
import com.moremy.style.R;
import com.moremy.style.Utills.AppSingleton;
import com.moremy.style.Utills.Preferences;

import com.moremy.style.Utills.utills;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Splash_Activity extends Base1_Activity {
    Preferences preferences;

    Tracker mTracker;
    Context context;

    String type = "";
    String order_id = "";
    String channelUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = Splash_Activity.this;

        // context=Splash_Activity.this;


        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        preferences = new Preferences(this);

        getKey();


        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        if (type != null) {
            if (type.equalsIgnoreCase("Notification")) {
                channelUrl = intent.getStringExtra("channelUrl");
            } else if (type.equalsIgnoreCase("booking")) {
                channelUrl = "" + intent.getStringExtra("channelUrl");
                order_id = "" + intent.getStringExtra("order_id");
            } else if (type.equalsIgnoreCase("order")) {
                channelUrl = "" + intent.getStringExtra("channelUrl");
                order_id = "" + intent.getStringExtra("order_id");
            }
        }


        getUserloginornot();
        // method_get_agency_list();

    }


    public static boolean isReadExternalStoragePermissionsRequired(
            @NonNull Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED;
    }


    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    public void getKey() {
        if (utills.isOnline(this)) {

            AndroidNetworking.get(Global_Service_Api.API_get_setting_keys)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);

                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String flag = jsonObject.getString("flag");
                                if (flag.equalsIgnoreCase("true")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String stripe_id = "" + jsonObject1.getString("stripe_id");
                                    String sb_application_id = "" + jsonObject1.getString("sb_application_id");
                                    String passbase_id = "" + jsonObject1.getString("passbase_id");
                                    String stripe_client_id = "" + jsonObject1.getString("stripe_client_id");

                                    preferences.setPRE_stripe_id("" + stripe_id);
                                    preferences.setPRE_sb_application_id("" + sb_application_id);
                                    preferences.setPRE_passbase_id_Key("" + passbase_id);
                                    preferences.setPRE_stripe_client_id("" + stripe_client_id);

                                    Log.e("stripe_id", stripe_id);
                                    Log.e("sb_application_id", sb_application_id);
                                    Log.e("passbase_id", passbase_id);
                                    Log.e("stripe_client_id", passbase_id);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    public void getUserloginornot() {
        if (utills.isOnline(this)) {

            AndroidNetworking.get(Global_Service_Api.API_logginuser)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);

                            try {
                                final JSONObject jsonObject = new JSONObject(result);
                                String flag = jsonObject.getString("flag");
                                if (flag.equalsIgnoreCase("true")) {

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {


                                            try {
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                                String user_type = "" + jsonObject1.getString("user_type");
                                                preferences.setPRE_SendBirdUserName("");
                                                preferences.setPRE_SendBirdUserId("");

                                                String id = "" + jsonObject1.getString("id");
                                                String name1 = "" + jsonObject1.getString("name");
                                                String lastname = "" + jsonObject1.getString("lastname");


                                                if (lastname.equalsIgnoreCase("null")) {
                                                    lastname = "";
                                                }

                                                preferences.setPRE_SendBirdUserName("" + name1 + " " + lastname);
                                                preferences.setPRE_SendBirdUserId("" + id);


                                                preferences.setPRE_Typee("" + user_type);
                                                if (user_type.equalsIgnoreCase("3")) {
                                                    preferences.setPRE_LoginType_Stylist_Seller("3");
                                                    Intent i = new Intent(Splash_Activity.this, StylistSeller_MainProfile_Activty.class);
                                                    i.putExtra("channelUrl", "" + channelUrl);
                                                    i.putExtra("type", "" + type);
                                                    i.putExtra("order_id", "" + order_id);
                                                    startActivity(i);
                                                } else if (user_type.equalsIgnoreCase("4")) {
                                                    Intent i = new Intent(Splash_Activity.this, Salon_MainProfile_Activty.class);
                                                    i.putExtra("channelUrl", "" + channelUrl);
                                                    i.putExtra("type", "" + type);
                                                    i.putExtra("order_id", "" + order_id);
                                                    startActivity(i);
                                                } else if (user_type.equalsIgnoreCase("2")) {
                                                    Intent i = new Intent(Splash_Activity.this, Customer_MainProfile_Activty.class);
                                                    i.putExtra("channelUrl", "" + channelUrl);
                                                    i.putExtra("type", "" + type);
                                                    i.putExtra("order_id", "" + order_id);
                                                    startActivity(i);
                                                } else if (user_type.equalsIgnoreCase("5")) {
                                                    preferences.setPRE_LoginType_Stylist_Seller("5");

                                                    Intent i = new Intent(Splash_Activity.this, StylistSeller_MainProfile_Activty.class);
                                                    i.putExtra("channelUrl", "" + channelUrl);
                                                    i.putExtra("type", "" + type);
                                                    i.putExtra("order_id", "" + order_id);
                                                    startActivity(i);
                                                }


                                                finish();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    }, 1000);

                                } else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {


                                            preferences.setPRE_Email("");

                                            preferences.setPRE_Main_ServiceID("");
                                            preferences.setPRE_Mile("");
                                            preferences.setPRE_Code("");
                                            preferences.setPRE_address("");
                                            preferences.setPRE_City("");
                                            preferences.setPRE_SelectedFile_logo("");
                                            preferences.setPRE_SelectedFile_profile("");
                                            preferences.setPRE_Gender("");
                                            preferences.setPRE_Password("");
                                            preferences.setPRE_FirstName("");
                                            preferences.setPRE_LastName("");
                                            preferences.setPRE_Main_Oneline("");
                                            preferences.setPRE_TOKEN("");
                                            preferences.setPRE_UserName("");
                                            preferences.setPRE_Latitude("");
                                            preferences.setPRE_Longitude("");
                                            preferences.setPRE_SalonName("");
                                            preferences.setPRE_passbase_verify(false);
                                            preferences.setPRE_passbase_id("");
                                            preferences.setPRE_SendBirdUserName("");
                                            preferences.setPRE_SendBirdUserId("");

                                            preferences.setPRE_Is_Premium(false);
                                            preferences.setPRE_Is_Premium_Salon(false);


                                            Intent i = new Intent(Splash_Activity.this, Login_activty.class);
                                            startActivity(i);
                                            finish();

                                        }
                                    }, 1000);
                                }


                            } catch (JSONException e) {

                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //sending tracking information
        mTracker.setScreenName("Splash Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void method_get(String channelUrl) {


        if (utills.isOnline(context)) {

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    Global_Service_Api.API_logginuser, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response_splesh", "" + response);
                            JSONObject JSONObject = null;
                            try {
                                JSONObject = response.getJSONObject("data");
                                String flag_true = response.getString("flag");
                                if (flag_true.equals("true")) {

                                } else if (flag_true.equals("false")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error1", "" + error.getMessage());
                    // hide the progress dialog

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + preferences.getPRE_TOKEN());
                    return params;
                }
            };
            AppSingleton.getInstance(context).addToRequestQueue(jsonObjReq, "AsyncCallShowAppList");
        } else {
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    Dialog progressDialog = null;


    private void method_get_agency_list() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get("http://kiosk.homecare2go.com/Api/v1/get-company-branches")
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            Log.e("Getfind_agency", result);
                            if (result == null || result == "") return;

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


}
