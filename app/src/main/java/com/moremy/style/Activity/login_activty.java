package com.moremy.style.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.moremy.style.R;

public class login_activty extends AppCompatActivity {


    LinearLayout ll_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        ll_register = findViewById(R.id.ll_register);
        ll_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(login_activty.this, register_activty.class);
                startActivity(i);

            }
        });


    }
}
