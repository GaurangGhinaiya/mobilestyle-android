package com.moremy.style.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;

public class verify_activty extends Base_Activity {


    List<String> data_review_list = new ArrayList<>();
    Adapter_review adapter_review;

    GridView listview_review;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_activty);

        context = verify_activty.this;

        data_review_list = new ArrayList<>();
        listview_review = findViewById(R.id.listview_review);
        adapter_review = new Adapter_review(context, data_review_list);
        listview_review.setAdapter(adapter_review);


    }


    public class Adapter_review extends BaseAdapter {

        Context context;
        List<String> listState;
        LayoutInflater inflter;


        public Adapter_review(Context applicationContext, List<String> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return listState.size() + 10;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.datalist_reviewlist, null);

            return view;
        }
    }

}
