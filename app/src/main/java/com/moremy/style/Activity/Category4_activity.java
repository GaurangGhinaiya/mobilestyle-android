package com.moremy.style.Activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;

public class Category4_activity extends Base_Activity {

    RecyclerView recycleview_cat;


    List<String> cat_dataList = new ArrayList<>();
    Adapter_cat adapter_cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category4);

        recycleview_cat = findViewById(R.id.recycleview_cat);
        cat_dataList = new ArrayList<>();
        //  recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 2));
        adapter_cat = new Adapter_cat(this, cat_dataList);
        recycleview_cat.setAdapter(adapter_cat);


    }


     class Adapter_cat extends RecyclerView.Adapter<Adapter_cat.MyViewHolder> {
        private List<String> arrayList;

        Context mcontext;
        int lastPosition = -1;

        public Adapter_cat(Context context, List<String> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            LinearLayout ll_main_layout;

            public MyViewHolder(View view) {
                super(view);
                ll_main_layout = view.findViewById(R.id.ll_main_layout);
            }
        }

        private void setAnimation(View viewToAnimate, int position) {

            Log.e("setAnimation", "setAnimation" + position);
            try {
               // viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, position > lastPosition ? R.anim.anim_scale_down : R.anim.anim_scale_up));
                viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_scale));
                lastPosition = position;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public Adapter_cat.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cat_datalist4, parent, false);

            return new Adapter_cat.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_cat.MyViewHolder holder, final int position) {
            setAnimation(holder.ll_main_layout, position);

            holder.ll_main_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


//                    Intent intent = new Intent(mcontext, profile_activity.class);
//                    Bundle b = ActivityOptionsCompat
//                            .makeSceneTransitionAnimation((Activity) mcontext, holder.ll_main_layout, "example_transition").toBundle();
//                    startActivity(intent, b);


                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size() + 20;
        }

    }


}
