package com.moremy.style.Activity;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;

public class Search_Activty extends Base_Activity {


    RecyclerView recycleview_cat;
    List<String> cat_dataList = new ArrayList<>();
    Adapter_cat adapter_cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__activty);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        recycleview_cat = findViewById(R.id.recycleview_cat);
        cat_dataList = new ArrayList<>();
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new LinearLayoutManager(this, 1, false));
        adapter_cat = new Adapter_cat(this, cat_dataList);
        recycleview_cat.setAdapter(adapter_cat);

    }


    public class Adapter_cat extends RecyclerView.Adapter<Adapter_cat.MyViewHolder> {
        private List<String> arrayList;

        Context mcontext;

        public Adapter_cat(Context context, List<String> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public MyViewHolder(View view) {
                super(view);

            }
        }

        @Override
        public Adapter_cat.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cat_dataserchlist, parent, false);

            return new Adapter_cat.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_cat.MyViewHolder holder, final int position) {


        }

        @Override
        public int getItemCount() {
            return arrayList.size() + 10;
        }

    }


}
