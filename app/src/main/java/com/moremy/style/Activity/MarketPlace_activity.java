package com.moremy.style.Activity;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.WindowManager;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;

public class MarketPlace_activity extends Base_Activity {




    RecyclerView recycleview_marketplaces;


    List<String> cat_dataList = new ArrayList<>();
   // Adapter_marketplaces adapter_cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_place_activity);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        recycleview_marketplaces = findViewById(R.id.recycleview_marketplaces);
        cat_dataList = new ArrayList<>();
        //  recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_marketplaces.setLayoutManager(new GridLayoutManager(this, 2));
      //  adapter_cat = new Adapter_marketplaces(this, cat_dataList);
      //  recycleview_marketplaces.setAdapter(adapter_cat);


    }


//    public class Adapter_marketplaces extends RecyclerView.Adapter<Adapter_marketplaces.MyViewHolder> {
//        private List<String> arrayList;
//
//        Context mcontext;
//        int lastPosition = -1;
//
//        public Adapter_marketplaces(Context context, List<String> arrayList) {
//            this.arrayList = arrayList;
//            mcontext = context;
//        }
//
//        public class MyViewHolder extends RecyclerView.ViewHolder {
//            CardView card_mainlayout;
//
//            public MyViewHolder(View view) {
//                super(view);
//                card_mainlayout = view.findViewById(R.id.card_mainlayout);
//            }
//        }
//
//        private void setAnimation(View viewToAnimate, int position) {
//
//            Log.e("setAnimation", "setAnimation" + position);
//            try {
//                // viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, position > lastPosition ? R.anim.anim_scale_down : R.anim.anim_scale_up));
//                viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_scale));
//                lastPosition = position;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//
//        @Override
//        public Adapter_marketplaces.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
//            final View itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.datalist_shoplist, parent, false);
//
//            return new Adapter_marketplaces.MyViewHolder(itemView);
//        }
//
//        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//        @Override
//        public void onBindViewHolder(final Adapter_marketplaces.MyViewHolder holder, final int position) {
//            setAnimation(holder.card_mainlayout, position);
//
//            holder.card_mainlayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//
//
//
//
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return arrayList.size() + 20;
//        }
//
//    }


}
