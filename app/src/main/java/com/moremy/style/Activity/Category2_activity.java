package com.moremy.style.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;

public class Category2_activity extends Base_Activity {

    RecyclerView recycleview_cat;
    List<String> cat_dataList = new ArrayList<>();
    Adapter_cat adapter_cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category2);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        recycleview_cat = findViewById(R.id.recycleview_cat);
        cat_dataList = new ArrayList<>();
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new LinearLayoutManager(this, 0, false));
        adapter_cat = new Adapter_cat(this, cat_dataList);
        recycleview_cat.setAdapter(adapter_cat);

        ImageView iv_logo = findViewById(R.id.iv_logo);
        // utills.scaleAndUnscaleAnimation(iv_logo);

        try {
            iv_logo.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.demo));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public class Adapter_cat extends RecyclerView.Adapter<Adapter_cat.MyViewHolder> {
        private List<String> arrayList;
        int lastPosition = -1;
        Context mcontext;

        public Adapter_cat(Context context, List<String> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            CardView card_main;

            public MyViewHolder(View view) {
                super(view);
                card_main = view.findViewById(R.id.card_main);
            }
        }

        @Override
        public Adapter_cat.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cat_datalist2, parent, false);

            return new Adapter_cat.MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_cat.MyViewHolder holder, final int position) {

            setAnimation(holder.card_main, position);

            holder.card_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mcontext, Category3_activity.class);

                    startActivity(i);
                   //overridePendingTransition(R.anim.scale_down,R.anim.scale_up);

                }
            });
        }

        private void setAnimation(View viewToAnimate, int position) {

            Log.e("setAnimation", "setAnimation" + position);
            try {
                //viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, position > lastPosition ? R.anim.anim_scale_down : R.anim.anim_scale_up));
                viewToAnimate.startAnimation(AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_scale));
                lastPosition = position;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayList.size() + 10;
        }

    }


}
