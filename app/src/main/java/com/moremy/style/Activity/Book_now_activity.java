package com.moremy.style.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.moremy.style.R;

public class Book_now_activity extends Base_Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_now);

        RelativeLayout rl_book_now = findViewById(R.id.rl_book_now);
        rl_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Book_now_activity.this, Done_activity.class);
                startActivity(i);
            }
        });

    }
}
