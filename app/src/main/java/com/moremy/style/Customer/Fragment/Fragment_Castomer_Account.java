package com.moremy.style.Customer.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Login_activty;

import com.moremy.style.Customer.Model.Customer_Data;
import com.moremy.style.Customer.Model.Customer_Responce;
import com.moremy.style.Customer.Model.Customer_Service;

import com.moremy.style.R;

import com.moremy.style.Salon.Fragment.Fragment_review_Salon;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentManager_CUSTOMER;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentTransaction_CUSTOMER;


public class Fragment_Castomer_Account extends Fragment {

    private Context context;


    public Fragment_Castomer_Account() {
    }


    Preferences preferences;
    Dialog progressDialog = null;


    TabLayout tabs;
    CustomViewPager viewPager;


    TextView tv_oneliner, tv_name, tv_servies;
    public static Customer_Data customer_data;

    ImageView img_profile;

    List<Customer_Service> customer_list_services;
    RecyclerView recycleview_services_profile;
    ProgressBar progress_bar_profile;


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_account, container, false);
        context = getActivity();

        preferences = new Preferences(context);

        NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.userProfile_NestedScrollView);
        scrollView.setFillViewport(true);

        progress_bar_profile = (ProgressBar) view.findViewById(R.id.progress_bar_profile);

        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (CustomViewPager) view.findViewById(R.id.viewPager);
        tv_oneliner = (TextView) view.findViewById(R.id.tv_oneliner);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_servies = (TextView) view.findViewById(R.id.tv_servies);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);


        tabs.addTab(tabs.newTab().setText("Reviews"));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        method_set_tab_font();

        method_get_api();

        recycleview_services_profile = view.findViewById(R.id.recycleview_services_profile);
        recycleview_services_profile.setNestedScrollingEnabled(false);


        ChipsLayoutManager spanLayoutManager = ChipsLayoutManager.newBuilder(context)
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .build();
        recycleview_services_profile.setLayoutManager(spanLayoutManager);


        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_more();
            }
        });


        return view;
    }


    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }


    private void method_more() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_customer);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);


        final TextView tv_MyProductOrders = dialog.findViewById(R.id.tv_MyProductOrders);
        tv_MyProductOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Castomer_EditAccount(2);
                LoadFragment(currentFragment);

            }
        });

        final TextView tv_MyBookings = dialog.findViewById(R.id.tv_MyBookings);
        tv_MyBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Castomer_EditAccount(3);
                LoadFragment(currentFragment);

            }
        });


        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (customer_data != null) {
            tv_name_title.setText("" + customer_data.getName());
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });

            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Customer_Sapport();
                LoadFragment(currentFragment);


            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Customer_FAQ();
                LoadFragment(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Castomer_EditAccount(0);
                LoadFragment(currentFragment);
            }
        });


        dialog.show();


    }


    public void LoadFragment(final Fragment fragment) {

        fragmentTransaction_CUSTOMER = fragmentManager_CUSTOMER.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_CUSTOMER.add(R.id.fmFragment, fragment);
        fragmentTransaction_CUSTOMER.addToBackStack(null);
        fragmentTransaction_CUSTOMER.commit();


        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                                preferences.setPRE_TOKEN("");

                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }


                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }


    private void method_get_api() {


        AndroidNetworking.get(Global_Service_Api.API_get_customer_profile)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {


                                Customer_Responce Response = new Gson().fromJson(result.toString(), Customer_Responce.class);

                                if (Response.getData() != null) {
                                    customer_data = new Customer_Data();
                                    customer_data = Response.getData();
                                    method_setdata();

                                }


                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }

    private void method_setdata() {

        if (customer_data != null) {
            tv_oneliner.setText("" + customer_data.getOneLine());
            tv_name.setText("" + customer_data.getName() + " " + customer_data.getLastname());
            //  tv_servies.setText("" + customer_data.getServiceName());


            try {
                if (customer_data.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + customer_data.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            if (customer_data.getService() != null) {
                customer_list_services = customer_data.getService();
                GridAdapter adapter_cat = new GridAdapter(context, customer_list_services);
                recycleview_services_profile.setAdapter(adapter_cat);

            }


            try {
                Pager adapter = new Pager(getChildFragmentManager(), tabs.getTabCount());
                viewPager.setAdapter(adapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    Fragment_review_Salon fragment_review;


    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);


            fragment_review = new Fragment_review_Salon();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_review;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }


    public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Customer_Service> arrayList;
        int lastPosition = -1;
        Context mcontext;

        public GridAdapter(Context context, List<Customer_Service> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_textcountry;

            public MyViewHolder(View view) {
                super(view);

                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
            }
        }

        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_customer_service, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {
            viewHolder.tv_textcountry.setText(arrayList.get(position).getName());

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


}
