package com.moremy.style.Customer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer_Subservice {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("customer_id")
@Expose
private Integer customerId;
@SerializedName("subservice_id")
@Expose
private Integer subserviceId;
@SerializedName("service")
@Expose
private Customer_Service_ service;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getCustomerId() {
return customerId;
}

public void setCustomerId(Integer customerId) {
this.customerId = customerId;
}

public Integer getSubserviceId() {
return subserviceId;
}

public void setSubserviceId(Integer subserviceId) {
this.subserviceId = subserviceId;
}

public Customer_Service_ getService() {
return service;
}

public void setService(Customer_Service_ service) {
this.service = service;
}

}