package com.moremy.style.Customer.Fragment.EditAccountFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Model.DefaultCard_Data;
import com.moremy.style.Model.DefaultCard_Response;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.Model_MyIncome.Model_MyIncome_BookingServiceItem;
import com.moremy.style.Model_MyIncome.Model_MyIncome_Transaction;
import com.moremy.style.Model_MyIncome.MyExpenditure_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class Fragment_Billing extends Fragment {
    Context context;


    public Fragment_Billing() {
    }

    LinearLayout ll_main_billing;
    Preferences preferences;
    ImageView iv_changecard_icon;
    TextView et_change_cardnumber;


    List<Model_MyIncome_Transaction> data_order_list = new ArrayList<>();
    Adapter_order adapter_review;
    NonScrollListView listview_order;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_billing, container, false);
        context = getActivity();


        preferences = new Preferences(context);
        ll_main_billing = view.findViewById(R.id.ll_main_billing);
        iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
        et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
        listview_order = view.findViewById(R.id.listview_order);


        TextView tv_add_payment_method = view.findViewById(R.id.tv_add_payment_method);
        tv_add_payment_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_mycard();
            }
        });
        data_card_list = new ArrayList<>();
        method_get_mycard();


        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (ll_main_billing != null) {
                ll_main_billing.setVisibility(View.VISIBLE);
                method_set_data();
            }
        } else {
            if (ll_main_billing != null) {
                ll_main_billing.setVisibility(View.GONE);
            }
        }
    }


    private void method_set_data() {

        method_get_default();

        data_order_list = new ArrayList<>();
        method_api_customer();


    }


    Dialog progressDialog = null;

    private void method_get_default() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_get_default_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String flag = jsonObject.getString("flag");
                                if (flag.equalsIgnoreCase("true")) {
                                    DefaultCard_Response Response = new Gson().fromJson(result.toString(), DefaultCard_Response.class);

                                    if (Response.getData() != null) {

                                        DefaultCard_Data defaultCard_data = new DefaultCard_Data();
                                        defaultCard_data = Response.getData();


                                        String brand = defaultCard_data.getBrand();

                                        if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                                        } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                                        } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                                        } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                                        } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                                        } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                                        } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                                        } else {
                                            iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                                        }

                                        et_change_cardnumber.setText("XXXX XXXX XXXX " + defaultCard_data.getLast4());
                                        stripe_id = "" + defaultCard_data.getId();

                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    public class Adapter_order extends BaseAdapter {

        Context context;
        List<Model_MyIncome_Transaction> listState;
        LayoutInflater inflater;


        public Adapter_order(Context applicationContext, List<Model_MyIncome_Transaction> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_myincome, viewGroup, false);
                viewHolder.tv_product_name_o = view.findViewById(R.id.tv_product_name_o);
                viewHolder.tv_product_date_o = view.findViewById(R.id.tv_product_date_o);
                viewHolder.tv_product_prize_o = view.findViewById(R.id.tv_product_prize_o);
                viewHolder.iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                viewHolder.tv_product_ref_o = view.findViewById(R.id.tv_product_ref_o);
                viewHolder.ll_main_cell = view.findViewById(R.id.ll_main_cell);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }


            String brand = listState.get(position).getCardBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.tv_product_prize_o.setText("xxxx - xxxx - xxxx - " + listState.get(position).getLast4());
            viewHolder.tv_product_ref_o.setText("ref " + listState.get(position).getStripeInvoiceNumber());


            if (listState.get(position).getPaymentType() == 1) {

                if (listState.get(position).getOrder() != null) {
                    viewHolder.tv_product_name_o.setText("" + listState.get(position).getOrder().getProduct().getName()
                            + " (£" + listState.get(position).getOrder().getProductPrice() + ")");

                    String date = utills.getTimeAccordingOrder(listState.get(position).getOrder().getOrderDate());
                    viewHolder.tv_product_date_o.setText("paid on " + date);
                }


            } else if (listState.get(position).getPaymentType() == 2) {


                final   List<Model_MyIncome_BookingServiceItem> data__list = new ArrayList<>();
                data__list.addAll(listState.get(position).getBooking().getBookingServiceItem());


                StringBuilder stringBuilder_ids = new StringBuilder();
                for (int i = 0; i < data__list.size(); i++) {
                    if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                        stringBuilder_ids.append(", ");
                    }
                    stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                }


                viewHolder.tv_product_name_o.setText("" + stringBuilder_ids);

                String date = utills.getTimeAccordingBooking(listState.get(position).getBooking().getBookingDate());
                viewHolder.tv_product_date_o.setText("paid " + listState.get(position).getBooking().getBookingTime() + " on " + date);
            }


            viewHolder.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse("http://docs.google.com/gview?embedded=true&url=" + "" + listState.get(position).getStripeInvoicePdf()));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            return view;
        }

        private class ViewHolder {
            TextView tv_product_name_o;
            TextView tv_product_ref_o;
            TextView tv_product_date_o;
            TextView tv_product_prize_o;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
        }

    }

    private void method_api_customer() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_expenditure)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("MyExpenditure", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");

                                if (flag.equalsIgnoreCase("true")) {
                                    MyExpenditure_Response Response = new Gson().fromJson(result.toString(), MyExpenditure_Response.class);
                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();

                                        adapter_review = new Adapter_order(context, data_order_list);
                                        listview_order.setAdapter(adapter_review);

                                    }

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    /*............................*/

    NonScrollListView listview_card;
    List<MyCard_Data> data_card_list = new ArrayList<>();

    String Token_card;
    String stripe_id="0";
    Card card_stripe;
    String brand_name;
    String brand_last4;


    private void method_mycard() {


        final Dialog dialog_listcard = new Dialog(context);
        dialog_listcard.setContentView(R.layout.dialog_my_card);
        dialog_listcard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_listcard.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        listview_card = dialog_listcard.findViewById(R.id.listview_order);

        TextView tv_add_method = dialog_listcard.findViewById(R.id.tv_add_method);

        dialog_listcard.show();

        TextView tv_next=dialog_listcard.findViewById(R.id.tv_next);
        tv_next.setVisibility(View.GONE);

        if (data_card_list != null) {
            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
            listview_card.setAdapter(adapter_review);
        }

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (data_card_list != null) {
                    Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                    listview_card.setAdapter(adapter_review);
                }


                method_change_card_dialog();

            }
        });

    }

    public class Adapter_card extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;

        int pos_selected = -1;

        public Adapter_card(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_card.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_card.ViewHolder();
                view = inflater.inflate(R.layout.datalist_mystripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_card.ViewHolder) view.getTag();
            }


            String brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.et_change_cardnumber.setText("" + brand + " / " + listState.get(position).getLast4());


            if (pos_selected == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            if (stripe_id != null) {
                if (stripe_id.equalsIgnoreCase(listState.get(position).getId())) {
                    viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
                } else {
                    viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
                }
            }

            final Adapter_card.ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    pos_selected = position;


                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);

                    stripe_id = "" + listState.get(position).getId();

                    brand_name = "" + listState.get(position).getBrand();
                    brand_last4 = "" + listState.get(position).getLast4();

                    notifyDataSetChanged();

                    method_set_default_card();

                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon, iv_checkbox;
            TextView et_change_cardnumber;
            CardView card_mylist;

        }

    }

    private void method_set_default_card() {
        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_set_default_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("card_id",""+stripe_id)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_set_default_card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
                                if (flag.equalsIgnoreCase("true")) {


                                    String brand = brand_name;

                                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                                    } else {
                                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                                    }

                                    et_change_cardnumber.setText("XXXX XXXX XXXX " + brand_last4);


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);

        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {

                    CardBrand brand = CardUtils.getPossibleCardBrand(s.toString());


                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString())) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }

    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);

        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void getUserKey(final Dialog dialog_card) {
        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, ""+preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,

                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());


                        Token_card = token.getId();

                        method_save_card(dialog_card);


                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


    }

    private void method_save_card(final Dialog dialog_card) {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token", "" + Token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }


                                    method_get_mycard1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }

    private void method_get_mycard1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();


                                        if (listview_card != null) {
                                            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                                            listview_card.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


}