
package com.moremy.style.Customer.Fragment.EditAccountFragment.Model_Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelOrder_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("sendgrid_order_id")
    @Expose
    private String sendgridOrderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_qty")
    @Expose
    private Integer productQty;
    @SerializedName("product_price")
    @Expose
    private float productPrice;
    @SerializedName("shipping_price")
    @Expose
    private float shippingPrice;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("shipping_date")
    @Expose
    private String shippingDate;
    @SerializedName("dispatch_date")
    @Expose
    private Object dispatchDate;


    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;

    @SerializedName("cancel_date")
    @Expose
    private String cancel_date;


    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private ModelOrder_User user;
    @SerializedName("seller")
    @Expose
    private ModelOrder_Seller seller;
    @SerializedName("product")
    @Expose
    private ModelOrder_Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSendgridOrderId() {
        return sendgridOrderId;
    }

    public void setSendgridOrderId(String sendgridOrderId) {
        this.sendgridOrderId = sendgridOrderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public float getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(float shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public Object getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(Object dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }


 public String getCancel_date() {
        return cancel_date;
    }

    public void setCancel_date(String cancel_date) {
        this.cancel_date = cancel_date;
    }




    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ModelOrder_User getUser() {
        return user;
    }

    public void setUser(ModelOrder_User user) {
        this.user = user;
    }

    public ModelOrder_Seller getSeller() {
        return seller;
    }

    public void setSeller(ModelOrder_Seller seller) {
        this.seller = seller;
    }

    public ModelOrder_Product getProduct() {
        return product;
    }

    public void setProduct(ModelOrder_Product product) {
        this.product = product;
    }

    @SerializedName("payment")
    @Expose
    private ModelOrder_Payment payment;

    public ModelOrder_Payment getPayment() {
        return payment;
    }

    public void setPayment(ModelOrder_Payment payment) {
        this.payment = payment;
    }


    @SerializedName("is_paid")
    @Expose
    private String is_paid;

    public String getis_paid() {
        return is_paid;
    }

    public void setis_paid(String is_paid) {
        this.is_paid = is_paid;
    }

}
