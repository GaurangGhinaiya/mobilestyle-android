package com.moremy.style.Customer;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.moremy.style.Customer.Customer_RegisterSubServicesFinal_Activty.customer_list_services_final;


public class Customer_RegisterLast_Activty extends Base1_Activity {


    Context context;
    Preferences preferences;

    Dialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_ready);
        context = Customer_RegisterLast_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(this);



        TextView tv_final_name=findViewById(R.id.tv_final_name);
        tv_final_name.setText("Use your profile to track orders, bookings, reviews and more!");




        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateBussinessInfo();

            }
        });

    }


    public void UpdateBussinessInfo() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            String profile = preferences.getPRE_SelectedFile_profile();

            if ( profile.equalsIgnoreCase("")) {
                method_api_both_null();
            }   File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                method_api_logo_null(file_profile_pic);
            }

        }

    private void method_api_both_null() {

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < customer_list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("service_id", customer_list_services_final.get(i).getId());
                Gson gson = new Gson();
                String jsonString = gson.toJson(customer_list_services_final.get(i).getSubServices());
                jsonObject.put("sub_service",jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }



        AndroidNetworking.upload(Global_Service_Api.API_customer_signup)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" +  preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                       // Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_api_logo_null(File file_profile_pic) {


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < customer_list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("service_id", customer_list_services_final.get(i).getId());
                Gson gson = new Gson();
                String jsonString = gson.toJson(customer_list_services_final.get(i).getSubServices());
                jsonObject.put("sub_service",jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }




        AndroidNetworking.upload(Global_Service_Api.API_customer_signup)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" +  preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                     //   Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                        Log.d("API", anError.toString());
                    }
                });

    }


    private void method_set_data(String result) {


        preferences.setPRE_Email("");

        preferences.setPRE_Main_ServiceID("");
        preferences.setPRE_Mile("");
        preferences.setPRE_Code("");
        preferences.setPRE_address("");
        preferences.setPRE_City("");
        preferences.setPRE_SelectedFile_logo("");
        preferences.setPRE_SelectedFile_profile("");
        preferences.setPRE_Gender("");
        preferences.setPRE_Password("");
        preferences.setPRE_FirstName("");
        preferences.setPRE_LastName("");
        preferences.setPRE_Main_Oneline("");


        preferences.setPRE_Latitude("");
        preferences.setPRE_Longitude("");
        preferences.setPRE_SalonName("");

        preferences.setPRE_UserName("");

        try {
            JSONObject jsonObject = new JSONObject(result);

            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            String s_token = "" + jsonObject1.get("token");
            preferences.setPRE_TOKEN(s_token);

            Intent i = new Intent(this, Customer_MainProfile_Activty.class);
            startActivity(i);
            finish();
            finishAffinity();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
