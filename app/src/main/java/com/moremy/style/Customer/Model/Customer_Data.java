package com.moremy.style.Customer.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer_Data {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;

@SerializedName("username")
@Expose
private String username;

@SerializedName("address_line_1")
@Expose
private String address_line_1;


@SerializedName("lastname")
@Expose
private String lastname;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("email")
@Expose
private String email;
@SerializedName("profile_pic")
@Expose
private String profilePic;
@SerializedName("status")
@Expose
private Integer status;
@SerializedName("service_id")
@Expose
private String serviceId;
@SerializedName("city")
@Expose
private String city;
@SerializedName("postal_code")
@Expose
private String postalCode;
@SerializedName("one_line")
@Expose
private String oneLine;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;
@SerializedName("service")
@Expose
private List<Customer_Service> service = null;
@SerializedName("customer_subservices")
@Expose
private List<Customer_Subservice> customerSubservices = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getusername() {
return username;
}
public void setusername(String username) {
this.username = username;
}

public String getaddress_line_1() {
return address_line_1;
}
public void setaddress_line_1(String address_line_1) {
this.address_line_1 = address_line_1;
}





public String getLastname() {
return lastname;
}

public void setLastname(String lastname) {
this.lastname = lastname;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getProfilePic() {
return profilePic;
}

public void setProfilePic(String profilePic) {
this.profilePic = profilePic;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getServiceId() {
return serviceId;
}

public void setServiceId(String serviceId) {
this.serviceId = serviceId;
}

public String getCity() {
return city;
}

public void setCity(String city) {
this.city = city;
}

public String getPostalCode() {
return postalCode;
}

public void setPostalCode(String postalCode) {
this.postalCode = postalCode;
}

public String getOneLine() {
return oneLine;
}

public void setOneLine(String oneLine) {
this.oneLine = oneLine;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public List<Customer_Service> getService() {
return service;
}

public void setService(List<Customer_Service> service) {
this.service = service;
}

public List<Customer_Subservice> getCustomerSubservices() {
return customerSubservices;
}

public void setCustomerSubservices(List<Customer_Subservice> customerSubservices) {
this.customerSubservices = customerSubservices;
}

}