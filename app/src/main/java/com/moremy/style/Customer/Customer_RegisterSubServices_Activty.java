package com.moremy.style.Customer;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Salon.Model.Services_SubModel_Response;
import com.moremy.style.Salon.Model.Services_SubModel_SubService;
import com.moremy.style.Salon.Model.Services_SubModel_data;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Customer_RegisterSubServices_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
     List<Services_SubModel_data> customer_list_services = new ArrayList<>();

    RecyclerView recycleview_cat;
    TextView tv_main_categoryname;

    String service_id;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_sub_services);
        context = Customer_RegisterSubServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(context);
        recycleview_cat = findViewById(R.id.listview_country);
        tv_main_categoryname = findViewById(R.id.tv_main_categoryname);
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));

        Intent intent = getIntent();
        service_id = "" + intent.getStringExtra("service_ids");
        page_count = 0;

        TextView tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_customer_name.setText("" + preferences.getPRE_FirstName() + ",");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                page_count++;

                method_final_next();


            }
        });

        customer_list_services = new ArrayList<>();
        get_list(service_id);


    }

    private void method_final_next() {

        try {
            List<Services_SubModel_data> list_services_test = new ArrayList<>();
            if (customer_list_services != null) {
                if (customer_list_services.size() > 0) {
                    if (customer_list_services.get(page_count - 1).getSubServices() != null) {
                        if (customer_list_services.get(page_count - 1).getSubServices().size() > 0) {
                            for (int j = 0; j < customer_list_services.get(page_count - 1).getSubServices().size(); j++) {
                                if (customer_list_services.get(page_count - 1).getSubServices().get(j).is_chechkbox) {
                                    list_services_test.add(customer_list_services.get(page_count - 1));
                                }
                            }
                        }
                    }

                }
            }
            if (list_services_test.size() > 0) {
                method_next();
            } else {

                Toast toast = Toast.makeText(context,"Please Select Atleast One SubService", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    int page_count = 0;

    private void method_next() {
        try {
            if (customer_list_services != null) {
                if (customer_list_services.size() > 0) {

                    if (page_count != customer_list_services.size()) {

                        tv_main_categoryname.setText("" + customer_list_services.get(page_count).getName());

                        if (customer_list_services.get(page_count).getSubServices() != null) {
                            if (customer_list_services.get(page_count).getSubServices().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_services_temp = new ArrayList<>();
                                salon_list_services_temp.addAll(customer_list_services.get(page_count).getSubServices());
                                GridAdapter adapter_counties = new GridAdapter(context, salon_list_services_temp);
                                recycleview_cat.setAdapter(adapter_counties);

                            }
                        }

                    } else {


                        page_count= page_count-1;

                        Intent i = new Intent(context, Customer_RegisterSubServicesFinal_Activty.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("arraylist",new Gson().toJson(customer_list_services));
                        i.putExtras(bundle);
                        startActivity(i);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void get_list(String service_id) {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.post(Global_Service_Api.API_get_salon_services)
                    .addBodyParameter("service_id", service_id)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                customer_list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_SubModel_Response Response = new Gson().fromJson(result.toString(), Services_SubModel_Response.class);

                                    if (Response.getData() != null) {

                                        customer_list_services.addAll(Response.getData());

                                        method_next();

                                    }

                                } else {

                                    Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Services_SubModel_SubService> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<Services_SubModel_SubService> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry;

            ImageView iv_checkbox;
            LinearLayout ll_chechkbox;


            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
                ll_chechkbox = (LinearLayout) view.findViewById(R.id.ll_chechkbox);
                iv_checkbox = (ImageView) view.findViewById(R.id.iv_checkbox);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_customer_sub_service, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {



            if (countries_list.get(position).is_chechkbox) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
            }





            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());


            viewHolder.ll_chechkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (countries_list.get(position).is_chechkbox) {
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                        viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                        countries_list.get(position).is_chechkbox = false;

                    } else {
                        viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                        countries_list.get(position).is_chechkbox = true;
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    @Override
    public void onBackPressed() {


        try {
            if (customer_list_services != null) {
                if (customer_list_services.size() > 0) {

                    if (page_count > 0) {

                        page_count = page_count - 1;

                        tv_main_categoryname.setText("" + customer_list_services.get(page_count).getName());

                        if (customer_list_services.get(page_count).getSubServices() != null) {
                            if (customer_list_services.get(page_count).getSubServices().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_services_temp = new ArrayList<>();
                                salon_list_services_temp.addAll(customer_list_services.get(page_count).getSubServices());
                                GridAdapter adapter_counties = new GridAdapter(context, salon_list_services_temp);
                                recycleview_cat.setAdapter(adapter_counties);

                            }
                        }

                    } else {

                        super.onBackPressed();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
