package com.moremy.style.Customer;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Salon.Model.Services_SubModel_SubService;
import com.moremy.style.Salon.Model.Services_SubModel_data;
import com.moremy.style.Utills.Preferences;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class Customer_RegisterSubServicesFinal_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    public static List<Services_SubModel_data> customer_list_services_final = new ArrayList<>();

    RecyclerView recycleview_cat;
    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_register_sub_services_final);
        context = Customer_RegisterSubServicesFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);

        recycleview_cat = findViewById(R.id.listview_country);
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));


        TextView tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_customer_name.setText("" + preferences.getPRE_FirstName() + ",");

        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context, Customer_RegisterBio_Activty.class);
                startActivity(i);

            }
        });



        Bundle extras = getIntent().getExtras();
        String arraylist_a =extras.getString("arraylist");
        Type listType = new TypeToken<List<Services_SubModel_data>>(){}.getType();

        customer_list_services_final = new ArrayList<>();


        List<Services_SubModel_data> customer_list_services_temp_move = new ArrayList<>();
        customer_list_services_temp_move=new Gson().fromJson(arraylist_a,listType);

        if (customer_list_services_temp_move != null) {
            if (customer_list_services_temp_move.size() > 0) {


                List<Services_SubModel_data> customer_list_services_tempp = new ArrayList<>();
                customer_list_services_tempp.addAll(customer_list_services_temp_move);

                for (int i = 0; i < customer_list_services_tempp.size(); i++) {
                    if (customer_list_services_tempp.get(i).getSubServices() != null) {
                        if (customer_list_services_tempp.get(i).getSubServices().size() > 0) {

                            List<Services_SubModel_SubService> subservices_final = new ArrayList<>();
                            List<Services_SubModel_SubService> subservices = new ArrayList<>();
                            subservices.addAll(customer_list_services_tempp.get(i).getSubServices());


                            for (int j = 0; j < subservices.size(); j++) {
                                if (subservices.get(j).is_chechkbox) {
                                    Services_SubModel_SubService servicem = new Services_SubModel_SubService();
                                    servicem.is_chechkbox = subservices.get(j).is_chechkbox;
                                    servicem.setId(subservices.get(j).getId());
                                    servicem.setName(subservices.get(j).getName());
                                    subservices_final.add(servicem);
                                }
                            }

                            customer_list_services_final.add(customer_list_services_tempp.get(i));
                            customer_list_services_final.get(i).setSubServices(subservices_final);
                        }
                    }
                }
            }
        }


        Log.e("onCreate: ", "" + customer_list_services_final.size());

        GridAdapter adapter_counties = new GridAdapter(context, customer_list_services_final);
        recycleview_cat.setAdapter(adapter_counties);


        TextView tv_add_more_back = findViewById(R.id.tv_add_more_back);
        tv_add_more_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {

        private List<Services_SubModel_data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<Services_SubModel_data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_category_list;
            RecyclerView Rv_prizelist;

            public MyViewHolder(View view) {
                super(view);
                tv_category_list = (TextView) view.findViewById(R.id.tv_category_list);
                Rv_prizelist = (RecyclerView) view.findViewById(R.id.Rv_prizelist);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_salon, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_category_list.setText(countries_list.get(position).getName());
            viewHolder.Rv_prizelist.setNestedScrollingEnabled(false);
            viewHolder.Rv_prizelist.setLayoutManager(new GridLayoutManager(context, 1));


            if (countries_list.get(position).getSubServices() != null) {

                List<Services_SubModel_SubService> salon_list_services_tempp = new ArrayList<>();
                salon_list_services_tempp.addAll(countries_list.get(position).getSubServices());
                GridAdapter_subprize adapter_counties = new GridAdapter_subprize(context, salon_list_services_tempp);
                viewHolder.Rv_prizelist.setAdapter(adapter_counties);

            }

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    class GridAdapter_subprize extends RecyclerView.Adapter<GridAdapter_subprize.MyViewHolder> {

        private List<Services_SubModel_SubService> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter_subprize(Context context, List<Services_SubModel_SubService> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry;

            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);

            }
        }


        @Override
        public GridAdapter_subprize.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_, parent, false);

            return new GridAdapter_subprize.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter_subprize.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


}
