/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moremy.style.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.moremy.style.Activity.Splash_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.ChatBookingDirect_Activity;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.OnPushTokenReceiveListener;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.SendBirdPushHandler;
import com.sendbird.android.SendBirdPushHelper;
import com.sendbird.android.User;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;

import static androidx.core.app.NotificationCompat.PRIORITY_HIGH;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.tv_count_total_Customer;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.tv_count_total_salon;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.tv_count_total_STYLE_SELLER;

public class MyFirebaseMessagingService extends SendBirdPushHandler {

    private static final String TAG = "MyFirebaseMsgService";
    private static final AtomicReference<String> pushToken = new AtomicReference<>();

    public interface ITokenResult {
        void onPushTokenReceived(String pushToken, SendBirdException e);
    }

    @Override
    protected boolean isUniquePushToken() {
        return false;
    }

    @Override
    public void onNewToken(String token) {
        Log.e(TAG, "onNewToken(" + token + ")");
        pushToken.set(token);

        SendBird.registerPushTokenForCurrentUser(token, new SendBird.RegisterPushTokenWithStatusHandler() {
            @Override
            public void onRegistered(SendBird.PushTokenRegistrationStatus ptrs, SendBirdException e) {
                if (e != null) {
                    return;
                }

                if (ptrs == SendBird.PushTokenRegistrationStatus.PENDING) {
                    // A token registration is pending.
                    // Retry the registration after a connection has been successfully established.
                }
            }
        });

    }


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(Context context, RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        String channelUrl = null;
        try {
            if (remoteMessage.getData().containsKey("sendbird")) {
                JSONObject sendBird = new JSONObject(remoteMessage.getData().get("sendbird"));
                String message = (String) sendBird.get("message");
                int unread_message_count = (int) sendBird.get("unread_message_count");


                JSONObject channel = (JSONObject) sendBird.get("channel");
                channelUrl = (String) channel.get("channel_url");

                String profile_url = "";
                String name = "";


                try {

                    JSONObject sender = (JSONObject) sendBird.get("sender");
                    if (sender != null) {
                        profile_url = (String) sender.get("profile_url");
                        name = (String) sender.get("name");
                    } else {
                        name = (String) channel.get("name");
                    }

                } catch (JSONException e) {
                    name = (String) channel.get("name");
                    e.printStackTrace();
                }


                // Also if you intend on generating your own notifications as a result of a received FCM
                // message, here is where that should be initiated. See sendNotification method below.

                sendNotification(context, name, message, profile_url, channelUrl, unread_message_count);

            } else {
                if (remoteMessage.getData().size() > 0) {
                    try {
                        sendNotification_chat(context, remoteMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void sendNotification_chat(Context context, RemoteMessage remoteMessage) {

        Log.e("sendNotification_chat: ", "" + remoteMessage.getData());


        String type = "" + remoteMessage.getData().get("type");
        Intent intent = new Intent(context, Splash_Activity.class);
        if (type.equalsIgnoreCase("booking")) {
            intent.putExtra("type", "" + type);
            intent.putExtra("channelUrl", "" + remoteMessage.getData().get("send_bird_id"));
            intent.putExtra("order_id", "" + remoteMessage.getData().get("id"));
        } else if (type.equalsIgnoreCase("order")) {
            intent.putExtra("type", "" + type);
            intent.putExtra("channelUrl", "" + remoteMessage.getData().get("send_bird_id"));
            intent.putExtra("order_id", "" + remoteMessage.getData().get("id"));
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);




        RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notification_custom);
            String s_name = "" + remoteMessage.getData().get("title").substring(0, 1).toUpperCase();
            notificationLayout.setTextViewText(R.id.tv_name_title, s_name);
            notificationLayout.setTextViewText(R.id.tv_title2, "" + remoteMessage.getData().get("title"));
            notificationLayout.setTextViewText(R.id.tv_sub2, "" + remoteMessage.getData().get("text"));


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_app_logo)
                        .setContent(notificationLayout)
                        .setAutoCancel(true)
                        .setPriority(PRIORITY_HIGH)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());



    }


    public static void sendNotification(Context context, String name, String message,
                                        String profile_url, String channelUrl, int unread_message_count) {


        Intent intent = new Intent(context, Splash_Activity.class);
        intent.putExtra("type", "Notification");
        intent.putExtra("channelUrl", "" + channelUrl);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notification_custom);

            String s_name = name.substring(0, 2).toUpperCase();
            notificationLayout.setTextViewText(R.id.tv_name_title, s_name);
            notificationLayout.setTextViewText(R.id.tv_title2, name);
            notificationLayout.setTextViewText(R.id.tv_sub2, message);



        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_app_logo)
                        .setContent(notificationLayout)
                        .setAutoCancel(true)
                        .setPriority(PRIORITY_HIGH)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


        method_UPDATE(context);


    }

    private static void method_UPDATE(Context context) {

        try {
            Preferences preferences = new Preferences(context);
            SendBird.connect("" + preferences.getPRE_SendBirdUserId(), new SendBird.ConnectHandler() {
                @Override
                public void onConnected(User user, SendBirdException e) {
                    if (e != null) {    // Error.
                    } else {
                        SendBird.getTotalUnreadMessageCount(new GroupChannel.GroupChannelTotalUnreadMessageCountHandler() {
                            @Override
                            public void onResult(int totalUnreadMessageCount, SendBirdException e) {
                                if (e != null) {    // Error.
                                    return;
                                }

                                Log.e("totalUnreadMessageCount", "" + totalUnreadMessageCount);


                                try {
                                    if (tv_count_total_STYLE_SELLER != null) {
                                        if (totalUnreadMessageCount != 0) {
                                            tv_count_total_STYLE_SELLER.setVisibility(View.VISIBLE);
                                            tv_count_total_STYLE_SELLER.setText("" + totalUnreadMessageCount);
                                        } else {
                                            tv_count_total_STYLE_SELLER.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                                try {
                                    if (tv_count_total_salon != null) {
                                        if (totalUnreadMessageCount != 0) {
                                            tv_count_total_salon.setVisibility(View.VISIBLE);
                                            tv_count_total_salon.setText("" + totalUnreadMessageCount);
                                        } else {
                                            tv_count_total_salon.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                                try {
                                    if (tv_count_total_Customer != null) {
                                        if (totalUnreadMessageCount != 0) {
                                            tv_count_total_Customer.setVisibility(View.VISIBLE);
                                            tv_count_total_Customer.setText("" + totalUnreadMessageCount);
                                        } else {
                                            tv_count_total_Customer.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                            }
                        });
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    static String channelId = "MobileStyle";

    public static void getPushToken(final ITokenResult listener) {
        String token = pushToken.get();
        if (!TextUtils.isEmpty(token)) {
            listener.onPushTokenReceived(token, null);
            return;
        }

        SendBirdPushHelper.getPushToken(new OnPushTokenReceiveListener() {
            @Override
            public void onReceived(String token1, SendBirdException e) {
                Log.d(TAG, "FCM token : " + token1);
                if (listener != null) {
                    listener.onPushTokenReceived(token1, e);
                }

                if (e == null) {
                    pushToken.set(token1);
                }
            }
        });
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
           /* Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;*/
        } catch (IOException e) {
            e.printStackTrace();
            // Log exception
            return null;
        }
    }


}