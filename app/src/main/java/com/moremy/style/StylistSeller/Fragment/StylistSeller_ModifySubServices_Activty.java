package com.moremy.style.StylistSeller.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.SelectModel_OtherSubService;
import com.moremy.style.Model.SelectModel_StylistSubservice;
import com.moremy.style.Model.SuB_Services_Data;
import com.moremy.style.Model.SuB_Services_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.Fragment.StylistSeller_ModifyServices_Activty.selectModel_data;


public class StylistSeller_ModifySubServices_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    public static List<SuB_Services_Data> list_services = new ArrayList<>();
    public static List<SuB_Services_Data> list_services_Premium_List = new ArrayList<>();

    RecyclerView recycleview_cat, listview_Premium;

    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_sub_services);
        context = StylistSeller_ModifySubServices_Activty.this;
        activity = StylistSeller_ModifySubServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        recycleview_cat = findViewById(R.id.listview_country);
        listview_Premium = findViewById(R.id.listview_Premium);
        recycleview_cat.setNestedScrollingEnabled(false);
        listview_Premium.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));
        listview_Premium.setLayoutManager(new GridLayoutManager(this, 1));


        TextView tv_category = findViewById(R.id.tv_category);

        Intent intent = getIntent();
        tv_category.setText("" + intent.getStringExtra("service_name"));
        final String service_id = "" + intent.getStringExtra("service_id");
        final String service_name = "" + intent.getStringExtra("service_name");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                List<SuB_Services_Data> list_services_test = new ArrayList<>();

                if (list_services != null) {
                    if (list_services.size() > 0) {
                        for (int i = 0; i < list_services.size(); i++) {
                            if (list_services.get(i).is_chechkbox) {
                                list_services_test.add(list_services.get(i));
                            }
                        }

                    }
                }


                if (list_services_test.size() > 0) {

                    int is_subservice_prize = 0;

                    for (int i = 0; i < list_services_test.size(); i++) {
                        if (!list_services_test.get(i).sub_Price.equalsIgnoreCase("")) {
                            float price = Float.parseFloat(list_services_test.get(i).sub_Price);
                            if (price > 250) {
                                is_subservice_prize = 1;
                            }else if (price < 4) {
                                is_subservice_prize = 3;
                            }
                        } else {
                            is_subservice_prize = 2;
                        }

                    }


                    if (is_subservice_prize == 1) {
                        Toast.makeText(context, "Price must be less than £250", Toast.LENGTH_SHORT).show();
                    } else if (is_subservice_prize == 3) {
                        Toast.makeText(context, "Minimum price should be £4", Toast.LENGTH_SHORT).show();
                    }else if (is_subservice_prize == 2) {
                        Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                    } else {


                        if (preferences.getPRE_Is_Premium()) {


                            int is_subservice_prize_premium = 0;

                            for (int i = 0; i < list_services_Premium_List.size(); i++) {
                                if (!list_services_Premium_List.get(i).sub_Price.equalsIgnoreCase("")) {
                                    float price = Float.parseFloat(list_services_Premium_List.get(i).sub_Price);
                                    if (price > 250) {
                                        is_subservice_prize_premium = 1;
                                    }else if (price < 4) {
                                        is_subservice_prize_premium = 3;
                                    }
                                } else {
                                    is_subservice_prize_premium = 2;
                                }

                            }

                            if (is_subservice_prize_premium == 1) {
                                Toast.makeText(context, "Price must be less than £250", Toast.LENGTH_SHORT).show();
                            }else if (is_subservice_prize_premium == 3) {
                                Toast.makeText(context, "Minimum price should be £4", Toast.LENGTH_SHORT).show();
                            } else if (is_subservice_prize_premium == 2) {
                                Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                            } else {
                                Intent i = new Intent(context, StylistSeller_ModifySubServicesFinal_Activty.class);
                                i.putExtra("service_name", service_name);
                                i.putExtra("service_id", service_id);
                                startActivity(i);
                            }

                        } else {
                            Intent i = new Intent(context, StylistSeller_ModifySubServicesFinal_Activty.class);
                            i.putExtra("service_name", service_name);
                            i.putExtra("service_id", service_id);
                            startActivity(i);
                        }

                    }


                } else {
                    Toast.makeText(context, "Please Select Atleast One SubService", Toast.LENGTH_SHORT).show();
                }


            }
        });

        list_services = new ArrayList<>();
        list_services_Premium_List = new ArrayList<>();
        get_list(service_id);

    }

    public void get_list(String service_id) {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_Sub_services + service_id)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                List<SuB_Services_Data> list_services_temp = new ArrayList<>();

                                list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    SuB_Services_Response Response = new Gson().fromJson(result.toString(), SuB_Services_Response.class);

                                    if (Response.getData() != null) {

                                        list_services_temp = Response.getData();


                                        List<SelectModel_OtherSubService> list_selected_premium = new ArrayList<>();
                                        if (selectModel_data != null) {
                                            list_selected_premium = selectModel_data.getOtherSubServices();
                                        }


                                        if (list_selected_premium != null) {
                                            if (list_selected_premium.size() > 0) {

                                                SuB_Services_Data suB_services_data = new SuB_Services_Data();
                                                suB_services_data.setName("Is_Premiummm");
                                                suB_services_data.is_premium_box = true;
                                                suB_services_data.is_chechkbox = false;
                                                list_services_temp.add(suB_services_data);


                                                for (int i = 0; i < list_selected_premium.size(); i++) {
                                                    SuB_Services_Data suB_services_data23 = new SuB_Services_Data();
                                                    suB_services_data23.sub_Price = "" + list_selected_premium.get(i).getPrice();
                                                    suB_services_data23.is_Services_description = "" + list_selected_premium.get(i).getService().getDescription();
                                                    suB_services_data23.setName("" + list_selected_premium.get(i).getService().getName());
                                                    suB_services_data23.setId(list_selected_premium.get(i).getId());
                                                    list_services_Premium_List.add(suB_services_data23);
                                                }


                                                GridAdapter_premium adapter_counties = new GridAdapter_premium(context, list_services_Premium_List);
                                                listview_Premium.setAdapter(adapter_counties);


                                            } else {
                                                SuB_Services_Data suB_services_data = new SuB_Services_Data();
                                                suB_services_data.setName("Is_Premiummm");
                                                suB_services_data.is_chechkbox = false;
                                                list_services_temp.add(suB_services_data);
                                            }
                                        } else {
                                            SuB_Services_Data suB_services_data = new SuB_Services_Data();
                                            suB_services_data.setName("Is_Premiummm");
                                            suB_services_data.is_chechkbox = false;
                                            list_services_temp.add(suB_services_data);
                                        }


                                        List<SelectModel_StylistSubservice> list_selected_temp = new ArrayList<>();
                                        if (selectModel_data != null) {
                                            list_selected_temp = selectModel_data.getStylistSubservices();
                                        }


                                        for (int j = 0; j < list_services_temp.size(); j++) {

                                            String i_id = "" + list_services_temp.get(j).getId();

                                            boolean found = false;
                                            int poss = 0;

                                            for (int i_select = 0; i_select < list_selected_temp.size(); i_select++) {
                                                if (i_id.equalsIgnoreCase("" + list_selected_temp.get(i_select).getSubserviceId())) {
                                                    found = true;
                                                    poss = i_select;
                                                }
                                            }

                                            if (found) {

                                                SuB_Services_Data suB_services_data2 = new SuB_Services_Data();
                                                suB_services_data2.setId(list_services_temp.get(j).getId());
                                                suB_services_data2.setName("" + list_services_temp.get(j).getName());
                                                suB_services_data2.sub_min = "" + list_selected_temp.get(poss).getTime();
                                                suB_services_data2.sub_Price = "" + list_selected_temp.get(poss).getPrice();
                                                suB_services_data2.is_chechkbox = true;
                                                suB_services_data2.is_premium_box = list_services_temp.get(poss).is_premium_box;

                                                list_services.add(suB_services_data2);
                                                poss = 0;
                                            } else {
                                                SuB_Services_Data suB_services_data2 = new SuB_Services_Data();
                                                suB_services_data2.setId(list_services_temp.get(j).getId());
                                                suB_services_data2.setName("" + list_services_temp.get(j).getName());
                                                suB_services_data2.sub_min = "" + list_services_temp.get(j).sub_min;
                                                suB_services_data2.sub_Price = "" + list_services_temp.get(j).sub_Price;
                                                suB_services_data2.is_chechkbox = false;
                                                suB_services_data2.is_premium_box = list_services_temp.get(j).is_premium_box;
                                                list_services.add(suB_services_data2);
                                            }

                                        }


                                        GridAdapter adapter_counties = new GridAdapter(context, list_services);
                                        recycleview_cat.setAdapter(adapter_counties);

                                    }

                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<SuB_Services_Data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<SuB_Services_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry;
            EditText et_prize;
            ImageView iv_checkbox;
            LinearLayout ll_15, ll_30, ll_45, ll_60, ll_70, ll_chechkbox, ll_horizontal, ll_premium;


            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);

                ll_15 = (LinearLayout) view.findViewById(R.id.ll_15);
                ll_30 = (LinearLayout) view.findViewById(R.id.ll_30);
                ll_45 = (LinearLayout) view.findViewById(R.id.ll_45);
                ll_60 = (LinearLayout) view.findViewById(R.id.ll_60);
                ll_70 = (LinearLayout) view.findViewById(R.id.ll_70);
                ll_horizontal = (LinearLayout) view.findViewById(R.id.ll_horizontal);
                ll_chechkbox = (LinearLayout) view.findViewById(R.id.ll_chechkbox);

                et_prize = (EditText) view.findViewById(R.id.et_prize);
                iv_checkbox = (ImageView) view.findViewById(R.id.iv_checkbox);
                ll_premium = (LinearLayout) view.findViewById(R.id.ll_premium);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_with_premium, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            if (countries_list.get(position).getName().equals("Is_Premiummm")) {
                viewHolder.tv_textcountry.setVisibility(View.GONE);
                viewHolder.ll_premium.setVisibility(View.VISIBLE);
                listview_Premium.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ll_premium.setVisibility(View.GONE);
                viewHolder.ll_horizontal.setVisibility(View.GONE);
                viewHolder.tv_textcountry.setVisibility(View.VISIBLE);
            }


            if (countries_list.get(position).is_chechkbox) {

                viewHolder.ll_horizontal.setVisibility(View.VISIBLE);
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));

            } else {

                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                viewHolder.ll_horizontal.setVisibility(View.GONE);

                if (countries_list.get(position).is_premium_box) {
                    viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                } else {
                    viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);

                }

            }


            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());
            viewHolder.et_prize.setText("" + countries_list.get(position).sub_Price);


            viewHolder.et_prize.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).sub_Price = "" + s.toString();
                }
            });


            viewHolder.ll_chechkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (countries_list.get(position).getName().equals("Is_Premiummm")) {

                        if (countries_list.get(position).is_premium_box) {

                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                            countries_list.get(position).is_premium_box = false;
                            preferences.setPRE_Is_Premium(false);
                            listview_Premium.setVisibility(View.GONE);
                            list_services_Premium_List = new ArrayList<>();
                        } else {
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                            countries_list.get(position).is_premium_box = true;
                            preferences.setPRE_Is_Premium(false);


                            list_services_Premium_List = new ArrayList<>();
                            SuB_Services_Data suB_services_data = new SuB_Services_Data();
                            suB_services_data.is_chechkbox = false;
                            list_services_Premium_List.add(suB_services_data);

                            listview_Premium.setVisibility(View.VISIBLE);
                            GridAdapter_premium adapter_counties = new GridAdapter_premium(context, list_services_Premium_List);
                            listview_Premium.setAdapter(adapter_counties);


                        }

                    } else {

                        if (countries_list.get(position).is_chechkbox) {
                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                            viewHolder.ll_horizontal.setVisibility(View.GONE);
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                            countries_list.get(position).is_chechkbox = false;
                            countries_list.get(position).sub_min = "15";
                            countries_list.get(position).sub_Price = "";
                            viewHolder.et_prize.setText("");

                        } else {
                            viewHolder.ll_horizontal.setVisibility(View.VISIBLE);
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                            countries_list.get(position).is_chechkbox = true;
                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));

                        }
                    }

                }
            });


            viewHolder.ll_15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).sub_min = "15";

                    viewHolder.ll_15.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_30.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "30";

                    viewHolder.ll_30.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_45.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "45";
                    viewHolder.ll_45.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_60.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "60";
                    viewHolder.ll_60.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_70.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "70";
                    viewHolder.ll_70.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }

    class GridAdapter_premium extends RecyclerView.Adapter<GridAdapter_premium.MyViewHolder> {
        private List<SuB_Services_Data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter_premium(Context context, List<SuB_Services_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            EditText et_prize, et_ServiceName, et_Description;
            LinearLayout ll_15, ll_30, ll_45, ll_60, ll_70, ll_add_another_servise;
            CardView card_cancel;

            public MyViewHolder(View view) {
                super(view);

                ll_15 = (LinearLayout) view.findViewById(R.id.ll_15);
                ll_30 = (LinearLayout) view.findViewById(R.id.ll_30);
                ll_45 = (LinearLayout) view.findViewById(R.id.ll_45);
                ll_60 = (LinearLayout) view.findViewById(R.id.ll_60);
                ll_70 = (LinearLayout) view.findViewById(R.id.ll_70);


                et_prize = (EditText) view.findViewById(R.id.et_prize);
                et_ServiceName = (EditText) view.findViewById(R.id.et_ServiceName);
                et_Description = (EditText) view.findViewById(R.id.et_Description);
                ll_add_another_servise = (LinearLayout) view.findViewById(R.id.ll_add_another_servise);
                card_cancel = (CardView) view.findViewById(R.id.card_cancel);


            }
        }


        @Override
        public GridAdapter_premium.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_with_premium_list, parent, false);

            return new GridAdapter_premium.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter_premium.MyViewHolder viewHolder, final int position) {


            if (position == 0) {
                viewHolder.card_cancel.setVisibility(View.GONE);
            } else {
                viewHolder.card_cancel.setVisibility(View.VISIBLE);
            }

            if (position == countries_list.size() - 1) {
                viewHolder.ll_add_another_servise.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ll_add_another_servise.setVisibility(View.GONE);
            }


            viewHolder.et_ServiceName.setText(countries_list.get(position).getName());
            viewHolder.et_Description.setText(countries_list.get(position).is_Services_description);
            viewHolder.et_prize.setText("" + countries_list.get(position).sub_Price);


            viewHolder.card_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).is_Services_description = "";
                    countries_list.get(position).sub_Price = "";
                    countries_list.get(position).setName("");
                    countries_list.remove(position);
                    notifyDataSetChanged();
                }
            });

            viewHolder.et_Description.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).is_Services_description = "" + s.toString();
                }
            });


            viewHolder.et_ServiceName.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).setName("" + s.toString());
                }
            });


            viewHolder.et_prize.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).sub_Price = "" + s.toString();
                }
            });


            viewHolder.ll_add_another_servise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SuB_Services_Data suB_services_data = new SuB_Services_Data();
                    list_services_Premium_List.add(suB_services_data);
                    notifyDataSetChanged();

                }
            });


            viewHolder.ll_15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).sub_min = "15";

                    viewHolder.ll_15.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_30.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "30";

                    viewHolder.ll_30.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_45.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "45";
                    viewHolder.ll_45.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_60.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "60";
                    viewHolder.ll_60.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_70.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).sub_min = "70";
                    viewHolder.ll_70.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    public static Activity activity = null;

    public static void finish_this() {
        if (activity != null) {
            activity.finish();
        }
    }


}
