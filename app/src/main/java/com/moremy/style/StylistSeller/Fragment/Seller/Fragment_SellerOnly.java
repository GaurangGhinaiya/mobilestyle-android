package com.moremy.style.StylistSeller.Fragment.Seller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.StylistSeller.Fragment.Fragment_Shop;
import com.moremy.style.StylistSeller.Fragment.Fragment_review;
import com.moremy.style.StylistSeller.model.Profile.Profile_Data;
import com.moremy.style.StylistSeller.model.Profile.Profile_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import org.json.JSONException;
import org.json.JSONObject;

import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentManager_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentTransaction_Stylist_seller;


public class Fragment_SellerOnly extends Fragment {


    public Fragment_SellerOnly() {
    }


    Preferences preferences;
    Context context;
    Dialog progressDialog = null;


    TabLayout tabs;
    CustomViewPager viewPager;

    ScaleRatingBar simpleRatingBar_quality;
    TextView tv_oneliner, tv_name, tv_servies;
    public static Profile_Data profile_data_SellerOnly_main;

    ImageView img_profile, iv_companylogo;

    ViewPager viewPager_images;
    TabLayout indicator;

    ProgressBar progress_bar_profile, progress_bar_companylogo;


    int id = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_seller_only, container, false);
        context = getActivity();


        preferences = new Preferences(context);


        simpleRatingBar_quality = (ScaleRatingBar) view.findViewById(R.id.simpleRatingBar_quality);
        progress_bar_profile = (ProgressBar) view.findViewById(R.id.progress_bar_profile);
        progress_bar_companylogo = (ProgressBar) view.findViewById(R.id.progress_bar_companylogo);
        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (CustomViewPager) view.findViewById(R.id.viewPager);
        tv_oneliner = (TextView) view.findViewById(R.id.tv_oneliner);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_servies = (TextView) view.findViewById(R.id.tv_servies);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);
        iv_companylogo = (ImageView) view.findViewById(R.id.iv_companylogo);

        viewPager_images = view.findViewById(R.id.viewPager_images);
        indicator = view.findViewById(R.id.indicator);


        tabs.addTab(tabs.newTab().setText("Products"));
        tabs.addTab(tabs.newTab().setText("Reviews"));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        method_set_tab_font();
        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_more();
            }
        });


        return view;
    }


    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }

    private void method_get_api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_get_seller_profile)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            Log.e("onResponse: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    Profile_Response Response = new Gson().fromJson(result.toString(), Profile_Response.class);

                                    if (Response.getData() != null) {
                                        profile_data_SellerOnly_main = new Profile_Data();
                                        profile_data_SellerOnly_main = Response.getData();
                                        method_setdata();

                                    }


                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }

    private void method_setdata() {

        if (profile_data_SellerOnly_main != null) {
            tv_oneliner.setText("" + profile_data_SellerOnly_main.getOneLine());
            tv_name.setText("" + profile_data_SellerOnly_main.getName() + " " + profile_data_SellerOnly_main.getLastname());
            tv_servies.setText("" + profile_data_SellerOnly_main.getServiceName());


            if (profile_data_SellerOnly_main.getId() != null) {
                id = profile_data_SellerOnly_main.getId();
            }

            if (profile_data_SellerOnly_main.getseller_rating() != null) {
                simpleRatingBar_quality.setRating(profile_data_SellerOnly_main.getseller_rating().getavg_rating());
            } else {
                simpleRatingBar_quality.setRating(0);
            }


            try {
                if (profile_data_SellerOnly_main.getCompanyLogo() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_SellerOnly_main.getCompanyLogo())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .addListener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    iv_companylogo.setImageResource(R.drawable.app_logo);
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(iv_companylogo);
                } else {
                    iv_companylogo.setImageResource(R.drawable.app_logo);
                    progress_bar_companylogo.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                iv_companylogo.setImageResource(R.drawable.app_logo);
                progress_bar_companylogo.setVisibility(View.GONE);

                e.printStackTrace();
            }

            try {
                if (profile_data_SellerOnly_main.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_SellerOnly_main.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            try {
                Pager
                        adapter = new Pager(getChildFragmentManager(), tabs.getTabCount());
                viewPager.setAdapter(adapter);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void method_more() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_seller);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);


        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (profile_data_SellerOnly_main != null) {
            tv_name_title.setText("" + profile_data_SellerOnly_main.getName());
        }

        final TextView tv_MyProductsOrders = dialog.findViewById(R.id.tv_MyProductsOrders);
        tv_MyProductsOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Seller_EditAccount(2);
                LoadFragment(currentFragment);
            }
        });


        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });
            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_ss_Sapport();
                LoadFragment(currentFragment);
            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_ss_FAQ();
                LoadFragment(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Seller_EditAccount(0);
                LoadFragment(currentFragment);
            }
        });


        dialog.show();


    }

    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                preferences.setPRE_TOKEN("");
                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    public void LoadFragment(final Fragment fragment) {


        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_Stylist_seller.add(R.id.fmFragment, fragment);
        fragmentTransaction_Stylist_seller.addToBackStack(null);
        fragmentTransaction_Stylist_seller.commit();


    }


    Fragment_review fragment_review2;
    Fragment_Shop fragment_shop;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);

            fragment_review2 = new Fragment_review();
            fragment_shop = new Fragment_Shop();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_shop;
                case 1:
                    return fragment_review2;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }


    @Override
    public void onResume() {

        method_get_api();

        super.onResume();
    }
}