package com.moremy.style.StylistSeller.model.Profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile_SubService {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("stylist_id")
    @Expose
    private Integer stylistId;
    @SerializedName("subservice_id")
    @Expose
    private Integer subserviceId;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("service")
    @Expose
    private Profile_Service service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStylistId() {
        return stylistId;
    }

    public void setStylistId(Integer stylistId) {
        this.stylistId = stylistId;
    }

    public Integer getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(Integer subserviceId) {
        this.subserviceId = subserviceId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Profile_Service getService() {
        return service;
    }

    public void setService(Profile_Service service) {
        this.service = service;
    }

}