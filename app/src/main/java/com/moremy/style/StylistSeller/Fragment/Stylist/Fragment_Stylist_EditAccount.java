package com.moremy.style.StylistSeller.Fragment.Stylist;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.BuildConfig;
import com.moremy.style.CommanActivity.Login_activty;


import com.moremy.style.CommanActivity.Premium_Activity;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.Model.StripePlan_Data;
import com.moremy.style.Model.StripePlan_Response;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.StylistSeller.model.Profile.Profile_Data;
import com.moremy.style.StylistSeller.model.Profile.Profile_Response;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.FileUtils;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentManager_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentTransaction_Stylist_seller;
import static com.theartofdev.edmodo.cropper.CropImage.getPickImageResultUri;

public class Fragment_Stylist_EditAccount extends Fragment {

    private Context context;


    public Fragment_Stylist_EditAccount() {
    }

    int i_position = 0;

    public Fragment_Stylist_EditAccount(int i_position_) {
        i_position = i_position_;
    }

    Preferences preferences;
    Dialog progressDialog = null;

    public static Profile_Data stylist_data_Editprofile;


    TextView tv_name;

    ImageView img_profile;

    ProgressBar progress_bar_profile;




    /*.....*/

    TabLayout tabs;
    ViewPager viewPager;
    Pager adapter;


    /*.....*/
    LinearLayout ll_premium, ll_upgrade;
    ScaleRatingBar simpleRatingBar_quality;


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylist_editaccount, container, false);
        context = getActivity();

        preferences = new Preferences(context);

        ImageView iv_more = view.findViewById(R.id.iv_more);
        tv_name = view.findViewById(R.id.tv_name);
        img_profile = view.findViewById(R.id.img_profile);
        simpleRatingBar_quality = view.findViewById(R.id.simpleRatingBar_quality);


        ll_premium = view.findViewById(R.id.ll_premium);
        ll_upgrade = view.findViewById(R.id.ll_upgrade);


        progress_bar_profile = view.findViewById(R.id.progress_bar_profile);

        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_more();
            }
        });

        RelativeLayout rl_edit_account = view.findViewById(R.id.rl_edit_account);
        rl_edit_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        tabs.addTab(tabs.newTab().setText("profile"));
        tabs.addTab(tabs.newTab().setText("location"));
        tabs.addTab(tabs.newTab().setText("orders"));
        tabs.addTab(tabs.newTab().setText("bookings"));
        tabs.addTab(tabs.newTab().setText("billings"));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        method_set_tab_font();


        method_get_api();

        CardView card_pencil = view.findViewById(R.id.card_pencil);
        card_pencil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectdocument();
            }
        });

        ll_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_dialog_isPremium();
            }
        });


        return view;
    }


    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }


    private void method_more() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_salon);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);


        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final TextView tv_MyProductsOrders = dialog.findViewById(R.id.tv_MyProductsOrders);
        tv_MyProductsOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                viewPager.setCurrentItem(2);
            }
        });

        final TextView tv_MyServicesBookings = dialog.findViewById(R.id.tv_MyServicesBookings);
        tv_MyServicesBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                viewPager.setCurrentItem(3);
            }
        });


        if (stylist_data_Editprofile != null) {
            tv_name_title.setText("" + stylist_data_Editprofile.getName());
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });
            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_stylist_Sapport();
                LoadFragment(currentFragment);
            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_stylist_FAQ();
                LoadFragment(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();


    }


    public void LoadFragment(final Fragment fragment) {


        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_Stylist_seller.add(R.id.fmFragment, fragment);
        fragmentTransaction_Stylist_seller.addToBackStack(null);
        fragmentTransaction_Stylist_seller.commit();


    }


    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                preferences.setPRE_TOKEN("");
                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    private void method_get_api() {


        AndroidNetworking.get(Global_Service_Api.API_get_stylist_profile)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {


                                Profile_Response Response = new Gson().fromJson(result.toString(), Profile_Response.class);

                                if (Response.getData() != null) {
                                    stylist_data_Editprofile = new Profile_Data();
                                    stylist_data_Editprofile = Response.getData();
                                    method_setdata();

                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }

    private void method_setdata() {

        if (stylist_data_Editprofile != null) {

            tv_name.setText("" + stylist_data_Editprofile.getName() + " " + stylist_data_Editprofile.getLastname());


            if (stylist_data_Editprofile.getis_premium() != null) {
                if (stylist_data_Editprofile.getis_premium().equalsIgnoreCase("1")) {
                    ll_premium.setVisibility(View.VISIBLE);
                    ll_upgrade.setVisibility(View.GONE);
                    utills.is_premium_image = true;
                } else {
                    ll_premium.setVisibility(View.GONE);
                    ll_upgrade.setVisibility(View.VISIBLE);
                    utills.is_premium_image = false;

                }
            }
            if (stylist_data_Editprofile.getstylist_rating() != null) {
                simpleRatingBar_quality.setRating(stylist_data_Editprofile.getstylist_rating().getavg_rating());
            }else {
                simpleRatingBar_quality.setRating(0);
            }

            try {
                if (stylist_data_Editprofile.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + stylist_data_Editprofile.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            adapter = new Pager(getChildFragmentManager(), tabs.getTabCount());
            viewPager.setAdapter(adapter);

            viewPager.setCurrentItem(i_position);

        }
    }


    Fragment_Stylist_Profile fragment_1;
    Fragment_Stylist_Location fragment_2;
    Fragment_Stylist_Order fragment_3;
    Fragment_Stylist_Booking fragment_4;
    Fragment_Stylist_Billing fragment_5;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);

            fragment_1 = new Fragment_Stylist_Profile();
            fragment_2 = new Fragment_Stylist_Location();
            fragment_3 = new Fragment_Stylist_Order();
            fragment_4 = new Fragment_Stylist_Booking();
            fragment_5 = new Fragment_Stylist_Billing();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_1;
                case 1:
                    return fragment_2;
                case 2:
                    return fragment_3;
                case 3:
                    return fragment_4;
                case 4:
                    return fragment_5;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }



    /*...profile pic..*/

    Integer PICKFILE_RESULT_CODE = 1001;

    void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(context)) {
            Request_READ_EXTERNAL_STORAGE();
        } else {


            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            tv_dialog_name.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.GONE);

            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }


    private void Request_READ_EXTERNAL_STORAGE() {
        requestPermissions(new String[]
                        {
                                WRITE_EXTERNAL_STORAGE, CAMERA
                        },
                200);
    }



    File files;
    Uri mCapturedImageURI;

    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {

            Uri imageUri = getPickImageResultUri(context, data);
            CropImage.activity(imageUri)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context, this);

        } else if (requestCode == 1 && resultCode == RESULT_OK) {


            if (files != null) {
                if (files.exists()) {
                    CropImage.activity(Uri.fromFile(files))
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .start(context, this);
                } else {
                    Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                String path = FileUtils.getPath(context, resultUri);
                File file123 = new File(path);

                try {
                    if (file123 != null) {
                        if (file123.exists()) {

                            File SelectedFile_profile;

                            try {
                                SelectedFile_profile = utills.saveBitmapToFile(file123);
                                try {
                                    long fileSizeInBytes = SelectedFile_profile.length();
                                    long fileSizeInKB = fileSizeInBytes / 1024;
                                    long fileSizeInMB = fileSizeInKB / 1024;
                                    if (fileSizeInMB > 5) {
                                        Toast.makeText(context,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                                        SelectedFile_profile=null;
                                        return;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                SelectedFile_profile = file123;
                                e.printStackTrace();
                            }

                            Glide.with(this)
                                    .load(SelectedFile_profile.getAbsolutePath())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(img_profile);


                            method_api_profile(SelectedFile_profile);


                        } else {
                            Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }



    private void method_api_profile(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_update_profile_picture)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addMultipartFile("profile_pic", file_logo)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    /*...premium..*/
    /*...premium..*/


    private void method_dialog_isPremium() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_is_premium);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardView card_premium = dialog.findViewById(R.id.card_premium);
        final TextView tv_nothanks = dialog.findViewById(R.id.tv_nothanks);

        final LinearLayout ll_3_plan = dialog.findViewById(R.id.ll_3_plan);
        final LinearLayout ll_2_plan = dialog.findViewById(R.id.ll_2_plan);
        final LinearLayout ll_1_plan = dialog.findViewById(R.id.ll_1_plan);

        ImageView iv_cancel_subscribe = dialog.findViewById(R.id.iv_cancel_subscribe);
        iv_cancel_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ll_1_month = dialog.findViewById(R.id.ll_1_month);
        ll_1_month_no = dialog.findViewById(R.id.ll_1_month_no);
        ll_1_prize = dialog.findViewById(R.id.ll_1_prize);


        ll_2_month = dialog.findViewById(R.id.ll_2_month);
        ll_2_month_no = dialog.findViewById(R.id.ll_2_month_no);
        ll_2_prize = dialog.findViewById(R.id.ll_2_prize);

        ll_3_month = dialog.findViewById(R.id.ll_3_month);
        ll_3_month_no = dialog.findViewById(R.id.ll_3_month_no);
        ll_3_prize = dialog.findViewById(R.id.ll_3_prize);

        TextView tv_add_method = dialog.findViewById(R.id.tv_add_method);
        final TextView tv_final_prize = dialog.findViewById(R.id.tv_final_prize);


        datalist_StripePlan = new ArrayList<>();
        method_dialog_plan();

        data_card_list = new ArrayList<>();
        method_get_mycard();

        dialog.show();

        ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
        ll_3_plan.setBackground(null);
        ll_1_plan.setBackground(null);

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_mycard();
            }
        });


        card_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                method_premium(dialog);

            }
        });


        tv_nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll_3_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ll_3_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_2_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_3_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 2) {
                            plan_id = datalist_StripePlan.get(2).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        ll_2_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_2_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 1) {
                            plan_id = datalist_StripePlan.get(1).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        ll_1_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_1_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_2_plan.setBackground(null);

                tv_final_prize.setText("" + ll_1_prize.getText().toString());

                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 0) {
                            plan_id = datalist_StripePlan.get(0).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    List<StripePlan_Data> datalist_StripePlan = new ArrayList<>();
    TextView ll_1_month,
            ll_1_month_no,
            ll_1_prize,
            ll_2_month,
            ll_2_month_no,
            ll_2_prize,
            ll_3_month,
            ll_3_month_no,
            ll_3_prize;

    String plan_id;
    String Token_card;

    public void method_dialog_plan() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_stripe_plans)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("home", result);
                            datalist_StripePlan = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    StripePlan_Response Response = new Gson().fromJson(result.toString(), StripePlan_Response.class);

                                    if (Response.getData() != null) {

                                        datalist_StripePlan.addAll(Response.getData());


                                        try {
                                            if (datalist_StripePlan != null) {

                                                if (datalist_StripePlan.size() > 0) {
                                                    ll_1_month.setText("" + datalist_StripePlan.get(0).getInterval());
                                                    ll_1_month_no.setText("" + datalist_StripePlan.get(0).getIntervalCount());
                                                    ll_1_prize.setText("£" + (datalist_StripePlan.get(0).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 1) {
                                                    ll_2_month.setText("" + datalist_StripePlan.get(1).getInterval());
                                                    ll_2_month_no.setText("" + datalist_StripePlan.get(1).getIntervalCount());
                                                    ll_2_prize.setText("£" + (datalist_StripePlan.get(1).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 2) {
                                                    ll_3_month.setText("" + datalist_StripePlan.get(2).getInterval());
                                                    ll_3_month_no.setText("" + datalist_StripePlan.get(2).getIntervalCount());
                                                    ll_3_prize.setText("£" + (datalist_StripePlan.get(2).getAmount() / 100));
                                                }


                                                try {
                                                    if (datalist_StripePlan != null) {
                                                        if (datalist_StripePlan.size() > 1) {
                                                            plan_id = datalist_StripePlan.get(1).getId();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);

        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {


                    CardBrand brand = CardUtils.getPossibleCardBrand(s.toString());


                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString())) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }

    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);

        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    Card card_stripe;

    private void getUserKey(final Dialog dialog_card) {

        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, ""+preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());

                        Token_card = token.getId();


                        method_save_card(dialog_card);


                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


//        CardInputWidget cardInputWidget = findViewById(R.id.cardInputWidget);
//        PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
//        if (params != null) {
//            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
//                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
//            stripe.confirmPayment(this, confirmParams);
//        }


    }

    private void method_save_card(final Dialog dialog_card) {


        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token", "" + Token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }

                                    method_get_mycard1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_premium(final Dialog dialog) {


        if (card_id.equalsIgnoreCase("")) {
            Toast.makeText(context,
                    "Please add a payment card",
                    Toast.LENGTH_LONG).show();
        } else {
            if (utills.isOnline(context)) {
                utills.stopLoader(progressDialog);
                progressDialog = utills.startLoader(context);

                AndroidNetworking.post(Global_Service_Api.API_subscribe_premium)
                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                        .addBodyParameter("card_id", "" + card_id)
                        .addBodyParameter("plan_id", "" + plan_id)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String result) {
                                utills.stopLoader(progressDialog);
                                if (result == null || result == "") return;
                                Log.e("card", result);
                                try {
                                    JSONObject jsonObject = new JSONObject(result);

                                    String flag = jsonObject.getString("flag");
                                    String message = jsonObject.getString("message");

                                    if (flag.equalsIgnoreCase("true")) {
                                        if (dialog != null) {
                                            dialog.dismiss();
                                        }

                                        utills.is_premium_image = true;

                                        ll_premium.setVisibility(View.VISIBLE);
                                        ll_upgrade.setVisibility(View.GONE);

                                        utills.stopLoader(progressDialog);

                                        Intent intent = new Intent(context, Premium_Activity.class);
                                        startActivity(intent);

                                    }

                                    Toast.makeText(context,
                                            message,
                                            Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    utills.stopLoader(progressDialog);
                                    e.printStackTrace();
                                }


                                utills.stopLoader(progressDialog);
                            }

                            @Override
                            public void onError(ANError e) {
                                Toast.makeText(context,
                                        e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                utills.stopLoader(progressDialog);
                                Log.d("API", e.toString());
                            }
                        });


            }

        }


    }


    NonScrollListView listview_order;

    String card_id = "";
    List<MyCard_Data> data_card_list = new ArrayList<>();


    private void method_mycard() {


        final Dialog dialog_listcard = new Dialog(context);
        dialog_listcard.setContentView(R.layout.dialog_my_card);
        dialog_listcard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_listcard.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        listview_order = dialog_listcard.findViewById(R.id.listview_order);
        TextView tv_add_method = dialog_listcard.findViewById(R.id.tv_add_method);


        final TextView tv_next = dialog_listcard.findViewById(R.id.tv_next);

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (card_id.equalsIgnoreCase("")) {
                    Toast.makeText(context,
                            "Please Select payment card",
                            Toast.LENGTH_LONG).show();
                } else {

                    if (dialog_listcard != null) {
                        dialog_listcard.dismiss();
                    }

                }
            }
        });


        dialog_listcard.show();

        if (data_card_list != null) {
            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
            listview_order.setAdapter(adapter_review);
        }

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_change_card_dialog();

                card_id = "";

                if (data_card_list != null) {
                    Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                    listview_order.setAdapter(adapter_review);
                }


            }
        });

    }


    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }


    private void method_get_mycard1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();


                                        if (listview_order != null) {
                                            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                                            listview_order.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    public class Adapter_card extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;
        int pos_selected = -1;

        public Adapter_card(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_card.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_card.ViewHolder();
                view = inflater.inflate(R.layout.datalist_mystripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_card.ViewHolder) view.getTag();
            }


            if (pos_selected == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);

            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            String brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.et_change_cardnumber.setText("" + brand + " / " + listState.get(position).getLast4());


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    pos_selected = position;

                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
                    card_id = "" + listState.get(position).getId();

                    notifyDataSetChanged();


                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon, iv_checkbox;
            TextView et_change_cardnumber;
            CardView card_mylist;

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectdocument();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
