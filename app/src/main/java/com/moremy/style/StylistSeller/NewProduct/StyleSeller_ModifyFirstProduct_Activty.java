package com.moremy.style.StylistSeller.NewProduct;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.BuildConfig;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Response;
import com.moremy.style.Utills.FileUtils;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class StyleSeller_ModifyFirstProduct_Activty extends Base1_Activity {

    Dialog progressDialogs = null;


    Context context;

    Preferences preferences;
    List<ModelLifeStyle_Data> sub_category_list;
    public static List<ModelLifeStyle_Data> lifestyle_list;


    RecyclerView rv_list, rv_sub_category;
    ImageView iv_product_image;

    String is_image_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylistseller_firstproduct_add);
        context = StyleSeller_ModifyFirstProduct_Activty.this;
        activity = StyleSeller_ModifyFirstProduct_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        final String service_ids = "" + intent.getStringExtra("service_ids");


        final EditText et_ProductDescription = findViewById(R.id.et_ProductDescription);
        final EditText et_productname = findViewById(R.id.et_productname);
        final EditText et_prize = findViewById(R.id.et_prize);
        final EditText et_skuname = findViewById(R.id.et_skuname);

        final EditText et_quantity = findViewById(R.id.et_quantity);
        final EditText et_postage = findViewById(R.id.et_postage);
        iv_product_image = findViewById(R.id.iv_product_image);

        et_ProductDescription.setText("" + preferences.getPRE_ProductDescription());
        et_productname.setText("" + preferences.getPRE_ProductName());
        et_prize.setText("" + preferences.getPRE_ProductPrize());
        et_skuname.setText("" + preferences.getPRE_ProductSKU());
        et_quantity.setText("" + preferences.getPRE_ProductQuantity());
        et_postage.setText("" + preferences.getPRE_ProductPostage());


        Glide.with(this)
                .load("" + preferences.getPRE_ProductImage())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(iv_product_image);


        rv_list = findViewById(R.id.rv_list);
        rv_list.setNestedScrollingEnabled(false);
        rv_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        rv_sub_category = findViewById(R.id.rv_sub_category);
        rv_sub_category.setNestedScrollingEnabled(false);
        rv_sub_category.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (et_productname.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product name", Toast.LENGTH_SHORT).show();
                } else if (et_ProductDescription.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product Description", Toast.LENGTH_SHORT).show();
                } else if (et_prize.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product Prize", Toast.LENGTH_SHORT).show();
                } else if (et_skuname.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product SKU", Toast.LENGTH_SHORT).show();
                } else if (et_quantity.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product quantity", Toast.LENGTH_SHORT).show();
                } else if (Integer.parseInt(et_quantity.getText().toString()) <= 0) {
                    Toast.makeText(context, "Please enter valid product quantity", Toast.LENGTH_SHORT).show();
                } else if (et_postage.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter product postage", Toast.LENGTH_SHORT).show();
                } else {


                    if (SelectedFile_logo != null) {
                        is_image_url = "";
                        preferences.setPRE_ProductImage("" + SelectedFile_logo.getAbsolutePath());
                    } else {
                        is_image_url = "" + preferences.getPRE_ProductImage();
                        preferences.setPRE_ProductImage("");
                    }

                    Log.e("onClick:is_image_url", "" + is_image_url);
                    Log.e("onClick:Product", "" + preferences.getPRE_ProductImage());

                    preferences.setPRE_ProductName("" + et_productname.getText().toString());
                    preferences.setPRE_ProductDescription("" + et_ProductDescription.getText().toString());
                    preferences.setPRE_ProductPrize("" + et_prize.getText().toString());
                    preferences.setPRE_ProductSKU("" + et_skuname.getText().toString());

                    preferences.setPRE_ProductQuantity("" + et_quantity.getText().toString());
                    preferences.setPRE_ProductPostage("" + et_postage.getText().toString());
                    preferences.setPRE_ProductPerItem("" + is_item_name);


                    List<ModelLifeStyle_Data> list_services_test_subcategory = new ArrayList<>();
                    StringBuilder stringBuilder_ids = new StringBuilder();
                    StringBuilder stringBuilder_name = new StringBuilder();

                    if (sub_category_list != null) {
                        if (sub_category_list.size() > 0) {
                            for (int i = 0; i < sub_category_list.size(); i++) {
                                if (sub_category_list.get(i).is_chech_item) {
                                    list_services_test_subcategory.add(sub_category_list.get(i));

                                    if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                        stringBuilder_ids.append(",");
                                    }

                                    if (!stringBuilder_name.toString().equalsIgnoreCase("")) {
                                        stringBuilder_name.append(" / ");
                                    }
                                    stringBuilder_ids.append(sub_category_list.get(i).getId());
                                    stringBuilder_name.append(sub_category_list.get(i).getName());
                                }
                            }
                        }
                    }


                    if (list_services_test_subcategory.size() == 0) {
                        Toast.makeText(context, "Please Select category for this product", Toast.LENGTH_SHORT).show();

                    } else {
                        if (list_services_test_subcategory.size() < 3) {

                            preferences.setPRE_ProductMainCategory("" + service_ids);
                            preferences.setPRE_ProductSubCategory("" + stringBuilder_ids);

                            Intent i = new Intent(context, StyleSeller_ModifyProductFinal_Activty.class);
                            i.putExtra("service_names", "" + stringBuilder_name);
                            i.putExtra("id", "" + getIntent().getStringExtra("id"));
                            i.putExtra("is_image_url", "" + is_image_url);
                            startActivity(i);

                        } else {
                            Toast.makeText(context, "Please Select maximum 2 category for this product", Toast.LENGTH_SHORT).show();
                        }
                    }


                }


            }
        });


        iv_product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectdocument();
            }
        });


        sub_category_list = new ArrayList<>();
        lifestyle_list = new ArrayList<>();

        get_list("" + service_ids);
        get_lifestyle();


        final Spinner spiner_item = findViewById(R.id.spiner_item);
        ImageView iv_item = findViewById(R.id.iv_spiner_item);
        iv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_item.performClick();
            }
        });


        final List<String> list_item = new ArrayList<>();
        list_item.add("per item");
        list_item.add("per order");

        SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(context, list_item);
        spiner_item.setAdapter((SpinnerAdapter) customAdapter_state);
        if (preferences.getPRE_ProductPerItem().equalsIgnoreCase("1")) {
            spiner_item.setSelection(1);
        } else {
            spiner_item.setSelection(0);
        }


        spiner_item.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                is_item_name = "" + list_item.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


    /*.......*/
    String is_item_name = "";

    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }

    public void get_list(String s) {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.post(Global_Service_Api.API_product_sub_categories_by_product_categories)
                    .addBodyParameter("category_id", s)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())

                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e("product_sub_categories", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                sub_category_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    ModelLifeStyle_Response Response = new Gson().fromJson(result.toString(), ModelLifeStyle_Response.class);

                                    if (Response.getData() != null) {

                                        List<ModelLifeStyle_Data> sub_category_temp = new ArrayList<>();
                                        sub_category_temp = Response.getData();

                                        String ids = "" + preferences.getPRE_ProductSubCategory();
                                        for (int j = 0; j < sub_category_temp.size(); j++) {
                                            if (ids.contains("" + sub_category_temp.get(j).getId())) {
                                                ModelLifeStyle_Data suB_services_data2 = new ModelLifeStyle_Data();
                                                suB_services_data2.setId(sub_category_temp.get(j).getId());
                                                suB_services_data2.setName("" + sub_category_temp.get(j).getName());
                                                suB_services_data2.is_chech_item = true;
                                                sub_category_list.add(suB_services_data2);
                                            } else {
                                                ModelLifeStyle_Data suB_services_data2 = new ModelLifeStyle_Data();
                                                suB_services_data2.setId(sub_category_temp.get(j).getId());
                                                suB_services_data2.setName("" + sub_category_temp.get(j).getName());
                                                suB_services_data2.is_chech_item = false;
                                                sub_category_list.add(suB_services_data2);
                                            }

                                        }


                                        GridAdapter adapter_cat = new GridAdapter(context, sub_category_list);
                                        rv_sub_category.setAdapter(adapter_cat);


                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    public void get_lifestyle() {
        if (utills.isOnline(this)) {
            AndroidNetworking.get(Global_Service_Api.API_lifestyles)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e("get_lifestyle", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                lifestyle_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    ModelLifeStyle_Response Response = new Gson().fromJson(result.toString(), ModelLifeStyle_Response.class);

                                    if (Response.getData() != null) {
                                        List<ModelLifeStyle_Data> sub_category_temp = new ArrayList<>();
                                        sub_category_temp = Response.getData();

                                        String ids = "" + preferences.getPRE_ProductLifeStyle();
                                        for (int j = 0; j < sub_category_temp.size(); j++) {
                                            if (ids.contains("" + sub_category_temp.get(j).getId())) {
                                                ModelLifeStyle_Data suB_services_data2 = new ModelLifeStyle_Data();
                                                suB_services_data2.setId(sub_category_temp.get(j).getId());
                                                suB_services_data2.setName("" + sub_category_temp.get(j).getName());
                                                suB_services_data2.is_chech_item = true;
                                                lifestyle_list.add(suB_services_data2);
                                            } else {
                                                ModelLifeStyle_Data suB_services_data2 = new ModelLifeStyle_Data();
                                                suB_services_data2.setId(sub_category_temp.get(j).getId());
                                                suB_services_data2.setName("" + sub_category_temp.get(j).getName());
                                                suB_services_data2.is_chech_item = false;
                                                lifestyle_list.add(suB_services_data2);
                                            }

                                        }


                                        GridAdapter_list adapter_cat = new GridAdapter_list(context, lifestyle_list);
                                        rv_list.setAdapter(adapter_cat);

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<ModelLifeStyle_Data> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<ModelLifeStyle_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);

            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText(countries_list.get(position).getName());


            if (countries_list.get(position).is_chech_item) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));

            } else {

                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }

            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (countries_list.get(position).is_chech_item) {
                        countries_list.get(position).is_chech_item = false;
                        viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                        viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
                    } else {
                        countries_list.get(position).is_chech_item = true;
                        viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                        viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    class GridAdapter_list extends RecyclerView.Adapter<GridAdapter_list.MyViewHolder> {
        private List<ModelLifeStyle_Data> countries_list;

        Context mcontext;

        public GridAdapter_list(Context context, List<ModelLifeStyle_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);
            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText(countries_list.get(position).getName());


            if (countries_list.get(position).is_chech_item) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }


            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (countries_list.get(position).is_chech_item) {
                        countries_list.get(position).is_chech_item = false;
                        viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                        viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
                    } else {
                        countries_list.get(position).is_chech_item = true;
                        viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                        viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    Integer PICKFILE_RESULT_CODE = 1001;

    void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(this)) {
            utills.Request_READ_EXTERNAL_STORAGE(this);
        } else {


            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            tv_dialog_name.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.GONE);


            tv_dialog_name.setText("For your product image");

            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }

    File files;
    Uri mCapturedImageURI;

    File SelectedFile_logo;

    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            Uri content_describer = data.getData();

            String path = "";
            try {
                path = FileUtils.getPath(this, content_describer);
                if (path != null) {
                    File file = new File(path);


                    try {
                        SelectedFile_logo = utills.saveBitmapToFile(file);
                        try {
                            long fileSizeInBytes = SelectedFile_logo.length();
                            long fileSizeInKB = fileSizeInBytes / 1024;
                            long fileSizeInMB = fileSizeInKB / 1024;
                            if (fileSizeInMB > 5) {
                                Toast.makeText(context, "File must be less than 5MB", Toast.LENGTH_SHORT).show();
                                SelectedFile_logo = null;
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        SelectedFile_logo = file;
                        e.printStackTrace();
                    }


                    Glide.with(this)
                            .load(SelectedFile_logo.getAbsolutePath())
                            .addListener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                    return false;
                                }
                            })
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(iv_product_image);


                } else {

                    Toast.makeText(this, "Please select file from your directory", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            try {
                if (files != null) {
                    if (files.exists()) {


                        try {
                            SelectedFile_logo = utills.saveBitmapToFile(files);
                            try {
                                long fileSizeInBytes = SelectedFile_logo.length();
                                long fileSizeInKB = fileSizeInBytes / 1024;
                                long fileSizeInMB = fileSizeInKB / 1024;
                                if (fileSizeInMB > 5) {
                                    Toast.makeText(context, "File must be less than 5MB", Toast.LENGTH_SHORT).show();
                                    SelectedFile_logo = null;
                                    return;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            SelectedFile_logo = files;
                            e.printStackTrace();
                        }

                        Glide.with(this)
                                .load(SelectedFile_logo.getAbsolutePath())
                                .addListener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                        return false;
                                    }
                                })
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_product_image);

                    } else {
                        Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "File is Null", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static Activity activity = null;

    public static void finish_this() {
        if (activity != null) {
            activity.finish();
        }
    }


}
