package com.moremy.style.StylistSeller.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.SelectModel_Data;
import com.moremy.style.Model.SelectModel_Response;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model.Services_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class StylistSeller_ModifyServices_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;

    ListView listview_country;
    Context context;
    CardView Card_listview;
    TextView tv_services_name;

    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_services);
        context = StylistSeller_ModifyServices_Activty.this;
        activity = StylistSeller_ModifyServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(context);

        listview_country = findViewById(R.id.listview_country);
        tv_services_name = findViewById(R.id.tv_services_name);
        Card_listview = findViewById(R.id.Card_listview);
        CardView card_select = findViewById(R.id.card_select);
        LinearLayout ll_List_select = findViewById(R.id.ll_List_select);
        RelativeLayout rl_next = findViewById(R.id.rl_next);
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText("Stylists");

        LinearLayout ll_sell_check = findViewById(R.id.ll_sell_check);
        ll_sell_check.setVisibility(View.GONE);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!service_name.equalsIgnoreCase("")) {
                    Intent i = new Intent(context, StylistSeller_ModifySubServices_Activty.class);
                    i.putExtra("service_name", service_name);
                    i.putExtra("service_id", service_id);
                    startActivity(i);
                } else {
                    Toast.makeText(context, "Please Select Service", Toast.LENGTH_SHORT).show();
                }

            }
        });

        card_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setPRE_Only_Sell(false);
                Card_listview.setVisibility(View.VISIBLE);
            }
        });

        ll_List_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview.setVisibility(View.GONE);
            }
        });

        get_list();

    }


    String service_name = "";
    String service_id = "";
    List<Services_Data> list_services = new ArrayList<>();

    public void get_list() {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_services)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                               list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {
                                        list_services = Response.getData();

                                        get_selected_list();

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    public class GridAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        List<Services_Data> countries_list = new ArrayList<>();

        GridAdapter(Context context, List<Services_Data> data_countries_list1) {
            this.context = context;
            countries_list = data_countries_list1;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return countries_list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GridAdapter.ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new GridAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.datalist_service1, parent, false);
                viewHolder.tv_textcountry = (TextView) convertView.findViewById(R.id.tv_textcountry);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (GridAdapter.ViewHolder) convertView.getTag();
            }



            if(countries_list.get(position).getIs_selected()){
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
            }else {
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Card_listview.setVisibility(View.GONE);
                    tv_services_name.setText(countries_list.get(position).getName());

                    service_name = "" + countries_list.get(position).getName();
                    service_id = "" + countries_list.get(position).getId();

                }
            });
            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());

            return convertView;
        }

        private class ViewHolder {
            TextView tv_textcountry;
        }


    }


    public static Activity activity = null;
    public static void finish_this() {
        if(activity != null){
            activity.finish();
        }
    }


   public static SelectModel_Data selectModel_data;

    public void get_selected_list() {
        if (utills.isOnline(this)) {

            AndroidNetworking.get(Global_Service_Api.API_get_stylist_services)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e( "onResponse: ",result );
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                List<Services_Data> list_services_selected = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    SelectModel_Response Response = new Gson().fromJson(result.toString(), SelectModel_Response.class);

                                    if (Response.getData() != null) {

                                     selectModel_data = new SelectModel_Data();
                                        selectModel_data = Response.getData();


                                        for (int i=0;i<list_services.size();i++){

                                            String idd=""+list_services.get(i).getId();

                                            if(idd.equalsIgnoreCase(selectModel_data.getServiceId())){
                                                Services_Data services_data=new Services_Data();
                                                services_data.setId(list_services.get(i).getId());
                                                services_data.setIs_selected(true);
                                                services_data.setName(list_services.get(i).getName());
                                                list_services_selected.add(services_data);
                                            }else {
                                                Services_Data services_data=new Services_Data();
                                                services_data.setId(list_services.get(i).getId());
                                                services_data.setIs_selected(false);
                                                services_data.setName(list_services.get(i).getName());
                                                list_services_selected.add(services_data);
                                            }

                                        }

                                        GridAdapter adapter_counties = new GridAdapter(context, list_services_selected);
                                        listview_country.setAdapter(adapter_counties);


                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


}
