package com.moremy.style.StylistSeller.Fragment.Seller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.BuildConfig;
import com.moremy.style.R;

import com.moremy.style.Utills.FileUtils;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.moremy.style.StylistSeller.Fragment.Seller.Fragment_Seller_EditAccount.seller_data_Editprofile;
import static com.theartofdev.edmodo.cropper.CropImage.getPickImageResultUri;


public class Fragment_Seller_Profile extends Fragment {
    Context context;


    public Fragment_Seller_Profile() {
    }


    Preferences preferences;
    EditText et_firstname, et_lastname, et_emailname, et_password, et_username, et_oneline;

    String is_gender = "";

    ImageView iv_show_hide_password, img_logo;

   // NestedScrollView scrollview;

    EditText et_current_password, et_password_Confirm;
    ImageView iv_show_hide_password_current, iv_show_hide_password_Confirm;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seller_profile, container, false);
        context = getActivity();
        preferences = new Preferences(context);
        findview(view);
        TextView tv_gender = view.findViewById(R.id.tv_gender);
        tv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_gendere);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final ImageView iv_cancel = dialog.findViewById(R.id.iv_cancel);

                dialog.show();


                iv_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });


     //   scrollview = view.findViewById(R.id.scrollview);
        CardView card_save = view.findViewById(R.id.card_save);
        card_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!et_current_password.getText().toString().equalsIgnoreCase("") || !et_password.getText().toString().equalsIgnoreCase("")
                        || !et_password_Confirm.getText().toString().equalsIgnoreCase("")) {

                    if (et_current_password.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The current password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_password.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The new password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_password_Confirm.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The confirm password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_current_password.getText().toString().equals(et_password.getText().toString())) {
                        Toast.makeText(context, "Current password and new password must not be same", Toast.LENGTH_LONG).show();
                    } else if (!et_password.getText().toString().equals(et_password_Confirm.getText().toString())) {
                        Toast.makeText(context, "New password and confirm password must be same", Toast.LENGTH_LONG).show();
                    } else {
                        method_api();
                    }

                } else {
                    method_api();
                }

            }
        });


        return view;
    }


    private void findview(View view) {


        img_logo = view.findViewById(R.id.img_logo);
        et_firstname = view.findViewById(R.id.et_firstname);
        et_lastname = view.findViewById(R.id.et_lastname);
        et_emailname = view.findViewById(R.id.et_emailname);
        et_password = view.findViewById(R.id.et_password);

        et_username = view.findViewById(R.id.et_username);
        et_oneline = view.findViewById(R.id.et_oneline);


        et_password.setTransformationMethod(new MyPasswordTransformationMethod());
        iv_show_hide_password = view.findViewById(R.id.iv_show_hide_password);
        iv_show_hide_password_current = view.findViewById(R.id.iv_show_hide_password_current);
        iv_show_hide_password_Confirm = view.findViewById(R.id.iv_show_hide_password_Confirm);
        et_current_password = view.findViewById(R.id.et_current_password);
        et_password_Confirm = view.findViewById(R.id.et_password_Confirm);

        et_current_password.setTransformationMethod(new MyPasswordTransformationMethod());
        et_password_Confirm.setTransformationMethod(new MyPasswordTransformationMethod());

        iv_show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });
        iv_show_hide_password_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });
        iv_show_hide_password_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });



        final Spinner spiner_gender = view.findViewById(R.id.spiner_gender);
        ImageView iv_spinner = view.findViewById(R.id.iv_spinner);
        iv_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_gender.performClick();
            }
        });


        final List<String> list_gender = new ArrayList<>();
        list_gender.add("Male");
        list_gender.add("Female");
        list_gender.add("Other");
        list_gender.add("Rather not say");
        list_gender.add("Please select");

        final SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(context, list_gender);
        spiner_gender.setAdapter((SpinnerAdapter) customAdapter_state);
        spiner_gender.setSelection(customAdapter_state.getCount() );


        spiner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (customAdapter_state.getCount() != position) {
                    is_gender = "" + list_gender.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        if (seller_data_Editprofile != null) {

            if (seller_data_Editprofile.getCompanyLogo() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + seller_data_Editprofile.getCompanyLogo())
                        .into(img_logo);
            }


            et_firstname.setText("" + seller_data_Editprofile.getName());
            et_lastname.setText("" + seller_data_Editprofile.getLastname());
            et_emailname.setText("" + seller_data_Editprofile.getEmail());

            if (seller_data_Editprofile.getusername() != null) {
                if (!seller_data_Editprofile.getusername().equalsIgnoreCase("null")) {
                    et_username.setText("" + seller_data_Editprofile.getusername());

                }
            }


            et_oneline.setText("" + seller_data_Editprofile.getOneLine());

            String Genderr = "" + seller_data_Editprofile.getGender();


            try {
                if (Genderr.equalsIgnoreCase("male")) {
                    spiner_gender.setSelection(0);
                } else if (Genderr.equalsIgnoreCase("female")) {
                    spiner_gender.setSelection(1);
                } else if (Genderr.equalsIgnoreCase("other")) {
                    spiner_gender.setSelection(2);
                } else if (Genderr.equalsIgnoreCase("Rather not say")) {
                    spiner_gender.setSelection(3);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        img_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectdocument();

            }
        });


    }


    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size()- 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }

    boolean is_show_pwd = false;

    public void hide_unhide_password() {

        if (!is_show_pwd) {

            iv_show_hide_password.setImageResource(R.drawable.ic_eye);
            et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password.setSelection(et_password.getText().length());

            iv_show_hide_password_Confirm.setImageResource(R.drawable.ic_eye);
            et_password_Confirm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password_Confirm.setSelection(et_password_Confirm.getText().length());

            iv_show_hide_password_current.setImageResource(R.drawable.ic_eye);
            et_current_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_current_password.setSelection(et_current_password.getText().length());


            is_show_pwd = true;


        } else  {
            iv_show_hide_password.setImageResource(R.drawable.ic_hide);
            et_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password.setSelection(et_password.getText().length());

            iv_show_hide_password_Confirm.setImageResource(R.drawable.ic_hide);
            et_password_Confirm.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password_Confirm.setSelection(et_password_Confirm.getText().length());

            iv_show_hide_password_current.setImageResource(R.drawable.ic_hide);
            et_current_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_current_password.setSelection(et_current_password.getText().length());


            is_show_pwd = false;

        }
    }


    public class MyPasswordTransformationMethod extends HideReturnsTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new MyPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }


    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {

            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_seller_profile)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("name", "" + et_firstname.getText().toString())
                    .addBodyParameter("lastname", "" + et_lastname.getText().toString())
                    .addBodyParameter("email", "" + et_emailname.getText().toString())
                    .addBodyParameter("username", "" + et_username.getText().toString())
                    .addBodyParameter("password", "" + et_password.getText().toString())
                    .addBodyParameter("old_password", "" + et_current_password.getText().toString())
                    .addBodyParameter("one_line", "" + et_oneline.getText().toString())
                    .addBodyParameter("gender", "" + is_gender)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }



    /*.....*/

    Integer PICKFILE_RESULT_CODE = 1001;

    void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(context)) {
            utills.Request_READ_EXTERNAL_STORAGE(getActivity());
        } else {


            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }

    File files;
    Uri mCapturedImageURI;

    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {

            Uri imageUri = getPickImageResultUri(context, data);
            CropImage.activity(imageUri)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context, this);

        } else if (requestCode == 1 && resultCode == RESULT_OK) {


            if (files != null) {
                if (files.exists()) {
                    CropImage.activity(Uri.fromFile(files))
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .start(context, this);
                } else {
                    Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                String path = FileUtils.getPath(context, resultUri);
                File file123 = new File(path);

                try {
                    if (file123 != null) {
                        if (file123.exists()) {

                            File SelectedFile_profile;

                            try {
                                SelectedFile_profile = utills.saveBitmapToFile(file123);
                                try {
                                    long fileSizeInBytes = SelectedFile_profile.length();
                                    long fileSizeInKB = fileSizeInBytes / 1024;
                                    long fileSizeInMB = fileSizeInKB / 1024;
                                    if (fileSizeInMB > 5) {
                                        Toast.makeText(context,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                                        SelectedFile_profile=null;
                                        return;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                SelectedFile_profile = file123;
                                e.printStackTrace();
                            }

                            Glide.with(this)
                                    .load(SelectedFile_profile.getAbsolutePath())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(img_logo);


                            method_api_logo(SelectedFile_profile);


                        } else {
                            Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }



    private void method_api_logo(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_update_company_logo)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addMultipartFile("company_logo", file_logo)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    /*.....*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectdocument();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.VISIBLE);
//            }
//        } else {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.GONE);
//            }
//        }
    }

}