package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Activity.Base_Activity;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class StipeCardList_Activty extends Base_Activity {


    Context context;

    Preferences preferences;
    List<MyCard_Data> data_order_list = new ArrayList<>();
    Adapter_order adapter_review;
    NonScrollListView listview_order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_card);
        context = StipeCardList_Activty.this;

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(context);
        data_order_list = new ArrayList<>();

        listview_order = findViewById(R.id.listview_order);

        method_get_mycard();

        ImageView iv_backk = findViewById(R.id.iv_backk);
        iv_backk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageView iv_add = findViewById(R.id.iv_add);
        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_change_card_dialog();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        data_order_list = new ArrayList<>();
        method_get_mycard();
    }

    Dialog progressDialog = null;

    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");


                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();


                                        adapter_review = new Adapter_order(context, data_order_list);
                                        listview_order.setAdapter(adapter_review);

                                    }


                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);

        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {


                    CardBrand  brand = CardUtils.getPossibleCardBrand(s.toString());

                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else  if (CardBrand.Discover.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    }else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else  if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else  if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    }else  if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    }else  {
                       iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString())) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }


    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);


        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    Card card_stripe;

    private void getUserKey(final Dialog dialog_card) {

        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, ""+preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());

                        String Token_card = token.getId();

                        if (dialog_card != null) {
                            dialog_card.dismiss();
                        }

                        method_save_card(Token_card);
                        
                        
                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


    }

    private void method_save_card(String token_card) {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token",""+token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    public class Adapter_order extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;


        public Adapter_order(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_stripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }




            String  brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else  if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            }else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else  if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else  if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            }else  if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            }else  {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.et_change_cardnumber.setText("XXXX XXXX XXXX " + listState.get(position).getLast4());



            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon;
            TextView et_change_cardnumber;

        }

    }


}
