package com.moremy.style.StylistSeller.NewProduct;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Fragment.Model_Shop.ModelShopDetails_Data;
import com.moremy.style.Fragment.Model_Shop.ModelShopDetails_Response;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model.Services_Response;
import com.moremy.style.Model.SuB_Services_Data;
import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class StyleSeller_ModifyProduct_Services_Activty extends Base1_Activity implements View.OnClickListener {

    Dialog progressDialogs = null;


    Context context;

    List<Services_Data> Salon_list_services;
    List<Services_Data> Salon_list_services_temp;

    RecyclerView recycleview_services;

    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_services);
        context = StyleSeller_ModifyProduct_Services_Activty.this;
        activity = StyleSeller_ModifyProduct_Services_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);

        recycleview_services = findViewById(R.id.recycleview_services);
        recycleview_services.setNestedScrollingEnabled(false);


        ChipsLayoutManager spanLayoutManager = ChipsLayoutManager.newBuilder(this)
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .build();
        recycleview_services.setLayoutManager(spanLayoutManager);

        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Services_Data> list_services_test_subcategory = new ArrayList<>();

                StringBuilder stringBuilder = new StringBuilder();
                if (Salon_list_services.size() > 0) {
                    for (int i = 0; i < Salon_list_services.size(); i++) {
                        if (Salon_list_services.get(i).getIs_selected()) {
                            list_services_test_subcategory.add(Salon_list_services.get(i));

                            if (!stringBuilder.toString().equalsIgnoreCase("")) {
                                stringBuilder.append(",");
                            }
                            stringBuilder.append(Salon_list_services.get(i).getId());
                        }
                    }
                }


                if (!stringBuilder.toString().equalsIgnoreCase("")) {

                    if (list_services_test_subcategory.size() < 4) {
                        Intent i = new Intent(context, StyleSeller_ModifyFirstProduct_Activty.class);
                        i.putExtra("service_ids", "" + stringBuilder);
                        i.putExtra("id",  "" + getIntent().getStringExtra("id"));
                        startActivity(i);
                    } else {
                        Toast toast = Toast.makeText(context, "Please Select maximum 3 Service", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                    }


                } else {
                    Toast toast = Toast.makeText(context, "Please Select Service", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                }

            }
        });

        Salon_list_services = new ArrayList<>();
        Salon_list_services_temp = new ArrayList<>();

        get_list();
        get_details("" + getIntent().getStringExtra("id"));


    }


    public void get_details(String id) {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_get_product_details + id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e("get_details", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {


                                    ModelShopDetails_Response Response = new Gson().fromJson(result.toString(), ModelShopDetails_Response.class);
                                    if (Response.getData() != null) {
                                        ModelShopDetails_Data modelShopDetails_data = new ModelShopDetails_Data();
                                        modelShopDetails_data = Response.getData();

                                        if (modelShopDetails_data.getcategory_list() != null) {

                                            List<Services_Data> list_services_selected = new ArrayList<>();
                                            Salon_list_services = new ArrayList<>();
                                            list_services_selected.addAll(modelShopDetails_data.getcategory_list());

                                            for (int j = 0; j < Salon_list_services_temp.size(); j++) {

                                                String i_id = "" + Salon_list_services_temp.get(j).getId();

                                                boolean found = false;
                                                int poss = 0;

                                                for (int i_select = 0; i_select < list_services_selected.size(); i_select++) {
                                                    if (i_id.equalsIgnoreCase("" + list_services_selected.get(i_select).getId())) {
                                                        found = true;
                                                        poss = i_select;
                                                    }
                                                }

                                                if (found) {

                                                    Services_Data suB_services_data2 = new Services_Data();
                                                    suB_services_data2.setId(Salon_list_services_temp.get(j).getId());
                                                    suB_services_data2.setName("" + Salon_list_services_temp.get(j).getName());
                                                    suB_services_data2.setIs_selected(true);
                                                    Salon_list_services.add(suB_services_data2);
                                                    poss = 0;
                                                } else {
                                                    Services_Data suB_services_data2 = new Services_Data();
                                                    suB_services_data2.setId(Salon_list_services_temp.get(j).getId());
                                                    suB_services_data2.setName("" + Salon_list_services_temp.get(j).getName());
                                                    suB_services_data2.setIs_selected(false);
                                                    Salon_list_services.add(suB_services_data2);
                                                }

                                            }

                                            GridAdapter adapter_cat = new GridAdapter(context, Salon_list_services);
                                            recycleview_services.setAdapter(adapter_cat);

                                        }


                                        if (modelShopDetails_data.getSubcategory_list() != null) {
                                            List<Services_Data> list_services_sub = new ArrayList<>();
                                            list_services_sub.addAll(modelShopDetails_data.getSubcategory_list());
                                            StringBuilder stringBuilder_ids = new StringBuilder();
                                            for (int i = 0; i < list_services_sub.size(); i++) {
                                                if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                                    stringBuilder_ids.append(",");
                                                }
                                                stringBuilder_ids.append(list_services_sub.get(i).getId());
                                            }
                                            preferences.setPRE_ProductSubCategory("" + stringBuilder_ids);
                                        }

                                        if (modelShopDetails_data.getlifestyle() != null) {
                                            List<ModelLifeStyle_Data> list_services_life = new ArrayList<>();
                                            list_services_life.addAll(modelShopDetails_data.getlifestyle());
                                            StringBuilder stringBuilder_ids = new StringBuilder();
                                            for (int i = 0; i < list_services_life.size(); i++) {
                                                if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                                    stringBuilder_ids.append(",");
                                                }
                                                stringBuilder_ids.append(list_services_life.get(i).getId());
                                            }
                                            preferences.setPRE_ProductLifeStyle("" + stringBuilder_ids);
                                        }



                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    public void get_list() {
        if (utills.isOnline(this)) {
            AndroidNetworking.get(Global_Service_Api.API_get_product_category)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e("product_sub_categories", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Salon_list_services_temp = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {
                                        Salon_list_services_temp = Response.getData();
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Services_Data> arrayList;
        int lastPosition = -1;
        Context mcontext;

        public GridAdapter(Context context, List<Services_Data> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_textcountry;

            public MyViewHolder(View view) {
                super(view);

                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_service, parent, false);

            return new MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {


            if (arrayList.get(position).getIs_selected()) {
                arrayList.get(position).setIs_selected(true);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
                viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_black));
            } else {
                arrayList.get(position).setIs_selected(false);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_white));
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (arrayList.get(position).getIs_selected()) {
                        arrayList.get(position).setIs_selected(false);
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                        viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_white));

                    } else {

                        arrayList.get(position).setIs_selected(true);
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
                        viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_black));

                    }


                }
            });


            viewHolder.tv_textcountry.setText(arrayList.get(position).getName());


        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


        }
    }


    public static Activity activity = null;

    public static void finish_this() {
        if (activity != null) {
            activity.finish();
        }
    }


}
