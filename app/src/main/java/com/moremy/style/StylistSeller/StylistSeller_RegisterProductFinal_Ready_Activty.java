package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


public class StylistSeller_RegisterProductFinal_Ready_Activty extends Base1_Activity {

    Preferences preferences;
    Context context;
    Dialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_ready);
        context = StylistSeller_RegisterProductFinal_Ready_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);


        TextView tv_final_name=findViewById(R.id.tv_final_name);
        tv_final_name.setText("Start selling on our marketplace and grow your business beyond just your network!");



        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateBussinessInfo();

            }
        });


    }

    File file_product;
    JSONObject jsonObject;
    String is_per_item = "";

    public void UpdateBussinessInfo() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            String logo = preferences.getPRE_SelectedFile_logo();
            String profile = preferences.getPRE_SelectedFile_profile();


            if (preferences.getPRE_ProductPerItem().equalsIgnoreCase("per item")) {
                /*2*/
                is_per_item = "2";
            } else {
                /*1*/
                is_per_item = "1";
            }


            if (!preferences.getPRE_ProductImage().equalsIgnoreCase("")) {
                file_product = new File(preferences.getPRE_ProductImage());


                jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", "" + preferences.getPRE_ProductName());
                    jsonObject.put("sku", "" + preferences.getPRE_ProductSKU());
                    jsonObject.put("description", "" + preferences.getPRE_ProductDescription());
                    jsonObject.put("price", "" + preferences.getPRE_ProductPrize());
                    jsonObject.put("category_id", "" + preferences.getPRE_ProductMainCategory());
                    jsonObject.put("sub_category_id", "" + preferences.getPRE_ProductSubCategory());
                    jsonObject.put("lifestyle_id", "" + preferences.getPRE_ProductLifeStyle());
                    jsonObject.put("quantity", "" + preferences.getPRE_ProductQuantity());
                    jsonObject.put("shipping_cost", "" + preferences.getPRE_ProductPostage());
                    jsonObject.put("shipping_type", ""+is_per_item );
                } catch (JSONException e) {
                    e.printStackTrace();
                }




                if (logo.equalsIgnoreCase("") && profile.equalsIgnoreCase("")) {
                    method_api_both_null();
                } else if (profile.equalsIgnoreCase("") && !logo.equalsIgnoreCase("")) {
                    File file_logo = new File(preferences.getPRE_SelectedFile_logo());
                    method_api_profile_null(file_logo);
                } else if (logo.equalsIgnoreCase("") && !profile.equalsIgnoreCase("")) {
                    File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                    method_api_logo_null(file_profile_pic);
                } else {

                    File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                    File file_logo = new File(preferences.getPRE_SelectedFile_logo());

                    method_api_not_null(file_profile_pic, file_logo);
                }


            }


        }

    }

    private void method_api_both_null() {


        AndroidNetworking.upload(Global_Service_Api.API_Seller_Signup)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("product", "" + jsonObject.toString())
                .addMultipartFile("picture", file_product)


                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                       // Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }

    private void method_api_not_null(File file_profile_pic, File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_Seller_Signup)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartFile("company_logo", file_logo)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("product", "" + jsonObject.toString())
                .addMultipartFile("picture", file_product)

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    //    Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }


    private void method_api_profile_null(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_Seller_Signup)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartFile("company_logo", file_logo)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("product", "" + jsonObject.toString())
                .addMultipartFile("picture", file_product)


                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                     //   Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_api_logo_null(File file_profile_pic) {


        AndroidNetworking.upload(Global_Service_Api.API_Seller_Signup)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("product", "" + jsonObject.toString())
                .addMultipartFile("picture", file_product)


                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    //    Toast.makeText(context, ""+ anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_set_data(String result) {


        preferences.setPRE_Email("");
        preferences.setPRE_Main_ServiceID("");
        preferences.setPRE_Mile("");
        preferences.setPRE_Code("");
        preferences.setPRE_address("");
        preferences.setPRE_City("");
        preferences.setPRE_SelectedFile_logo("");
        preferences.setPRE_SelectedFile_profile("");
        preferences.setPRE_Gender("");
        preferences.setPRE_Password("");
        preferences.setPRE_FirstName("");
        preferences.setPRE_LastName("");
        preferences.setPRE_Main_Oneline("");

        preferences.setPRE_Latitude("");
        preferences.setPRE_Longitude("");
        preferences.setPRE_SalonName("");
        preferences.setPRE_passbase_verify(false);

        preferences.setPRE_passbase_id("");

        preferences.setPRE_UserName("");
        preferences.setPRE_ProductImage("");
        preferences.setPRE_ProductSubCategory("");
        preferences.setPRE_ProductMainCategory("");
        preferences.setPRE_ProductLifeStyle("");
        preferences.setPRE_ProductSKU("");
        preferences.setPRE_ProductPrize("");
        preferences.setPRE_ProductDescription("");
        preferences.setPRE_ProductName("");
        preferences.setPRE_Only_Sell(false);

        try {
            JSONObject jsonObject = new JSONObject(result);

            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            String s_token = "" + jsonObject1.get("token");
            preferences.setPRE_TOKEN(s_token);
            preferences.setPRE_LoginType_Stylist_Seller("5");

            Intent i = new Intent(this, StylistSeller_MainProfile_Activty.class);
            startActivity(i);
            finish();
            finishAffinity();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
