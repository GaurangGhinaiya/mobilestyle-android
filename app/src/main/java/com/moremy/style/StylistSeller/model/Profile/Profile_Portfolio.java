package com.moremy.style.StylistSeller.model.Profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile_Portfolio {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stylist_id")
    @Expose
    private Integer stylistId;
    @SerializedName("image")
    @Expose
    private String image;

    public Profile_Portfolio(String s_id, String s_image) {
        id = s_id;
        image = s_image;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStylistId() {
        return stylistId;
    }

    public void setStylistId(Integer stylistId) {
        this.stylistId = stylistId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}