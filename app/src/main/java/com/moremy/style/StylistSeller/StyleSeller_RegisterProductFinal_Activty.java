package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.TextViewCustom;
import com.moremy.style.Utills.utills;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.StyleSeller_RegisterFirstProduct_Activty.lifestyle_list;


public class StyleSeller_RegisterProductFinal_Activty extends Base1_Activity {

    Dialog progressDialogs = null;


    Context context;

    Preferences preferences;

    List<ModelLifeStyle_Data> lifestyle_list_temp;


    RecyclerView rv_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylistseller_firstproduct_final);
        context = StyleSeller_RegisterProductFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        String service_names = "" + intent.getStringExtra("service_names");

        TextView tv_title_cutomername = findViewById(R.id.tv_title_cutomername);
        TextView tv_subcategory = findViewById(R.id.tv_subcategory);
        TextView tv_ProductDescription = findViewById(R.id.tv_ProductDescription);
        TextView tv_prize = findViewById(R.id.tv_prize);
        ImageView iv_profile_image = findViewById(R.id.iv_profile_image);


        tv_title_cutomername.setText("" + preferences.getPRE_ProductName());
        tv_ProductDescription.setText("" + preferences.getPRE_ProductDescription());
        tv_subcategory.setText("shop > " + service_names);
        tv_prize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(preferences.getPRE_ProductPrize())));


        if (!preferences.getPRE_ProductImage().equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(preferences.getPRE_ProductImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }


        rv_list = findViewById(R.id.rv_list);
        rv_list.setNestedScrollingEnabled(false);
        rv_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        TextViewCustom tv_edit = findViewById(R.id.tv_edit);
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LinearLayout ll_lifestyle = findViewById(R.id.ll_lifestyle);
        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context, StylistSeller_RegisterProductBio_Activty.class);
                startActivity(i);

            }
        });


        lifestyle_list_temp = new ArrayList<>();
        StringBuilder stringBuilder_ids = new StringBuilder();

        if (lifestyle_list != null) {
            if (lifestyle_list.size() > 0) {
                for (int i = 0; i < lifestyle_list.size(); i++) {
                    if (lifestyle_list.get(i).is_chech_item) {
                        lifestyle_list_temp.add(lifestyle_list.get(i));

                        if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                            stringBuilder_ids.append(",");
                        }
                        stringBuilder_ids.append(lifestyle_list.get(i).getId());
                    }
                }
            }
        }


        if (lifestyle_list_temp.size() == 0) {
            ll_lifestyle.setVisibility(View.GONE);
        } else {
            ll_lifestyle.setVisibility(View.VISIBLE);
            GridAdapter adapter_cat = new GridAdapter(context, lifestyle_list_temp);
            rv_list.setAdapter(adapter_cat);

        }

        preferences.setPRE_ProductLifeStyle("" + stringBuilder_ids);
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<ModelLifeStyle_Data> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<ModelLifeStyle_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_lifestyle, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText(countries_list.get(position).getName());


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }



}
