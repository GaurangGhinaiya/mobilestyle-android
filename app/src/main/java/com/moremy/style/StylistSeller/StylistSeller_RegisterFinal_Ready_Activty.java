package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.StylistSeller_RegisterPortPholio_Activty.Image_List;
import static com.moremy.style.StylistSeller.StylistSeller_RegisterSubServicesFinal_Activty.list_services_final;
import static com.moremy.style.StylistSeller.StylistSeller_RegisterSubServices_Activty.list_services_Premium_List;


public class StylistSeller_RegisterFinal_Ready_Activty extends Base1_Activity {

    Preferences preferences;
    Context context;
    Dialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_ready);
        context = StylistSeller_RegisterFinal_Ready_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);


        TextView tv_final_name = findViewById(R.id.tv_final_name);
        tv_final_name.setText("You'll be able to update and maintain your profile. Customers will get to learn more about you and chat with you if interested.");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateBussinessInfo();

            }
        });


    }


    int is_verify;

    public void UpdateBussinessInfo() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            String logo = preferences.getPRE_SelectedFile_logo();
            String profile = preferences.getPRE_SelectedFile_profile();


            if (preferences.getPRE_passbase_verify()) {
                is_verify = 1;
            } else {
                is_verify = 0;
            }


            if (logo.equalsIgnoreCase("") && profile.equalsIgnoreCase("")) {
                method_api_both_null();
            } else if (profile.equalsIgnoreCase("") && !logo.equalsIgnoreCase("")) {
                File file_logo = new File(preferences.getPRE_SelectedFile_logo());
                method_api_profile_null(file_logo);
            } else if (logo.equalsIgnoreCase("") && !profile.equalsIgnoreCase("")) {
                File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                method_api_logo_null(file_profile_pic);
            } else {

                File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                File file_logo = new File(preferences.getPRE_SelectedFile_logo());

                method_api_not_null(file_profile_pic, file_logo);
            }


        }

    }

    private void method_api_both_null() {


        JSONArray jsonArray_premium = new JSONArray();
        for (int i = 0; i < list_services_Premium_List.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", list_services_Premium_List.get(i).getName());
                jsonObject.put("description", list_services_Premium_List.get(i).is_Services_description);
                jsonObject.put("price", list_services_Premium_List.get(i).sub_Price);
                jsonObject.put("time", list_services_Premium_List.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray_premium.put(jsonObject);
        }


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("subservice_id", list_services_final.get(i).getId());
                jsonObject.put("price", list_services_final.get(i).sub_Price);
                jsonObject.put("time", list_services_final.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        List<File> FOTO = new ArrayList<File>();
        for (int i = 0; i < Image_List.size(); i++) {
            FOTO.add(new File(Image_List.get(i).toString()));

        }


        AndroidNetworking.upload(Global_Service_Api.API_Stylist_Signup)
                .addMultipartFileList("portfolio[]", FOTO)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartParameter("is_selfie_verify", "" + is_verify)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("service_id", "" + preferences.getPRE_Main_ServiceID())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())

                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .addMultipartParameter("passbase_id", "" + preferences.getPRE_passbase_id())


                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API",""+ anError.getErrorDetail());
                        Log.d("API", ""+anError.getErrorBody());
                        Log.d("API",""+ anError.getErrorCode());
                          Toast.makeText(context, ""+anError.getErrorDetail(), Toast.LENGTH_LONG).show();

                    }
                });

    }

    private void method_api_not_null(File file_profile_pic, File file_logo) {


        JSONArray jsonArray_premium = new JSONArray();
        for (int i = 0; i < list_services_Premium_List.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", list_services_Premium_List.get(i).getName());
                jsonObject.put("description", list_services_Premium_List.get(i).is_Services_description);
                jsonObject.put("price", list_services_Premium_List.get(i).sub_Price);
                jsonObject.put("time", list_services_Premium_List.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray_premium.put(jsonObject);
        }

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("subservice_id", list_services_final.get(i).getId());
                jsonObject.put("price", list_services_final.get(i).sub_Price);
                jsonObject.put("time", list_services_final.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        List<File> FOTO = new ArrayList<File>();
        for (int i = 0; i < Image_List.size(); i++) {
            FOTO.add(new File(Image_List.get(i).toString()));

        }


        AndroidNetworking.upload(Global_Service_Api.API_Stylist_Signup)
                .addMultipartFileList("portfolio[]", FOTO)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartParameter("passbase_id", "" + preferences.getPRE_passbase_id())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartFile("company_logo", file_logo)
                .addMultipartParameter("is_selfie_verify", "" + is_verify)
                .addMultipartParameter("service_id", "" + preferences.getPRE_Main_ServiceID())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API",""+ anError.getErrorDetail());
                        Log.d("API", ""+anError.getErrorBody());
                        Log.d("API",""+ anError.getErrorCode());
                          Toast.makeText(context, ""+anError.getErrorDetail(), Toast.LENGTH_LONG).show();
                    }
                });

    }


    private void method_api_profile_null(File file_logo) {


        JSONArray jsonArray_premium = new JSONArray();
        for (int i = 0; i < list_services_Premium_List.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", list_services_Premium_List.get(i).getName());
                jsonObject.put("description", list_services_Premium_List.get(i).is_Services_description);
                jsonObject.put("price", list_services_Premium_List.get(i).sub_Price);
                jsonObject.put("time", list_services_Premium_List.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray_premium.put(jsonObject);
        }

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("subservice_id", list_services_final.get(i).getId());
                jsonObject.put("price", list_services_final.get(i).sub_Price);
                jsonObject.put("time", list_services_final.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        List<File> FOTO = new ArrayList<File>();
        for (int i = 0; i < Image_List.size(); i++) {
            FOTO.add(new File(Image_List.get(i).toString()));

        }

        AndroidNetworking.upload(Global_Service_Api.API_Stylist_Signup)
                .addMultipartFileList("portfolio[]", FOTO)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartFile("company_logo", file_logo)
                .addMultipartParameter("is_selfie_verify", "" + is_verify)
                .addMultipartParameter("service_id", "" + preferences.getPRE_Main_ServiceID())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("passbase_id", "" + preferences.getPRE_passbase_id())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API",""+ anError.getErrorDetail());
                        Log.d("API", ""+anError.getErrorBody());
                        Log.d("API",""+ anError.getErrorCode());
                          Toast.makeText(context, ""+anError.getErrorDetail(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_api_logo_null(File file_profile_pic) {


        JSONArray jsonArray_premium = new JSONArray();
        for (int i = 0; i < list_services_Premium_List.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", list_services_Premium_List.get(i).getName());
                jsonObject.put("description", list_services_Premium_List.get(i).is_Services_description);
                jsonObject.put("price", list_services_Premium_List.get(i).sub_Price);
                jsonObject.put("time", list_services_Premium_List.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray_premium.put(jsonObject);
        }


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("subservice_id", list_services_final.get(i).getId());
                jsonObject.put("price", list_services_final.get(i).sub_Price);
                jsonObject.put("time", list_services_final.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        List<File> FOTO = new ArrayList<File>();
        for (int i = 0; i < Image_List.size(); i++) {
            FOTO.add(new File(Image_List.get(i).toString()));

        }

        AndroidNetworking.upload(Global_Service_Api.API_Stylist_Signup)
                .addMultipartFileList("portfolio[]", FOTO)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("radius_from_postal_code", "" + preferences.getPRE_Mile())
                .addMultipartParameter("is_selfie_verify", "" + is_verify)
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("passbase_id", "" + preferences.getPRE_passbase_id())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())

                .addMultipartParameter("service_id", "" + preferences.getPRE_Main_ServiceID())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API",""+ anError.getErrorDetail());
                          Log.d("API", ""+anError.getErrorBody());
                          Log.d("API",""+ anError.getErrorCode());
                          Toast.makeText(context, ""+anError.getErrorDetail(), Toast.LENGTH_LONG).show();

                    }
                });

    }

    private void method_set_data(String result) {


        preferences.setPRE_Email("");

        preferences.setPRE_Main_ServiceID("");
        preferences.setPRE_Mile("");
        preferences.setPRE_Code("");
        preferences.setPRE_address("");
        preferences.setPRE_City("");
        preferences.setPRE_SelectedFile_logo("");
        preferences.setPRE_SelectedFile_profile("");
        preferences.setPRE_Gender("");
        preferences.setPRE_Password("");
        preferences.setPRE_FirstName("");
        preferences.setPRE_LastName("");
        preferences.setPRE_Main_Oneline("");

        preferences.setPRE_Latitude("");
        preferences.setPRE_Longitude("");
        preferences.setPRE_SalonName("");
        preferences.setPRE_passbase_verify(false);

        preferences.setPRE_passbase_id("");
        preferences.setPRE_UserName("");

        preferences.setPRE_Is_Premium(false);


        try {
            JSONObject jsonObject = new JSONObject(result);

            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            String s_token = "" + jsonObject1.get("token");
            preferences.setPRE_TOKEN(s_token);
            preferences.setPRE_LoginType_Stylist_Seller("3");

            Intent i = new Intent(this, StylistSeller_MainProfile_Activty.class);
            startActivity(i);
            finish();
            finishAffinity();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
