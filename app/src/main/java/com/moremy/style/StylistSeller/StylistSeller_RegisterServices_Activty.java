package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model.Services_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class StylistSeller_RegisterServices_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;

    ListView listview_country;
    Context context;
    CardView Card_listview;
    TextView tv_services_name;

    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_services);
        context = StylistSeller_RegisterServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences=new Preferences(context);

        listview_country = findViewById(R.id.listview_country);
        tv_services_name = findViewById(R.id.tv_services_name);
        Card_listview = findViewById(R.id.Card_listview);
        final CardView card_select = findViewById(R.id.card_select);
        LinearLayout ll_List_select = findViewById(R.id.ll_List_select);
        RelativeLayout rl_next = findViewById(R.id.rl_next);
        LinearLayout ll_sell_check = findViewById(R.id.ll_sell_check);
        final ImageView iv_checkbox_sell = findViewById(R.id.iv_checkbox_sell);
        final ImageView iv_spinner = findViewById(R.id.iv_spinner);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (preferences.getPRE_Only_Sell()){

                    Intent i = new Intent(context, StyleSeller_RegisterSellingServices_Activty.class);
                    startActivity(i);


                }else {
                    if(!service_name.equalsIgnoreCase("")){
                        Intent i = new Intent(context, StylistSeller_RegisterSubServices_Activty.class);
                        i.putExtra("service_name", service_name);
                        i.putExtra("service_id", service_id);
                        startActivity(i);
                    }else {
                        Toast.makeText(context,"Please Select Service",Toast.LENGTH_SHORT).show();
                    }
                }




            }
        });

        card_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setPRE_Only_Sell(false);
                Card_listview.setVisibility(View.VISIBLE);
            }
        });

        ll_List_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview.setVisibility(View.GONE);
            }
        });

        get_list();


        ll_sell_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (preferences.getPRE_Only_Sell()) {
                    iv_checkbox_sell.setImageResource(R.drawable.ic_chechkbox_plain);
                    preferences.setPRE_Only_Sell(false);
                    card_select.setClickable(true);
                    card_select.setEnabled(true);
                    tv_services_name.setTextColor(getResources().getColor(R.color.black));
                    tv_services_name.setHintTextColor(getResources().getColor(R.color.black));
                    iv_spinner.setColorFilter(getResources().getColor(R.color.black));
                } else {
                    iv_checkbox_sell.setImageResource(R.drawable.ic_chechkbox_fill);
                    preferences.setPRE_Only_Sell(true);
                    card_select.setClickable(false);
                    card_select.setEnabled( false);
                    tv_services_name.setTextColor(getResources().getColor(R.color.gray2));
                    tv_services_name.setHintTextColor(getResources().getColor(R.color.gray2));
                    iv_spinner.setColorFilter(getResources().getColor(R.color.gray2));
                }


            }
        });

    }


    String service_name = "";
    String service_id = "";

    public void get_list() {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_services)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                List<Services_Data> list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {
                                        list_services = Response.getData();
                                        GridAdapter adapter_counties = new GridAdapter(context, list_services);
                                        listview_country.setAdapter(adapter_counties);

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    public class GridAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        List<Services_Data> countries_list = new ArrayList<>();

        GridAdapter(Context context, List<Services_Data> data_countries_list1) {
            this.context = context;
            countries_list = data_countries_list1;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return countries_list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GridAdapter.ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new GridAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.datalist_service1, parent, false);
                viewHolder.tv_textcountry = (TextView) convertView.findViewById(R.id.tv_textcountry);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (GridAdapter.ViewHolder) convertView.getTag();
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Card_listview.setVisibility(View.GONE);
                    tv_services_name.setText(countries_list.get(position).getName());

                    service_name = "" + countries_list.get(position).getName();
                    service_id = "" + countries_list.get(position).getId();

                }
            });
            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());

            return convertView;
        }

        private class ViewHolder {
            TextView tv_textcountry;
        }


    }


}
