package com.moremy.style.StylistSeller.Fragment.Seller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Booking_Fragment.ChatBookingDirect_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.ModelBooking_BookingServiceItem;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.ModelBooking_Response;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.ModelBooking_data;
import com.moremy.style.R;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Fragment_Seller_Booking extends Fragment {

    Context context;

    Preferences preferences;

    public Fragment_Seller_Booking() {
    }

    TextView tv_no_data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricelist, container, false);
        context = getActivity();

        preferences = new Preferences(context);

        data_booking_list = new ArrayList<>();
        tv_no_data = view.findViewById(R.id.tv_no_data);

        listview_countiy = view.findViewById(R.id.listview_countiy);
        method_api();

        return view;
    }

    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_get_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_booking", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_no_data.setVisibility(View.GONE);
                                    listview_countiy.setVisibility(View.VISIBLE);
                                    ModelBooking_Response Response = new Gson().fromJson(result.toString(), ModelBooking_Response.class);

                                    if (Response.getData() != null) {

                                        data_booking_list = new ArrayList<>();
                                        data_booking_list = Response.getData();


                                        Adapter_Booking adapter_review = new Adapter_Booking(context, data_booking_list);
                                        listview_countiy.setAdapter(adapter_review);

                                    }


                                } else {
                                    tv_no_data.setVisibility(View.VISIBLE);
                                    listview_countiy.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }


    public class Adapter_Booking extends BaseAdapter {

        Context context;
        List<ModelBooking_data> listState;
        LayoutInflater inflater;


        public Adapter_Booking(Context applicationContext, List<ModelBooking_data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_Booking.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_Booking.ViewHolder();
                view = inflater.inflate(R.layout.datalist_bookinglist, viewGroup, false);

                viewHolder.tv_service_name_b = view.findViewById(R.id.tv_service_name_b);
                viewHolder.tv_address_b = view.findViewById(R.id.tv_address_b);
                viewHolder.tv_date_b = view.findViewById(R.id.tv_date_b);

                viewHolder.iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                viewHolder.ll_main_cell = view.findViewById(R.id.ll_main_cell);
                viewHolder.progress_bar = view.findViewById(R.id.progress_bar);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_Booking.ViewHolder) view.getTag();
            }


            String pic = "";
            if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + listState.get(position).getUserId())) {
                pic = "" + listState.get(position).getStylist().getProfilePic();
            } else {
                if(listState.get(position).getUser() != null){
                    pic = "" + listState.get(position).getUser().getProfilePic();
                }
            }

            if (!pic.equalsIgnoreCase("") && !pic.equalsIgnoreCase("null")) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + pic)
                        .into(viewHolder.iv_product_image_o);
            }

            String Address ="";
            String City ="";
            String Zipcode ="";



            if(listState.get(position).getAddress() != null){
                if(!listState.get(position).getAddress().equalsIgnoreCase("") && !listState.get(position).getAddress().equalsIgnoreCase("null")){
                    Address =""+listState.get(position).getAddress();
                }
            }

            if(listState.get(position).getCity() != null){
                if(!listState.get(position).getCity().equalsIgnoreCase("") && !listState.get(position).getCity().equalsIgnoreCase("null")){
                    City =""+listState.get(position).getCity();
                }
            }

            if(listState.get(position).getZipcode() != null){
                if(!listState.get(position).getZipcode().equalsIgnoreCase("") && !listState.get(position).getZipcode().equalsIgnoreCase("null")){
                    Zipcode =""+listState.get(position).getZipcode();
                }
            }



            viewHolder.tv_address_b.setText("" +Address + ", "
                    + City + " " +Zipcode);


            final    List<ModelBooking_BookingServiceItem> data__list = new ArrayList<>();
            data__list.addAll(listState.get(position).getBookingServiceItem());


            StringBuilder stringBuilder_ids = new StringBuilder();
            for (int i = 0; i < data__list.size(); i++) {
                if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                    stringBuilder_ids.append(", ");
                }
                stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
            }


            viewHolder.tv_service_name_b.setText("" + stringBuilder_ids);


            String date = utills.getTimeAccordingBooking(listState.get(position).getBookingDate());


            String is_wait = "";
            if (listState.get(position).getStatus() == 0) {
                is_wait = "cancelled for ";
                viewHolder.progress_bar.setProgress(0);
            } else if (listState.get(position).getStatus() == 1) {
                is_wait = "awaiting confirmation for ";
                viewHolder.progress_bar.setProgress(30);
            } else if (listState.get(position).getStatus() == 2) {
                is_wait = "confirmed for ";
                viewHolder.progress_bar.setProgress(60);
            } else if (listState.get(position).getStatus() == 3) {
                is_wait = "completed for ";
                viewHolder.progress_bar.setProgress(100);
            }

            viewHolder.tv_date_b.setText(is_wait + "" + listState.get(position).getBookingTime() + " on " + date);


            viewHolder.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    float price = 0;

                    StringBuilder stringBuilder_ids = new StringBuilder();
                    for (int i = 0; i < data__list.size(); i++) {
                        if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                            stringBuilder_ids.append(", ");
                        }
                        price = price + data__list.get(i).getServicePrice();
                        stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                    }


                    Intent i = new Intent(context, ChatBookingDirect_Activity.class);
                    i.putExtra("grup_Name", "" + stringBuilder_ids);
                    i.putExtra("grup_price", "" + price);

                    if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + listState.get(position).getUserId())) {
                        i.putExtra("profile_pic", "" + listState.get(position).getStylist().getProfilePic());
                    } else {
                        i.putExtra("profile_pic", "" + listState.get(position).getUser().getProfilePic());
                    }

                    i.putExtra("channelUrl", "" + listState.get(position).getSendBirdId());
                    i.putExtra("order_id", "" + listState.get(position).getId());

                    startActivity(i);

                }
            });


            return view;
        }

        private class ViewHolder {
            TextView tv_service_name_b;
            TextView tv_address_b;
            TextView tv_date_b;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
            ProgressBar progress_bar;
        }

    }


    List<ModelBooking_data> data_booking_list = new ArrayList<>();
    NonScrollListView listview_countiy;


//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//
//        if (isVisibleToUser) {
//            if (listview_countiy != null) {
//                listview_countiy.setVisibility(View.VISIBLE);
//                data_booking_list = new ArrayList<>();
//                method_api();
//            }
//
//        } else {
//            if (listview_countiy != null) {
//                listview_countiy.setVisibility(View.GONE);
//            }
//        }
//
//    }


}