package com.moremy.style.StylistSeller.Fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.SuB_Services_Data;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.Fragment.StylistSeller_ModifySubServices_Activty.list_services;
import static com.moremy.style.StylistSeller.Fragment.StylistSeller_ModifySubServices_Activty.list_services_Premium_List;


public class StylistSeller_ModifySubServicesFinal_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    public static List<SuB_Services_Data> list_services_final = new ArrayList<>();

    RecyclerView recycleview_cat, listview_premium;
    Preferences preferences;

    String service_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_sub_services_final);
        context = StylistSeller_ModifySubServicesFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);

        recycleview_cat = findViewById(R.id.listview_country);
        listview_premium = findViewById(R.id.listview_premium);
        recycleview_cat.setNestedScrollingEnabled(false);
        listview_premium.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));
        listview_premium.setLayoutManager(new GridLayoutManager(this, 1));


        TextView tv_category = findViewById(R.id.tv_category);

        Intent intent = getIntent();
        tv_category.setText("" + intent.getStringExtra("service_name"));
        service_id = "" + intent.getStringExtra("service_id");
        String service_name = "" + intent.getStringExtra("service_name");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (utills.isOnline(context)) {
                    progressDialog = utills.startLoader(context);


                    method_update_stylist_services();


                }


            }
        });

        list_services_final = new ArrayList<>();

        if (list_services != null) {
            if (list_services.size() > 0) {

                for (int i = 0; i < list_services.size(); i++) {
                    if (list_services.get(i).is_chechkbox) {
                        list_services_final.add(list_services.get(i));
                    }
                }

            }
        }

        GridAdapter adapter_counties = new GridAdapter(context, list_services_final);
        recycleview_cat.setAdapter(adapter_counties);


        if (list_services_Premium_List != null) {
            if (list_services_Premium_List.size() > 0) {
                GridAdapter adapter_counties2 = new GridAdapter(context, list_services_Premium_List);
                listview_premium.setAdapter(adapter_counties2);
            }
        }


    }


    Dialog progressDialog = null;

    private void method_update_stylist_services() {


        JSONArray jsonArray_premium = new JSONArray();
        for (int i = 0; i < list_services_Premium_List.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", ""+list_services_Premium_List.get(i).getName());
                jsonObject.put("description", ""+list_services_Premium_List.get(i).is_Services_description);
                jsonObject.put("price", ""+list_services_Premium_List.get(i).sub_Price);
                jsonObject.put("time", ""+list_services_Premium_List.get(i).sub_min);
                jsonObject.put("id", ""+list_services_Premium_List.get(i).getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray_premium.put(jsonObject);
        }


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("subservice_id", list_services_final.get(i).getId());
                jsonObject.put("price", list_services_final.get(i).sub_Price);
                jsonObject.put("time", list_services_final.get(i).sub_min);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }


        AndroidNetworking.post(Global_Service_Api.API_update_stylist_services)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addBodyParameter("service_id", "" +service_id)
                .addBodyParameter("sub_services", "" + jsonArray.toString())
                .addBodyParameter("other_sub_services", "" + jsonArray_premium.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                finish();

                                StylistSeller_ModifyServices_Activty.finish_this();
                                StylistSeller_ModifySubServices_Activty.finish_this();
                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {

        private List<SuB_Services_Data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<SuB_Services_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry, tv_textPrize;

            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
                tv_textPrize = (TextView) view.findViewById(R.id.tv_textPrize);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_prize, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());
            if (!countries_list.get(position).sub_Price.equalsIgnoreCase("")) {
                viewHolder.tv_textPrize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(countries_list.get(position).sub_Price)));
            }
        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }

}
