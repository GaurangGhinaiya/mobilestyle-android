package com.moremy.style.StylistSeller.Fragment.Stylist;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.BuildConfig;
import com.moremy.style.CommanActivity.Premium_Activity;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.Model.StripePlan_Data;
import com.moremy.style.Model.StripePlan_Response;
import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.Profile.Profile_Portfolio;
import com.moremy.style.Utills.FileUtils;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.moremy.style.StylistSeller.Fragment.Stylist.Fragment_Stylist_EditAccount.stylist_data_Editprofile;
import static com.theartofdev.edmodo.cropper.CropImage.getPickImageResultUri;


public class Fragment_Stylist_Profile extends Fragment {
    Context context;


    public Fragment_Stylist_Profile() {
    }


    Preferences preferences;
    EditText et_firstname, et_lastname, et_emailname, et_password, et_username, et_oneline;

    String is_gender = "";

    ImageView iv_show_hide_password, img_logo;


    EditText et_current_password, et_password_Confirm;
    ImageView iv_show_hide_password_current, iv_show_hide_password_Confirm;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylist_profile, container, false);
        context = getActivity();
        preferences = new Preferences(context);
        findview(view);
        //   scrollview = view.findViewById(R.id.scrollview);
        TextView tv_gender = view.findViewById(R.id.tv_gender);
        tv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_gendere);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final ImageView iv_cancel = dialog.findViewById(R.id.iv_cancel);

                dialog.show();


                iv_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });


        CardView card_salon_name = view.findViewById(R.id.card_salon_name);
        card_salon_name.setVisibility(View.GONE);

        CardView card_save = view.findViewById(R.id.card_save);
        card_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!et_current_password.getText().toString().equalsIgnoreCase("") || !et_password.getText().toString().equalsIgnoreCase("")
                        || !et_password_Confirm.getText().toString().equalsIgnoreCase("")) {

                    if (et_current_password.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The current password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_password.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The new password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_password_Confirm.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "The confirm password field is required.", Toast.LENGTH_LONG).show();
                    } else if (et_current_password.getText().toString().equals(et_password.getText().toString())) {
                        Toast.makeText(context, "Current password and new password must not be same", Toast.LENGTH_LONG).show();
                    } else if (!et_password.getText().toString().equals(et_password_Confirm.getText().toString())) {
                        Toast.makeText(context, "New password and confirm password must be same", Toast.LENGTH_LONG).show();
                    } else {
                        method_api();
                    }

                } else {
                    method_api();
                }

            }
        });


        return view;
    }


    private void findview(View view) {

        img_logo = view.findViewById(R.id.img_logo);
        et_firstname = view.findViewById(R.id.et_firstname);
        et_lastname = view.findViewById(R.id.et_lastname);
        et_emailname = view.findViewById(R.id.et_emailname);
        et_password = view.findViewById(R.id.et_password);

        et_username = view.findViewById(R.id.et_username);
        et_oneline = view.findViewById(R.id.et_oneline);


        iv_show_hide_password = view.findViewById(R.id.iv_show_hide_password);
        et_password.setTransformationMethod(new MyPasswordTransformationMethod());


        iv_show_hide_password_current = view.findViewById(R.id.iv_show_hide_password_current);
        iv_show_hide_password_Confirm = view.findViewById(R.id.iv_show_hide_password_Confirm);
        et_current_password = view.findViewById(R.id.et_current_password);
        et_password_Confirm = view.findViewById(R.id.et_password_Confirm);

        et_current_password.setTransformationMethod(new MyPasswordTransformationMethod());
        et_password_Confirm.setTransformationMethod(new MyPasswordTransformationMethod());

        iv_show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });
        iv_show_hide_password_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });
        iv_show_hide_password_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });


        final Spinner spiner_gender = view.findViewById(R.id.spiner_gender);
        ImageView iv_spinner = view.findViewById(R.id.iv_spinner);
        iv_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_gender.performClick();
            }
        });


        final List<String> list_gender = new ArrayList<>();
        list_gender.add("Male");
        list_gender.add("Female");
        list_gender.add("Other");
        list_gender.add("Rather not say");
        list_gender.add("Please select");

        final SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(context, list_gender);
        spiner_gender.setAdapter((SpinnerAdapter) customAdapter_state);
        spiner_gender.setSelection(customAdapter_state.getCount());


        spiner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (customAdapter_state.getCount() != position) {
                    is_gender = "" + list_gender.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        rv_bussinessIcon = view.findViewById(R.id.rv_bussinessIcon);
        rv_bussinessIcon.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));


        if (stylist_data_Editprofile != null) {

            if (stylist_data_Editprofile.getCompanyLogo() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + stylist_data_Editprofile.getCompanyLogo())
                        .into(img_logo);
            }


            et_firstname.setText("" + stylist_data_Editprofile.getName());
            et_lastname.setText("" + stylist_data_Editprofile.getLastname());
            et_emailname.setText("" + stylist_data_Editprofile.getEmail());

            if (stylist_data_Editprofile.getusername() != null) {
                if (!stylist_data_Editprofile.getusername().equalsIgnoreCase("null")) {
                    et_username.setText("" + stylist_data_Editprofile.getusername());

                }
            }


            et_oneline.setText("" + stylist_data_Editprofile.getOneLine());

            String Genderr = "" + stylist_data_Editprofile.getGender();


            try {
                if (Genderr.equalsIgnoreCase("male")) {
                    spiner_gender.setSelection(0);
                } else if (Genderr.equalsIgnoreCase("female")) {
                    spiner_gender.setSelection(1);
                } else if (Genderr.equalsIgnoreCase("other")) {
                    spiner_gender.setSelection(2);
                } else if (Genderr.equalsIgnoreCase("Rather not say")) {
                    spiner_gender.setSelection(3);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            Temp_Imagess_list = new ArrayList<>();


            if (stylist_data_Editprofile.getPortfolio() != null) {
                if (stylist_data_Editprofile.getPortfolio().size() > 0) {
                    Temp_Imagess_list.addAll(stylist_data_Editprofile.getPortfolio());
                } else {
                    Temp_Imagess_list.add(new Profile_Portfolio("", ""));
                }
            } else {
                Temp_Imagess_list.add(new Profile_Portfolio("", ""));
            }


            adapter_image = new Adapter_Image(context, Temp_Imagess_list);
            rv_bussinessIcon.setAdapter(adapter_image);

        }


        img_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectdocument();

            }
        });

    }


    List<Profile_Portfolio> Temp_Imagess_list = new ArrayList<>();
    RecyclerView rv_bussinessIcon;
    Adapter_Image adapter_image;


    class Adapter_Image extends RecyclerView.Adapter<Adapter_Image.MyViewHolder> {
        private List<Profile_Portfolio> arrayList;
        Context mcontext;

        public Adapter_Image(Context context, List<Profile_Portfolio> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView iv_portfolio;
            CardView card_upload, card_main_image, card_delete;

            public MyViewHolder(View view) {
                super(view);
                iv_portfolio = view.findViewById(R.id.iv_portfolio);
                card_upload = view.findViewById(R.id.card_upload);
                card_main_image = view.findViewById(R.id.card_main_image);
                card_delete = view.findViewById(R.id.card_delete);
            }
        }


        @Override
        public Adapter_Image.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cell_portpholio_imagelist, parent, false);

            return new Adapter_Image.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Image.MyViewHolder holder, final int position) {


            if (position == arrayList.size() - 1) {
                holder.card_upload.setVisibility(View.VISIBLE);
            } else {
                holder.card_upload.setVisibility(View.GONE);
            }


            if (arrayList.get(position).getImage() != null) {
                if (!arrayList.get(position).getImage().equalsIgnoreCase("")) {

                    if (!arrayList.get(position).getId().equalsIgnoreCase("")) {
                        Glide.with(mcontext)
                                .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getImage())
                                .into(holder.iv_portfolio);
                    } else {
                        Glide.with(mcontext)
                                .load(arrayList.get(position).getImage())
                                .into(holder.iv_portfolio);
                    }


                } else {

                    holder.card_main_image.setVisibility(View.GONE);
                    holder.card_delete.setVisibility(View.GONE);
                }
            } else {

                holder.card_main_image.setVisibility(View.GONE);
                holder.card_delete.setVisibility(View.GONE);
            }


            holder.card_upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Log.e("position", "" + position);


                    if (position > 2) {
                        if (!utills.is_premium_image) {
                            method_dialog_isPremium();
                        } else {
                            selectdocument_potfolio();
                        }
                    } else {
                        selectdocument_potfolio();
                    }


                }
            });


            holder.card_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog_card = new Dialog(context);
                    dialog_card.setContentView(R.layout.dialog_chancel);
                    dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    Window window = dialog_card.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                    TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                    TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                    tv_name_dialog.setText("Are you sure you want to delete?");

                    tv_No.setText("Cancel");
                    tv_yes.setText("Ok");

                    dialog_card.show();

                    tv_No.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog_card.dismiss();
                        }
                    });

                    tv_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog_card.dismiss();

                            if (arrayList.get(position).getId() != null) {
                                if (!arrayList.get(position).getId().equalsIgnoreCase("")) {
                                    method_delete_portfolio(arrayList.get(position).getId());
                                }
                            }


                            arrayList.remove(position);

                            if (arrayList.size() == 0) {
                                Temp_Imagess_list.add(new Profile_Portfolio("", ""));
                            }

                            notifyDataSetChanged();
                        }
                    });


                }
            });


        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


    public void selectdocument_potfolio() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(context)) {
            utills.Request_READ_EXTERNAL_STORAGE(getActivity());
        } else {


            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            tv_dialog_name.setText("For your Portfolio");
            tv_dialog_name.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.GONE);


            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage_potfolio();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            10055
                    );
                    dialog.dismiss();
                }
            });


        }
    }

    private void SelectImage_potfolio() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 10056);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 10056);
        }
    }

    private void method_file_set_portfolio(File files1) {

        File files = utills.saveBitmapToFile(files1);
        try {
            long fileSizeInBytes = files.length();
            long fileSizeInKB = fileSizeInBytes / 1024;
            long fileSizeInMB = fileSizeInKB / 1024;
            if (fileSizeInMB > 5) {
                Toast.makeText(context, "File must be less than 5MB", Toast.LENGTH_SHORT).show();
                files = null;
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Temp_Imagess_list.add(new Profile_Portfolio("", files.getAbsolutePath()));
        adapter_image.notifyDataSetChanged();

        method_api_portfolio(files);
    }

    private void method_api_portfolio(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_update_stylist_portfolio)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addMultipartFile("portfolio", file_logo)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }

    private void method_delete_portfolio(String id) {


        AndroidNetworking.get(Global_Service_Api.API_delete_stylist_portfolio + "/" + id)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }

    /*.......*/


    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size() - 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }

    boolean is_show_pwd = false;

    public void hide_unhide_password() {

        if (!is_show_pwd) {

            iv_show_hide_password.setImageResource(R.drawable.ic_eye);
            et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password.setSelection(et_password.getText().length());

            iv_show_hide_password_Confirm.setImageResource(R.drawable.ic_eye);
            et_password_Confirm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password_Confirm.setSelection(et_password_Confirm.getText().length());

            iv_show_hide_password_current.setImageResource(R.drawable.ic_eye);
            et_current_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_current_password.setSelection(et_current_password.getText().length());


            is_show_pwd = true;


        } else {
            iv_show_hide_password.setImageResource(R.drawable.ic_hide);
            et_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password.setSelection(et_password.getText().length());

            iv_show_hide_password_Confirm.setImageResource(R.drawable.ic_hide);
            et_password_Confirm.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password_Confirm.setSelection(et_password_Confirm.getText().length());

            iv_show_hide_password_current.setImageResource(R.drawable.ic_hide);
            et_current_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_current_password.setSelection(et_current_password.getText().length());


            is_show_pwd = false;

        }
    }


    public class MyPasswordTransformationMethod extends HideReturnsTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new MyPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }


    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {

            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_stylist_profile)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("name", "" + et_firstname.getText().toString())
                    .addBodyParameter("lastname", "" + et_lastname.getText().toString())
                    .addBodyParameter("email", "" + et_emailname.getText().toString())
                    .addBodyParameter("username", "" + et_username.getText().toString())
                    .addBodyParameter("password", "" + et_password.getText().toString())
                    .addBodyParameter("old_password", "" + et_current_password.getText().toString())
                    .addBodyParameter("one_line", "" + et_oneline.getText().toString())
                    .addBodyParameter("gender", "" + is_gender)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    /*.....*/

    Integer PICKFILE_RESULT_CODE = 1001;


    private void Request_READ_EXTERNAL_STORAGE() {
        requestPermissions(new String[]
                        {
                                WRITE_EXTERNAL_STORAGE, CAMERA
                        },
                200);
    }


    void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(context)) {
            Request_READ_EXTERNAL_STORAGE();
        } else {


            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }

    File files;
    Uri mCapturedImageURI;

    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {

            Uri imageUri = getPickImageResultUri(context, data);
            CropImage.activity(imageUri)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context, this);

        } else if (requestCode == 1 && resultCode == RESULT_OK) {


            if (files != null) {
                if (files.exists()) {
                    CropImage.activity(Uri.fromFile(files))
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .start(context, this);
                } else {
                    Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                String path = FileUtils.getPath(context, resultUri);
                File file123 = new File(path);

                try {
                    if (file123 != null) {
                        if (file123.exists()) {

                            File SelectedFile_profile;

                            try {
                                SelectedFile_profile = utills.saveBitmapToFile(file123);
                                try {
                                    long fileSizeInBytes = SelectedFile_profile.length();
                                    long fileSizeInKB = fileSizeInBytes / 1024;
                                    long fileSizeInMB = fileSizeInKB / 1024;
                                    if (fileSizeInMB > 5) {
                                        Toast.makeText(context, "File must be less than 5MB", Toast.LENGTH_SHORT).show();
                                        SelectedFile_profile = null;
                                        return;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                SelectedFile_profile = file123;
                                e.printStackTrace();
                            }

                            Glide.with(this)
                                    .load(SelectedFile_profile.getAbsolutePath())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(img_logo);


                            method_api_logo(SelectedFile_profile);


                        } else {
                            Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == 10055 && resultCode == RESULT_OK) {


            Uri content_describer = data.getData();

            String path = "";
            try {
                path = FileUtils.getPath(context, content_describer);
                if (path != null) {
                    File file = new File(path);

                    method_file_set_portfolio(file);
                } else {
                    Toast.makeText(context, "Please select file from your directory", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == 10056 && resultCode == RESULT_OK) {

            try {
                if (files != null) {
                    if (files.exists()) {
                        method_file_set_portfolio(files);
                    } else {
                        Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


    private void method_api_logo(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_update_company_logo)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addMultipartFile("company_logo", file_logo)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    /*..premium...*/


    private void method_dialog_isPremium() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_is_premium);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardView card_premium = dialog.findViewById(R.id.card_premium);
        final TextView tv_nothanks = dialog.findViewById(R.id.tv_nothanks);

        final LinearLayout ll_3_plan = dialog.findViewById(R.id.ll_3_plan);
        final LinearLayout ll_2_plan = dialog.findViewById(R.id.ll_2_plan);
        final LinearLayout ll_1_plan = dialog.findViewById(R.id.ll_1_plan);

        ImageView iv_cancel_subscribe = dialog.findViewById(R.id.iv_cancel_subscribe);
        iv_cancel_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ll_1_month = dialog.findViewById(R.id.ll_1_month);
        ll_1_month_no = dialog.findViewById(R.id.ll_1_month_no);
        ll_1_prize = dialog.findViewById(R.id.ll_1_prize);


        ll_2_month = dialog.findViewById(R.id.ll_2_month);
        ll_2_month_no = dialog.findViewById(R.id.ll_2_month_no);
        ll_2_prize = dialog.findViewById(R.id.ll_2_prize);

        ll_3_month = dialog.findViewById(R.id.ll_3_month);
        ll_3_month_no = dialog.findViewById(R.id.ll_3_month_no);
        ll_3_prize = dialog.findViewById(R.id.ll_3_prize);

        TextView tv_add_method = dialog.findViewById(R.id.tv_add_method);
        final TextView tv_final_prize = dialog.findViewById(R.id.tv_final_prize);


        datalist_StripePlan = new ArrayList<>();
        method_dialog_plan();

        data_card_list = new ArrayList<>();
        method_get_mycard();

        dialog.show();

        ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
        ll_3_plan.setBackground(null);
        ll_1_plan.setBackground(null);

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_mycard();
            }
        });


        card_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                method_premium(dialog);

            }
        });


        tv_nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll_3_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ll_3_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_2_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_3_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 2) {
                            plan_id = datalist_StripePlan.get(2).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        ll_2_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_2_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 1) {
                            plan_id = datalist_StripePlan.get(1).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        ll_1_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_1_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_2_plan.setBackground(null);

                tv_final_prize.setText("" + ll_1_prize.getText().toString());

                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 0) {
                            plan_id = datalist_StripePlan.get(0).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    List<StripePlan_Data> datalist_StripePlan = new ArrayList<>();
    TextView ll_1_month,
            ll_1_month_no,
            ll_1_prize,
            ll_2_month,
            ll_2_month_no,
            ll_2_prize,
            ll_3_month,
            ll_3_month_no,
            ll_3_prize;

    String plan_id;
    String Token_card;

    public void method_dialog_plan() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_stripe_plans)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("home", result);
                            datalist_StripePlan = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    StripePlan_Response Response = new Gson().fromJson(result.toString(), StripePlan_Response.class);

                                    if (Response.getData() != null) {

                                        datalist_StripePlan.addAll(Response.getData());


                                        try {
                                            if (datalist_StripePlan != null) {

                                                if (datalist_StripePlan.size() > 0) {
                                                    ll_1_month.setText("" + datalist_StripePlan.get(0).getInterval());
                                                    ll_1_month_no.setText("" + datalist_StripePlan.get(0).getIntervalCount());
                                                    ll_1_prize.setText("£" + (datalist_StripePlan.get(0).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 1) {
                                                    ll_2_month.setText("" + datalist_StripePlan.get(1).getInterval());
                                                    ll_2_month_no.setText("" + datalist_StripePlan.get(1).getIntervalCount());
                                                    ll_2_prize.setText("£" + (datalist_StripePlan.get(1).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 2) {
                                                    ll_3_month.setText("" + datalist_StripePlan.get(2).getInterval());
                                                    ll_3_month_no.setText("" + datalist_StripePlan.get(2).getIntervalCount());
                                                    ll_3_prize.setText("£" + (datalist_StripePlan.get(2).getAmount() / 100));
                                                }


                                                try {
                                                    if (datalist_StripePlan != null) {
                                                        if (datalist_StripePlan.size() > 1) {
                                                            plan_id = datalist_StripePlan.get(1).getId();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);

        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {


                    CardBrand brand = CardUtils.getPossibleCardBrand(s.toString());


                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }

                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString())) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }

    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);

        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    Card card_stripe;

    private void getUserKey(final Dialog dialog_card) {

        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, "" + preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());

                        Token_card = token.getId();


                        method_save_card(dialog_card);


                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


    }

    private void method_save_card(final Dialog dialog_card) {


        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token", "" + Token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }

                                    method_get_mycard1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_premium(final Dialog dialog) {


        if (card_id.equalsIgnoreCase("")) {
            Toast.makeText(context,
                    "Please add a payment card",
                    Toast.LENGTH_LONG).show();
        } else {
            if (utills.isOnline(context)) {
                utills.stopLoader(progressDialog);
                progressDialog = utills.startLoader(context);

                AndroidNetworking.post(Global_Service_Api.API_subscribe_premium)
                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                        .addBodyParameter("card_id", "" + card_id)
                        .addBodyParameter("plan_id", "" + plan_id)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String result) {
                                utills.stopLoader(progressDialog);
                                if (result == null || result == "") return;
                                Log.e("card", result);
                                try {
                                    JSONObject jsonObject = new JSONObject(result);

                                    String flag = jsonObject.getString("flag");
                                    String message = jsonObject.getString("message");

                                    if (flag.equalsIgnoreCase("true")) {
                                        if (dialog != null) {
                                            dialog.dismiss();
                                        }


                                        utills.is_premium_image = true;

                                        utills.stopLoader(progressDialog);

                                        Intent intent = new Intent(context, Premium_Activity.class);
                                        startActivity(intent);

                                    }

                                    Toast.makeText(context,
                                            message,
                                            Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    utills.stopLoader(progressDialog);
                                    e.printStackTrace();
                                }


                                utills.stopLoader(progressDialog);
                            }

                            @Override
                            public void onError(ANError e) {
                                Toast.makeText(context,
                                        e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                utills.stopLoader(progressDialog);
                                Log.d("API", e.toString());
                            }
                        });


            }

        }


    }


    NonScrollListView listview_order;

    String card_id = "";
    List<MyCard_Data> data_card_list = new ArrayList<>();


    private void method_mycard() {


        final Dialog dialog_listcard = new Dialog(context);
        dialog_listcard.setContentView(R.layout.dialog_my_card);
        dialog_listcard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_listcard.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        listview_order = dialog_listcard.findViewById(R.id.listview_order);
        TextView tv_add_method = dialog_listcard.findViewById(R.id.tv_add_method);


        final TextView tv_next = dialog_listcard.findViewById(R.id.tv_next);


        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (card_id.equalsIgnoreCase("")) {
                    Toast.makeText(context,
                            "Please Select payment card",
                            Toast.LENGTH_LONG).show();
                } else {

                    if (dialog_listcard != null) {
                        dialog_listcard.dismiss();
                    }

                }
            }
        });


        dialog_listcard.show();

        if (data_card_list != null) {
            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
            listview_order.setAdapter(adapter_review);
        }

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_change_card_dialog();

                card_id = "";

                if (data_card_list != null) {
                    Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                    listview_order.setAdapter(adapter_review);
                }


            }
        });

    }


    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }


    private void method_get_mycard1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_card_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_card_list = Response.getData();


                                        if (listview_order != null) {
                                            Adapter_card adapter_review = new Adapter_card(context, data_card_list);
                                            listview_order.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    public class Adapter_card extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;
        int pos_selected = -1;

        public Adapter_card(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_card.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_card.ViewHolder();
                view = inflater.inflate(R.layout.datalist_mystripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_card.ViewHolder) view.getTag();
            }


            if (pos_selected == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);

            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            String brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }

            viewHolder.et_change_cardnumber.setText("" + brand + " / " + listState.get(position).getLast4());


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    pos_selected = position;


                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
                    card_id = "" + listState.get(position).getId();

                    notifyDataSetChanged();


                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon, iv_checkbox;
            TextView et_change_cardnumber;
            CardView card_mylist;

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectdocument();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // NestedScrollView scrollview;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.VISIBLE);
//            }
//        } else {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.GONE);
//            }
//        }
    }


}