package com.moremy.style.StylistSeller.Fragment.Seller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Model_MyIncome.Model_MyIncome_BookingServiceItem;


import com.moremy.style.Model_MyIncome.Model_MyIncome_Transaction;
import com.moremy.style.Model_MyIncome.MyExpenditure_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.stripe.android.model.CardBrand;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Fragment_MyExpenditure extends Fragment {

    Context context;

    public Fragment_MyExpenditure() {
    }

    Preferences preferences;

    List<Model_MyIncome_Transaction> data_order_list = new ArrayList<>();
    Adapter_order adapter_review;
    NonScrollListView listview_order;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_expenditure, container, false);
        context = getActivity();

        preferences = new Preferences(context);


        data_order_list = new ArrayList<>();


        listview_order = view.findViewById(R.id.listview_order);
        data_order_list = new ArrayList<>();
        method_api_customer();

        return view;
    }


    Dialog progressDialog = null;

    public class Adapter_order extends BaseAdapter {

        Context context;
        List<Model_MyIncome_Transaction> listState;
        LayoutInflater inflater;


        public Adapter_order(Context applicationContext, List<Model_MyIncome_Transaction> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_myincome, viewGroup, false);
                viewHolder.tv_product_name_o = view.findViewById(R.id.tv_product_name_o);
                viewHolder.tv_product_date_o = view.findViewById(R.id.tv_product_date_o);
                viewHolder.tv_product_prize_o = view.findViewById(R.id.tv_product_prize_o);
                viewHolder.iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                viewHolder.tv_product_ref_o = view.findViewById(R.id.tv_product_ref_o);
                viewHolder.ll_main_cell = view.findViewById(R.id.ll_main_cell);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }



            String brand = listState.get(position).getCardBrand();



            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else  if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_discover_template_32);
            }else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else  if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else  if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            }else  if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            }else  {
                viewHolder.iv_product_image_o.setImageResource(R.drawable.stripe_ic_unknown);
            }




            viewHolder.tv_product_prize_o.setText("xxxx - xxxx - xxxx - " + listState.get(position).getLast4());
            viewHolder.tv_product_ref_o.setText("ref " + listState.get(position).getStripeInvoiceNumber());


            if (listState.get(position).getPaymentType() == 1) {

                if(listState.get(position).getOrder() != null){
                    viewHolder.tv_product_name_o.setText("" + listState.get(position).getOrder().getProduct().getName()
                            + " (£" + listState.get(position).getOrder().getProductPrice() + ")");

                    String date = utills.getTimeAccordingOrder(listState.get(position).getOrder().getOrderDate());
                    viewHolder.tv_product_date_o.setText("paid on " + date);
                }

            } else if (listState.get(position).getPaymentType() == 2) {



                final  List<Model_MyIncome_BookingServiceItem> data__list = new ArrayList<>();
                if(listState.get(position).getBooking() != null){
                    data__list.addAll(listState.get(position).getBooking().getBookingServiceItem());


                    StringBuilder stringBuilder_ids = new StringBuilder();
                    for (int i = 0; i < data__list.size(); i++) {
                        if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                            stringBuilder_ids.append(", ");
                        }
                        stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                    }


                    viewHolder.tv_product_name_o.setText("" + stringBuilder_ids);

                    String date = utills.getTimeAccordingBooking(listState.get(position).getBooking().getBookingDate());
                    viewHolder.tv_product_date_o.setText("paid " + listState.get(position).getBooking().getBookingTime() + " on " + date);


                }

                   }


            viewHolder.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse( "http://docs.google.com/gview?embedded=true&url=" + "" + listState.get(position).getStripeInvoicePdf()));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            return view;
        }

        private class ViewHolder {
            TextView tv_product_name_o;
            TextView tv_product_ref_o;
            TextView tv_product_date_o;
            TextView tv_product_prize_o;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
        }

    }

    private void method_api_customer() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_expenditure)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("MyExpenditure", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");

                                if (flag.equalsIgnoreCase("true")) {
                                    MyExpenditure_Response Response = new Gson().fromJson(result.toString(), MyExpenditure_Response.class);
                                    if (Response.getData() != null) {
                                        data_order_list =Response.getData();

                                        adapter_review = new Adapter_order(context, data_order_list);
                                        listview_order.setAdapter(adapter_review);

                                    }

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

}