package com.moremy.style.StylistSeller.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.StylistSeller.NewProduct.StyleSeller_AddNewProduct_Services_Activty;
import com.moremy.style.StylistSeller.NewProduct.StyleSeller_ModifyProduct_Services_Activty;
import com.moremy.style.StylistSeller.model.Profile.Profile_Data;

import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.Profile.SellerProduct;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import java.util.ArrayList;
import java.util.List;


import static com.moremy.style.StylistSeller.Fragment.Seller.Fragment_SellerOnly.profile_data_SellerOnly_main;


public class Fragment_Shop extends Fragment {
    Context context;


    public Fragment_Shop() {
    }

    List<SellerProduct> data_Shop_list = new ArrayList<>();
    Adapter_Shop adapter_Shop;

    GridView listview_Shop;

    Preferences preferences;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shoplist, container, false);
        context = getActivity();
        preferences = new Preferences(context);

        data_Shop_list = new ArrayList<>();


        listview_Shop = view.findViewById(R.id.listview_countiy);


        return view;
    }


    public class Adapter_Shop extends BaseAdapter {

        Context context;
        List<SellerProduct> listState;
        LayoutInflater inflater;


        public Adapter_Shop(Context applicationContext, List<SellerProduct> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_Shop.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_Shop.ViewHolder();
                view = inflater.inflate(R.layout.datalist_shoplist, viewGroup, false);

                viewHolder.iv_product_image = view.findViewById(R.id.iv_product_image);
                viewHolder.tv_product_prize = view.findViewById(R.id.tv_product_prize);
                viewHolder.tv_product_name = view.findViewById(R.id.tv_product_name);

                viewHolder.ll_main_poduct = view.findViewById(R.id.ll_main_poduct);
                viewHolder.ll_add_poduct = view.findViewById(R.id.ll_add_poduct);
                viewHolder.card_pencil = view.findViewById(R.id.card_pencil);

                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_Shop.ViewHolder) view.getTag();
            }


            if (!listState.get(position).is_add_product) {

                viewHolder.ll_main_poduct.setVisibility(View.VISIBLE);
                viewHolder.ll_add_poduct.setVisibility(View.GONE);

                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + listState.get(position).getPicture())
                        .into(viewHolder.iv_product_image);


                viewHolder.tv_product_name.setText("" + listState.get(position).getName());
                viewHolder.tv_product_prize.setText("£" + utills.roundTwoDecimals(listState.get(position).getPrice()));

            } else {

                viewHolder.ll_main_poduct.setVisibility(View.GONE);
                viewHolder.ll_add_poduct.setVisibility(View.VISIBLE);

            }


            viewHolder.ll_add_poduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    preferences.setPRE_ProductImage("");
                    preferences.setPRE_ProductSKU("");
                    preferences.setPRE_ProductPrize("");
                    preferences.setPRE_ProductDescription("");
                    preferences.setPRE_ProductName("");

                    preferences.setPRE_ProductSubCategory("");
                    preferences.setPRE_ProductMainCategory("");
                    preferences.setPRE_ProductLifeStyle("");

                    Intent i = new Intent(context, StyleSeller_AddNewProduct_Services_Activty.class);
                    startActivity(i);


                }
            });

            viewHolder.card_pencil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    preferences.setPRE_ProductImage("" + Global_Service_Api.IMAGE_URL + listState.get(position).getPicture());
                    preferences.setPRE_ProductSKU("" + listState.get(position).getSku());
                    preferences.setPRE_ProductPrize("" + listState.get(position).getPrice());
                    preferences.setPRE_ProductDescription("" + listState.get(position).getDescription());
                    preferences.setPRE_ProductName("" + listState.get(position).getName());
                    preferences.setPRE_ProductPerItem("" + listState.get(position).getshipping_type());


                    Intent i = new Intent(context, StyleSeller_ModifyProduct_Services_Activty.class);
                    i.putExtra("id", "" + listState.get(position).getId());
                    startActivity(i);


                }
            });


            return view;
        }

        private class ViewHolder {

            LinearLayout ll_main_poduct, ll_add_poduct;
            TextView tv_product_prize, tv_product_name;
            ImageView iv_product_image;
            CardView card_pencil;
        }


    }


    private boolean isVisible;
    private boolean isStarted;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            setData();
        } else {
            if (listview_Shop != null) {
                listview_Shop.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            setData();
        } else {
            if (listview_Shop != null) {
                listview_Shop.setVisibility(View.GONE);
            }
        }

    }

    private void setData() {
        if (listview_Shop != null) {
            listview_Shop.setVisibility(View.VISIBLE);
            data_Shop_list = new ArrayList<>();


            if (profile_data_SellerOnly_main != null) {
                Profile_Data profile_data1 = new Profile_Data();
                profile_data1 = profile_data_SellerOnly_main;
                data_Shop_list.addAll(profile_data1.getSellerProducts());


                SellerProduct sellerProduct = new SellerProduct();
                sellerProduct.is_add_product = true;
                data_Shop_list.add(sellerProduct);
            }

            Adapter_Shop adapter_prize = new Adapter_Shop(context, data_Shop_list);
            listview_Shop.setAdapter(adapter_prize);
        }

    }


}