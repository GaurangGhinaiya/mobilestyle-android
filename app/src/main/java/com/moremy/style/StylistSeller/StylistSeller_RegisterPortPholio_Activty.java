package com.moremy.style.StylistSeller;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.BuildConfig;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.CommanActivity.Premium_Activity;
import com.moremy.style.Model.StripePlan_Data;
import com.moremy.style.Model.StripePlan_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.FileUtils;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class StylistSeller_RegisterPortPholio_Activty extends Base1_Activity implements View.OnClickListener {


    Context context;
    public static List<String> Image_List = new ArrayList<>();


    ImageView iv_buss_iconn1, iv_buss_iconn2, iv_buss_iconn3,
            iv_buss_iconn4, iv_buss_iconn5, iv_buss_iconn6,
            iv_buss_iconn7, iv_buss_iconn8, iv_buss_iconn9;


    LinearLayout ll_premium_5, ll_premium_6, ll_premium_7, ll_premium_8, ll_premium_9;

    CardView card5, card6, card7, card8, card9;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_portpholio);
        context = StylistSeller_RegisterPortPholio_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        preferences = new Preferences(context);

        iv_buss_iconn1 = findViewById(R.id.iv_buss_iconn1);
        iv_buss_iconn1.setOnClickListener(this);
        iv_buss_iconn2 = findViewById(R.id.iv_buss_iconn2);
        iv_buss_iconn2.setOnClickListener(this);
        iv_buss_iconn3 = findViewById(R.id.iv_buss_iconn3);
        iv_buss_iconn3.setOnClickListener(this);
        iv_buss_iconn4 = findViewById(R.id.iv_buss_iconn4);
        iv_buss_iconn4.setOnClickListener(this);
        iv_buss_iconn5 = findViewById(R.id.iv_buss_iconn5);
        iv_buss_iconn5.setOnClickListener(this);
        iv_buss_iconn6 = findViewById(R.id.iv_buss_iconn6);
        iv_buss_iconn6.setOnClickListener(this);
        iv_buss_iconn7 = findViewById(R.id.iv_buss_iconn7);
        iv_buss_iconn7.setOnClickListener(this);
        iv_buss_iconn8 = findViewById(R.id.iv_buss_iconn8);
        iv_buss_iconn8.setOnClickListener(this);
        iv_buss_iconn9 = findViewById(R.id.iv_buss_iconn9);
        iv_buss_iconn9.setOnClickListener(this);

        Image_List = new ArrayList<>();
        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (SelectedFile_1 != null) {
                    Image_List.add("" + SelectedFile_1.getAbsolutePath());
                }

                if (SelectedFile_2 != null) {
                    Image_List.add("" + SelectedFile_2.getAbsolutePath());
                }
                if (SelectedFile_3 != null) {
                    Image_List.add("" + SelectedFile_3.getAbsolutePath());
                }
                if (SelectedFile_4 != null) {
                    Image_List.add("" + SelectedFile_4.getAbsolutePath());
                }
                if (SelectedFile_5 != null) {
                    Image_List.add("" + SelectedFile_5.getAbsolutePath());
                }
                if (SelectedFile_6 != null) {
                    Image_List.add("" + SelectedFile_6.getAbsolutePath());
                }
                if (SelectedFile_7 != null) {
                    Image_List.add("" + SelectedFile_7.getAbsolutePath());
                }
                if (SelectedFile_8 != null) {
                    Image_List.add("" + SelectedFile_8.getAbsolutePath());
                }
                if (SelectedFile_9 != null) {
                    Image_List.add("" + SelectedFile_9.getAbsolutePath());
                }


                if (Image_List.size() > 0) {
                    Intent i = new Intent(context, StylistSeller_RegisterBio_Activty.class);
                    startActivity(i);
                } else {
                    Toast toast = Toast.makeText(context, "please add your portfolio", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                }
            }
        });

        ll_premium_5 = findViewById(R.id.ll_premium_5);
        ll_premium_6 = findViewById(R.id.ll_premium_6);
        ll_premium_7 = findViewById(R.id.ll_premium_7);
        ll_premium_8 = findViewById(R.id.ll_premium_8);
        ll_premium_9 = findViewById(R.id.ll_premium_9);

        card5 = findViewById(R.id.card5);
        card6 = findViewById(R.id.card6);
        card7 = findViewById(R.id.card7);
        card8 = findViewById(R.id.card8);
        card9 = findViewById(R.id.card9);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_buss_iconn1:
                is_image_where = 1;
                selectdocument();
                break;
            case R.id.iv_buss_iconn2:
                is_image_where = 2;
                selectdocument();
                break;
            case R.id.iv_buss_iconn3:
                is_image_where = 3;
                selectdocument();
                break;
            case R.id.iv_buss_iconn4:
                is_image_where = 4;
                selectdocument();
                break;
            case R.id.iv_buss_iconn5:
                is_image_where = 5;

                if (preferences.getPRE_Is_Premium()) {
                    selectdocument();
                } else {
                    method_dialog_isPremium();
                }

                break;
            case R.id.iv_buss_iconn6:
                is_image_where = 6;
                if (preferences.getPRE_Is_Premium()) {
                    selectdocument();
                } else {
                    method_dialog_isPremium();
                }
                break;
            case R.id.iv_buss_iconn7:
                is_image_where = 7;
                if (preferences.getPRE_Is_Premium()) {
                    selectdocument();
                } else {
                    method_dialog_isPremium();
                }
                break;
            case R.id.iv_buss_iconn8:
                is_image_where = 8;
                if (preferences.getPRE_Is_Premium()) {
                    selectdocument();
                } else {
                    method_dialog_isPremium();
                }
                break;
            case R.id.iv_buss_iconn9:
                is_image_where = 9;
                if (preferences.getPRE_Is_Premium()) {
                    selectdocument();
                } else {
                    method_dialog_isPremium();
                }
                break;

        }
    }


    int is_image_where = 0;
    public static final Integer PICKFILE_RESULT_CODE = 1001;
    public static File files;
    public static Uri mCapturedImageURI;

    /*..........................................*/

    public void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(this)) {
            utills.Request_READ_EXTERNAL_STORAGE(this);
        } else {


            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            tv_dialog_name.setText("For your Portfolio");
            tv_dialog_name.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.GONE);


            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }


    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            Uri content_describer = data.getData();

            String path = "";
            try {
                path = FileUtils.getPath(context, content_describer);
                if (path != null) {
                    File file = new File(path);
                    File file123 = utills.saveBitmapToFile(file);
                    try {
                        long fileSizeInBytes = file123.length();
                        long fileSizeInKB = fileSizeInBytes / 1024;
                        long fileSizeInMB = fileSizeInKB / 1024;
                        if (fileSizeInMB > 5) {
                            Toast.makeText(this,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                            file123=null;
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    method_file_set_all_images(file123);
                } else {
                    Toast.makeText(context, "Please select file from your directory", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            try {
                if (files != null) {
                    if (files.exists()) {
                        File file123 = utills.saveBitmapToFile(files);
                        try {
                            long fileSizeInBytes = file123.length();
                            long fileSizeInKB = fileSizeInBytes / 1024;
                            long fileSizeInMB = fileSizeInKB / 1024;
                            if (fileSizeInMB > 5) {
                                Toast.makeText(this,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                                file123=null;
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        method_file_set_all_images(file123);
                    } else {
                        Toast.makeText(context, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "File is Null", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void method_file_set_all_images(File file) {

        if (is_image_where == 1) {

            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn1);
            SelectedFile_1 = file;

        } else if (is_image_where == 2) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn2);
            SelectedFile_2 = file;

        } else if (is_image_where == 3) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn3);
            SelectedFile_3 = file;
        } else if (is_image_where == 4) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn4);
            SelectedFile_4 = file;
        } else if (is_image_where == 5) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn5);
            SelectedFile_5 = file;
        } else if (is_image_where == 6) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn6);
            SelectedFile_6 = file;
        } else if (is_image_where == 7) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn7);
            SelectedFile_7 = file;
        } else if (is_image_where == 8) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn8);
            SelectedFile_8 = file;
        } else if (is_image_where == 9) {
            Glide.with(context)
                    .load(file.getAbsolutePath())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_buss_iconn9);
            SelectedFile_9 = file;
        }

    }

    File SelectedFile_1;
    File SelectedFile_2;
    File SelectedFile_3;
    File SelectedFile_4;
    File SelectedFile_5;
    File SelectedFile_6;
    File SelectedFile_7;
    File SelectedFile_8;
    File SelectedFile_9;


    /*.........premium....*/
    Preferences preferences;

    private void method_dialog_isPremium() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_is_premium);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardView card_premium = dialog.findViewById(R.id.card_premium);
        final TextView tv_nothanks = dialog.findViewById(R.id.tv_nothanks);

        final LinearLayout ll_3_plan = dialog.findViewById(R.id.ll_3_plan);
        final LinearLayout ll_2_plan = dialog.findViewById(R.id.ll_2_plan);
        final LinearLayout ll_1_plan = dialog.findViewById(R.id.ll_1_plan);
        ImageView iv_cancel_subscribe = dialog.findViewById(R.id.iv_cancel_subscribe);
        iv_cancel_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll_1_month = dialog.findViewById(R.id.ll_1_month);
        ll_1_month_no = dialog.findViewById(R.id.ll_1_month_no);
        ll_1_prize = dialog.findViewById(R.id.ll_1_prize);


        ll_2_month = dialog.findViewById(R.id.ll_2_month);
        ll_2_month_no = dialog.findViewById(R.id.ll_2_month_no);
        ll_2_prize = dialog.findViewById(R.id.ll_2_prize);

        ll_3_month = dialog.findViewById(R.id.ll_3_month);
        ll_3_month_no = dialog.findViewById(R.id.ll_3_month_no);
        ll_3_prize = dialog.findViewById(R.id.ll_3_prize);

        TextView tv_add_method = dialog.findViewById(R.id.tv_add_method);
        final TextView tv_final_prize = dialog.findViewById(R.id.tv_final_prize);


        datalist_StripePlan = new ArrayList<>();
        method_dialog_plan();


        dialog.show();

        ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
        ll_3_plan.setBackground(null);
        ll_1_plan.setBackground(null);


        card_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setPRE_Is_Premium(true);


                ll_premium_5.setVisibility(View.GONE);
                ll_premium_6.setVisibility(View.GONE);
                ll_premium_7.setVisibility(View.GONE);
                ll_premium_8.setVisibility(View.GONE);
                ll_premium_9.setVisibility(View.GONE);


                card5.setCardBackgroundColor(getResources().getColor(R.color.white));
                card6.setCardBackgroundColor(getResources().getColor(R.color.white));
                card7.setCardBackgroundColor(getResources().getColor(R.color.white));
                card8.setCardBackgroundColor(getResources().getColor(R.color.white));
                card9.setCardBackgroundColor(getResources().getColor(R.color.white));

                Intent intent = new Intent(context, Premium_Activity.class);
                startActivity(intent);

                dialog.dismiss();
            }
        });


        tv_nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                preferences.setPRE_Is_Premium(false);

                dialog.dismiss();
            }
        });

        ll_3_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ll_3_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_2_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_3_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 2) {
                            plan_id = datalist_StripePlan.get(2).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        ll_2_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_2_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 1) {
                            plan_id = datalist_StripePlan.get(1).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        ll_1_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_1_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_2_plan.setBackground(null);

                tv_final_prize.setText("" + ll_1_prize.getText().toString());

                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 0) {
                            plan_id = datalist_StripePlan.get(0).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    List<StripePlan_Data> datalist_StripePlan = new ArrayList<>();
    TextView ll_1_month,
            ll_1_month_no,
            ll_1_prize,
            ll_2_month,
            ll_2_month_no,
            ll_2_prize,
            ll_3_month,
            ll_3_month_no,
            ll_3_prize;

    String plan_id;

    public void method_dialog_plan() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_stripe_plans)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("home", result);
                            datalist_StripePlan = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    StripePlan_Response Response = new Gson().fromJson(result.toString(), StripePlan_Response.class);

                                    if (Response.getData() != null) {

                                        datalist_StripePlan.addAll(Response.getData());


                                        try {
                                            if (datalist_StripePlan != null) {

                                                if (datalist_StripePlan.size() > 0) {
                                                    ll_1_month.setText("" + datalist_StripePlan.get(0).getInterval());
                                                    ll_1_month_no.setText("" + datalist_StripePlan.get(0).getIntervalCount());
                                                    ll_1_prize.setText("£" + (datalist_StripePlan.get(0).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 1) {
                                                    ll_2_month.setText("" + datalist_StripePlan.get(1).getInterval());
                                                    ll_2_month_no.setText("" + datalist_StripePlan.get(1).getIntervalCount());
                                                    ll_2_prize.setText("£" + (datalist_StripePlan.get(1).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 2) {
                                                    ll_3_month.setText("" + datalist_StripePlan.get(2).getInterval());
                                                    ll_3_month_no.setText("" + datalist_StripePlan.get(2).getIntervalCount());
                                                    ll_3_prize.setText("£" + (datalist_StripePlan.get(2).getAmount() / 100));
                                                }


                                                try {
                                                    if (datalist_StripePlan != null) {
                                                        if (datalist_StripePlan.size() > 1) {
                                                            plan_id = datalist_StripePlan.get(1).getId();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    Dialog progressDialog = null;


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectdocument();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
