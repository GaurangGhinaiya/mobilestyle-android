package com.moremy.style.StylistSeller.Fragment.Seller;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.ChatOrderActivity;
import com.moremy.style.Customer.Fragment.EditAccountFragment.Model_Order.ModelOrder_Data;
import com.moremy.style.Customer.Fragment.EditAccountFragment.Model_Order.ModelOrder_Response;
import com.moremy.style.R;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Fragment_Seller_MYOrder extends Fragment {

    Context context;

    public Fragment_Seller_MYOrder() {
    }

    Preferences preferences;

    List<ModelOrder_Data> data_order_list = new ArrayList<>();
    Adapter_order adapter_review;
    ListView listview_order;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seller_m_c_order, container, false);
        context = getActivity();

        preferences = new Preferences(context);


        data_order_list = new ArrayList<>();


        listview_order = view.findViewById(R.id.listview_order);

        data_order_list = new ArrayList<>();
        method_api();

        return view;
    }


    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_get_orders)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_orders", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");

                                if (flag.equalsIgnoreCase("true")) {

                                    ModelOrder_Response Response = new Gson().fromJson(result.toString(), ModelOrder_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();


                                        adapter_review = new Adapter_order(context, data_order_list);
                                        listview_order.setAdapter(adapter_review);

                                    }


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    public class Adapter_order extends BaseAdapter {

        Context context;
        List<ModelOrder_Data> listState;
        LayoutInflater inflater;


        public Adapter_order(Context applicationContext, List<ModelOrder_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_orderlist, viewGroup, false);
                viewHolder.tv_product_name_o = view.findViewById(R.id.tv_product_name_o);
                viewHolder.tv_product_date_o = view.findViewById(R.id.tv_product_date_o);
                viewHolder.tv_product_prize_o = view.findViewById(R.id.tv_product_prize_o);
                viewHolder.iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                viewHolder.ll_main_cell = view.findViewById(R.id.ll_main_cell);
                viewHolder.progress_bar = view.findViewById(R.id.progress_bar);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }

            viewHolder.tv_product_name_o.setText("" + listState.get(position).getProduct().getName());
            viewHolder.tv_product_prize_o.setText("£" + utills.roundTwoDecimals(listState.get(position).getProductPrice()));
            viewHolder.tv_product_date_o.setText("dispatched on " + utills.getTimeAccordingCuntry1(listState.get(position).getOrderDate()));



            if (listState.get(position).getOrderDate() != null) {
                if (!listState.get(position).getOrderDate().equalsIgnoreCase("") && !listState.get(position).getOrderDate().equalsIgnoreCase("null")) {
                    viewHolder.progress_bar.setProgress(33);
                    viewHolder.tv_product_date_o.setText("ordered on " + utills.getTimeAccordingCuntry1(listState.get(position).getOrderDate()));
                }
            }


            if (listState.get(position).getShippingDate() != null) {
                if (!listState.get(position).getShippingDate().equalsIgnoreCase("") && !listState.get(position).getShippingDate().equalsIgnoreCase("null")) {
                    viewHolder.progress_bar.setProgress(60);
                    viewHolder.tv_product_date_o.setText("dispatched on " + utills.getTimeAccordingCuntry1(listState.get(position).getShippingDate()));
                }
            }

            if (listState.get(position).getDeliveryDate() != null) {
                if (!listState.get(position).getDeliveryDate().equalsIgnoreCase("") && !listState.get(position).getDeliveryDate().equalsIgnoreCase("null")) {
                    viewHolder.progress_bar.setProgress(100);
                    viewHolder.tv_product_date_o.setText("completed on " + utills.getTimeAccordingCuntry1(listState.get(position).getDeliveryDate()));
                }
            }

            if (listState.get(position).getCancel_date() != null) {
                if (!listState.get(position).getCancel_date().equalsIgnoreCase("") && !listState.get(position).getCancel_date().equalsIgnoreCase("null")) {
                    viewHolder.progress_bar.setProgress(0);
                    viewHolder.tv_product_date_o.setText("cancelled on " + utills.getTimeAccordingCuntry1(listState.get(position).getCancel_date()));
                }
            }



            if (listState.get(position).getProduct().getPicture() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + listState.get(position).getProduct().getPicture())
                        .into(viewHolder.iv_product_image_o);

            }


            viewHolder.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ChatOrderActivity.class);
                    i.putExtra("channelUrl", "" + listState.get(position).getSendgridOrderId());
                    i.putExtra("ProductId", "" + listState.get(position).getId());
                    i.putExtra("ProductName", "" + listState.get(position).getProduct().getName());
                    i.putExtra("ProductImage", "" + listState.get(position).getProduct().getPicture());
                   i.putExtra("ProductPrize", "" + listState.get(position).getProductPrice());
                    i.putExtra("SellerImage", "" + listState.get(position).getSeller().getProfilePic());
                    i.putExtra("SellerId", "" + listState.get(position).getSeller().getId());
                    i.putExtra("SellerName", "" + listState.get(position).getSeller().getName() + " " + listState.get(position).getSeller().getLastname());

                    i.putExtra("ProductDeliveryCharge", "" + listState.get(position).getShippingPrice());
                    i.putExtra("order_id", "" + listState.get(position).getId());

                    if (listState.get(position).getPayment() != null) {
                        i.putExtra("InvoiceId", "" + listState.get(position).getPayment().getStripeInvoiceNumber());
                    }else {
                        i.putExtra("InvoiceId", "");
                    }
                    i.putExtra("is_paid", "" + listState.get(position).getis_paid());
                    i.putExtra("getShippingDate", "" + listState.get(position).getShippingDate());
                    i.putExtra("getDeliveryDate", "" + listState.get(position).getDeliveryDate());
                    i.putExtra("getOrderDate", "" + listState.get(position).getOrderDate());
                    i.putExtra("status", "" +  listState.get(position).getStatus());

                    startActivity(i);
                }
            });


            return view;
        }

        private class ViewHolder {
            TextView tv_product_name_o;
            TextView tv_product_date_o;
            TextView tv_product_prize_o;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
            ProgressBar progress_bar;
        }

    }


}