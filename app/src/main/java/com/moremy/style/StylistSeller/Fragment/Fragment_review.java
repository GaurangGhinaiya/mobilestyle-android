package com.moremy.style.StylistSeller.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Model.Review.ModelReview_Data;
import com.moremy.style.Model.Review.ModelReview_Response;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Fragment_review extends Fragment {
    Context context;


    public Fragment_review() {
    }

    List<ModelReview_Data> data_review_list = new ArrayList<>();
    Adapter_review adapter_review;

    GridView listview_review;
    TextView tv_no_data;
    Preferences preferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricelist, container, false);
        context = getActivity();
        preferences = new Preferences(context);

        data_review_list = new ArrayList<>();
        listview_review = view.findViewById(R.id.listview_countiy);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        tv_no_data.setText("there is no data");
        method_api();


        return view;
    }

    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_my_rating)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_my_rating", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_no_data.setVisibility(View.GONE);
                                    listview_review.setVisibility(View.VISIBLE);
                                    ModelReview_Response Response = new Gson().fromJson(result.toString(), ModelReview_Response.class);

                                    if (Response.getData() != null) {

                                        data_review_list = new ArrayList<>();
                                        data_review_list = Response.getData();


                                        Adapter_review adapter_review = new Adapter_review(context, data_review_list);
                                        listview_review.setAdapter(adapter_review);

                                    }


                                } else {
                                    tv_no_data.setVisibility(View.VISIBLE);
                                    listview_review.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    public class Adapter_review extends BaseAdapter {

        Context context;
        List<ModelReview_Data> listState;
        LayoutInflater inflater;


        public Adapter_review(Context applicationContext, List<ModelReview_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_review.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_review.ViewHolder();
                view = inflater.inflate(R.layout.datalist_reviewlist, viewGroup, false);
                viewHolder.tv_name = view.findViewById(R.id.tv_name);
                viewHolder.tv_date = view.findViewById(R.id.tv_date);
                viewHolder.tv_description = view.findViewById(R.id.tv_description);
                viewHolder.simpleRatingBar_ = view.findViewById(R.id.simpleRatingBar_quality);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_review.ViewHolder) view.getTag();
            }


            viewHolder.tv_date.setText("" + listState.get(position).getCreatedAt());
            viewHolder.tv_name.setText("" + listState.get(position).getCustomer().getName() + " " + listState.get(position).getCustomer().getLastname());
            viewHolder.tv_description.setText("" + listState.get(position).getComments());


            try {
                String rating = "" + listState.get(position).getRating();
                viewHolder.simpleRatingBar_.setRating(Float.parseFloat(rating));
            } catch (Exception e) {
                e.printStackTrace();
            }


            return view;
        }

        private class ViewHolder {
            TextView tv_name, tv_date, tv_description;
            ScaleRatingBar simpleRatingBar_;

        }

    }


}