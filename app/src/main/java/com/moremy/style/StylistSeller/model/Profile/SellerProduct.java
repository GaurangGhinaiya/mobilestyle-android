package com.moremy.style.StylistSeller.model.Profile;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Model.Services_Data;

public class SellerProduct {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("stylist_user")
    @Expose
    private Integer stylistUser;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_category_id")
    @Expose
    private String subCategoryId;
    @SerializedName("lifestyle_id")
    @Expose
    private Object lifestyleId;
    @SerializedName("quantity")
    @Expose
    private Object quantity;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;
    @SerializedName("shipping_lead_time")
    @Expose
    private Object shippingLeadTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("images")
    @Expose
    private List<Object> images = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStylistUser() {
        return stylistUser;
    }

    public void setStylistUser(Integer stylistUser) {
        this.stylistUser = stylistUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Object getLifestyleId() {
        return lifestyleId;
    }

    public void setLifestyleId(Object lifestyleId) {
        this.lifestyleId = lifestyleId;
    }

    public Object getQuantity() {
        return quantity;
    }

    public void setQuantity(Object quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public Object getShippingLeadTime() {
        return shippingLeadTime;
    }

    public void setShippingLeadTime(Object shippingLeadTime) {
        this.shippingLeadTime = shippingLeadTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }


//    @SerializedName("category")
//    @Expose
//    private List<Services_Data> category = null;
//
//
//    public List<Services_Data> getCategory() {
//        return category;
//    }
//
//    public void setCategory(List<Services_Data> category) {
//        this.category = category;
//    }
//
//
//
//
//    @SerializedName("subcategory")
//    @Expose
//    private List<Services_Data> subcategory = null;
//
//
//    public List<Services_Data> getSubcategory() {
//        return subcategory;
//    }
//
//    public void setSubcategory(List<Services_Data> subcategory) {
//        this.subcategory = subcategory;
//    }


    @SerializedName("shipping_type")
    @Expose
    private String shipping_type;


    public String getshipping_type() {
        return shipping_type;
    }

    public void setshipping_type(String shipping_type) {
        this.shipping_type = shipping_type;
    }


    public boolean is_add_product = false;
}