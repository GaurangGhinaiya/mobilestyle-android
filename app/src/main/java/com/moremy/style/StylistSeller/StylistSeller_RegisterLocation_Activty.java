package com.moremy.style.StylistSeller;


import android.Manifest;
import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Utills.AutoCompleteTextviewCustom;
import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.Preferences;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import com.mapbox.turf.TurfConstants;
import com.mapbox.turf.TurfMeta;
import com.mapbox.turf.TurfTransformation;
import com.moremy.style.Utills.utills;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;

public class StylistSeller_RegisterLocation_Activty extends Base1_Activity implements View.OnClickListener,
        OnMapReadyCallback, PermissionsListener {
    RelativeLayout rl_next;

    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;

    private static final String TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID
            = "TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID";
    private static final String TURF_CALCULATION_FILL_LAYER_ID = "TURF_CALCULATION_FILL_LAYER_ID";
    private static final String CIRCLE_CENTER_LAYER_ID = "CIRCLE_CENTER_LAYER_ID";

    private String circleUnit = TurfConstants.UNIT_MILES;
    private int circleSteps = 360;
    private double circleRadius = 0.5;


    String token = "pk.eyJ1IjoiZm9yZXNpZ2h0dGVjaG5vbG9naWVzIiwiYSI6ImNqa3phMXBiNDBydjczcXFwNWQzbWlqNnkifQ.fQQWorOl5PVt4s4M0pwYaA";

    LinearLayout ll_mieles;
    Preferences preferences;

    Context context;

    ArrayList<String> search_list = new ArrayList<>();
    ArrayList<String> search_list_postcode = new ArrayList<>();

    AutoCompleteTextviewCustom et_postol_code;
    EditText et_address_line;
    NestedScrollView scrollview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = StylistSeller_RegisterLocation_Activty.this;
        Mapbox.getInstance(this, token);
        setContentView(R.layout.activity_register_location);
        preferences = new Preferences(this);
        userIsInteracting = true;
        search_list = new ArrayList<>();
        search_list_postcode = new ArrayList<>();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        findview();

        ll_mieles = findViewById(R.id.ll_mieles);
        et_postol_code = findViewById(R.id.et_postol_code);
        et_address_line = findViewById(R.id.et_address_line);


        et_postol_code.setDropDownWidth(getResources().getDisplayMetrics().widthPixels - 50);
        ArrayAdapter<String> adapter_code = new ArrayAdapter<String>
                (context, R.layout.autotextview_layout1, search_list_postcode);
        et_postol_code.setAdapter(adapter_code);
        et_postol_code.setThreshold(1);

        //et_postol_code.setText("" + preferences.getPRE_Code());


        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        scrollview = findViewById(R.id.scrollview);

        mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {  // & MotionEvent.ACTION_MASK
                    case MotionEvent.ACTION_DOWN:
                        scrollview.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        scrollview.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                mapView.onTouchEvent(event);
                return true;
            }

        });


        et_postol_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (et_postol_code.getText().toString().length() >= 1) {
                    makeGeocodeSearch_postalcode();
                }

            }
        });


        et_postol_code.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                utills.hideKeyboard(StylistSeller_RegisterLocation_Activty.this);
                scrollview.smoothScrollTo(0, scrollview.getBottom());

                makeGeocodeSearch_code();

            }
        });


        method_get_city();


    }

    String City_Name = "";
    String Current_city = "";
    String Longitude = "";
    String Latitude = "";

    private void method_get_city() {
        if (utills.isOnline(this)) {
            AndroidNetworking.get(Global_Service_Api.API_cities)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    search_list = new ArrayList<>();


                                    JSONArray jsonObject2 = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonObject2.length(); i++) {
                                        JSONObject jsonObject1 = jsonObject2.getJSONObject(i);
                                        search_list.add("" + jsonObject1.getString("name"));
                                    }
                                    search_list.add("please select");

                                    SpinnerdocumentAdapter1 customAdapter_state = new SpinnerdocumentAdapter1(context, search_list);
                                    spiner_city.setAdapter((SpinnerAdapter) customAdapter_state);

                                    spiner_city.setSelection(customAdapter_state.getCount());

//                                    City_Name = "" + preferences.getPRE_City();
//                                    if (!City_Name.equalsIgnoreCase("")) {
//                                        try {
//                                            for (int i = 0; i < search_list.size(); i++) {
//                                                if (City_Name.equalsIgnoreCase(search_list.get(i))) {
//                                                    spiner_city.setSelection(i);
//                                                }
//                                            }
//                                        } catch (Exception e) {
//                                            spiner_city.setSelection(customAdapter_state.getCount());
//                                            e.printStackTrace();
//                                        }
//
//                                    } else
                                    if (!Current_city.equalsIgnoreCase("")) {
                                        try {
                                            for (int i = 0; i < search_list.size(); i++) {
                                                if (Current_city.equalsIgnoreCase(search_list.get(i))) {
                                                    spiner_city.setSelection(i);
                                                }
                                            }
                                        } catch (Exception e) {
                                            spiner_city.setSelection(customAdapter_state.getCount());
                                            e.printStackTrace();
                                        }
                                    } else {
                                        spiner_city.setSelection(customAdapter_state.getCount());
                                    }


                              }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    private void makeGeocodeSearch_postalcode() {

        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + et_postol_code.getText().toString())
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {


                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {
                                search_list_postcode = new ArrayList<>();

                                for (int i = 0; i < results.size(); i++) {
                                    String name = "" + results.get(i).placeName();
                                    if (name.contains(City_Name)) {
                                        search_list_postcode.add("" + results.get(i).text());
                                    }
                                }

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                                (context, R.layout.autotextview_layout1, search_list_postcode);

                                        et_postol_code.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();

                                    }
                                });


                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.e("maps", "failed");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Log.e("maps", "failed");
                }
            });
        } catch (ServicesException servicesException) {
            Log.e("maps", "failed");
            servicesException.printStackTrace();
        }
    }


    private void makeGeocodeSearch_code() {
        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + et_postol_code.getText().toString())
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {

                                Point firstResultPoint = results.get(0).center();
                                Final_point = firstResultPoint;
                                LatLng cityLatLng = new LatLng(Final_point.latitude(), Final_point.longitude());
                                animateCameraToNewPosition(cityLatLng);
                                drawPolygonCircle(Final_point, 11);
                                Longitude = "" + Final_point.longitude();
                                Latitude = "" + Final_point.latitude();

                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    // Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            //  Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }


    private void makeGeocodeSearch() {
        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + City_Name)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {

                                Point firstResultPoint = results.get(0).center();
                                Final_point = firstResultPoint;
                                LatLng cityLatLng = new LatLng(Final_point.latitude(), Final_point.longitude());
                                animateCameraToNewPosition(cityLatLng);
                                drawPolygonCircle(Final_point, 10);
                                Longitude = "" + Final_point.longitude();
                                Latitude = "" + Final_point.latitude();

                            } else {
                                Toast.makeText(StylistSeller_RegisterLocation_Activty.this, "no result",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    // Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            //  Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }


    Point Final_point;
    Spinner spiner_city;
    private boolean userIsInteracting;


    private void animateCameraToNewPosition(LatLng latLng) {

        mapboxMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(10)
                        .build()), 1500);


        if (mapboxMap.getStyle() != null) {
            GeoJsonSource spaceStationSource = mapboxMap.getStyle().getSourceAs("source-id");
            if (spaceStationSource != null) {
                spaceStationSource.setGeoJson(FeatureCollection.fromFeature(
                        Feature.fromGeometry(Point.fromLngLat(latLng.getLongitude(), latLng.getLatitude()))
                ));
            }
        }

    }

    private void findview() {

        rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(this);

        spiner_city = findViewById(R.id.spiner_city);
        ImageView iv_spinner_city = findViewById(R.id.iv_spinner_city);
        iv_spinner_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_city.performClick();
            }
        });

        spiner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (userIsInteracting) {
                    userIsInteracting = false;
                } else {
                    City_Name = "" + search_list.get(position);
                    makeGeocodeSearch();


                    et_postol_code.setText("");
                    search_list_postcode = new ArrayList<>();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (context, R.layout.autotextview_layout1, search_list_postcode);
                    et_postol_code.setAdapter(adapter);



                    scrollview.smoothScrollTo(0, scrollview.getBottom());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        final Spinner spiner_miles = findViewById(R.id.spiner_miles);
        ImageView iv_spinner = findViewById(R.id.iv_spinner);
        iv_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_miles.performClick();
            }
        });


        List<String> list_mile = new ArrayList<>();
        list_mile.add("0.5 miles");
        list_mile.add("1 mile");
        list_mile.add("2 miles");
        list_mile.add("3 miles");
        list_mile.add("5 miles");
        list_mile.add("7 miles");
        list_mile.add("10 miles");
        list_mile.add("15 miles");
        list_mile.add("25 miles");


        final  List<String> list_mile_final = new ArrayList<>();
        list_mile_final.add("0.5");
        list_mile_final.add("1");
        list_mile_final.add("2");
        list_mile_final.add("3");
        list_mile_final.add("5");
        list_mile_final.add("7");
        list_mile_final.add("10");
        list_mile_final.add("15");
        list_mile_final.add("25");


        SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(this, list_mile);
        spiner_miles.setAdapter((SpinnerAdapter) customAdapter_state);
        spiner_miles.setSelection(0);

        spiner_miles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (is_true) {
                    circleRadius = Double.parseDouble("" + list_mile_final.get(position));
                    scrollview.smoothScrollTo(0, scrollview.getBottom());
                    if (circleRadius == 0.5) {
                        drawPolygonCircle(Final_point, 12);
                    } else if (circleRadius == 1) {
                        drawPolygonCircle(Final_point, 11);
                    } else if (circleRadius == 2) {
                        drawPolygonCircle(Final_point, 10);
                    } else if (circleRadius == 3) {
                        drawPolygonCircle(Final_point, 10);
                    } else if (circleRadius == 5) {
                        drawPolygonCircle(Final_point, 9);
                    } else if (circleRadius == 7) {
                        drawPolygonCircle(Final_point, 9);
                    } else if (circleRadius == 10) {
                        drawPolygonCircle(Final_point, 8);
                    } else if (circleRadius == 15) {
                        drawPolygonCircle(Final_point, 8);
                    } else if (circleRadius == 25) {
                        drawPolygonCircle(Final_point, 7);
                    }


                } else {
                    is_true = true;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


    boolean is_true = false;

    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }


    class SpinnerdocumentAdapter1 extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter1(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size() - 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }

    private void drawPolygonCircle(final Point circleCenter,final int i) {


        try {
            if (circleCenter != null) {
                mapboxMap.getStyle(new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        if (style.isFullyLoaded()) {

                            LatLng cityLatLng = new LatLng(circleCenter.latitude(), circleCenter.longitude());

                            mapboxMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(new CameraPosition.Builder()
                                            .target(cityLatLng)
                                            .zoom(i)
                                            .build()), 1500);


                            Polygon polygonArea = getTurfPolygon(circleCenter, circleRadius, circleSteps, circleUnit);
                            GeoJsonSource polygonCircleSource = style.getSourceAs(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID);
                            if (polygonCircleSource != null) {
                                polygonCircleSource.setGeoJson(Polygon.fromOuterInner(
                                        LineString.fromLngLats(TurfMeta.coordAll(polygonArea, false))));
                            }
                        }
                    }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private Polygon getTurfPolygon(@NonNull Point centerPoint1, @NonNull double radius,
                                   @NonNull int steps, @NonNull String units) {
        return TurfTransformation.circle(centerPoint1, radius, steps, units);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.rl_next:


                if (ll_mieles.getVisibility() == View.GONE) {
                    ll_mieles.setVisibility(View.VISIBLE);
                } else {


                    if (City_Name.equalsIgnoreCase("")) {
                        Toast toast = Toast.makeText(context, "plesae select your city", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                    } else if (et_postol_code.getText().toString().equalsIgnoreCase("")) {
                        Toast toast = Toast.makeText(context, "plesae enter your postal code", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                    }else if (et_address_line.getText().toString().equalsIgnoreCase("")) {
                        Toast toast = Toast.makeText(context, "plesae enter your address", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                    } else {


                        int is_valid = 0;
                        for (int i = 0; i < search_list_postcode.size(); i++) {
                            String postal_code = "" + et_postol_code.getText().toString().trim();
                            if (postal_code.equalsIgnoreCase(search_list_postcode.get(i).toString())) {
                                is_valid = 1;
                            }
                        }

                        if (is_valid == 1) {

                            preferences.setPRE_City("" + City_Name);
                            preferences.setPRE_Code("" + et_postol_code.getText().toString());
                            preferences.setPRE_address("" + et_address_line.getText().toString());
                            preferences.setPRE_Mile("" + circleRadius);

                            preferences.setPRE_Longitude("" + Longitude);
                            preferences.setPRE_Latitude("" + Latitude);


                            Intent i = new Intent(StylistSeller_RegisterLocation_Activty.this, StylistSeller_RegisterServices_Activty.class);
                            startActivity(i);

                        } else {
                            Toast toast = Toast.makeText(context, "plesae enter your postal code", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();
                        }


                    }

                }


                break;
        }
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap1) {
        mapboxMap = mapboxMap1;

        mapboxMap.setStyle(Style.MAPBOX_STREETS,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);


                        style.addImage(("marker_icon"), getResources().getDrawable(R.drawable.ic_dot));
                        style.addSource(new GeoJsonSource("source-id"));
                        style.addLayer(new SymbolLayer("layer-id", "source-id")
                                .withProperties(
                                        PropertyFactory.iconImage("marker_icon"),
                                        PropertyFactory.iconIgnorePlacement(true),
                                        PropertyFactory.iconAllowOverlap(true)
                                ));

                        style.addSource(new GeoJsonSource(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID));

                        FillLayer fillLayer = new FillLayer(TURF_CALCULATION_FILL_LAYER_ID,
                                TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID);
                        fillLayer.setProperties(
                                fillColor(getResources().getColor(R.color.purpule)),
                                fillOpacity(.7f));
                        style.addLayerBelow(fillLayer, CIRCLE_CENTER_LAYER_ID);


                    }
                });


//        mapboxMap.addOnMoveListener(new MapboxMap.OnMoveListener() {
//            @Override
//            public void onMoveBegin(MoveGestureDetector detector) {
//
//            }
//
//            @Override
//            public void onMove(MoveGestureDetector detector) {
//
//                scrollview.requestDisallowInterceptTouchEvent(false);
////
//                Log.e("onMove: ", "" + detector);
//
//
//            }
//
//            @Override
//            public void onMoveEnd(MoveGestureDetector detector) {
//
//            }
//        });
//
//
//        Set<Integer> mutuallyExclusive1 = new HashSet<>();
//        mutuallyExclusive1.add(AndroidGesturesManager.GESTURE_TYPE_MULTI_FINGER_TAP);
//
//        Set<Integer> mutuallyExclusive2 = new HashSet<>();
//        mutuallyExclusive2.add(AndroidGesturesManager.GESTURE_TYPE_MULTI_FINGER_TAP);
//
//        AndroidGesturesManager androidGesturesManager = new AndroidGesturesManager(
//                context,
//                mutuallyExclusive1,
//                mutuallyExclusive2
//        );
//
//
//        mapboxMap.setGesturesManager(androidGesturesManager, true, true);
//
//
//        AndroidGesturesManager gesturesManager = mapboxMap.getGesturesManager();
//        gesturesManager.setMultiFingerTapGestureListener(new MultiFingerTapGestureDetector.OnMultiFingerTapGestureListener() {
//            @Override
//            public boolean onMultiFingerTap(MultiFingerTapGestureDetector detector, int pointersCount) {
//                scrollview.requestDisallowInterceptTouchEvent(false);
//                return false;
//            }
//        });

    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

//            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(this)
//                    .accuracyAlpha(.8f)
//                    .accuracyColor(R.color.purpule)
//                    .backgroundTintColor(R.color.black)
//                    .foregroundTintColor(R.color.black)
//                    .build();
//            LocationComponent locationComponent = mapboxMap.getLocationComponent();
//            LocationComponentActivationOptions locationComponentActivationOptions =
//                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
//                            .locationComponentOptions(customLocationComponentOptions)
//                            .build();
//            locationComponent.activateLocationComponent(locationComponentActivationOptions);
//            locationComponent.setLocationComponentEnabled(true);
//            locationComponent.setCameraMode(CameraMode.TRACKING);
//            locationComponent.setRenderMode(RenderMode.NORMAL);


            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build());
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);


            try {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    GPSTracker gpsTracker = new GPSTracker(context);

                    if (gpsTracker != null) {
                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                            String city = gpsTracker.getLocality(context);
                            Log.e("city", "" + city);
                            if (city != null) {
                                Current_city = "" + city;
                            }

                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        // Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            finish();
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        is_true = false;
        super.onBackPressed();
    }


}
