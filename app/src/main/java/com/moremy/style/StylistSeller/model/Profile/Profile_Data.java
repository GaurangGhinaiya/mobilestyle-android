package com.moremy.style.StylistSeller.model.Profile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.CommanModel.ModelComman_Review_Data;
import com.moremy.style.Model.Model_Rating;

public class Profile_Data {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facebook_id")
    @Expose
    private String facebookId;
    @SerializedName("googe_id")
    @Expose
    private String googeId;
    @SerializedName("apple_id")
    @Expose
    private String appleId;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified_at")
    @Expose
    private String emailVerifiedAt;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("one_line")
    @Expose
    private String oneLine;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;




    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("selling_status")
    @Expose
    private String sellingStatus;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("radius_from_postal_code")
    @Expose
    private String radiusFromPostalCode;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("portfolio")
    @Expose
    private List<Profile_Portfolio> portfolio = null;
    @SerializedName("sub_services")
    @Expose
    private List<Profile_SubService> subServices = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getGoogeId() {
        return googeId;
    }

    public void setGoogeId(String googeId) {
        this.googeId = googeId;
    }

    public String getAppleId() {
        return appleId;
    }

    public void setAppleId(String appleId) {
        this.appleId = appleId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOneLine() {
        return oneLine;
    }

    public void setOneLine(String oneLine) {
        this.oneLine = oneLine;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(String sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRadiusFromPostalCode() {
        return radiusFromPostalCode;
    }

    public void setRadiusFromPostalCode(String radiusFromPostalCode) {
        this.radiusFromPostalCode = radiusFromPostalCode;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<Profile_Portfolio> getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(List<Profile_Portfolio> portfolio) {
        this.portfolio = portfolio;
    }

    public List<Profile_SubService> getSubServices() {
        return subServices;
    }

    public void setSubServices(List<Profile_SubService> subServices) {
        this.subServices = subServices;
    }




    @SerializedName("is_selfie_verify")
    @Expose
    private Integer is_selfie_verify;


    public Integer getis_selfie_verify() {
        return is_selfie_verify;
    }

    public void setis_selfie_verify(Integer is_selfie_verify1) {
        this.is_selfie_verify = is_selfie_verify1;
    }

    @SerializedName("seller_products")
    @Expose
    private List<SellerProduct> sellerProducts = null;

    public List<SellerProduct> getSellerProducts() {
        return sellerProducts;
    }

    public void setSellerProducts(List<SellerProduct> sellerProducts) {
        this.sellerProducts = sellerProducts;
    }




    @SerializedName("username")
    @Expose
    private String username;


    public String getusername() {
        return username;
    }
    public void setusername(String username) {
        this.username = username;
    }


    @SerializedName("address_line_1")
    @Expose
    private String address_line_1;


    @SerializedName("latitude")
    @Expose
    private String latitude;


    @SerializedName("longitude")
    @Expose
    private String longitude;


    @SerializedName("is_premium")
    @Expose
    private String is_premium;


    public String getaddress_line_1() {
        return address_line_1;
    }
    public void setaddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }



    public String getlatitude() {
        return latitude;
    }
    public void setlatitude(String latitude) {
        this.latitude = latitude;
    }


public String getlongitude() {
        return longitude;
    }
    public void setlongitude(String longitude) {
        this.longitude = longitude;
    }



public String getis_premium() {
        return is_premium;
    }
    public void setis_premium(String is_premium) {
        this.is_premium = is_premium;
    }


    @SerializedName("stylist_rating")
    @Expose
    private Model_Rating stylist_rating;

    public Model_Rating getstylist_rating() {
        return stylist_rating;
    }

    public void setstylist_rating(Model_Rating stylist_rating) {
        this.stylist_rating = stylist_rating;
    }


    @SerializedName("seller_rating")
    @Expose
    private Model_Rating seller_rating;

    public Model_Rating getseller_rating() {
        return seller_rating;
    }

    public void setseller_rating(Model_Rating seller_rating) {
        this.seller_rating = seller_rating;
    }



    @SerializedName("seller_rating_list")
    @Expose
    private List<ModelComman_Review_Data> stylist_rating_list = null;

    public List<ModelComman_Review_Data> getReview() {
        return stylist_rating_list;
    }

    public void setReview(List<ModelComman_Review_Data> stylist_rating_list) {
        this.stylist_rating_list = stylist_rating_list;
    }


}