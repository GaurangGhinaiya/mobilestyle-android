package com.moremy.style.StylistSeller.NewProduct;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.TextViewCustom;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.NewProduct.StyleSeller_ModifyFirstProduct_Activty.lifestyle_list;


public class StyleSeller_ModifyProductFinal_Activty extends Base1_Activity {

    Dialog progressDialogs = null;


    Context context;

    Preferences preferences;

    List<ModelLifeStyle_Data> lifestyle_list_temp;


    RecyclerView rv_list;


    String final_id = "";
    String is_image_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylistseller_add_product_final);
        context = StyleSeller_ModifyProductFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        String service_names = "" + intent.getStringExtra("service_names");
        final_id = "" + intent.getStringExtra("id");
        is_image_url = "" + intent.getStringExtra("is_image_url");

        TextView tv_title_cutomername = findViewById(R.id.tv_title_cutomername);
        TextView tv_subcategory = findViewById(R.id.tv_subcategory);
        TextView tv_ProductDescription = findViewById(R.id.tv_ProductDescription);
        TextView tv_prize = findViewById(R.id.tv_prize);
        ImageView iv_profile_image = findViewById(R.id.iv_profile_image);


        tv_title_cutomername.setText("" + preferences.getPRE_ProductName());
        tv_ProductDescription.setText("" + preferences.getPRE_ProductDescription());
        tv_subcategory.setText("shop > " + service_names);
        tv_prize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(preferences.getPRE_ProductPrize())));


        if (!is_image_url.equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(""+is_image_url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        } else {
            Glide.with(this)
                    .load(""+preferences.getPRE_ProductImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }


        Log.e("onClick1:is_image_url", "" + is_image_url);
        Log.e("onClick1:Product", "" + preferences.getPRE_ProductImage());


        rv_list = findViewById(R.id.rv_list);
        rv_list.setNestedScrollingEnabled(false);
        rv_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        TextViewCustom tv_edit = findViewById(R.id.tv_edit);
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LinearLayout ll_lifestyle = findViewById(R.id.ll_lifestyle);
        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateBussinessInfo();

            }
        });


        lifestyle_list_temp = new ArrayList<>();
        StringBuilder stringBuilder_ids = new StringBuilder();

        if (lifestyle_list != null) {
            if (lifestyle_list.size() > 0) {
                for (int i = 0; i < lifestyle_list.size(); i++) {
                    if (lifestyle_list.get(i).is_chech_item) {
                        lifestyle_list_temp.add(lifestyle_list.get(i));

                        if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                            stringBuilder_ids.append(",");
                        }
                        stringBuilder_ids.append(lifestyle_list.get(i).getId());
                    }
                }
            }
        }


        if (lifestyle_list_temp.size() == 0) {
            ll_lifestyle.setVisibility(View.GONE);
        } else {
            ll_lifestyle.setVisibility(View.VISIBLE);
            GridAdapter adapter_cat = new GridAdapter(context, lifestyle_list_temp);
            rv_list.setAdapter(adapter_cat);

        }

        preferences.setPRE_ProductLifeStyle("" + stringBuilder_ids);
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<ModelLifeStyle_Data> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<ModelLifeStyle_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);

            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_lifestyle, parent, false);

            return new MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText(countries_list.get(position).getName());


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    File file_product;

    Dialog progressDialog = null;

    String is_per_item = "";

    public void UpdateBussinessInfo() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            if (!preferences.getPRE_ProductImage().equalsIgnoreCase("")) {
                file_product = new File(preferences.getPRE_ProductImage());


                if (preferences.getPRE_ProductPerItem().equalsIgnoreCase("per item")) {
                    /*2*/
                    is_per_item = "2";
                } else {
                    /*1*/
                    is_per_item = "1";
                }


                if (!is_image_url.equalsIgnoreCase("")) {
                    method_api_both_null2();
                } else {
                    method_api_both_null();
                }


            }


        }

    }

    private void method_api_both_null2() {


        AndroidNetworking.post(Global_Service_Api.API_update_product + final_id)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addBodyParameter("name", "" + preferences.getPRE_ProductName())
                .addBodyParameter("sku", "" + preferences.getPRE_ProductSKU())
                .addBodyParameter("description", "" + preferences.getPRE_ProductDescription())
                .addBodyParameter("price", "" + preferences.getPRE_ProductPrize())
                .addBodyParameter("category_id", "" + preferences.getPRE_ProductMainCategory())
                .addBodyParameter("sub_category_id", "" + preferences.getPRE_ProductSubCategory())
                .addBodyParameter("lifestyle_id", "" + preferences.getPRE_ProductLifeStyle())
                .addBodyParameter("quantity", "" + preferences.getPRE_ProductQuantity())
                .addBodyParameter("shipping_cost", "" + preferences.getPRE_ProductPostage())
                .addBodyParameter("shipping_type", "" + is_per_item)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");

                            Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show();

                            if (flag.equalsIgnoreCase("true")) {
                                preferences.setPRE_ProductImage("");
                                preferences.setPRE_ProductSubCategory("");
                                preferences.setPRE_ProductMainCategory("");
                                preferences.setPRE_ProductLifeStyle("");
                                preferences.setPRE_ProductSKU("");
                                preferences.setPRE_ProductPrize("");
                                preferences.setPRE_ProductDescription("");
                                preferences.setPRE_ProductName("");


                                finish();

                                StyleSeller_ModifyFirstProduct_Activty.finish_this();
                                StyleSeller_ModifyProduct_Services_Activty.finish_this();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }


    private void method_api_both_null() {


        AndroidNetworking.upload(Global_Service_Api.API_update_product + final_id)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addMultipartParameter("name", "" + preferences.getPRE_ProductName())
                .addMultipartParameter("sku", "" + preferences.getPRE_ProductSKU())
                .addMultipartParameter("description", "" + preferences.getPRE_ProductDescription())
                .addMultipartParameter("price", "" + preferences.getPRE_ProductPrize())
                .addMultipartParameter("category_id", "" + preferences.getPRE_ProductMainCategory())
                .addMultipartParameter("sub_category_id", "" + preferences.getPRE_ProductSubCategory())
                .addMultipartParameter("lifestyle_id", "" + preferences.getPRE_ProductLifeStyle())
                .addMultipartParameter("quantity", "" + preferences.getPRE_ProductQuantity())
                .addMultipartParameter("shipping_cost", "" + preferences.getPRE_ProductPostage())
                .addMultipartParameter("shipping_type", "" + is_per_item)
                .addMultipartFile("picture", file_product)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");

                            Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show();

                            if (flag.equalsIgnoreCase("true")) {
                                preferences.setPRE_ProductImage("");
                                preferences.setPRE_ProductSubCategory("");
                                preferences.setPRE_ProductMainCategory("");
                                preferences.setPRE_ProductLifeStyle("");
                                preferences.setPRE_ProductSKU("");
                                preferences.setPRE_ProductPrize("");
                                preferences.setPRE_ProductDescription("");
                                preferences.setPRE_ProductName("");


                                finish();

                                StyleSeller_ModifyFirstProduct_Activty.finish_this();
                                StyleSeller_ModifyProduct_Services_Activty.finish_this();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.getErrorDetail());
                    }
                });

    }


}
