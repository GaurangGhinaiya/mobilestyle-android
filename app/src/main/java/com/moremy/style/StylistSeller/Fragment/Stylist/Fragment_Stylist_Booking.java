package com.moremy.style.StylistSeller.Fragment.Stylist;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.moremy.style.R;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


public class Fragment_Stylist_Booking extends Fragment {
    Context context;


    public Fragment_Stylist_Booking() {
    }

    Preferences preferences;


    TabLayout tabs;
    ViewPager viewPager;
    Pager adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seller_order, container, false);
        context = getActivity();


        preferences = new Preferences(context);

        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);


        tabs.addTab(tabs.newTab().setText("my bookings"));
        tabs.addTab(tabs.newTab().setText("customer bookings"));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        method_set_tab_font();

        adapter = new Pager(getChildFragmentManager(), tabs.getTabCount());
        viewPager.setAdapter(adapter);




        return view;
    }

    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }


    Fragment_Stylist_My_Booking fragment_review1;
    Fragment_Stylist_Customer_Booking fragment_review2;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);

            fragment_review1 = new Fragment_Stylist_My_Booking();
            fragment_review2 = new Fragment_Stylist_Customer_Booking();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_review1;
                case 1:
                    return fragment_review2;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }



}