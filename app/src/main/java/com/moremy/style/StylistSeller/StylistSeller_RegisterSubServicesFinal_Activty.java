package com.moremy.style.StylistSeller;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.SuB_Services_Data;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.StylistSeller.StylistSeller_RegisterSubServices_Activty.list_services;
import static com.moremy.style.StylistSeller.StylistSeller_RegisterSubServices_Activty.list_services_Premium_List;


public class StylistSeller_RegisterSubServicesFinal_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    public static List<SuB_Services_Data> list_services_final = new ArrayList<>();

    RecyclerView recycleview_cat, listview_premium;
    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_sub_services_final);
        context = StylistSeller_RegisterSubServicesFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);

        recycleview_cat = findViewById(R.id.listview_country);
        listview_premium = findViewById(R.id.listview_premium);
        recycleview_cat.setNestedScrollingEnabled(false);
        listview_premium.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));
        listview_premium.setLayoutManager(new GridLayoutManager(this, 1));


        TextView tv_category = findViewById(R.id.tv_category);

        Intent intent = getIntent();
        tv_category.setText("" + intent.getStringExtra("service_name"));
        final String service_id = "" + intent.getStringExtra("service_id");
        String service_name = "" + intent.getStringExtra("service_name");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                preferences.setPRE_Main_ServiceID("" + service_id);


                Intent i = new Intent(context, StylistSeller_RegisterPortPholio_Activty.class);
                startActivity(i);

            }
        });

        list_services_final = new ArrayList<>();

        if (list_services != null) {
            if (list_services.size() > 0) {

                for (int i = 0; i < list_services.size(); i++) {
                    if (list_services.get(i).is_chechkbox) {
                        list_services_final.add(list_services.get(i));
                    }
                }

            }
        }

        GridAdapter adapter_counties = new GridAdapter(context, list_services_final);
        recycleview_cat.setAdapter(adapter_counties);


        if (list_services_Premium_List != null) {
            if (list_services_Premium_List.size() > 0) {
                GridAdapter adapter_counties2 = new GridAdapter(context, list_services_Premium_List);
                listview_premium.setAdapter(adapter_counties2);
            }
        }


    }

    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {

        private List<SuB_Services_Data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<SuB_Services_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry, tv_textPrize;

            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
                tv_textPrize = (TextView) view.findViewById(R.id.tv_textPrize);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_prize, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());
            if (!countries_list.get(position).sub_Price.equalsIgnoreCase("")) {
                viewHolder.tv_textPrize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(countries_list.get(position).sub_Price)));
            }
        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }

}
