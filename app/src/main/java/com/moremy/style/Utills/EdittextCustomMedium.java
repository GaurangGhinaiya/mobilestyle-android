package com.moremy.style.Utills;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class EdittextCustomMedium extends androidx.appcompat.widget.AppCompatEditText {
    public EdittextCustomMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EdittextCustomMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EdittextCustomMedium(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "font/WorkSans-Regular.ttf"), 1);
    }
}
