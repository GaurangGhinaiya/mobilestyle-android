package com.moremy.style.Utills;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextViewCustomMedium extends AppCompatTextView {
    public TextViewCustomMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewCustomMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewCustomMedium(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "font/WorkSans-Regular.ttf"), 1);
    }
}
