package com.moremy.style.Utills;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;


public class AutoCompleteTextviewCustom extends AppCompatAutoCompleteTextView {
    public AutoCompleteTextviewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public AutoCompleteTextviewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoCompleteTextviewCustom(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "font/WorkSans-Light.ttf"), 1);
    }
}
