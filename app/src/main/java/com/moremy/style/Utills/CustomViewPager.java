package com.moremy.style.Utills;

import android.content.Context;

import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class CustomViewPager extends ViewPager {
    public CustomViewPager(Context context) {
        super(context);
    }
    private int width = 0;
    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            width=widthMeasureSpec;
            int h = child.getMeasuredHeight();
            if (h > height) height = h;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


    }

    public void onRefresh()
    {
//        try {
//            int height = 0;
//
//            for (int i = 0; i < getChildCount(); i++) {
//                View child = getChildAt(i);
//                child.measure(width, View.MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//
//                int h = child.getMeasuredHeight();
//                if (h > height) height = h;
//            }
//
//            int heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
//            ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
//            layoutParams.height = heightMeasureSpec;
//            this.setLayoutParams(layoutParams);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}