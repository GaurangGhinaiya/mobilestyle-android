package com.moremy.style.Utills;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * @author Administrator
 * <p/>
 * Class for methods which save settings values
 */
public class Preferences {


    private String PRE_FirstName = "PRE_FirstName";
    private String PRE_LastName = "PRE_LastName";
    private String PRE_Email = "PRE_Email";
    private String PRE_Password = "PRE_Password";
    private String PRE_Gender = "PRE_Gender";

    private String PRE_SelectedFile_profile = "SelectedFile_profile";
    private String PRE_SelectedFile_logo = "SelectedFile_logo";


    private String PRE_City = "PRE_City";
    private String PRE_Code = "PRE_Code";
    private String PRE_Mile = "PRE_Mile";

    private String PRE_Main_ServiceID = "PRE_Main_ServiceID";


    private String PRE_Main_Oneline = "PRE_Main_Oneline";
    private String PRE_TOKEN = "PRE_TOKEN";
    private String PRE_SalonName = "PRE_SalonName";


    private String PRE_Latitude = "PRE_Latitude";
    private String PRE_Longitude = "PRE_Longitude";
    private String PRE_passbase_verify = "PRE_passbase_verify";
    private String PRE_passbase_id = "passbase_id";

    private String PRE_Only_Sell = "PRE_Only_Sell";
    private String PRE_Is_Premium = "PRE_Is_Premium";


    private String PRE_LoginType_Stylist_Seller = "PRE_LoginType_Stylist_Seller";


    private SharedPreferences pref;
    public static Preferences store;

    public static Preferences getInstance(Context context) {
        if (store == null)
            store = new Preferences(context);
        return store;
    }

    public Preferences(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void setPRE_passbase_verify(boolean link) {
        pref.edit().putBoolean(PRE_passbase_verify, link).commit();
    }

    public boolean getPRE_passbase_verify() {
        return pref.getBoolean(PRE_passbase_verify, false);
    }


    public void setPRE_Only_Sell(boolean link) {
        pref.edit().putBoolean(PRE_Only_Sell, link).commit();
    }

    public boolean getPRE_Only_Sell() {
        return pref.getBoolean(PRE_Only_Sell, false);
    }


    public void setPRE_passbase_id(String link) {
        pref.edit().putString(PRE_passbase_id, link).commit();
    }

    public String getPRE_passbase_id() {
        return pref.getString(PRE_passbase_id, "");
    }


    public void setPRE_Latitude(String link) {
        pref.edit().putString(PRE_Latitude, link).commit();
    }

    public String getPRE_Latitude() {
        return pref.getString(PRE_Latitude, "");
    }


    public void setPRE_Longitude(String link) {
        pref.edit().putString(PRE_Longitude, link).commit();
    }

    public String getPRE_Longitude() {
        return pref.getString(PRE_Longitude, "");
    }


    public void setPRE_SalonName(String link) {
        pref.edit().putString(PRE_SalonName, link).commit();
    }

    public String getPRE_SalonName() {
        return pref.getString(PRE_SalonName, "");
    }


    public void setPRE_TOKEN(String link) {
        pref.edit().putString(PRE_TOKEN, link).commit();
    }

    public String getPRE_TOKEN() {
        return pref.getString(PRE_TOKEN, "");
    }


    public void setPRE_FirstName(String link) {
        pref.edit().putString(PRE_FirstName, link).commit();
    }

    public String getPRE_FirstName() {
        return pref.getString(PRE_FirstName, "");
    }


    public void setPRE_LastName(String link) {
        pref.edit().putString(PRE_LastName, link).commit();
    }

    public String getPRE_LastName() {
        return pref.getString(PRE_LastName, "");
    }

    public void setPRE_Password(String link) {
        pref.edit().putString(PRE_Password, link).commit();
    }

    public String getPRE_Password() {
        return pref.getString(PRE_Password, "");
    }


    public void setPRE_Email(String link) {
        pref.edit().putString(PRE_Email, link).commit();
    }

    public String getPRE_Email() {
        return pref.getString(PRE_Email, "");
    }


    public void setPRE_Gender(String link) {
        pref.edit().putString(PRE_Gender, link).commit();
    }

    public String getPRE_Gender() {
        return pref.getString(PRE_Gender, "");
    }


    public void setPRE_SelectedFile_profile(String link) {
        pref.edit().putString(PRE_SelectedFile_profile, link).commit();
    }

    public String getPRE_SelectedFile_profile() {
        return pref.getString(PRE_SelectedFile_profile, "");
    }

    public void setPRE_SelectedFile_logo(String link) {
        pref.edit().putString(PRE_SelectedFile_logo, link).commit();
    }

    public String getPRE_SelectedFile_logo() {
        return pref.getString(PRE_SelectedFile_logo, "");
    }


    public void setPRE_City(String link) {
        pref.edit().putString(PRE_City, link).commit();
    }

    public String getPRE_City() {
        return pref.getString(PRE_City, "");
    }


    public void setPRE_Code(String link) {
        pref.edit().putString(PRE_Code, link).commit();
    }

    public String getPRE_Code() {
        return pref.getString(PRE_Code, "");
    }


    public void setPRE_address(String link) {
        pref.edit().putString("PRE_address", link).commit();
    }

    public String getPRE_address() {
        return pref.getString("PRE_address", "");
    }



    public void setPRE_Mile(String link) {
        pref.edit().putString(PRE_Mile, link).commit();
    }

    public String getPRE_Mile() {
        return pref.getString(PRE_Mile, "");
    }


    public void setPRE_Main_ServiceID(String link) {
        pref.edit().putString(PRE_Main_ServiceID, link).commit();
    }

    public String getPRE_Main_ServiceID() {
        return pref.getString(PRE_Main_ServiceID, "");
    }


    public void setPRE_Main_Oneline(String link) {
        pref.edit().putString(PRE_Main_Oneline, link).commit();
    }

    public String getPRE_Main_Oneline() {
        return pref.getString(PRE_Main_Oneline, "");
    }



    /*.............product..................*/


    private String PRE_ProductName = "PRE_ProductName";
    private String PRE_ProductDescription = "PRE_ProductDescription";
    private String PRE_ProductPrize = "PRE_ProductPrize";
    private String PRE_ProductSKU = "PRE_ProductSKU";
    private String PRE_ProductMainCategory = "PRE_ProductMainCategory";
    private String PRE_ProductSubCategory = "PRE_ProductSubCategory";
    private String PRE_ProductLifeStyle = "PRE_ProductLifeStyle";
    private String PRE_ProductImage = "PRE_ProductImage";
    private String PRE_UserName = "PRE_UserName";
    private String PRE_ProductQuantity = "PRE_ProductQuantity";
    private String PRE_ProductPostage = "PRE_ProductPostage";
    private String PRE_ProductPerItem = "PRE_ProductPerItem";

    public void setPRE_ProductName(String link) {
        pref.edit().putString(PRE_ProductName, link).commit();
    }

    public String getPRE_ProductName() {
        return pref.getString(PRE_ProductName, "");
    }

    public void setPRE_ProductDescription(String link) {
        pref.edit().putString(PRE_ProductDescription, link).commit();
    }

    public String getPRE_ProductDescription() {
        return pref.getString(PRE_ProductDescription, "");
    }


    public void setPRE_ProductPrize(String link) {
        pref.edit().putString(PRE_ProductPrize, link).commit();
    }

    public String getPRE_ProductPrize() {
        return pref.getString(PRE_ProductPrize, "");
    }

    public void setPRE_ProductSKU(String link) {
        pref.edit().putString(PRE_ProductSKU, link).commit();
    }

    public String getPRE_ProductSKU() {
        return pref.getString(PRE_ProductSKU, "");
    }





    public void setPRE_ProductQuantity(String link) {
        pref.edit().putString(PRE_ProductQuantity, link).commit();
    }

    public String getPRE_ProductQuantity() {
        return pref.getString(PRE_ProductQuantity, "");
    }




    public void setPRE_ProductPerItem(String link) {
        pref.edit().putString(PRE_ProductPerItem, link).commit();
    }

    public String getPRE_ProductPerItem() {
        return pref.getString(PRE_ProductPerItem, "");
    }





    public void setPRE_ProductPostage(String link) {
        pref.edit().putString(PRE_ProductPostage, link).commit();
    }

    public String getPRE_ProductPostage() {
        return pref.getString(PRE_ProductPostage, "");
    }





    public void setPRE_ProductMainCategory(String link) {
        pref.edit().putString(PRE_ProductMainCategory, link).commit();
    }

    public String getPRE_ProductMainCategory() {
        return pref.getString(PRE_ProductMainCategory, "");
    }


    public void setPRE_ProductSubCategory(String link) {
        pref.edit().putString(PRE_ProductSubCategory, link).commit();
    }

    public String getPRE_ProductSubCategory() {
        return pref.getString(PRE_ProductSubCategory, "");
    }

    public void setPRE_ProductLifeStyle(String link) {
        pref.edit().putString(PRE_ProductLifeStyle, link).commit();
    }

    public String getPRE_ProductLifeStyle() {
        return pref.getString(PRE_ProductLifeStyle, "");
    }

    public void setPRE_ProductImage(String link) {
        pref.edit().putString(PRE_ProductImage, link).commit();
    }

    public String getPRE_ProductImage() {
        return pref.getString(PRE_ProductImage, "");
    }


    public void setPRE_LoginType_Stylist_Seller(String link) {
        pref.edit().putString(PRE_LoginType_Stylist_Seller, link).commit();
    }

    public String getPRE_LoginType_Stylist_Seller() {
        return pref.getString(PRE_LoginType_Stylist_Seller, "");
    }

    public void setPRE_Is_Premium(boolean link) {
        pref.edit().putBoolean(PRE_Is_Premium, link).commit();
    }

    public boolean getPRE_Is_Premium() {
        return pref.getBoolean(PRE_Is_Premium, false);
    }

    public void setPRE_UserName(String link) {
        pref.edit().putString(PRE_UserName, link).commit();
    }

    public String getPRE_UserName() {
        return pref.getString(PRE_UserName, "");
    }


    private String PRE_SendBirdUserName = "PRE_SendBirdUserName";
    private String PRE_SendBirdUserId = "PRE_SendBirdUserId";


    public void setPRE_SendBirdUserName(String link) {
        pref.edit().putString(PRE_SendBirdUserName, link).commit();
    }

    public String getPRE_SendBirdUserName() {
        return pref.getString(PRE_SendBirdUserName, "");
    }

    public void setPRE_SendBirdUserId(String link) {
        pref.edit().putString(PRE_SendBirdUserId, link).commit();
    }

    public String getPRE_SendBirdUserId() {
        return pref.getString(PRE_SendBirdUserId, "");
    }



    private String PRE_Typee = "PRE_Typee";
    public void setPRE_Typee(String link) {
        pref.edit().putString(PRE_Typee, link).commit();
    }

    public String getPRE_Typee() {
        return pref.getString(PRE_Typee, "");
    }



    private String PRE_Is_Premium_Salon = "PRE_Is_Premium_Salon";


    public void setPRE_Is_Premium_Salon(boolean link) {
        pref.edit().putBoolean(PRE_Is_Premium_Salon, link).commit();
    }

    public boolean getPRE_Is_Premium_Salon() {
        return pref.getBoolean(PRE_Is_Premium_Salon, false);
    }


    private String PRE_FCMtoken = "FCMtoken";

    public void setPRE_FCMtoken(String link) {
        pref.edit().putString(PRE_FCMtoken, link).commit();
    }

    public String getPRE_FCMtoken() {
        return pref.getString(PRE_FCMtoken, "");
    }

/*..........................*/



    private String PRE_stripe_id = "stripe_id";

    public void setPRE_stripe_id(String link) {
        pref.edit().putString(PRE_stripe_id, link).commit();
    }

    public String getPRE_stripe_id() {
        return pref.getString(PRE_stripe_id, "");
    }




    private String PRE_sb_application_id = "sb_application_id";

    public void setPRE_sb_application_id(String link) {
        pref.edit().putString(PRE_sb_application_id, link).commit();
    }

    public String getPRE_sb_application_id() {
        return pref.getString(PRE_sb_application_id, "");
    }




    private String PRE_passbase_id_Key = "passbase_id_key";

    public void setPRE_passbase_id_Key(String link) {
        pref.edit().putString(PRE_passbase_id_Key, link).commit();
    }

    public String getPRE_passbase_id_Key() {
        return pref.getString(PRE_passbase_id_Key, "");
    }



 private String PRE_stripe_client_id = "stripe_client_id";

    public void setPRE_stripe_client_id(String link) {
        pref.edit().putString(PRE_stripe_client_id, link).commit();
    }

    public String getPRE_stripe_client_id() {
        return pref.getString(PRE_stripe_client_id, "");
    }




}