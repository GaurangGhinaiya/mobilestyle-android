package com.moremy.style.Utills;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.R;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static androidx.core.app.ActivityCompat.requestPermissions;

public class utills {


    public static String clientId = "ca_HhmrScqtSoPDBRSSLGQzMNjmZbgTjkUi";


    public static String api_key = "792bf8a385f5e719665f11789513bea85d0e7a2261e9ad9847a7f2cd005f6aab";
    public static String user_id = "a2ab4-f2jc4-k2dd4-1fa3x";

    public static String APP_ID = "86EB4F51-34E6-432E-9FC2-546BABC1D543";
    public static String stripe_key = "pk_test_RrbIPBtzKqcmc9nXh8J5qhZU00cbM7Aaxf";


    public static final String CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_GROUP_CHAT";

    public static boolean is_premium_image = false;
    public static boolean is_premium_image_Salon = false;


    public static boolean is_paid_shipped = false;


    public static void scaleAndUnscaleAnimation(View view) {
        ScaleAnimation scal_grow = new ScaleAnimation(0.90f, 1.0f, 0.90f, 1.0f, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
        scal_grow.setDuration(900);
        scal_grow.setFillAfter(true);
        scal_grow.setRepeatCount(ObjectAnimator.INFINITE);
        scal_grow.setRepeatMode(ObjectAnimator.REVERSE);
        view.startAnimation(scal_grow);
    }


    public static Typeface customTypeface_Bold(Context ctx) {
        return Typeface.createFromAsset(ctx.getAssets(), "font/WorkSans-SemiBold.ttf");
    }

    public static Typeface customTypeface(Context ctx) {
        return Typeface.createFromAsset(ctx.getAssets(), "font/WorkSans-Light.ttf");
    }

    public static Typeface customTypeface_medium(Context ctx) {
        return Typeface.createFromAsset(ctx.getAssets(), "font/WorkSans-Regular.ttf");
    }


    public static Dialog startLoader(final Context context) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        d.setContentView(R.layout.progress);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(d.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lp);

        return d;
    }

    public static Dialog startLoader_cancel(final Context context) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        d.setContentView(R.layout.progress);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(d.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lp);

        return d;
    }

    public static Dialog stopLoader(final Dialog d) {
        if (d != null && d.isShowing()) {
            d.cancel();
            //d.hide();
        }
        return null;
    }

    public static Boolean isOnline(Context context) {
        boolean connected = false;
        final ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            connected = true;
        } else if (netInfo != null && netInfo.isConnected()
                && cm.getActiveNetworkInfo().isAvailable()) {
            connected = true;
        } else if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL("http://www.google.com");
                HttpURLConnection urlc = (HttpURLConnection) url
                        .openConnection();
                urlc.setConnectTimeout(3000);
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    connected = true;
                }
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (cm != null) {
            final NetworkInfo[] netInfoAll = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfoAll) {
                System.out.println("get network type :::" + ni.getTypeName());
                if ((ni.getTypeName().equalsIgnoreCase("WIFI") || ni
                        .getTypeName().equalsIgnoreCase("MOBILE"))
                        && ni.isConnected() && ni.isAvailable()) {
                    connected = true;
                    if (connected) {
                        break;
                    }
                }
            }
        }
        return connected;
    }

    public static Interpolator INTERPOLATOR = new OvershootInterpolator();

    public static void animationPopUp(View view) {
        if (view == null) return;

        view.setScaleY(0.9f);
        view.setScaleX(0.9f);

        view.animate()
                .scaleY(1)
                .scaleX(1)
                .setDuration(40)
                .setInterpolator(INTERPOLATOR)
                .start();
    }

    public static final int PERMISSION_REQUEST_CODE = 200;

    public static String[] permissions = new String[]{
            WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA};

    public static boolean Permissions_READ_EXTERNAL_STORAGE(Context context) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(context, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }

        if (!listPermissionsNeeded.isEmpty()) {
            return false;
        }
        return true;
    }

    public static void Request_READ_EXTERNAL_STORAGE(Activity activity) {
        requestPermissions(activity, new String[]
                        {
                                WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA
                        },
                PERMISSION_REQUEST_CODE);
    }


    public static String[] permissions_location = new String[]{
            ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};

    public static boolean Permissions_READ_EXTERNAL_location(Context context) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String p : permissions_location) {
            result = ContextCompat.checkSelfPermission(context, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }

        if (!listPermissionsNeeded.isEmpty()) {
            return false;
        }
        return true;
    }


    public static final int PERMISSION_REQUEST_CODE2 = 300;

    public static void Request_READ_EXTERNAL_Location(Activity activity) {
        requestPermissions(activity, new String[]
                        {
                                ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION
                        },
                PERMISSION_REQUEST_CODE2);
    }


    public static File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".png",
                storageDir
        );
        return image;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getTimeAccordingCuntry1(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("d");
            String date = format.format(new Date());

            if (date.endsWith("1") && !date.endsWith("11"))
                format = new SimpleDateFormat("d'st' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("2") && !date.endsWith("12"))
                format = new SimpleDateFormat("d'nd' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("3") && !date.endsWith("13"))
                format = new SimpleDateFormat("d'rd' MMM yyyy", Locale.ENGLISH);
            else
                format = new SimpleDateFormat("d'th' MMM yyyy", Locale.ENGLISH);

            _messageDate = format.format(msgdate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingOrder(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("d");
            String date = format.format(new Date());

            if (date.endsWith("1") && !date.endsWith("11"))
                format = new SimpleDateFormat("EEEE d'st' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("2") && !date.endsWith("12"))
                format = new SimpleDateFormat("EEEE d'nd' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("3") && !date.endsWith("13"))
                format = new SimpleDateFormat("EEEE d'rd' MMM yyyy", Locale.ENGLISH);
            else
                format = new SimpleDateFormat("EEEE d'th' MMM yyyy", Locale.ENGLISH);

            _messageDate = format.format(msgdate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }



    public static String getTimeAccordingBooking(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("d");
            String date = format.format(new Date());

            if (date.endsWith("1") && !date.endsWith("11"))
                format = new SimpleDateFormat("EEEE d'st' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("2") && !date.endsWith("12"))
                format = new SimpleDateFormat("EEEE d'nd' MMM yyyy", Locale.ENGLISH);
            else if (date.endsWith("3") && !date.endsWith("13"))
                format = new SimpleDateFormat("EEEE d'rd' MMM yyyy", Locale.ENGLISH);
            else
                format = new SimpleDateFormat("EEEE d'th' MMM yyyy", Locale.ENGLISH);

            _messageDate = format.format(msgdate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingBooking_date(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("d");
            String date = format.format(new Date());

            if (date.endsWith("1") && !date.endsWith("11"))
                format = new SimpleDateFormat("MMMM d'st' yyyy", Locale.ENGLISH);
            else if (date.endsWith("2") && !date.endsWith("12"))
                format = new SimpleDateFormat("MMMM d'nd' yyyy", Locale.ENGLISH);
            else if (date.endsWith("3") && !date.endsWith("13"))
                format = new SimpleDateFormat("MMMM d'rd' yyyy", Locale.ENGLISH);
            else
                format = new SimpleDateFormat("MMMM d'th' yyyy", Locale.ENGLISH);

            _messageDate = format.format(msgdate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingTime(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            _messageDate = format.format(msgdate);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingDate(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("dd/MM", Locale.ENGLISH);
            _messageDate = format.format(msgdate);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingDate_Activity(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {


            String[] separated = datetime.split(" ");
            String date = separated[0];
            String month = separated[1];

            DateFormat sdf = new SimpleDateFormat("MMM");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(month);

            SimpleDateFormat format = new SimpleDateFormat("MMMM", Locale.ENGLISH);
            _messageDate = "" + date + " " + format.format(msgdate);


            SimpleDateFormat format_date = new SimpleDateFormat("d", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date d = format_date.parse(_messageDate.replaceAll("(?<=\\d)(st|nd|rd|th)", ""));

            String test_date = format_date.format(d) + " " + format.format(msgdate);


            Calendar mod = Calendar.getInstance();
            mod.setTime(mod.getTime());
            List<String> Model_Date_list = new ArrayList<>();

            for (int i = 0; i < 2; ++i) {
                SimpleDateFormat format2 = new SimpleDateFormat("d MMMM");
                Model_Date_list.add("" + format2.format(mod.getTime()));
                mod.add(Calendar.DATE, 1);
            }

            String date1 = "" + Model_Date_list.get(0).toString();
            String date2 = "" + Model_Date_list.get(1).toString();
            if (date1.equalsIgnoreCase(test_date)) {
                _messageDate = "Today";
            } else if (date2.equalsIgnoreCase(test_date)) {
                _messageDate = "Tomorrow";
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String getTimeAccordingTime_Date(String datetime) {

        if (datetime == null) {
            return "";
        }

        String _messageDate = null;
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date msgdate = sdf.parse(datetime);


            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            _messageDate = format.format(msgdate);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return _messageDate;
    }

    public static String roundTwoDecimals(double d) {
        String strDouble = String.format("%.2f", d);
        return strDouble;
    }

    public static File saveBitmapToFile(File file123) {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + ".png";


        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file123);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file123);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            File file = utills.createImageFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            Log.e("saveBitmapToFile", "null true");
            return file;
        } catch (Exception e) {
            Log.e("saveBitmapToFile", "null false");
            return null;
        }
    }

    public static void method_guest_redirect_login(Context ctx) {
        Toast.makeText(ctx, "You are not authorized to access this, Please Login.", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(ctx, Login_activty.class);
        ctx.startActivity(i);
    }


}
