package com.moremy.style.CommanActivity.Booking_Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.ModelBooking_BookingServiceItem;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Data;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Response;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Review;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ChatBookingDirect_Activity extends Base1_Activity {


    Handler mHandler;
    Fragment currentFragment;
    FragmentManager fragmentManager;

    FragmentTransaction fragmentTransaction;


    String grup_Name = "", grup_price = "";
    String profile_pic = "";
    String channelUrl = "";
    String order_id = "";

    ImageView iv_back;
    Preferences preferences;

    Context context;
    ImageView iv_profile_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_booking);


        context = ChatBookingDirect_Activity.this;
        preferences = new Preferences(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        mHandler = new Handler();


        iv_back = findViewById(R.id.iv_back);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Intent intent = getIntent();
        grup_Name = "" + intent.getStringExtra("grup_Name");
        grup_price = "" + intent.getStringExtra("grup_price");
        profile_pic = "" + intent.getStringExtra("profile_pic");
        channelUrl = "" + intent.getStringExtra("channelUrl");
        order_id = "" + intent.getStringExtra("order_id");


        iv_profile_image = findViewById(R.id.iv_profile_image);
        TextView tv_Name = findViewById(R.id.tv_Name);
        TextView tv_Price = findViewById(R.id.tv_Price);
        tv_Name.setText("" + grup_Name);
        tv_Price.setText("£" + utills.roundTwoDecimals(Double.parseDouble(grup_price)));


        method_get_details();

    }

    Dialog progressDialog = null;

    private void method_get_details() {

        if (utills.isOnline(context)) {

            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_get_booking_details + order_id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card123", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_BookingDetails_Response Response = new Gson().fromJson(result.toString(), Model_BookingDetails_Response.class);

                                    if (Response.getData() != null) {

                                        Model_BookingDetails_Data details_data = new Model_BookingDetails_Data();
                                        details_data = Response.getData();


                                        List<ModelBooking_BookingServiceItem> datalist_Services = new ArrayList<>();
                                        datalist_Services.addAll(details_data.getBookingServiceItem());

                                        String chechkout_price = "";
                                        String chechkout_name = "";

                                        if (datalist_Services.size() > 0) {
                                            float price = 0;
                                            StringBuilder stringBuilder_ids_name = new StringBuilder();
                                            for (int i = 0; i < datalist_Services.size(); i++) {

                                                if (!stringBuilder_ids_name.toString().equalsIgnoreCase("")) {
                                                    stringBuilder_ids_name.append(",");
                                                }

                                                stringBuilder_ids_name.append("" + datalist_Services.get(i).getService().getName()
                                                        + " (£" + utills.roundTwoDecimals(datalist_Services.get(i).getServicePrice()) + ")");
                                                price = price + datalist_Services.get(i).getServicePrice();

                                            }

                                            chechkout_price = "" + price;
                                            chechkout_name = "" + stringBuilder_ids_name;

                                        }


                                        if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + details_data.getStylistId())) {
                                            if (details_data.getUser() != null) {
                                                profile_pic = "" + details_data.getUser().getProfilePic();
                                            }
                                        } else {
                                            profile_pic = "" + details_data.getStylist().getProfilePic();
                                        }

                                        String chechkout_InvoiceId = "";
                                        String chechkout_Last4 = "";
                                        String chechkout_date = "";
                                        if (details_data.getPayment() != null) {
                                            chechkout_InvoiceId = "" + details_data.getPayment().getStripeInvoiceNumber();
                                            chechkout_Last4 = "" + details_data.getPayment().getLast4();
                                            chechkout_date = "" + details_data.getPayment().getCreatedAt();
                                        }

                                        String rating = "";
                                        if (details_data.getbooking_rating() != null) {
                                            String jsonSender = new Gson().toJson(details_data.getbooking_rating(),
                                                    new TypeToken<Model_BookingDetails_Review>() {
                                                    }.getType());
                                            rating = "" + jsonSender;
                                        }


                                        String is_status = "" + details_data.getStatus();


                                        if (!profile_pic.equalsIgnoreCase("")&& !profile_pic.equalsIgnoreCase("null")) {
                                            Glide.with(context)
                                                    .load(Global_Service_Api.IMAGE_URL + profile_pic)
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .into(iv_profile_image);
                                        }else {
                                            Glide.with(context)
                                                    .load(R.drawable.app_logo)
                                                    .into(iv_profile_image);
                                        }


                                        currentFragment = new BokingGroupChatFragment(channelUrl, "" + order_id);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("is_status", is_status);
                                        bundle.putString("chechkout_InvoiceId", chechkout_InvoiceId);
                                        bundle.putString("chechkout_Last4", chechkout_Last4);
                                        bundle.putString("chechkout_date", chechkout_date);
                                        bundle.putString("chechkout_name", chechkout_name);
                                        bundle.putString("chechkout_price", chechkout_price);
                                        bundle.putString("profile_pic", profile_pic);
                                        bundle.putString("is_user_id", "" + details_data.getUserId());
                                        bundle.putString("is_stylist_id",""+details_data.getStylistId() );
                                        bundle.putString("BookingDate", "" + details_data.getBookingDate());
                                        bundle.putString("BookingTime", "" + details_data.getBookingTime());
                                        bundle.putString("Is_Paid", "" + details_data.getIsPaid());
                                        bundle.putString("rating", "" + rating);

                                        currentFragment.setArguments(bundle);
                                        LoadFragment(currentFragment);


                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    public void LoadFragment(final Fragment fragment) {

        Runnable mPendingRunnable = new Runnable() {
            public void run() {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_group_channel, fragment);
                fragmentTransaction.commit();

            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


    }

    @Override
    public void onBackPressed() {


        Log.e("onBackPressed", "" + fragmentManager.getBackStackEntryCount());

        if (fragmentManager.getBackStackEntryCount() > 0) {
            try {
                try {
                    fragmentManager.popBackStack();

                    FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                            0);
                    getSupportFragmentManager().popBackStack(entry.getId(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().executePendingTransactions();

                    super.onBackPressed();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            super.onBackPressed();
        }


    }


}
