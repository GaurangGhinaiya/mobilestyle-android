package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Fragment.Model_Shop.ModelShopDetails_Data;
import com.moremy.style.Fragment.Model_Shop.ModelShopDetails_Response;
import com.moremy.style.R;

import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;
import com.moremy.style.Utills.Preferences;

import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductDeatails_Activty extends Base1_Activity {

    Dialog progressDialogs = null;


    Context context;

    Preferences preferences;

    List<ModelLifeStyle_Data> lifestyle_list_temp;


    RecyclerView rv_list;
    Dialog progressDialog = null;

    TextView tv_title_cutomername, tv_subcategory, tv_ProductDescription, tv_prize, tv_name, tv_Delivery;
    ImageView iv_profile_image, img_profile;
    ScaleRatingBar simpleRatingBar_quality;
    LinearLayout ll_lifestyle;

    int total_quantity = 1;
    String product_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        context = ProductDeatails_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        product_id = "" + intent.getStringExtra("product_id");

        tv_title_cutomername = findViewById(R.id.tv_title_cutomername);
        tv_subcategory = findViewById(R.id.tv_subcategory);
        tv_ProductDescription = findViewById(R.id.tv_ProductDescription);
        tv_prize = findViewById(R.id.tv_prize);
        iv_profile_image = findViewById(R.id.iv_profile_image);
        img_profile = findViewById(R.id.img_profile);
        tv_name = findViewById(R.id.tv_name);
        simpleRatingBar_quality = findViewById(R.id.simpleRatingBar_quality);


        rv_list = findViewById(R.id.rv_list);
        rv_list.setNestedScrollingEnabled(false);
        rv_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

            LinearLayout  ll_seller_details = findViewById(R.id.ll_seller_details);
        ll_seller_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SellerDeatails_Activty.class);
                i.putExtra("Seller_id", "" + modelShopDetails_data.getStylist().getId());
                  startActivity(i);
            }
        });


        ll_lifestyle = findViewById(R.id.ll_lifestyle);
        CardView card_enquire = findViewById(R.id.card_enquire);
        TextView card_buy_now = findViewById(R.id.card_buy_now);
        tv_Delivery = findViewById(R.id.tv_Delivery);

        card_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (preferences.getPRE_TOKEN().equalsIgnoreCase("")) {
                    utills.method_guest_redirect_login(context);
                } else {


                    if (modelShopDetails_data.getsendbird_order() != null) {


                        Intent i = new Intent(context, ChatOrderActivity.class);
                        i.putExtra("channelUrl", "" + modelShopDetails_data.getsendbird_order().getSendgridOrderId());
                        i.putExtra("ProductId", "" + modelShopDetails_data.getsendbird_order().getId());

                        i.putExtra("ProductName", "" + modelShopDetails_data.getName());
                        i.putExtra("ProductImage", "" + modelShopDetails_data.getPicture());

                        i.putExtra("ProductPrize", "" + modelShopDetails_data.getsendbird_order().getProductPrice());
                        i.putExtra("SellerImage", "" + modelShopDetails_data.getStylist().getProfilePic());
                        i.putExtra("SellerId", "" + modelShopDetails_data.getStylist().getId());
                        i.putExtra("SellerName", "" + modelShopDetails_data.getStylist().getName() + " " +
                                modelShopDetails_data.getStylist().getLastname());
                        i.putExtra("ProductDeliveryCharge", "" + modelShopDetails_data.getsendbird_order().getShippingPrice());
                        i.putExtra("order_id", "" + modelShopDetails_data.getsendbird_order().getId());
                        i.putExtra("InvoiceId", "");
                        i.putExtra("is_paid", "" + modelShopDetails_data.getsendbird_order().getIsPaid());

                        i.putExtra("getShippingDate", "" + modelShopDetails_data.getsendbird_order().getShippingDate());
                        i.putExtra("getDeliveryDate", "" + modelShopDetails_data.getsendbird_order().getDeliveryDate());
                        i.putExtra("getOrderDate", "" + modelShopDetails_data.getsendbird_order().getOrderDate());
                        i.putExtra("status", "" + modelShopDetails_data.getsendbird_order().getStatus());


                        startActivity(i);


                    } else {


                        Intent i = new Intent(context, ChatActivity.class);
                        i.putExtra("total_quantity", "" + total_quantity);
                        i.putExtra("buy_now", "1");
                        i.putExtra("ProductId", "" + modelShopDetails_data.getId());
                        i.putExtra("ProductName", "" + modelShopDetails_data.getName());
                        i.putExtra("ProductImage", "" + modelShopDetails_data.getPicture());
                        i.putExtra("ProductPrize", "" + modelShopDetails_data.getPrice());
                        i.putExtra("ProductDeliveryCharge", "" + modelShopDetails_data.getShippingCost());
                        i.putExtra("SellerImage", "" + modelShopDetails_data.getStylist().getProfilePic());
                        i.putExtra("SellerId", "" + modelShopDetails_data.getStylist().getId());
                        i.putExtra("SellerName", "" + modelShopDetails_data.getStylist().getName() + " " + modelShopDetails_data.getStylist().getLastname());
                        startActivity(i);
                    }
                    finish();
                }
            }
        });

        card_enquire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (preferences.getPRE_TOKEN().equalsIgnoreCase("")) {
                    utills.method_guest_redirect_login(context);
                } else {


                    if (modelShopDetails_data.getsendbird_order() != null) {


                        Intent i = new Intent(context, ChatOrderActivity.class);
                        i.putExtra("channelUrl", "" + modelShopDetails_data.getsendbird_order().getSendgridOrderId());
                        i.putExtra("ProductId", "" + modelShopDetails_data.getsendbird_order().getId());

                        i.putExtra("ProductName", "" + modelShopDetails_data.getName());
                        i.putExtra("ProductImage", "" + modelShopDetails_data.getPicture());

                        i.putExtra("ProductPrize", "" + modelShopDetails_data.getsendbird_order().getProductPrice());
                        i.putExtra("SellerImage", "" + modelShopDetails_data.getStylist().getProfilePic());
                        i.putExtra("SellerId", "" + modelShopDetails_data.getStylist().getId());
                        i.putExtra("SellerName", "" + modelShopDetails_data.getStylist().getName() + " " +
                                modelShopDetails_data.getStylist().getLastname());
                        i.putExtra("ProductDeliveryCharge", "" + modelShopDetails_data.getsendbird_order().getShippingPrice());
                        i.putExtra("order_id", "" + modelShopDetails_data.getsendbird_order().getId());
                        i.putExtra("InvoiceId", "");
                        i.putExtra("is_paid", "" + modelShopDetails_data.getsendbird_order().getIsPaid());

                        i.putExtra("getShippingDate", "" + modelShopDetails_data.getsendbird_order().getShippingDate());
                        i.putExtra("getDeliveryDate", "" + modelShopDetails_data.getsendbird_order().getDeliveryDate());
                        i.putExtra("getOrderDate", "" + modelShopDetails_data.getsendbird_order().getOrderDate());
                        i.putExtra("status", "" + modelShopDetails_data.getsendbird_order().getStatus());


                        startActivity(i);


                    } else {
                        Intent i = new Intent(context, ChatActivity.class);
                        i.putExtra("total_quantity", "1");
                        i.putExtra("buy_now", "0");
                        i.putExtra("ProductId", "" + modelShopDetails_data.getId());
                        i.putExtra("ProductName", "" + modelShopDetails_data.getName());
                        i.putExtra("ProductImage", "" + modelShopDetails_data.getPicture());
                        i.putExtra("ProductPrize", "" + modelShopDetails_data.getPrice());
                        i.putExtra("ProductDeliveryCharge", "" + modelShopDetails_data.getShippingCost());
                        i.putExtra("SellerImage", "" + modelShopDetails_data.getStylist().getProfilePic());
                        i.putExtra("SellerId", "" + modelShopDetails_data.getStylist().getId());
                        i.putExtra("SellerName", "" + modelShopDetails_data.getStylist().getName() + " " + modelShopDetails_data.getStylist().getLastname());
                        startActivity(i);
                    }


                    finish();
                }
            }
        });


        final TextView tv_cardname, tv_minus, tv_plus, tv_quantity;
        tv_minus = findViewById(R.id.tv_minus);
        tv_quantity = findViewById(R.id.tv_quantity);
        tv_plus = findViewById(R.id.tv_plus);


        tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (total_quantity > 1) {
                    total_quantity = total_quantity - 1;
                }
                tv_quantity.setText("" + total_quantity);
            }
        });


        tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total_quantity = total_quantity + 1;
                tv_quantity.setText("" + total_quantity);
            }
        });


    }

    ModelShopDetails_Data modelShopDetails_data;

    private void method_get_api(String id) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_Get_product_details + id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("on", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    ModelShopDetails_Response Response = new Gson().fromJson(result.toString(), ModelShopDetails_Response.class);

                                    if (Response.getData() != null) {
                                        modelShopDetails_data = new ModelShopDetails_Data();
                                        modelShopDetails_data = Response.getData();
                                        method_setdata();

                                    }


                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });

        }
    }

    private void method_setdata() {


        tv_title_cutomername.setText("" + modelShopDetails_data.getName());
        tv_ProductDescription.setText("" + modelShopDetails_data.getDescription());

        if (modelShopDetails_data.getCategory() != null) {
            tv_subcategory.setText("shop > " + modelShopDetails_data.getCategory().getName());
        }

        tv_prize.setText("£" + utills.roundTwoDecimals(modelShopDetails_data.getPrice()));


        if (modelShopDetails_data.getShippingCost() != null) {
            if ((!modelShopDetails_data.getShippingCost().equalsIgnoreCase("null")) &&
                    (!modelShopDetails_data.getShippingCost().equalsIgnoreCase(""))) {
                tv_Delivery.setText("+ £" + utills.roundTwoDecimals(Double.parseDouble(modelShopDetails_data.getShippingCost())) + " delivery");
            } else {
                tv_Delivery.setText("+ £0 delivery");
            }
        } else {
            tv_Delivery.setText("+ £0 delivery");
        }


        if (!modelShopDetails_data.getPicture().equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(Global_Service_Api.IMAGE_URL + modelShopDetails_data.getPicture())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }

        lifestyle_list_temp = new ArrayList<>();

        if (modelShopDetails_data.getlifestyle() != null) {
            lifestyle_list_temp.addAll(modelShopDetails_data.getlifestyle());
            if (lifestyle_list_temp.size() == 0) {
                ll_lifestyle.setVisibility(View.GONE);
            } else {
                ll_lifestyle.setVisibility(View.VISIBLE);
                GridAdapter adapter_cat = new GridAdapter(context, lifestyle_list_temp);
                rv_list.setAdapter(adapter_cat);
            }
        } else {
            ll_lifestyle.setVisibility(View.GONE);
        }


        if (modelShopDetails_data.getStylist() != null) {
            tv_name.setText(modelShopDetails_data.getStylist().getName() + " " + modelShopDetails_data.getStylist().getLastname());

            if (modelShopDetails_data.getStylist().getProfilePic() != null) {
                if (!modelShopDetails_data.getStylist().getProfilePic().equalsIgnoreCase("")) {
                    Glide.with(this)
                            .load(Global_Service_Api.IMAGE_URL + modelShopDetails_data.getStylist().getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(img_profile);
                }
            }


            if (modelShopDetails_data.getStylist().getseller_rating() != null) {
                simpleRatingBar_quality.setRating(modelShopDetails_data.getStylist().getseller_rating().getavg_rating());
            } else {
                simpleRatingBar_quality.setRating(0);
            }


        }


    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<ModelLifeStyle_Data> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<ModelLifeStyle_Data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_lifestyle, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText(countries_list.get(position).getName());


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    @Override
    protected void onResume() {
        method_get_api(product_id);
        super.onResume();
    }


}
