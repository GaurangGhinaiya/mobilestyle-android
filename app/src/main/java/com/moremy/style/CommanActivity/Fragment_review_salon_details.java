package com.moremy.style.CommanActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.moremy.style.CommanModel.ModelComman_Review_Data;
import com.moremy.style.CommanModel.Salonn_Data;

import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.Preferences;

import java.util.ArrayList;
import java.util.List;


public class Fragment_review_salon_details extends Fragment {
    Context context;


    Salonn_Data profile_data_Stylist;


    public Fragment_review_salon_details(Salonn_Data profile_data_StylistOnly1) {
        profile_data_Stylist = profile_data_StylistOnly1;
    }


    public Fragment_review_salon_details() {
    }

    List<ModelComman_Review_Data> data_review_list = new ArrayList<>();

    GridView listview_review;
    TextView tv_no_data;
    Preferences preferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricelist, container, false);
        context = getActivity();
        preferences = new Preferences(context);

        data_review_list = new ArrayList<>();
        listview_review = view.findViewById(R.id.listview_countiy);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        tv_no_data.setText("there is no data");


        if (profile_data_Stylist != null) {
            if (profile_data_Stylist.getReview() != null) {
                data_review_list = new ArrayList<>();
                data_review_list = profile_data_Stylist.getReview();
                Adapter_review adapter_review = new Adapter_review(context, data_review_list);
                listview_review.setAdapter(adapter_review);
            } else {
                tv_no_data.setVisibility(View.VISIBLE);
                listview_review.setVisibility(View.GONE);
            }

        } else {
            tv_no_data.setVisibility(View.VISIBLE);
            listview_review.setVisibility(View.GONE);
        }


        return view;
    }

    Dialog progressDialog = null;


    public class Adapter_review extends BaseAdapter {

        Context context;
        List<ModelComman_Review_Data> listState;
        LayoutInflater inflater;


        public Adapter_review(Context applicationContext, List<ModelComman_Review_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_reviewlist, viewGroup, false);
                viewHolder.tv_name = view.findViewById(R.id.tv_name);
                viewHolder.tv_date = view.findViewById(R.id.tv_date);
                viewHolder.tv_description = view.findViewById(R.id.tv_description);
                viewHolder.simpleRatingBar_ = view.findViewById(R.id.simpleRatingBar_quality);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            viewHolder.tv_date.setText("" + listState.get(position).getCreatedAt());
            viewHolder.tv_name.setVisibility(View.GONE);
            viewHolder.tv_description.setText("" + listState.get(position).getComments());


            try {
                String rating = "" + listState.get(position).getRating();
                viewHolder.simpleRatingBar_.setRating(Float.parseFloat(rating));
            } catch (Exception e) {
                e.printStackTrace();
            }


            return view;
        }

        private class ViewHolder {
            TextView tv_name, tv_date, tv_description;
            ScaleRatingBar simpleRatingBar_;

        }

    }


}