
package com.moremy.style.CommanActivity.Booking_Fragment.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_BookingDetails_Payment {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("card_id")
    @Expose
    private String cardId;
    @SerializedName("last4")
    @Expose
    private String last4;
    @SerializedName("card_brand")
    @Expose
    private String cardBrand;
    @SerializedName("amount")
    @Expose
    private float amount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("payment_type")
    @Expose
    private Integer paymentType;
    @SerializedName("stripe_invoice_id")
    @Expose
    private String stripeInvoiceId;
    @SerializedName("stripe_invoice_number")
    @Expose
    private String stripeInvoiceNumber;
    @SerializedName("stripe_invoice_pdf")
    @Expose
    private String stripeInvoicePdf;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("receipt_number")
    @Expose
    private Object receiptNumber;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getStripeInvoiceId() {
        return stripeInvoiceId;
    }

    public void setStripeInvoiceId(String stripeInvoiceId) {
        this.stripeInvoiceId = stripeInvoiceId;
    }

    public String getStripeInvoiceNumber() {
        return stripeInvoiceNumber;
    }

    public void setStripeInvoiceNumber(String stripeInvoiceNumber) {
        this.stripeInvoiceNumber = stripeInvoiceNumber;
    }

    public String getStripeInvoicePdf() {
        return stripeInvoicePdf;
    }

    public void setStripeInvoicePdf(String stripeInvoicePdf) {
        this.stripeInvoicePdf = stripeInvoicePdf;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public Object getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(Object receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
