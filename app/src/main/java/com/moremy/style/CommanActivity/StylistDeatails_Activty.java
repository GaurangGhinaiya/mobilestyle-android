package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Activity.Base_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Style_Servicelist;
import com.moremy.style.CommanModel.Stylistt_Data;
import com.moremy.style.CommanModel.Stylistt_Response;
import com.moremy.style.CommanModel.Stylistt_StylistPortfolio;
import com.moremy.style.R;

import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Style_Servicelist.listview_prize;
import static com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Style_Servicelist.ll_select_Address;
import static com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Style_Servicelist.ll_selected;


public class StylistDeatails_Activty extends Base_Activity {

    Dialog progressDialog = null;


    Context context;

    Preferences preferences;


    TextView tv_oneliner, tv_name, tv_servies;
    ScaleRatingBar simpleRatingBar_quality;
    Stylistt_Data profile_data_StylistOnly;

    ImageView img_profile, iv_companylogo;

    List<Stylistt_StylistPortfolio> arrayList1_random = new ArrayList<>();
    ViewPager viewPager_images;
    TabLayout indicator;

    ProgressBar progress_bar_profile, progress_bar_companylogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylist_details);
        context = StylistDeatails_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        String product_id = "" + intent.getStringExtra("Stylist_id");


        progress_bar_profile = (ProgressBar) findViewById(R.id.progress_bar_profile);
        progress_bar_companylogo = (ProgressBar) findViewById(R.id.progress_bar_companylogo);
        tv_oneliner = (TextView) findViewById(R.id.tv_oneliner);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_servies = (TextView) findViewById(R.id.tv_servies);
        simpleRatingBar_quality = (ScaleRatingBar) findViewById(R.id.simpleRatingBar_quality);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        iv_companylogo = (ImageView) findViewById(R.id.iv_companylogo);

        viewPager_images = findViewById(R.id.viewPager_images);
        indicator = findViewById(R.id.indicator);

        method_settab();



        method_get_api(product_id);


    }

    private void method_settab() {
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);

        tabs.addTab(tabs.newTab().setText("Services"));
      //  tabs.addTab(tabs.newTab().setText("Products"));
        tabs.addTab(tabs.newTab().setText("Reviews"));

        method_set_tab_font();
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }


    private void method_get_api(String id) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_Get_stylist_details + id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("stylist: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    Stylistt_Response Response = new Gson().fromJson(result.toString(), Stylistt_Response.class);

                                    if (Response.getData() != null) {
                                        profile_data_StylistOnly = new Stylistt_Data();
                                        profile_data_StylistOnly = Response.getData();
                                        method_setdata();

                                    }


                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });

        }
    }

    private void method_setdata() {

        if (profile_data_StylistOnly != null) {
            tv_oneliner.setText("" + profile_data_StylistOnly.getOneLine());
            tv_name.setText("" + profile_data_StylistOnly.getName()+" " + profile_data_StylistOnly.getLastname());
            tv_servies.setText("" + profile_data_StylistOnly.getService().getName());


            if (profile_data_StylistOnly.getstylist_rating() != null) {
                simpleRatingBar_quality.setRating(profile_data_StylistOnly.getstylist_rating().getavg_rating());
            }else {
                simpleRatingBar_quality.setRating(0);
            }


            try {
                if (profile_data_StylistOnly.getCompanyLogo() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_StylistOnly.getCompanyLogo())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .addListener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    iv_companylogo.setImageResource(R.drawable.app_logo);
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(iv_companylogo);
                } else {
                    iv_companylogo.setImageResource(R.drawable.app_logo);
                    progress_bar_companylogo.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                iv_companylogo.setImageResource(R.drawable.app_logo);
                progress_bar_companylogo.setVisibility(View.GONE);

                e.printStackTrace();
            }

            try {
                if (profile_data_StylistOnly.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_StylistOnly.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            if (profile_data_StylistOnly.getStylistPortfolio() != null) {
                arrayList1_random = new ArrayList<>();
                arrayList1_random.addAll(profile_data_StylistOnly.getStylistPortfolio());
                viewPager_images.setAdapter(new SliderAdapter(context, arrayList1_random));
                indicator.setupWithViewPager(viewPager_images, true);
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
            }

            adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount());
            viewPager.setAdapter(adapter);

        }
    }

    private class SliderTimer extends TimerTask {
        @Override
        public void run() {

            try {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (arrayList1_random.size() > 0) {
                            if (viewPager_images.getCurrentItem() < arrayList1_random.size() - 1) {
                                viewPager_images.setCurrentItem(viewPager_images.getCurrentItem() + 1);
                            } else {
                                viewPager_images.setCurrentItem(0);
                            }
                        }

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class SliderAdapter extends PagerAdapter {

        private Context context;
        private List<Stylistt_StylistPortfolio> color;

        public SliderAdapter(Context context, List<Stylistt_StylistPortfolio> color) {
            this.context = context;
            this.color = color;
        }

        @Override
        public int getCount() {
            return color.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_slider, null);


            ImageView iv_appicon__pager = (ImageView) view.findViewById(R.id.iv_appicon__pager);
            final ProgressBar progress_bar_slider = (ProgressBar) view.findViewById(R.id.progress_bar_slider);


            Glide.with(context)
                    .load(Global_Service_Api.IMAGE_URL + color.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(iv_appicon__pager);


            ViewPager viewPager = (ViewPager) container;
            viewPager.addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPager viewPager = (ViewPager) container;
            View view = (View) object;
            viewPager.removeView(view);
        }
    }


    TabLayout tabs;
    CustomViewPager viewPager;
    Pager adapter;


    Fragment_Style_Servicelist style_servicelist;
    Fragment_review_stylist_details fragment_review1;
   // Fragment_review fragment_review2;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            style_servicelist = new Fragment_Style_Servicelist(profile_data_StylistOnly);
            fragment_review1 = new Fragment_review_stylist_details(profile_data_StylistOnly);
          //  fragment_review2 = new Fragment_review();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return style_servicelist;
                case 1:
                    return fragment_review1;
//                case 2:
//                    return fragment_review1;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }


    @Override
    public void onBackPressed() {

        if(ll_selected != null){
            if (ll_selected.getVisibility() == View.VISIBLE) {
                listview_prize.setVisibility(View.VISIBLE);
                ll_selected.setVisibility(View.GONE);
                ll_select_Address.setVisibility(View.GONE);
            } else {
                super.onBackPressed();

            }
        }else {
            super.onBackPressed();

        }

    }
}
