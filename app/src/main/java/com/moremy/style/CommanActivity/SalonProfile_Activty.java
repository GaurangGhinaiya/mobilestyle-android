package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Activity.Base_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Salon_Servicelist;

import com.moremy.style.CommanModel.Salonn_Data;
import com.moremy.style.CommanModel.Salonn_Response;
import com.moremy.style.CommanModel.Salonn_SalonImage;
import com.moremy.style.CommanModel.Stylistt_StylistPortfolio;
import com.moremy.style.R;

import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Salon_Servicelist.listview_prize;
import static com.moremy.style.CommanActivity.Booking_Fragment.Fragment_Salon_Servicelist.ll_selected;


public class SalonProfile_Activty extends Base_Activity {

    Dialog progressDialog = null;


    Context context;

    Preferences preferences;

    ScaleRatingBar simpleRatingBar_quality;
    TextView tv_oneliner, tv_name, tv_servies, tv_address;
    Salonn_Data salon_data;

    ImageView img_profile, iv_companylogo;

    List<Stylistt_StylistPortfolio> arrayList1_random = new ArrayList<>();
    List<Salonn_SalonImage> Salon_imageslist = new ArrayList<>();
    ViewPager viewPager_images;
    TabLayout indicator;

    ProgressBar progress_bar_profile, progress_bar_companylogo;





    RecyclerView Rv_images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_details);
        context = SalonProfile_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
        String salon_id = "" + intent.getStringExtra("salon_id");



        progress_bar_profile = (ProgressBar) findViewById(R.id.progress_bar_profile);
        progress_bar_companylogo = (ProgressBar) findViewById(R.id.progress_bar_companylogo);

        simpleRatingBar_quality = (ScaleRatingBar) findViewById(R.id.simpleRatingBar_quality);
        tv_oneliner = (TextView) findViewById(R.id.tv_oneliner);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_servies = (TextView) findViewById(R.id.tv_servies);
        tv_address = (TextView) findViewById(R.id.tv_address);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        iv_companylogo = (ImageView) findViewById(R.id.iv_companylogo);

        viewPager_images = findViewById(R.id.viewPager_images);
        indicator = findViewById(R.id.indicator);


        Rv_images = findViewById(R.id.Rv_images);
        Rv_images.setNestedScrollingEnabled(false);
        Rv_images.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));


        method_settab();



        method_get_api(salon_id);


    }

    private void method_settab() {
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);

        tabs.addTab(tabs.newTab().setText("Services"));
        //  tabs.addTab(tabs.newTab().setText("Products"));
         tabs.addTab(tabs.newTab().setText("Reviews"));

        method_set_tab_font();
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }




    private void method_get_api(String id) {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_Get_salon_details + id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e( "salon: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    Salonn_Response Response = new Gson().fromJson(result.toString(), Salonn_Response.class);

                                    if (Response.getData() != null) {
                                        salon_data = new Salonn_Data();
                                        salon_data = Response.getData();
                                        method_setdata();

                                    }


                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });

        }
    }
    private void method_setdata() {

        if (salon_data != null) {
            tv_oneliner.setText("" + salon_data.getOneLine());
            tv_name.setText("" + salon_data.getName()+" " + salon_data.getLastname());

            if (salon_data.getService() != null) {
                if (salon_data.getService().size() > 0) {
                    tv_servies.setText("" + salon_data.getService().get(0).getName());
                }
            }

            tv_address.setText("" + salon_data.getPostalCode() + ", " + salon_data.getCity());

            if (salon_data.getstylist_rating() != null) {
                simpleRatingBar_quality.setRating(salon_data.getstylist_rating().getavg_rating());
            }else {
                simpleRatingBar_quality.setRating(0);
            }

            try {
                if (salon_data.getCompanyLogo() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + salon_data.getCompanyLogo())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .addListener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    iv_companylogo.setImageResource(R.drawable.app_logo);
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(iv_companylogo);
                } else {
                    iv_companylogo.setImageResource(R.drawable.app_logo);
                    progress_bar_companylogo.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                iv_companylogo.setImageResource(R.drawable.app_logo);
                progress_bar_companylogo.setVisibility(View.GONE);

                e.printStackTrace();
            }

            try {
                if (salon_data.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + salon_data.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            if (salon_data.getSalonPortfolio() != null) {
                arrayList1_random = new ArrayList<>();
                arrayList1_random.addAll(salon_data.getSalonPortfolio());
                viewPager_images.setAdapter(new SliderAdapter(context, arrayList1_random));
                indicator.setupWithViewPager(viewPager_images, true);
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
            }


            if (salon_data.getSalonImages() != null) {
                Salon_imageslist = new ArrayList<>();
                Salon_imageslist = salon_data.getSalonImages();

                GridAdapter adapter_counties = new GridAdapter(context, Salon_imageslist);
                Rv_images.setAdapter(adapter_counties);

            }

            adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount());
            viewPager.setAdapter(adapter);

        }
    }

    private class SliderTimer extends TimerTask {
        @Override
        public void run() {

            try {

               runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (arrayList1_random.size() > 0) {
                                if (viewPager_images.getCurrentItem() < arrayList1_random.size() - 1) {
                                    viewPager_images.setCurrentItem(viewPager_images.getCurrentItem() + 1);
                                } else {
                                    viewPager_images.setCurrentItem(0);
                                }
                            }

                        }
                    });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class SliderAdapter extends PagerAdapter {

        private Context context;
        private List<Stylistt_StylistPortfolio> color;

        public SliderAdapter(Context context, List<Stylistt_StylistPortfolio> color) {
            this.context = context;
            this.color = color;
        }

        @Override
        public int getCount() {
            return color.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_slider, null);


            ImageView iv_appicon__pager = (ImageView) view.findViewById(R.id.iv_appicon__pager);
            final ProgressBar progress_bar_slider = (ProgressBar) view.findViewById(R.id.progress_bar_slider);


            Glide.with(context)
                    .load(Global_Service_Api.IMAGE_URL + color.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(iv_appicon__pager);


            ViewPager viewPager = (ViewPager) container;
            viewPager.addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPager viewPager = (ViewPager) container;
            View view = (View) object;
            viewPager.removeView(view);
        }
    }

//    public void UpdateProfile_Image() {
//        if (utills.isOnline(context)) {
//
//            if (SelectedFile_profile != null) {
//
//                AndroidNetworking.upload(Global_Service_Api.API_update_profile_img)
//                        .addMultipartFile("profile_pic", SelectedFile_profile)
//                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
//                        .build()
//                        .getAsString(new StringRequestListener() {
//                            @Override
//                            public void onResponse(String result) {
//                                utills.stopLoader(progressDialog);
//                                if (result == null || result == "") return;
//
//                                try {
//                                    JSONObject jsonObject = new JSONObject(result);
//                                    String msg = jsonObject.getString("message");
//                                    String flag = jsonObject.getString("flag");
//                                    String code = jsonObject.getString("code");
//
//                                    if (flag.equalsIgnoreCase("true")) {
//                                        SelectedFile_profile = null;
//                                    }
//
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//
//                            @Override
//                            public void onError(ANError anError) {
//                                utills.stopLoader(progressDialog);
//                                Toast.makeText(context, "Internal Server Error", Toast.LENGTH_SHORT).show();
//                                Log.d("API", anError.toString());
//                            }
//                        });
//            }
//
//        }
//    }



    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Salonn_SalonImage> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<Salonn_SalonImage> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView iv_appicon__pager;
            ProgressBar progress_bar_slider;

            public MyViewHolder(View view) {
                super(view);

                iv_appicon__pager = (ImageView) view.findViewById(R.id.iv_appicon__pager);
                progress_bar_slider = (ProgressBar) view.findViewById(R.id.progress_bar_slider);
            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_images, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {


            Glide.with(context)
                    .load(Global_Service_Api.IMAGE_URL + countries_list.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(viewHolder.iv_appicon__pager);


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    TabLayout tabs;
    CustomViewPager viewPager;
    Pager adapter;


    Fragment_Salon_Servicelist style_servicelist;
    Fragment_review_salon_details fragment_review1;
    // Fragment_review fragment_review2;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            style_servicelist = new Fragment_Salon_Servicelist(salon_data);
              fragment_review1 = new Fragment_review_salon_details(salon_data);
            //  fragment_review2 = new Fragment_review();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return style_servicelist;
                case 1:
                    return fragment_review1;
//                case 2:
//                    return fragment_review1;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }



    @Override
    public void onBackPressed() {

        if(ll_selected != null){
            if (ll_selected.getVisibility() == View.VISIBLE) {
                listview_prize.setVisibility(View.VISIBLE);
                ll_selected.setVisibility(View.GONE);
            } else {
                super.onBackPressed();

            }
        }else {
            super.onBackPressed();

        }

    }

}
