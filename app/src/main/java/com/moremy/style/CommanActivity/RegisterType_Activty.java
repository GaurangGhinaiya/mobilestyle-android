package com.moremy.style.CommanActivity;


import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.moremy.style.Customer.Customer_RegisterData_Activty;
import com.moremy.style.R;
import com.moremy.style.Salon.Salon_RegisterData_Activty;
import com.moremy.style.Salon.Salon_RegisterServices_Activty;
import com.moremy.style.StylistSeller.StylistSeller_RegisterData_Activty;
import com.moremy.style.StylistSeller.StylistSeller_RegisterServices_Activty;

public class RegisterType_Activty extends Base1_Activity implements View.OnClickListener {

    CardView card_seller, card_customer,card_salon;
    TextView tv_customer1, tv_customer2;
    TextView tv_seller1, tv_seller2;
    TextView tv_salon2, tv_salon1;
    ImageView iv_seller, iv_customer,iv_salon;


    int is_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        card_seller = findViewById(R.id.card_seller);
        card_customer = findViewById(R.id.card_customer);
        card_salon = findViewById(R.id.card_salon);

        tv_customer1 = findViewById(R.id.tv_customer1);
        tv_customer2 = findViewById(R.id.tv_customer2);
        iv_customer = findViewById(R.id.iv_customer);


        tv_seller1 = findViewById(R.id.tv_seller1);
        tv_seller2 = findViewById(R.id.tv_seller2);
        iv_seller = findViewById(R.id.iv_seller);


        tv_salon2 = findViewById(R.id.tv_salon2);
        tv_salon1 = findViewById(R.id.tv_salon1);
        iv_salon = findViewById(R.id.iv_salon);


        LinearLayout ll_customer = findViewById(R.id.ll_customer);
        LinearLayout ll_seller = findViewById(R.id.ll_seller);
        LinearLayout ll_salon = findViewById(R.id.ll_salon);
        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(this);
        ll_customer.setOnClickListener(this);
        ll_seller.setOnClickListener(this);
        ll_salon.setOnClickListener(this);


    }

    private void method_default() {

        card_seller.setCardElevation(0);
        card_customer.setCardElevation(0);
        card_salon.setCardElevation(0);

        iv_seller.setVisibility(View.GONE);
        iv_customer.setVisibility(View.GONE);
        iv_salon.setVisibility(View.GONE);

        tv_seller1.setTextColor(getResources().getColor(R.color.black_light_trans1));
        tv_seller2.setTextColor(getResources().getColor(R.color.black_light_trans1));
        tv_customer1.setTextColor(getResources().getColor(R.color.black_light_trans1));
        tv_customer2.setTextColor(getResources().getColor(R.color.black_light_trans1));
        tv_salon2.setTextColor(getResources().getColor(R.color.black_light_trans1));
        tv_salon1.setTextColor(getResources().getColor(R.color.black_light_trans1));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ll_seller:

                is_type = 2;
                method_default();
                card_seller.setCardElevation(6);
                iv_seller.setVisibility(View.VISIBLE);
                tv_seller1.setTextColor(getResources().getColor(R.color.black));
                tv_seller2.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.ll_customer:

                is_type = 1;
                method_default();
                card_customer.setCardElevation(6);
                iv_customer.setVisibility(View.VISIBLE);
                tv_customer1.setTextColor(getResources().getColor(R.color.black));
                tv_customer2.setTextColor(getResources().getColor(R.color.black));

                break;


            case R.id.ll_salon:

                is_type = 3;
                method_default();
                card_salon.setCardElevation(6);
                iv_salon.setVisibility(View.VISIBLE);
                tv_salon1.setTextColor(getResources().getColor(R.color.black));
                tv_salon2.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.rl_next:


                if (is_type == 2) {

                    //  Intent i = new Intent(RegisterType_Activty.this, StylistSeller_RegisterServices_Activty.class);
                    Intent i = new Intent(RegisterType_Activty.this, StylistSeller_RegisterData_Activty.class);
                    i.putExtra("is_type", "" + is_type);
                    startActivity(i);

                } else if (is_type == 3) {

                    //  Intent i = new Intent(RegisterType_Activty.this, Salon_RegisterServices_Activty.class);
                    Intent i = new Intent(RegisterType_Activty.this, Salon_RegisterData_Activty.class);
                    i.putExtra("is_type", "" + is_type);
                    startActivity(i);

                } else if (is_type == 1) {


                    Intent i = new Intent(RegisterType_Activty.this, Customer_RegisterData_Activty.class);
                    i.putExtra("is_type", "" + is_type);
                    startActivity(i);


                } else {
                    Toast.makeText(this, "Please Select Type", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }
}
