package com.moremy.style.CommanActivity.Booking_Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.ModelBooking_BookingServiceItem;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Data;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Response;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Review;
import com.moremy.style.Fragment.Fragment_MyActivity;
import com.moremy.style.R;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelParams;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Customer.Customer_MainProfile_Activty.currentFragment_CUSTOMER;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentManager_CUSTOMER;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentTransaction_CUSTOMER;

import static com.moremy.style.Customer.Customer_MainProfile_Activty.ll_My_Activity_cust;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.ll_cart_cust;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.ll_discover_cust;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.ll_profile_cust;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.ll_search_cust;

import static com.moremy.style.Salon.Salon_MainProfile_Activty.currentFragment_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;

import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_My_Activity_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_cart_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_discover_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_profile_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_search_salon;


import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.currentFragment_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentManager_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentTransaction_Stylist_seller;

import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.ll_My_Activity;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.ll_cart;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.ll_discover;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.ll_profile;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.ll_search;


public class ChatBookingActivity extends Base1_Activity {


    Handler mHandler;
    Fragment currentFragment;
    FragmentManager fragmentManager;

    FragmentTransaction fragmentTransaction;


    String USER_ID = "", Stylish_Id = "", grup_Name = "", grup_price = "";
    String Final_date = "", Final_time = "", Address = "", ids = "", profile_pic = "", city = "", postal_code = "";

    ImageView iv_back;
    Preferences preferences;
    Dialog progressDialog = null;

    Context context;
    ImageView iv_profile_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_booking);


        context = ChatBookingActivity.this;
        preferences = new Preferences(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        mHandler = new Handler();


        iv_back = findViewById(R.id.iv_back);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Intent intent = getIntent();
        Stylish_Id = "" + intent.getStringExtra("Stylish_Id");
        grup_Name = "" + intent.getStringExtra("grup_Name");
        grup_price = "" + intent.getStringExtra("grup_price");
        Final_date = "" + intent.getStringExtra("Final_date");
        Final_time = "" + intent.getStringExtra("Final_time");
        Address = "" + intent.getStringExtra("Address");
        ids = "" + intent.getStringExtra("ids");
        profile_pic = "" + intent.getStringExtra("profile_pic");
        city = "" + intent.getStringExtra("city");
        postal_code = "" + intent.getStringExtra("postal_code");


        USER_ID = "" + preferences.getPRE_SendBirdUserId();


         iv_profile_image = findViewById(R.id.iv_profile_image);
        TextView tv_Name = findViewById(R.id.tv_Name);
        TextView tv_Price = findViewById(R.id.tv_Price);
        tv_Name.setText("" + grup_Name);
        tv_Price.setText("£" + utills.roundTwoDecimals(Double.parseDouble(grup_price)));




        SendBird.connect(USER_ID, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {    // Error.
                    Toast.makeText(ChatBookingActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                method_create_channel1();

            }
        });


    }


    private void method_create_channel1() {


        List<String> users = new ArrayList<>();
        users.add("" + USER_ID);
        users.add("" + Stylish_Id);

        GroupChannelParams params = new GroupChannelParams()
                .setPublic(false)
                .setEphemeral(false)
                .setDistinct(false)
                .addUserIds(users)
                .setName("" + "" + preferences.getPRE_SendBirdUserName());  // In a group channel, you can create a new channel by specifying its unique channel URL in a 'GroupChannelParams' object.;

        GroupChannel.createChannel(params, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {       // Error.

                    return;
                } else {

                    String channelUrl = groupChannel.getUrl();
                    if (channelUrl != null) {
                        // If started from notification


                        method_api_call(channelUrl);

                    }
                }
            }
        });


    }


    public void LoadFragment(final Fragment fragment) {

        Runnable mPendingRunnable = new Runnable() {
            public void run() {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_group_channel, fragment);
                fragmentTransaction.commit();

            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


    }

    @Override
    public void onBackPressed() {


        Log.e("onBackPressed", "" + fragmentManager.getBackStackEntryCount());

        if (fragmentManager.getBackStackEntryCount() > 0) {
            try {
                try {
                    fragmentManager.popBackStack();

                    FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                            0);
                    getSupportFragmentManager().popBackStack(entry.getId(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().executePendingTransactions();

                    super.onBackPressed();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {


            if (currentFragment_CUSTOMER != null) {
                currentFragment_CUSTOMER = new Fragment_MyActivity();
                Runnable mPendingRunnable = new Runnable() {
                    public void run() {
                        fragmentTransaction_CUSTOMER = fragmentManager_CUSTOMER.beginTransaction();
                        fragmentTransaction_CUSTOMER.replace(R.id.fmFragment, currentFragment_CUSTOMER);
                        fragmentTransaction_CUSTOMER.commit();

                    }
                };


                ll_discover_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_cart_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_search_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_profile_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_My_Activity_cust.setBackgroundColor(getResources().getColor(R.color.gray_trans1));


            }

            if (currentFragment_SALON != null) {
                currentFragment_SALON = new Fragment_MyActivity();
                Runnable mPendingRunnable = new Runnable() {
                    public void run() {
                        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
                        fragmentTransaction_SALON.replace(R.id.fmFragment, currentFragment_SALON);
                        fragmentTransaction_SALON.commit();

                    }
                };


                ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_cart_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_search_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_profile_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_My_Activity_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));


            }

            if (currentFragment_Stylist_seller != null) {
                currentFragment_Stylist_seller = new Fragment_MyActivity();
                Runnable mPendingRunnable = new Runnable() {
                    public void run() {
                        fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
                        fragmentTransaction_Stylist_seller.replace(R.id.fmFragment, currentFragment_Stylist_seller);
                        fragmentTransaction_Stylist_seller.commit();

                    }
                };

                ll_discover.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_cart.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_search.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_profile.setBackgroundColor(getResources().getColor(R.color.transparent));
                ll_My_Activity.setBackgroundColor(getResources().getColor(R.color.gray_trans1));


            }



            super.onBackPressed();
        }


    }






    private void method_api_call(final String channelUrl) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.post(Global_Service_Api.API_submit_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("send_bird_id", "" + channelUrl)
                    .addBodyParameter("stylist_service_id", "" + ids)
                    .addBodyParameter("stylist_id", "" + Stylish_Id)
                    .addBodyParameter("booking_date", "" + Final_date)
                    .addBodyParameter("booking_time", "" + Final_time)
                    .addBodyParameter("address", "" + Address)
                    .addBodyParameter("zipcode", "" + postal_code)
                    .addBodyParameter("city", "" + city)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("bbokking", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");


                                    method_get_details("" + jsonObject1.get("id"),channelUrl);



                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });


        }

    }


    private void method_get_details(final String order_id, final String channelUrl) {

        if (utills.isOnline(context)) {

            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_get_booking_details + order_id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card123", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_BookingDetails_Response Response = new Gson().fromJson(result.toString(), Model_BookingDetails_Response.class);

                                    if (Response.getData() != null) {

                                        Model_BookingDetails_Data details_data = new Model_BookingDetails_Data();
                                        details_data = Response.getData();


                                        List<ModelBooking_BookingServiceItem> datalist_Services = new ArrayList<>();
                                        datalist_Services.addAll(details_data.getBookingServiceItem());

                                        String chechkout_price = "";
                                        String chechkout_name = "";

                                        if (datalist_Services.size() > 0) {
                                            float price = 0;
                                            StringBuilder stringBuilder_ids_name = new StringBuilder();
                                            for (int i = 0; i < datalist_Services.size(); i++) {

                                                if (!stringBuilder_ids_name.toString().equalsIgnoreCase("")) {
                                                    stringBuilder_ids_name.append(",");
                                                }

                                                stringBuilder_ids_name.append("" + datalist_Services.get(i).getService().getName()
                                                        + " (£" + utills.roundTwoDecimals(datalist_Services.get(i).getServicePrice()) + ")");
                                                price = price + datalist_Services.get(i).getServicePrice();

                                            }

                                            chechkout_price = "" + price;
                                            chechkout_name = "" + stringBuilder_ids_name;

                                        }



                                        if (preferences.getPRE_SendBirdUserId().
                                                equalsIgnoreCase("" + details_data.getStylistId())) {
                                            profile_pic="" + details_data.getUser().getProfilePic();
                                        } else {
                                            profile_pic="" + details_data.getStylist().getProfilePic();
                                        }

                                        String chechkout_InvoiceId = "";
                                        String chechkout_Last4 = "";
                                        String chechkout_date = "";
                                        if (details_data.getPayment() != null) {
                                            chechkout_InvoiceId = "" + details_data.getPayment().getStripeInvoiceNumber();
                                            chechkout_Last4 = "" + details_data.getPayment().getLast4();
                                            chechkout_date = "" + details_data.getPayment().getCreatedAt();
                                        }

                                        String is_status = "" + details_data.getStatus();


                                        if (!profile_pic.equalsIgnoreCase("") && !profile_pic.equalsIgnoreCase("null")) {
                                            Glide.with(context)
                                                    .load(Global_Service_Api.IMAGE_URL + profile_pic)
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .into(iv_profile_image);
                                        }else {
                                            Glide.with(context)
                                                    .load(R.drawable.app_logo)
                                                    .into(iv_profile_image);
                                        }


                                        String rating = "";
                                        if (details_data.getbooking_rating() != null) {
                                            String jsonSender = new Gson().toJson(details_data.getbooking_rating(),
                                                    new TypeToken<Model_BookingDetails_Review>() {
                                                    }.getType());
                                            rating = "" + jsonSender;
                                        }



                                        currentFragment = new BokingGroupChatFragment(channelUrl, "" + order_id);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("is_status", is_status);
                                        bundle.putString("chechkout_InvoiceId", chechkout_InvoiceId);
                                        bundle.putString("chechkout_Last4", chechkout_Last4);
                                        bundle.putString("chechkout_date", chechkout_date);
                                        bundle.putString("chechkout_name", chechkout_name);
                                        bundle.putString("chechkout_price", chechkout_price);
                                        bundle.putString("profile_pic", profile_pic);
                                        bundle.putString("is_user_id",""+details_data.getUserId() );
                                        bundle.putString("is_stylist_id",""+details_data.getStylistId() );
                                        bundle.putString("BookingDate",""+details_data.getBookingDate() );
                                        bundle.putString("BookingTime",""+details_data.getBookingTime() );
                                        bundle.putString("Is_Paid",""+details_data.getIsPaid() );
                                        bundle.putString("rating", "" + rating);

                                        currentFragment.setArguments(bundle);
                                        LoadFragment(currentFragment);


                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


}
