package com.moremy.style.CommanActivity.Booking_Fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.moremy.style.CommanModel.Salonn_Data;
import com.moremy.style.CommanModel.Salonn_SalonSubservice;

import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Fragment_Salon_Servicelist extends Fragment {

    private Context context;

    Salonn_Data profile_data_Salon;


    public Fragment_Salon_Servicelist(Salonn_Data profile_data_StylistOnly1) {
        profile_data_Salon = profile_data_StylistOnly1;
    }

    public Fragment_Salon_Servicelist() {
    }

    List<Salonn_SalonSubservice> data_prize_list = new ArrayList<>();

    public static GridView listview_prize;
    GridView listview_SelectedCountiy;
    public static LinearLayout ll_selected;


    RecyclerView rv_time, rv_date;

    Preferences preferences;
    List<Salonn_SalonSubservice> list_services_test = new ArrayList<>();

    String token = "pk.eyJ1IjoiZm9yZXNpZ2h0dGVjaG5vbG9naWVzIiwiYSI6ImNqa3phMXBiNDBydjczcXFwNWQzbWlqNnkifQ.fQQWorOl5PVt4s4M0pwYaA";
    RelativeLayout rl_next_service;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylist_salonlist, container, false);
        context = getActivity();

        preferences = new Preferences(context);
        data_prize_list = new ArrayList<>();

        listview_prize = view.findViewById(R.id.listview_countiy);
        listview_SelectedCountiy = view.findViewById(R.id.listview_SelectedCountiy);
        ll_selected = view.findViewById(R.id.ll_selected);

        rv_date = view.findViewById(R.id.rv_date);
        rv_time = view.findViewById(R.id.rv_time);

        listview_prize.setVisibility(View.VISIBLE);
        ll_selected.setVisibility(View.GONE);

        list_services_test = new ArrayList<>();


        rl_next_service = view.findViewById(R.id.rl_next_service);
        rl_next_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getPRE_TOKEN().equalsIgnoreCase("")) {
                    utills.method_guest_redirect_login(context);
                }else {



                if (ll_selected.getVisibility() == View.GONE) {
                    list_services_test = new ArrayList<>();
                    if (data_prize_list != null) {
                        if (data_prize_list.size() > 0) {
                            for (int i = 0; i < data_prize_list.size(); i++) {
                                if (data_prize_list.get(i).is_chechkbox) {
                                    list_services_test.add(data_prize_list.get(i));
                                }
                            }

                        }
                    }



                    if(list_services_test.size() > 0){

                        listview_prize.setVisibility(View.GONE);
                        ll_selected.setVisibility(View.VISIBLE);

                        Adapter_Prize12 adapter_prize = new Adapter_Prize12(context, list_services_test);
                        listview_SelectedCountiy.setAdapter(adapter_prize);

                    }else {
                        Toast.makeText(context, "Please select Services", Toast.LENGTH_SHORT).show();
                    }



                } else {


                    if (Final_date.equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show();
                    } else if (Final_time.equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select time", Toast.LENGTH_SHORT).show();
                    } else {


                        float price = 0;
                        StringBuilder stringBuilder_ids = new StringBuilder();
                        StringBuilder stringBuilder_ids_name = new StringBuilder();
                        for (int i = 0; i < list_services_test.size(); i++) {
                            if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                stringBuilder_ids.append(",");
                            }
                            if (!stringBuilder_ids_name.toString().equalsIgnoreCase("")) {
                                stringBuilder_ids_name.append(",");
                            }
                            stringBuilder_ids.append(list_services_test.get(i).getId());
                            stringBuilder_ids_name.append("" + list_services_test.get(i).getService().getName()
                                    + " (£" + utills.roundTwoDecimals(list_services_test.get(i).getPrice()) + ")");

                            price = price + list_services_test.get(i).getPrice();

                        }


                        Intent i = new Intent(context, ChatSalonBookingActivity.class);
                        i.putExtra("Salon_Id", "" + profile_data_Salon.getId());
                        i.putExtra("grup_Name", "" + stringBuilder_ids_name);
                        i.putExtra("grup_price", "" + price);
                        i.putExtra("Final_date", "" + Final_date);
                        i.putExtra("Final_time", "" + Final_time);
                        i.putExtra("ids", "" + stringBuilder_ids);
                        i.putExtra("profile_pic", "" + profile_data_Salon.getProfilePic());
                        startActivity(i);


                        if (getActivity() != null) {
                            getActivity().finish();
                        }

                    }
                }


            }

            }
        });


        return view;
    }


    private void getTimes() {


        TEMP_MOdelTime m_1 = new TEMP_MOdelTime("06:00 am", "06:00");
        TEMP_MOdelTime m_2 = new TEMP_MOdelTime("06:30 am", "06:30");

        TEMP_MOdelTime m_3 = new TEMP_MOdelTime("07:00 am", "07:00");
        TEMP_MOdelTime m_4 = new TEMP_MOdelTime("07:30 am", "07:30");

        TEMP_MOdelTime m_5 = new TEMP_MOdelTime("08:00 am", "08:00");
        TEMP_MOdelTime m_6 = new TEMP_MOdelTime("08:30 am", "08:30");

        TEMP_MOdelTime m_7 = new TEMP_MOdelTime("09:00 am", "09:00");
        TEMP_MOdelTime m_8 = new TEMP_MOdelTime("09:30 am", "09:30");

        TEMP_MOdelTime m_9 = new TEMP_MOdelTime("10:00 am", "10:00");
        TEMP_MOdelTime m_10 = new TEMP_MOdelTime("10:30 am", "10:30");

        TEMP_MOdelTime m_11 = new TEMP_MOdelTime("11:00 am", "11:00");
        TEMP_MOdelTime m_12 = new TEMP_MOdelTime("11:30 am", "11:30");


        TEMP_MOdelTime m_13 = new TEMP_MOdelTime("12:00 am", "12:00");
        TEMP_MOdelTime m_14 = new TEMP_MOdelTime("12:30 pm", "12:30");


        TEMP_MOdelTime m_15 = new TEMP_MOdelTime("01:00 pm", "13:00");
        TEMP_MOdelTime m_16 = new TEMP_MOdelTime("01:30 pm", "13:30");

        TEMP_MOdelTime m_17 = new TEMP_MOdelTime("02:00 pm", "14:00");
        TEMP_MOdelTime m_18 = new TEMP_MOdelTime("02:30 pm", "14:30");

        TEMP_MOdelTime m_19 = new TEMP_MOdelTime("03:00 pm", "15:00");
        TEMP_MOdelTime m_20 = new TEMP_MOdelTime("03:30 pm", "15:30");

        TEMP_MOdelTime m_21 = new TEMP_MOdelTime("04:00 pm", "16:00");
        TEMP_MOdelTime m_22 = new TEMP_MOdelTime("04:30 pm", "16:30");


        TEMP_MOdelTime m_23 = new TEMP_MOdelTime("05:00 pm", "17:00");
        TEMP_MOdelTime m_24 = new TEMP_MOdelTime("05:30 pm", "17:30");

        TEMP_MOdelTime m_25 = new TEMP_MOdelTime("06:00 pm", "18:00");
        TEMP_MOdelTime m_26 = new TEMP_MOdelTime("06:30 pm", "18:30");


        TEMP_MOdelTime m_27 = new TEMP_MOdelTime("07:00 pm", "19:00");
        TEMP_MOdelTime m_28 = new TEMP_MOdelTime("07:30 pm", "19:30");


        TEMP_MOdelTime m_29 = new TEMP_MOdelTime("08:00 pm", "20:00");
        TEMP_MOdelTime m_30 = new TEMP_MOdelTime("08:30 pm", "20:30");


        TEMP_MOdelTime m_31 = new TEMP_MOdelTime("09:00 pm", "21:00");


        Model_Time_list.add(m_1);
        Model_Time_list.add(m_2);
        Model_Time_list.add(m_3);
        Model_Time_list.add(m_4);
        Model_Time_list.add(m_5);
        Model_Time_list.add(m_6);
        Model_Time_list.add(m_7);
        Model_Time_list.add(m_8);
        Model_Time_list.add(m_9);
        Model_Time_list.add(m_10);
        Model_Time_list.add(m_11);
        Model_Time_list.add(m_12);
        Model_Time_list.add(m_13);
        Model_Time_list.add(m_14);
        Model_Time_list.add(m_15);
        Model_Time_list.add(m_16);
        Model_Time_list.add(m_17);
        Model_Time_list.add(m_18);
        Model_Time_list.add(m_19);
        Model_Time_list.add(m_20);
        Model_Time_list.add(m_21);
        Model_Time_list.add(m_22);
        Model_Time_list.add(m_23);
        Model_Time_list.add(m_24);
        Model_Time_list.add(m_25);
        Model_Time_list.add(m_26);
        Model_Time_list.add(m_27);
        Model_Time_list.add(m_28);
        Model_Time_list.add(m_29);
        Model_Time_list.add(m_30);
        Model_Time_list.add(m_31);


    }


    private boolean isVisible;
    private boolean isStarted;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
                ll_selected.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
                ll_selected.setVisibility(View.GONE);
            }
        }

    }

    private void setData() {
        if (listview_prize != null) {

            listview_prize.setVisibility(View.VISIBLE);
            ll_selected.setVisibility(View.GONE);

            data_prize_list = new ArrayList<>();


            if (profile_data_Salon != null) {
                if (profile_data_Salon.getSalonSubservices() != null) {
                    if (profile_data_Salon.getSalonSubservices().size() > 0) {
                        data_prize_list.addAll(profile_data_Salon.getSalonSubservices());
                    } else {
                        rl_next_service.setVisibility(View.GONE);
                    }
                } else {
                    rl_next_service.setVisibility(View.GONE);
                }
            } else {
                rl_next_service.setVisibility(View.GONE);
            }

            Adapter_Prize adapter_prize = new Adapter_Prize(context, data_prize_list);
            listview_prize.setAdapter(adapter_prize);

            Model_Date_list = new ArrayList<>();
            Model_Time_list = new ArrayList<>();

            getDays();
            getTimes();


            rv_date.setNestedScrollingEnabled(false);
            rv_date.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            rv_time.setNestedScrollingEnabled(false);
            rv_time.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));


            Adapter_Date adapter_cat = new Adapter_Date(context, Model_Date_list);
            rv_date.setAdapter(adapter_cat);

            Adapter_Time adapter_time = new Adapter_Time(context, Model_Time_list);
            rv_time.setAdapter(adapter_time);


        }

    }

    class Adapter_Prize extends BaseAdapter {

        Context context;
        List<Salonn_SalonSubservice> listState;
        LayoutInflater inflater;

        boolean is_selected = false;
        int pos_selected = -1;

        Adapter_Prize(Context applicationContext, List<Salonn_SalonSubservice> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_servicelist, viewGroup, false);
                viewHolder.ll_mainprizelayout = view.findViewById(R.id.ll_mainprizelayout);
                viewHolder.tv_treatmentname = view.findViewById(R.id.tv_treatmentname);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                viewHolder.tv_prize = view.findViewById(R.id.tv_prize);
                viewHolder.tv_time = view.findViewById(R.id.tv_time);
                viewHolder.view_line = view.findViewById(R.id.view_line);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            if (listState.size() - 1 == position) {
                viewHolder.view_line.setVisibility(View.GONE);
            } else {
                viewHolder.view_line.setVisibility(View.VISIBLE);
            }

            if (listState.get(position).getService() != null) {
                viewHolder.tv_treatmentname.setText("" + listState.get(position).getService().getName());
            }


            if (listState.get(position).is_chechkbox) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_medium(context));
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
            }

            viewHolder.tv_prize.setText("£" + utills.roundTwoDecimals(listState.get(position).getPrice()));
            viewHolder.tv_time.setText("" + listState.get(position).getTime() + "m");


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.ll_mainprizelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listState.get(position).is_chechkbox) {
                        finalViewHolder.tv_treatmentname.setTypeface(utills.customTypeface_medium(context));
                        finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                        listState.get(position).is_chechkbox = false;

                    } else {
                        finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                        listState.get(position).is_chechkbox = true;
                        finalViewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));

                    }


                }
            });


            return view;
        }

        public class ViewHolder {
            LinearLayout ll_mainprizelayout;
            TextView tv_treatmentname, tv_prize, tv_time;
            ImageView iv_checkbox;
            View view_line;
        }

    }


    class Adapter_Prize12 extends BaseAdapter {

        Context context;
        List<Salonn_SalonSubservice> listState;
        LayoutInflater inflater;

        Adapter_Prize12(Context applicationContext, List<Salonn_SalonSubservice> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_servicelist, viewGroup, false);
                viewHolder.ll_mainprizelayout = view.findViewById(R.id.ll_mainprizelayout);
                viewHolder.tv_treatmentname = view.findViewById(R.id.tv_treatmentname);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                viewHolder.tv_prize = view.findViewById(R.id.tv_prize);
                viewHolder.tv_time = view.findViewById(R.id.tv_time);
                viewHolder.view_line = view.findViewById(R.id.view_line);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
            viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));


            if (listState.size() - 1 == position) {
                viewHolder.view_line.setVisibility(View.GONE);
            } else {
                viewHolder.view_line.setVisibility(View.VISIBLE);
            }

            if (listState.get(position).getService() != null) {
                viewHolder.tv_treatmentname.setText("" + listState.get(position).getService().getName());
            }


            viewHolder.tv_prize.setText("£" +utills.roundTwoDecimals( listState.get(position).getPrice()));
            viewHolder.tv_time.setText("" + listState.get(position).getTime() + "m");


            return view;
        }

        public class ViewHolder {
            LinearLayout ll_mainprizelayout;
            TextView tv_treatmentname, tv_prize, tv_time;
            ImageView iv_checkbox;
            View view_line;
        }

    }


    void getDays() {
        SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat format_day = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Calendar mod = Calendar.getInstance();
        mod.setTime(mod.getTime());

        for (int i = 0; i < 7; ++i) {

            TEMP_MOdelDate temp_mOdelDate = new TEMP_MOdelDate();
            temp_mOdelDate.Model_Date = "" + format_date.format(mod.getTime());
            temp_mOdelDate.Model_Name = "" + format_day.format(mod.getTime());
            Model_Date_list.add(temp_mOdelDate);


            mod.add(Calendar.DATE, 1);
        }

        TEMP_MOdelDate temp_mOdelDate1 = new TEMP_MOdelDate();
        temp_mOdelDate1.Model_Date = "";
        temp_mOdelDate1.Model_Name = "Custom";
        Model_Date_list.add(temp_mOdelDate1);


    }


    List<TEMP_MOdelDate> Model_Date_list = new ArrayList<>();
    List<TEMP_MOdelTime> Model_Time_list = new ArrayList<>();

    class TEMP_MOdelDate {
        String Model_Date = "";
        String Model_Name = "";
    }

    class TEMP_MOdelTime {
        String Model_time = "";
        String Final_Model_time = "";

        public TEMP_MOdelTime(String s_time, String s1_final_time) {
            Model_time = s_time;
            Final_Model_time = s1_final_time;
        }
    }

    String Final_date = "";
    String Final_time = "";

    class Adapter_Date extends RecyclerView.Adapter<Adapter_Date.MyViewHolder> {
        private List<TEMP_MOdelDate> countries_list;

        Context mcontext;
        int selectposition = -1;

        public Adapter_Date(Context context, List<TEMP_MOdelDate> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);
            }
        }


        @Override
        public Adapter_Date.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new Adapter_Date.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Date.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText("" + countries_list.get(position).Model_Name);

            if (position == 0) {
                viewHolder.tv_name.setText("Today");
            } else if (position == 1) {
                viewHolder.tv_name.setText("Tomorrow");
            }

            if (selectposition == position) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }

            final Calendar newCalendar_date = Calendar.getInstance();

            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectposition = position;


                    if(countries_list.get(position).Model_Name.equalsIgnoreCase("Custom")){

                        DatePickerDialog timePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);


                                int month = monthOfYear + 1;

                                String month_final = "";
                                String day_final = "";

                                if (month < 10) {
                                    month_final = "0" + month;
                                }else {
                                    month_final = "" + month;
                                }

                                if (dayOfMonth < 10) {
                                    day_final = "0" + dayOfMonth;
                                }else {
                                    day_final = "" + dayOfMonth;
                                }


                                countries_list.get(position).Model_Date=""+"" + year + "-" + month_final + "-" + day_final;

                                Final_date = ""+"" + year + "-" + month_final + "-" + day_final;

                                Log.e("date",""+"" + year + "-" + month_final + "-" + day_final );

                            }

                        }, newCalendar_date.get(Calendar.YEAR), newCalendar_date.get(Calendar.MONTH), newCalendar_date.get(Calendar.DAY_OF_MONTH));
                        timePickerDialog.show();
                        timePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                    }else {

                        Final_date = "" + countries_list.get(position).Model_Date;


                    }

                    notifyDataSetChanged();



                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }

    class Adapter_Time extends RecyclerView.Adapter<Adapter_Time.MyViewHolder> {
        private List<TEMP_MOdelTime> countries_list;
        int selectposition = -1;
        Context mcontext;

        public Adapter_Time(Context context, List<TEMP_MOdelTime> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);
            }
        }


        @Override
        public Adapter_Time.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new Adapter_Time.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Time.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText("" + countries_list.get(position).Model_time);


            if (selectposition == position) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }


            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectposition = position;
                    Final_time = "" + countries_list.get(position).Final_Model_time;
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


}
