package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Customer.Customer_MainProfile_Activty;
import com.moremy.style.R;
import com.moremy.style.Salon.Salon_MainProfile_Activty;
import com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class Login_activty extends Base1_Activity {


    LinearLayout ll_signup, ll_login;
    EditText et_password, et_email;
    Dialog progressDialog = null;
    Context context;
    Preferences preferences;
    CardView card_error;
    TextView tv_error;

    LinearLayout ll_facebook_login, ll_google_login;

  //  LoginButton btn_fb_login;

    private GoogleSignInClient mGoogleApiClient;
    CallbackManager callbackManager;

    ImageView iv_show_hide_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        context = Login_activty.this;
        preferences = new Preferences(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = GoogleSignIn.getClient(this, gso);


        findview();


    }

    private void findview() {


      //  btn_fb_login = findViewById(R.id.btn_fb_login);
        iv_show_hide_password = findViewById(R.id.iv_show_hide_password);
        card_error = findViewById(R.id.card_error);
        ll_signup = findViewById(R.id.ll_signup);
        ll_login = findViewById(R.id.ll_login);
        et_password = findViewById(R.id.et_password);
        et_email = findViewById(R.id.et_email);
        TextView tv_forgot_password = findViewById(R.id.tv_forgot_password);
        tv_error = findViewById(R.id.tv_error);
        ll_facebook_login = findViewById(R.id.ll_facebook_login);
        ll_google_login = findViewById(R.id.ll_google_login);


        iv_show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });


        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ForgotPassWord_Activty.class);
                startActivity(i);
            }
        });


        ll_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                preferences.setPRE_Is_Premium(false);
                preferences.setPRE_Is_Premium_Salon(false);


                preferences.setPRE_Email("" + et_email.getText().toString());
                preferences.setPRE_Password("" + et_password.getText().toString());

                preferences.setPRE_FirstName("");
                preferences.setPRE_LastName("");
                preferences.setPRE_UserName("");


                if (!et_email.getText().toString().equalsIgnoreCase("")) {
                    method_call_email_chechk_api();
                } else {
                    card_error.setVisibility(View.INVISIBLE);
                    tv_error.setText("");
                    Intent i = new Intent(context, RegisterType_Activty.class);
                    startActivity(i);
                }


            }
        });


        ll_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                card_error.setVisibility(View.INVISIBLE);
                tv_error.setText("");
                Login_Api();
            }
        });

        et_password.setTransformationMethod(new MyPasswordTransformationMethod());

        ll_google_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(ll_google_login);

                if (mGoogleApiClient != null) {
                    mGoogleApiClient.signOut();
                }

                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
                handleSignInResult(account);

            }
        });

        ll_facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utills.animationPopUp(ll_facebook_login);

                card_error.setVisibility(View.INVISIBLE);
                tv_error.setText("");
             //   btn_fb_login.performClick();
            }
        });

       // FacebookSdk.sdkInitialize(getApplicationContext());
      //  FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
        callbackManager = CallbackManager.Factory.create();
//        btn_fb_login.setReadPermissions(Arrays.asList("email", "public_profile"));
//        btn_fb_login.registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//
//                        System.out.println("onSuccess");
//
//                        String accessToken = loginResult.getAccessToken().getToken();
//                        Log.i("accessToken", accessToken);
//
//                        GraphRequest request = GraphRequest.newMeRequest(
//                                loginResult.getAccessToken(),
//                                new GraphRequest.GraphJSONObjectCallback() {
//                                    @Override
//                                    public void onCompleted(JSONObject object,
//                                                            GraphResponse response) {
//
//                                        try {
//                                            String id = object.getString("id");
//                                            try {
//
//                                                String profile_pic = "https://graph.facebook.com/" + id + "/picture?type=normal";
//                                                String first_name = object.getString("first_name");
//                                                String last_name = object.getString("last_name");
//
//
//                                                String email = "";
//                                                if (object.has("email")) {
//                                                    email = object.getString("email");
//                                                } else {
//                                                    email = "";
//                                                }
//
//
//                                                method_set_other_login_data(first_name, last_name, email, profile_pic);
//
//
//                                                Log.e("fb_success", response.toString());
//
//
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//
//
//                                        } catch (JSONException e) {
//
//
//                                            Toast toast = Toast.makeText(context, "Please Verify Your Email with Facebook Account", Toast.LENGTH_LONG);
//                                            toast.setGravity(Gravity.TOP, 0, 0);
//                                            toast.show();
//
//
//                                        }
//                                    }
//                                });
//                        Bundle parameters = new Bundle();
//                        parameters.putString("fields", "first_name,last_name,email,id,gender");
//                        request.setParameters(parameters);
//                        request.executeAsync();
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        System.out.println("onCancel");
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        System.out.println("onError");
//                        Log.e("LoginActivity", exception + "");
//                    }
//                });


        LinearLayout ll_guest_login = findViewById(R.id.ll_guest_login);
        ll_guest_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                preferences.setPRE_SendBirdUserName("");
                preferences.setPRE_SendBirdUserId("");
                preferences.setPRE_TOKEN("");


                preferences.setPRE_Typee("2");
                Intent i = new Intent(context, Customer_MainProfile_Activty.class);
                startActivity(i);
                finish();


            }
        });

    }


    /*........................*/


    private void method_call_email_chechk_api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_check_email)
                    .addBodyParameter("email", "" + et_email.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    card_error.setVisibility(View.INVISIBLE);
                                    tv_error.setText("");
                                    Intent i = new Intent(context, RegisterType_Activty.class);
                                    startActivity(i);

                                } else {

                                    Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    /*........................*/


    boolean is_show_pwd = false;

    public void hide_unhide_password() {
        if (is_show_pwd == false) {
            iv_show_hide_password.setImageResource(R.drawable.ic_eye);
            et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password.setSelection(et_password.getText().length());
            is_show_pwd = true;

        } else if (is_show_pwd == true) {
            iv_show_hide_password.setImageResource(R.drawable.ic_hide);
            et_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password.setSelection(et_password.getText().length());

            is_show_pwd = false;
        }
    }


    private static final int RC_SIGN_IN = 007;

    public void handleSignInResult(GoogleSignInAccount acct) {
        if (acct != null) {
            String PhotoUrl = "", Email = "", firstname = "", lastname = "", ID = "";
            if (acct.getId() != null)
                ID = acct.getId();
            if (acct.getGivenName() != null)
                firstname = acct.getGivenName();
            if (acct.getFamilyName() != null)
                lastname = acct.getFamilyName();
            if (acct.getPhotoUrl() != null)
                PhotoUrl = acct.getPhotoUrl().toString();
            if (acct.getEmail() != null)
                Email = acct.getEmail();

            if (mGoogleApiClient != null) {
                mGoogleApiClient.signOut();
            }

            method_set_other_login_data(firstname, lastname, Email, PhotoUrl);


        } else {

            Intent signInIntent = mGoogleApiClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);

        }
    }

    private void getProfileGoogleData(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
            if (acct != null) {

                String PhotoUrl = "", Email = "", firstname = "", lastname = "", ID = "";
                if (acct.getId() != null)
                    ID = acct.getId();
                if (acct.getGivenName() != null)
                    firstname = acct.getGivenName();
                if (acct.getFamilyName() != null)
                    lastname = acct.getFamilyName();
                if (acct.getPhotoUrl() != null)
                    PhotoUrl = acct.getPhotoUrl().toString();
                if (acct.getEmail() != null)
                    Email = acct.getEmail();

                if (mGoogleApiClient != null) {
                    mGoogleApiClient.signOut();
                }


                method_set_other_login_data(firstname, lastname, Email, PhotoUrl);


            }
        } catch (ApiException e) {
            Log.e("getProfileGoogleData: ", "" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void method_set_other_login_data(String firstname, String lastname, String email, String photoUrl) {


        preferences.setPRE_FirstName("" + firstname);
        preferences.setPRE_LastName("" + lastname);
        preferences.setPRE_Email("" + email);

        photoUrl_final = photoUrl;
        method_download_image(photoUrl);

        method_call_email_chechk(email);


    }

    String photoUrl_final = "";

    private void method_call_email_chechk(String email) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_check_email)
                    .addBodyParameter("email", "" + email)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    Intent i = new Intent(context, RegisterType_Activty.class);
                                    startActivity(i);

                                } else {

                                    Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    private void method_download_image(String profile_pic) {


        if (!utills.Permissions_READ_EXTERNAL_STORAGE(context)) {
            utills.Request_READ_EXTERNAL_STORAGE(Login_activty.this);
        } else {

            String root = Environment.getExternalStorageDirectory().getAbsolutePath();
            File file = new File(root + "/" + context.getString(R.string.app_name), ".Temp_images");
            if (!file.exists()) {
                file.mkdirs();
                file.mkdir();
            }


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            Time time = new Time(System.currentTimeMillis());


            final File output_file = new File(file, sdf.format(time));

            AndroidNetworking.download(profile_pic, file.getAbsolutePath(), sdf.format(time))
                    .setTag("downloadTest")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            // do anything with progress
                        }
                    })
                    .startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            Log.e("download", "download");


                            preferences.setPRE_SelectedFile_profile("" + output_file.getAbsolutePath());

                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("download", "" + error.getMessage());
                        }
                    });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    method_download_image(photoUrl_final);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }

        try {
            if (requestCode == RC_SIGN_IN) {
                //-------------------------Google-------------------
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                getProfileGoogleData(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public class MyPasswordTransformationMethod extends HideReturnsTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new MyPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }


    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
     //   if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
//        }
//        this.doubleBackToExitPressedOnce = true;
//        getFragmentManager().popBackStack();
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
//        return;

    }


    public void Login_Api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.post(Global_Service_Api.API_Login)
                    .addBodyParameter("email", "" + et_email.getText().toString())
                    .addBodyParameter("password", "" + et_password.getText().toString())
                    .addBodyParameter("device", "1")
                    .addBodyParameter("device_token", "" + preferences.getPRE_FCMtoken())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    preferences.setPRE_SendBirdUserName("");
                                    preferences.setPRE_SendBirdUserId("");

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String s_token = "" + jsonObject1.get("token");
                                    preferences.setPRE_TOKEN(s_token);


                                    String user_type = "" + jsonObject1.getString("user_type");
                                    preferences.setPRE_Typee("" + user_type);
                                    if (user_type.equalsIgnoreCase("3")) {
                                        preferences.setPRE_LoginType_Stylist_Seller("3");
                                        Intent i = new Intent(context, StylistSeller_MainProfile_Activty.class);
                                        startActivity(i);
                                    } else if (user_type.equalsIgnoreCase("4")) {

                                        Intent i = new Intent(context, Salon_MainProfile_Activty.class);
                                        startActivity(i);
                                    } else if (user_type.equalsIgnoreCase("2")) {
                                        Intent i = new Intent(context, Customer_MainProfile_Activty.class);
                                        startActivity(i);
                                    } else if (user_type.equalsIgnoreCase("5")) {
                                        preferences.setPRE_LoginType_Stylist_Seller("5");
                                        Intent i = new Intent(context, StylistSeller_MainProfile_Activty.class);
                                        startActivity(i);
                                    }


                                    finish();

                                    Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                                } else {
                                    tv_error.setText("" + message);
                                    card_error.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }


}
