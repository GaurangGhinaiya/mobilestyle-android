
package com.moremy.style.CommanActivity.Booking_Fragment.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_BookingDetails_Data {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("user_id")
    @Expose
    private Integer userId;

    @SerializedName("stylist_id")
    @Expose
    private Integer stylistId;
    @SerializedName("service_price")
    @Expose
    private float servicePrice;
    @SerializedName("send_bird_id")
    @Expose
    private String sendBirdId;
    @SerializedName("booking_date")
    @Expose
    private String bookingDate;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("is_paid")
    @Expose
    private Integer isPaid;
    @SerializedName("confirm_date")
    @Expose
    private String confirmDate;
    @SerializedName("complete_date")
    @Expose
    private String completeDate;
    @SerializedName("cancel_date")
    @Expose
    private String cancelDate;
    @SerializedName("cancel_by")
    @Expose
    private Integer cancelBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private ModelBooking_User user;
    @SerializedName("stylist")
    @Expose
    private ModelBooking_Stylist stylist;
    @SerializedName("booking_service_item")
    @Expose
    private List<ModelBooking_BookingServiceItem> bookingServiceItem = null;

    @SerializedName("payment")
    @Expose
    private Model_BookingDetails_Payment payment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStylistId() {
        return stylistId;
    }

    public void setStylistId(Integer stylistId) {
        this.stylistId = stylistId;
    }

    public float getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(float servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getSendBirdId() {
        return sendBirdId;
    }

    public void setSendBirdId(String sendBirdId) {
        this.sendBirdId = sendBirdId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public String getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(String confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Integer getCancelBy() {
        return cancelBy;
    }

    public void setCancelBy(Integer cancelBy) {
        this.cancelBy = cancelBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ModelBooking_User getUser() {
        return user;
    }

    public void setUser(ModelBooking_User user) {
        this.user = user;
    }

    public ModelBooking_Stylist getStylist() {
        return stylist;
    }

    public void setStylist(ModelBooking_Stylist stylist) {
        this.stylist = stylist;
    }

    public List<ModelBooking_BookingServiceItem> getBookingServiceItem() {
        return bookingServiceItem;
    }

    public void setBookingServiceItem(List<ModelBooking_BookingServiceItem> bookingServiceItem) {
        this.bookingServiceItem = bookingServiceItem;
    }

    public Model_BookingDetails_Payment getPayment() {
        return payment;
    }

    public void setPayment(Model_BookingDetails_Payment payment) {
        this.payment = payment;
    }



    @SerializedName("booking_rating")
    @Expose
    private Model_BookingDetails_Review booking_rating;

    public Model_BookingDetails_Review getbooking_rating() {
        return booking_rating;
    }

    public void setbooking_rating(Model_BookingDetails_Review booking_rating) {
        this.booking_rating = booking_rating;
    }



}
