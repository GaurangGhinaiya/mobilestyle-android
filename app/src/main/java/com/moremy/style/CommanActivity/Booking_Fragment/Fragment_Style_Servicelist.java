package com.moremy.style.CommanActivity.Booking_Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_Adresss_Data;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_Adresss_Response;
import com.moremy.style.CommanModel.Stylistt_Data;
import com.moremy.style.CommanModel.Stylistt_StylistSubservice;
import com.moremy.style.R;
import com.moremy.style.Utills.AutoCompleteTextviewCustom;
import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Style_Servicelist extends Fragment {

    private Context context;

    Stylistt_Data profile_data_Stylist;


    public Fragment_Style_Servicelist(Stylistt_Data profile_data_StylistOnly1) {
        profile_data_Stylist = profile_data_StylistOnly1;
    }

    public Fragment_Style_Servicelist() {
    }

    List<Stylistt_StylistSubservice> data_prize_list = new ArrayList<>();

    public static GridView listview_prize;
    GridView listview_SelectedCountiy;
    public static LinearLayout ll_selected, ll_select_Address;


    RecyclerView rv_time, rv_date;

    Preferences preferences;
    List<Stylistt_StylistSubservice> list_services_test = new ArrayList<>();

    String token = "pk.eyJ1IjoiZm9yZXNpZ2h0dGVjaG5vbG9naWVzIiwiYSI6ImNqa3phMXBiNDBydjczcXFwNWQzbWlqNnkifQ.fQQWorOl5PVt4s4M0pwYaA";

    TextView tv_add_address;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylist_servicelist, container, false);
        context = getActivity();

        preferences = new Preferences(context);
        data_prize_list = new ArrayList<>();

        listview_prize = view.findViewById(R.id.listview_countiy);
        listview_SelectedCountiy = view.findViewById(R.id.listview_SelectedCountiy);
        ll_selected = view.findViewById(R.id.ll_selected);

        rv_date = view.findViewById(R.id.rv_date);
        rv_time = view.findViewById(R.id.rv_time);

        listview_prize.setVisibility(View.VISIBLE);
        ll_selected.setVisibility(View.GONE);

        list_services_test = new ArrayList<>();

        ll_select_Address = view.findViewById(R.id.ll_select_Address);

        listview_address = view.findViewById(R.id.listview_address);
        tv_add_address = view.findViewById(R.id.tv_add_address);


        RelativeLayout rl_next_service = view.findViewById(R.id.rl_next_service);
        rl_next_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getPRE_TOKEN().equalsIgnoreCase("")) {
                    utills.method_guest_redirect_login(context);
                } else {

                    if (ll_selected.getVisibility() == View.GONE) {
                        list_services_test = new ArrayList<>();
                        if (data_prize_list != null) {
                            if (data_prize_list.size() > 0) {
                                for (int i = 0; i < data_prize_list.size(); i++) {
                                    if (data_prize_list.get(i).is_chechkbox) {
                                        list_services_test.add(data_prize_list.get(i));
                                    }
                                }
                            }
                        }

                        if (list_services_test.size() > 0) {

                            listview_prize.setVisibility(View.GONE);
                            ll_selected.setVisibility(View.VISIBLE);

                            Adapter_Prize12 adapter_prize = new Adapter_Prize12(context, list_services_test);
                            listview_SelectedCountiy.setAdapter(adapter_prize);

                        } else {
                            Toast.makeText(context, "Please select Services", Toast.LENGTH_SHORT).show();
                        }

                    } else if (ll_selected.getVisibility() == View.VISIBLE && ll_select_Address.getVisibility() == View.GONE) {

                        if (Final_date.equalsIgnoreCase("")) {
                            Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show();
                        } else if (Final_time.equalsIgnoreCase("")) {
                            Toast.makeText(context, "Please select time", Toast.LENGTH_SHORT).show();
                        } else {

                            ll_select_Address.setVisibility(View.VISIBLE);
                            Dialog_address(view);

                        }


                    } else if (ll_selected.getVisibility() == View.VISIBLE && ll_select_Address.getVisibility() == View.VISIBLE) {

                        if (Final_City.equalsIgnoreCase("")) {
                            Toast.makeText(context, "Please select Your Address", Toast.LENGTH_SHORT).show();
                        } else if (Final_Pincode.equalsIgnoreCase("")) {
                            Toast.makeText(context, "Please select Your Address", Toast.LENGTH_SHORT).show();
                        } else {
                            method_next_screen();
                        }


                    }

                }

            }
        });

        search_list = new ArrayList<>();
        search_list_postcode = new ArrayList<>();


        method_get_city();

        method_get_Address_Book();


        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                GPSTracker gpsTracker = new GPSTracker(context);
                if (gpsTracker != null) {
                    if (gpsTracker.getIsGPSTrackingEnabled()) {
                        getLongitude = gpsTracker.getLongitude();
                        getLatitude = gpsTracker.getLatitude();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }


    double getLongitude = 0;
    double getLatitude = 0;

    private void makeGeocodeSearch_postalcode(String s) {

        try {
            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + s)
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {


                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {
                                search_list_postcode = new ArrayList<>();

                                for (int i = 0; i < results.size(); i++) {
                                    String name = "" + results.get(i).placeName();
                                    if (name.contains(City_Name)) {
                                        search_list_postcode.add("" + results.get(i).text());
                                    }
                                }

                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {

                                            if (et_code != null) {
                                                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                                        (context, R.layout.autotextview_layout1, search_list_postcode);

                                                et_code.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();
                                            }

                                        }
                                    });
                                }


                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.e("maps", "failed");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Log.e("maps", "failed");
                }
            });
        } catch (ServicesException servicesException) {
            Log.e("maps", "failed");
            servicesException.printStackTrace();
        }
    }


    Spinner spiner_city;
    ArrayList<String> search_list = new ArrayList<>();
    AutoCompleteTextviewCustom et_code;
    ArrayList<String> search_list_postcode = new ArrayList<>();


    private void getTimes() {


        TEMP_MOdelTime m_1 = new TEMP_MOdelTime("06:00 am", "06:00");
        TEMP_MOdelTime m_2 = new TEMP_MOdelTime("06:30 am", "06:30");

        TEMP_MOdelTime m_3 = new TEMP_MOdelTime("07:00 am", "07:00");
        TEMP_MOdelTime m_4 = new TEMP_MOdelTime("07:30 am", "07:30");

        TEMP_MOdelTime m_5 = new TEMP_MOdelTime("08:00 am", "08:00");
        TEMP_MOdelTime m_6 = new TEMP_MOdelTime("08:30 am", "08:30");

        TEMP_MOdelTime m_7 = new TEMP_MOdelTime("09:00 am", "09:00");
        TEMP_MOdelTime m_8 = new TEMP_MOdelTime("09:30 am", "09:30");

        TEMP_MOdelTime m_9 = new TEMP_MOdelTime("10:00 am", "10:00");
        TEMP_MOdelTime m_10 = new TEMP_MOdelTime("10:30 am", "10:30");

        TEMP_MOdelTime m_11 = new TEMP_MOdelTime("11:00 am", "11:00");
        TEMP_MOdelTime m_12 = new TEMP_MOdelTime("11:30 am", "11:30");


        TEMP_MOdelTime m_13 = new TEMP_MOdelTime("12:00 am", "12:00");
        TEMP_MOdelTime m_14 = new TEMP_MOdelTime("12:30 pm", "12:30");


        TEMP_MOdelTime m_15 = new TEMP_MOdelTime("01:00 pm", "13:00");
        TEMP_MOdelTime m_16 = new TEMP_MOdelTime("01:30 pm", "13:30");

        TEMP_MOdelTime m_17 = new TEMP_MOdelTime("02:00 pm", "14:00");
        TEMP_MOdelTime m_18 = new TEMP_MOdelTime("02:30 pm", "14:30");

        TEMP_MOdelTime m_19 = new TEMP_MOdelTime("03:00 pm", "15:00");
        TEMP_MOdelTime m_20 = new TEMP_MOdelTime("03:30 pm", "15:30");

        TEMP_MOdelTime m_21 = new TEMP_MOdelTime("04:00 pm", "16:00");
        TEMP_MOdelTime m_22 = new TEMP_MOdelTime("04:30 pm", "16:30");


        TEMP_MOdelTime m_23 = new TEMP_MOdelTime("05:00 pm", "17:00");
        TEMP_MOdelTime m_24 = new TEMP_MOdelTime("05:30 pm", "17:30");

        TEMP_MOdelTime m_25 = new TEMP_MOdelTime("06:00 pm", "18:00");
        TEMP_MOdelTime m_26 = new TEMP_MOdelTime("06:30 pm", "18:30");


        TEMP_MOdelTime m_27 = new TEMP_MOdelTime("07:00 pm", "19:00");
        TEMP_MOdelTime m_28 = new TEMP_MOdelTime("07:30 pm", "19:30");


        TEMP_MOdelTime m_29 = new TEMP_MOdelTime("08:00 pm", "20:00");
        TEMP_MOdelTime m_30 = new TEMP_MOdelTime("08:30 pm", "20:30");


        TEMP_MOdelTime m_31 = new TEMP_MOdelTime("09:00 pm", "21:00");


        Model_Time_list.add(m_1);
        Model_Time_list.add(m_2);
        Model_Time_list.add(m_3);
        Model_Time_list.add(m_4);
        Model_Time_list.add(m_5);
        Model_Time_list.add(m_6);
        Model_Time_list.add(m_7);
        Model_Time_list.add(m_8);
        Model_Time_list.add(m_9);
        Model_Time_list.add(m_10);
        Model_Time_list.add(m_11);
        Model_Time_list.add(m_12);
        Model_Time_list.add(m_13);
        Model_Time_list.add(m_14);
        Model_Time_list.add(m_15);
        Model_Time_list.add(m_16);
        Model_Time_list.add(m_17);
        Model_Time_list.add(m_18);
        Model_Time_list.add(m_19);
        Model_Time_list.add(m_20);
        Model_Time_list.add(m_21);
        Model_Time_list.add(m_22);
        Model_Time_list.add(m_23);
        Model_Time_list.add(m_24);
        Model_Time_list.add(m_25);
        Model_Time_list.add(m_26);
        Model_Time_list.add(m_27);
        Model_Time_list.add(m_28);
        Model_Time_list.add(m_29);
        Model_Time_list.add(m_30);
        Model_Time_list.add(m_31);


    }


    private boolean isVisible;
    private boolean isStarted;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
                ll_selected.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
                ll_selected.setVisibility(View.GONE);
            }
        }

    }

    private void setData() {
        if (listview_prize != null) {

            listview_prize.setVisibility(View.VISIBLE);
            ll_selected.setVisibility(View.GONE);

            data_prize_list = new ArrayList<>();


            if (profile_data_Stylist != null) {
                data_prize_list.addAll(profile_data_Stylist.getStylistSubservices());

                // City_Name = "" + profile_data_Stylist.getCity();
            }

            Adapter_Prize adapter_prize = new Adapter_Prize(context, data_prize_list);
            listview_prize.setAdapter(adapter_prize);

            Model_Date_list = new ArrayList<>();
            Model_Time_list = new ArrayList<>();

            getDays();
            getTimes();


            rv_date.setNestedScrollingEnabled(false);
            rv_date.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            rv_time.setNestedScrollingEnabled(false);
            rv_time.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));


            Adapter_Date adapter_cat = new Adapter_Date(context, Model_Date_list);
            rv_date.setAdapter(adapter_cat);

            Adapter_Time adapter_time = new Adapter_Time(context, Model_Time_list);
            rv_time.setAdapter(adapter_time);


        }


    }

    class Adapter_Prize extends BaseAdapter {

        Context context;
        List<Stylistt_StylistSubservice> listState;
        LayoutInflater inflater;

        boolean is_selected = false;
        int pos_selected = -1;

        Adapter_Prize(Context applicationContext, List<Stylistt_StylistSubservice> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_servicelist, viewGroup, false);
                viewHolder.ll_mainprizelayout = view.findViewById(R.id.ll_mainprizelayout);
                viewHolder.tv_treatmentname = view.findViewById(R.id.tv_treatmentname);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                viewHolder.tv_prize = view.findViewById(R.id.tv_prize);
                viewHolder.tv_time = view.findViewById(R.id.tv_time);
                viewHolder.view_line = view.findViewById(R.id.view_line);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            if (listState.size() - 1 == position) {
                viewHolder.view_line.setVisibility(View.GONE);
            } else {
                viewHolder.view_line.setVisibility(View.VISIBLE);
            }

            if (listState.get(position).getService() != null) {
                viewHolder.tv_treatmentname.setText("" + listState.get(position).getService().getName());
            }


            if (listState.get(position).is_chechkbox) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_medium(context));
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
            }

            viewHolder.tv_prize.setText("£" + utills.roundTwoDecimals(listState.get(position).getPrice()));
            viewHolder.tv_time.setText("" + listState.get(position).getTime() + "m");


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.ll_mainprizelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (listState.get(position).is_chechkbox) {
                        finalViewHolder.tv_treatmentname.setTypeface(utills.customTypeface_medium(context));
                        finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                        listState.get(position).is_chechkbox = false;

                    } else {
                        finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                        listState.get(position).is_chechkbox = true;
                        finalViewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));

                    }


                }
            });


            return view;
        }

        public class ViewHolder {
            LinearLayout ll_mainprizelayout;
            TextView tv_treatmentname, tv_prize, tv_time;
            ImageView iv_checkbox;
            View view_line;
        }

    }


    class Adapter_Prize12 extends BaseAdapter {

        Context context;
        List<Stylistt_StylistSubservice> listState;
        LayoutInflater inflater;

        Adapter_Prize12(Context applicationContext, List<Stylistt_StylistSubservice> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_servicelist, viewGroup, false);
                viewHolder.ll_mainprizelayout = view.findViewById(R.id.ll_mainprizelayout);
                viewHolder.tv_treatmentname = view.findViewById(R.id.tv_treatmentname);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                viewHolder.tv_prize = view.findViewById(R.id.tv_prize);
                viewHolder.tv_time = view.findViewById(R.id.tv_time);
                viewHolder.view_line = view.findViewById(R.id.view_line);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
            viewHolder.tv_treatmentname.setTypeface(utills.customTypeface_Bold(context));


            if (listState.size() - 1 == position) {
                viewHolder.view_line.setVisibility(View.GONE);
            } else {
                viewHolder.view_line.setVisibility(View.VISIBLE);
            }

            if (listState.get(position).getService() != null) {
                viewHolder.tv_treatmentname.setText("" + listState.get(position).getService().getName());
            }


            viewHolder.tv_prize.setText("£" + utills.roundTwoDecimals(listState.get(position).getPrice()));
            viewHolder.tv_time.setText("" + listState.get(position).getTime() + "m");


            return view;
        }

        public class ViewHolder {
            LinearLayout ll_mainprizelayout;
            TextView tv_treatmentname, tv_prize, tv_time;
            ImageView iv_checkbox;
            View view_line;
        }

    }


    void getDays() {
        SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat format_day = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Calendar mod = Calendar.getInstance();
        mod.setTime(mod.getTime());

        for (int i = 0; i < 7; ++i) {

            TEMP_MOdelDate temp_mOdelDate = new TEMP_MOdelDate();
            temp_mOdelDate.Model_Date = "" + format_date.format(mod.getTime());
            temp_mOdelDate.Model_Name = "" + format_day.format(mod.getTime());
            Model_Date_list.add(temp_mOdelDate);


            mod.add(Calendar.DATE, 1);
        }

        TEMP_MOdelDate temp_mOdelDate1 = new TEMP_MOdelDate();
        temp_mOdelDate1.Model_Date = "";
        temp_mOdelDate1.Model_Name = "Custom";
        Model_Date_list.add(temp_mOdelDate1);


    }


    List<TEMP_MOdelDate> Model_Date_list = new ArrayList<>();
    List<TEMP_MOdelTime> Model_Time_list = new ArrayList<>();

    class TEMP_MOdelDate {
        String Model_Date = "";
        String Model_Name = "";
    }

    class TEMP_MOdelTime {
        String Model_time = "";
        String Final_Model_time = "";

        public TEMP_MOdelTime(String s_time, String s1_final_time) {
            Model_time = s_time;
            Final_Model_time = s1_final_time;
        }
    }

    String Final_date = "";
    String Final_time = "";

    class Adapter_Date extends RecyclerView.Adapter<Adapter_Date.MyViewHolder> {
        private List<TEMP_MOdelDate> countries_list;

        Context mcontext;
        int selectposition = -1;

        public Adapter_Date(Context context, List<TEMP_MOdelDate> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);
            }
        }


        @Override
        public Adapter_Date.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new Adapter_Date.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Date.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText("" + countries_list.get(position).Model_Name);

            if (position == 0) {
                viewHolder.tv_name.setText("Today");
            } else if (position == 1) {
                viewHolder.tv_name.setText("Tomorrow");
            }

            if (selectposition == position) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }


            final Calendar newCalendar_date = Calendar.getInstance();


            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectposition = position;


                    if (countries_list.get(position).Model_Name.equalsIgnoreCase("Custom")) {

                        DatePickerDialog timePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);


                                int month = monthOfYear + 1;

                                String month_final = "";
                                String day_final = "";

                                if (month < 10) {
                                    month_final = "0" + month;
                                } else {
                                    month_final = "" + month;
                                }

                                if (dayOfMonth < 10) {
                                    day_final = "0" + dayOfMonth;
                                } else {
                                    day_final = "" + dayOfMonth;
                                }


                                countries_list.get(position).Model_Date = "" + "" + year + "-" + month_final + "-" + day_final;

                                Final_date = "" + "" + year + "-" + month_final + "-" + day_final;

                                Log.e("date", "" + "" + year + "-" + month_final + "-" + day_final);

                            }

                        }, newCalendar_date.get(Calendar.YEAR), newCalendar_date.get(Calendar.MONTH), newCalendar_date.get(Calendar.DAY_OF_MONTH));
                        timePickerDialog.show();
                        timePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                    } else {

                        Final_date = "" + countries_list.get(position).Model_Date;


                    }

                    notifyDataSetChanged();


                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    class Adapter_Time extends RecyclerView.Adapter<Adapter_Time.MyViewHolder> {
        private List<TEMP_MOdelTime> countries_list;
        int selectposition = -1;
        Context mcontext;

        public Adapter_Time(Context context, List<TEMP_MOdelTime> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_name;
            LinearLayout ll_productt;


            public MyViewHolder(View view) {
                super(view);
                tv_name = (TextView) view.findViewById(R.id.tv_name);
                ll_productt = (LinearLayout) view.findViewById(R.id.ll_productt);
            }
        }


        @Override
        public Adapter_Time.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_product, parent, false);

            return new Adapter_Time.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Time.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_name.setText("" + countries_list.get(position).Model_time);


            if (selectposition == position) {
                viewHolder.ll_productt.setBackgroundResource(R.drawable.bg_gray_light);
                viewHolder.tv_name.setTypeface(utills.customTypeface_Bold(context));
            } else {
                viewHolder.ll_productt.setBackgroundColor(getResources().getColor(R.color.white));
                viewHolder.tv_name.setTypeface(utills.customTypeface_medium(context));
            }


            viewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectposition = position;
                    Final_time = "" + countries_list.get(position).Final_Model_time;
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    String City_Name = "";
    private boolean userIsInteracting;
    NonScrollListView listview_address;

    private void method_get_city() {
        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_cities)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    search_list = new ArrayList<>();


                                    JSONArray jsonObject2 = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonObject2.length(); i++) {
                                        JSONObject jsonObject1 = jsonObject2.getJSONObject(i);
                                        search_list.add("" + jsonObject1.getString("name"));
                                    }
                                    search_list.add("please select");


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    class SpinnerdocumentAdapter1 extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter1(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size() - 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }


    private void Dialog_address(View view) {


        if (data_address_list != null) {
            Adapter_Address adapter_review = new Adapter_Address(context, data_address_list);
            listview_address.setAdapter(adapter_review);
        }

        tv_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_change_Address();
            }
        });


    }


    String Final_Address = "";
    String Final_Pincode = "";
    String Final_City = "";


    private void method_next_screen() {


        float price = 0;
        StringBuilder stringBuilder_ids = new StringBuilder();
        StringBuilder stringBuilder_ids_name = new StringBuilder();
        for (int i = 0; i < list_services_test.size(); i++) {
            if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                stringBuilder_ids.append(",");
            }
            if (!stringBuilder_ids_name.toString().equalsIgnoreCase("")) {
                stringBuilder_ids_name.append(",");
            }
            stringBuilder_ids.append(list_services_test.get(i).getId());
            stringBuilder_ids_name.append("" + list_services_test.get(i).getService().getName()
                    + " (£" + utills.roundTwoDecimals(list_services_test.get(i).getPrice()) + ")");

            price = price + list_services_test.get(i).getPrice();

        }

        Intent i = new Intent(context, ChatBookingActivity.class);
        i.putExtra("Stylish_Id", "" + profile_data_Stylist.getId());
        i.putExtra("grup_Name", "" + stringBuilder_ids_name);
        i.putExtra("grup_price", "" + price);
        i.putExtra("Final_date", "" + Final_date);
        i.putExtra("Final_time", "" + Final_time);
        i.putExtra("Address", "" + Final_Address);
        i.putExtra("ids", "" + stringBuilder_ids);
        i.putExtra("profile_pic", "" + profile_data_Stylist.getProfilePic());
        i.putExtra("city", "" + Final_City);
        i.putExtra("postal_code", "" + Final_Pincode);
        startActivity(i);

        if (getActivity() != null) {
            getActivity().finish();
        }

    }

    List<Model_Adresss_Data> data_address_list = new ArrayList<>();

    public class Adapter_Address extends BaseAdapter {

        Context context;
        List<Model_Adresss_Data> listState;
        LayoutInflater inflater;

        int pos_selected = -1;

        public Adapter_Address(Context applicationContext, List<Model_Adresss_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_Address.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_Address.ViewHolder();
                view = inflater.inflate(R.layout.datalist_address_list, viewGroup, false);
                viewHolder.et_city_name = view.findViewById(R.id.et_city_name);
                viewHolder.et_Address_name = view.findViewById(R.id.et_Address_name);

                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_Address.ViewHolder) view.getTag();
            }


            viewHolder.et_city_name.setText("" + listState.get(position).getPostalCode() + ", " + listState.get(position).getCity());
            viewHolder.et_Address_name.setText("" + listState.get(position).getAddress());


            if (pos_selected == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);

            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            final Adapter_Address.ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    pos_selected = position;
                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);


                    Final_City = listState.get(position).getCity();
                    Final_Address = listState.get(position).getAddress();
                    Final_Pincode = listState.get(position).getPostalCode();

                    notifyDataSetChanged();
                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_checkbox;
            TextView et_city_name, et_Address_name;
            CardView card_mylist;

        }

    }

    private void method_change_Address() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_address);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        final CardView card_Address = dialog_card.findViewById(R.id.card_Address);
        final TextView tv_cancel_Address = dialog_card.findViewById(R.id.tv_cancel_Address);

        final EditText et_address_line = dialog_card.findViewById(R.id.et_address_line);


        et_code = dialog_card.findViewById(R.id.et_code);
        et_code.setDropDownWidth(getResources().getDisplayMetrics().widthPixels - 50);
        et_code.setThreshold(1);
//        ArrayAdapter<String> adapter_code = new ArrayAdapter<String>
//                (context, R.layout.autotextview_layout1, search_list_postcode);
//        et_code.setAdapter(adapter_code);
//        adapter_code.notifyDataSetChanged();

        spiner_city = dialog_card.findViewById(R.id.spiner_city);
        ImageView iv_spinner_city = dialog_card.findViewById(R.id.iv_spinner_city);
        iv_spinner_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_city.performClick();
            }
        });

        spiner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (userIsInteracting) {
                    userIsInteracting = false;
                } else {

                    Final_City = "" + search_list.get(position);

                    City_Name = Final_City;

                    et_code.setText("");
                    search_list_postcode = new ArrayList<>();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (context, R.layout.autotextview_layout1, search_list_postcode);
                    et_code.setAdapter(adapter);


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        et_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (et_code.getText().toString().length() >= 1) {
                    makeGeocodeSearch_postalcode("" + et_code.getText().toString());
                }

            }
        });
        et_code.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });


        SpinnerdocumentAdapter1 customAdapter_state = new SpinnerdocumentAdapter1(context, search_list);
        spiner_city.setAdapter((SpinnerAdapter) customAdapter_state);
        spiner_city.setSelection(customAdapter_state.getCount());

//
//        if (!City_Name.equalsIgnoreCase("")) {
//            try {
//                for (int i = 0; i < search_list.size(); i++) {
//                    if (City_Name.equalsIgnoreCase(search_list.get(i))) {
//                        //  spiner_city.setSelection(i);
//                        Final_City = City_Name;
//                    }
//                }
//            } catch (Exception e) {
//                spiner_city.setSelection(customAdapter_state.getCount());
//                e.printStackTrace();
//            }
//
//        } else {
//            spiner_city.setSelection(customAdapter_state.getCount());
//        }


        dialog_card.show();


        card_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int is_valid = 0;
                for (int i = 0; i < search_list_postcode.size(); i++) {
                    String postal_code = "" + et_code.getText().toString().trim();
                    if (postal_code.equalsIgnoreCase(search_list_postcode.get(i).toString())) {
                        is_valid = 1;
                    }
                }

                if (is_valid == 1) {

                    method_save_Address(dialog_card, et_address_line.getText().toString(), et_code.getText().toString());

                } else {
                    Toast.makeText(context, "Please select valid postal code", Toast.LENGTH_SHORT).show();
                }


            }
        });

        tv_cancel_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });


    }

    Dialog progressDialog = null;

    private void method_save_Address(final Dialog dialog_card, String et_address_line, String et_code) {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_address)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("address", "" + et_address_line)
                    .addBodyParameter("city", "" + Final_City)
                    .addBodyParameter("postal_code", "" + et_code)
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }


                                    method_get_Address_Book1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_get_Address_Book1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_get_address_book)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_address_book", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_address_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_Adresss_Response Response = new Gson().fromJson(result.toString(), Model_Adresss_Response.class);

                                    if (Response.getData() != null) {
                                        data_address_list = Response.getData();


                                        if (listview_address != null) {
                                            Adapter_Address adapter_review = new Adapter_Address(context, data_address_list);
                                            listview_address.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private void method_get_Address_Book() {

        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_get_address_book)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_address_book", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_address_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {
                                    Model_Adresss_Response Response = new Gson().fromJson(result.toString(), Model_Adresss_Response.class);
                                    if (Response.getData() != null) {
                                        data_address_list = Response.getData();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


}
