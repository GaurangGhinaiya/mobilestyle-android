package com.moremy.style.CommanActivity.Booking_Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Data;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Response;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Salon.Salon_RegisterData_Activty;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import com.moremy.style.chat.utils.ConnectionManager;
import com.moremy.style.chat.utils.FileUtils;
import com.moremy.style.chat.utils.MediaPlayerActivity;
import com.moremy.style.chat.utils.PhotoViewerActivity;

import com.moremy.style.chat.utils.TextUtils;
import com.moremy.style.chat.utils.UrlPreviewInfo;
import com.moremy.style.chat.utils.WebUtils;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import static com.moremy.style.Utills.utills.CONNECTION_HANDLER_ID;


public class BokingGroupChatFragment extends Fragment {

    private static final String LOG_TAG = BokingGroupChatFragment.class.getSimpleName();

    private static final int CHANNEL_LIST_LIMIT = 30;

    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_CHAT";

    private static final int STATE_NORMAL = 0;
    private static final int STATE_EDIT = 1;

    private static final String STATE_CHANNEL_URL = "STATE_CHANNEL_URL";
    private static final int INTENT_REQUEST_CHOOSE_MEDIA = 301;
    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 13;
    static final String EXTRA_CHANNEL_URL = "EXTRA_CHANNEL_URL";

    private InputMethodManager mIMM;
    private HashMap<BaseChannel.SendFileMessageWithProgressHandler, FileMessage> mFileProgressHandlerMap;

    private RelativeLayout mRootLayout;
    private RecyclerView mRecyclerView;
    private BokingGroupChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;
    private EditText mMessageEditText;
    private ImageView mMessageSendButton;
    private ImageView mUploadFileButton;
    private View mCurrentEventLayout;
    private TextView mCurrentEventText;

    private GroupChannel mChannel;
    private String mChannelUrl;
    private PreviousMessageListQuery mPrevMessageListQuery;

    private boolean mIsTyping;

    private int mCurrentState = STATE_NORMAL;
    private BaseMessage mEditingMessage = null;


    public BokingGroupChatFragment(String channelUrl, String order_id1) {
        order_id_booking = "" + order_id1;
        mChannelUrl = channelUrl;
    }

    public BokingGroupChatFragment() {

    }

   static Context context;
    static  Preferences preferences;

    static TextView tv_btn_changedate, tv_btn_confirm, tv_btn_cancel, tv_btn_chechkout, tv_btn_complete, tv_btn_WriteReview;

    Dialog progressDialog = null;
    /*...............................................................*/
    public static String order_id_booking = "";
    static  String is_status = "";

    String BookingDate = "";
    String BookingTime = "";
    static  String Is_Paid = "";
    String is_stylist_id = "";

    public static String chechkout_price = "";
    public static String chechkout_name = "";
    public static String chechkout_InvoiceId = "";
    public static String chechkout_Image = "";
    public static String is_user_id = "";
    public static String chechkout_Last4 = "";
    public static String chechkout_date = "";
    public static String rating_booking = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        preferences = new Preferences(context);

        if (getArguments() != null) {

            is_status = "" + getArguments().getString("is_status");
            chechkout_price = "" + getArguments().getString("chechkout_price");
            chechkout_name = "" + getArguments().getString("chechkout_name");
            chechkout_InvoiceId = "" + getArguments().getString("chechkout_InvoiceId");
            chechkout_Last4 = "" + getArguments().getString("chechkout_Last4");
            chechkout_date = "" + getArguments().getString("chechkout_date");
            chechkout_Image = "" + getArguments().getString("profile_pic");
            is_user_id = "" + getArguments().getString("is_user_id");
            is_stylist_id = "" + getArguments().getString("is_stylist_id");


            BookingDate = "" + getArguments().getString("BookingDate");
            BookingTime = "" + getArguments().getString("BookingTime");
            Is_Paid = "" + getArguments().getString("Is_Paid");
            rating_booking = "" + getArguments().getString("rating");

        }

        mIMM = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mFileProgressHandlerMap = new HashMap<>();

        // Log.d(LOG_TAG, mChannelUrl);

        mChatAdapter = new BokingGroupChatAdapter(getActivity());
        setUpChatListAdapter();

        // Load messages from cache.
        mChatAdapter.load(mChannelUrl);
        mChatAdapter.load(mChannelUrl);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_booking_group_chat, container, false);

        setRetainInstance(true);


        tv_btn_changedate = (TextView) rootView.findViewById(R.id.tv_btn_changedate);
        tv_btn_confirm = (TextView) rootView.findViewById(R.id.tv_btn_confirm);
        tv_btn_cancel = (TextView) rootView.findViewById(R.id.tv_btn_cancel);
        tv_btn_chechkout = (TextView) rootView.findViewById(R.id.tv_btn_chechkout);
        tv_btn_complete = (TextView) rootView.findViewById(R.id.tv_btn_complete);
        tv_btn_WriteReview = (TextView) rootView.findViewById(R.id.tv_btn_WriteReview);


        mRootLayout = (RelativeLayout) rootView.findViewById(R.id.layout_group_chat_root);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_group_chat);

        mCurrentEventLayout = rootView.findViewById(R.id.layout_group_chat_current_event);
        mCurrentEventText = (TextView) rootView.findViewById(R.id.text_group_chat_current_event);

        mMessageEditText = (EditText) rootView.findViewById(R.id.edittext_group_chat_message);
        mMessageSendButton = (ImageView) rootView.findViewById(R.id.button_group_chat_send);
        mUploadFileButton = (ImageView) rootView.findViewById(R.id.button_group_chat_upload);

        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mMessageSendButton.setEnabled(true);
                } else {
                    mMessageSendButton.setEnabled(false);
                }
            }
        });

        mMessageSendButton.setEnabled(false);
        mMessageSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentState == STATE_EDIT) {
                    String userInput = mMessageEditText.getText().toString().trim();
                    if (userInput.length() > 0) {
                        if (mEditingMessage != null) {
                            editMessage(mEditingMessage, userInput);
                        }
                    }
                    setState(STATE_NORMAL, null, -1);
                } else {
                    String userInput = mMessageEditText.getText().toString().trim();
                    if (userInput.length() > 0) {
                        sendUserMessage(userInput);
                        mMessageEditText.setText("");
                    }
                }
            }
        });

        mUploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMedia();
            }
        });

        mIsTyping = false;
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mIsTyping) {
                    setTypingStatus(true);
                }

                if (s.length() == 0) {
                    setTypingStatus(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        setUpRecyclerView();
        setHasOptionsMenu(true);

//        tv_chechk_out_prize.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                method_mycard();
//            }
//        });
//


        data_order_list = new ArrayList<>();
        method_get_mycard();


        tv_btn_chechkout.setText("checkout (£" + utills.roundTwoDecimals(Double.parseDouble(chechkout_price)) + ")");


        tv_btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);

                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_cancel();
                    }
                });


            }
        });


        tv_btn_changedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_modify_dialog();
            }
        });


        tv_btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to confirm?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_confirm();
                    }
                });


            }
        });


// userid == preid == coustomer
        //  status=1 chnage and cancel  =customer


        if (is_user_id.equalsIgnoreCase(preferences.getPRE_SendBirdUserId())) {  // customer

            tv_btn_changedate.setVisibility(View.GONE);
            tv_btn_confirm.setVisibility(View.GONE);
            tv_btn_cancel.setVisibility(View.GONE);
            tv_btn_chechkout.setVisibility(View.GONE);
            tv_btn_complete.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);


            if (is_status.equalsIgnoreCase("1")) {
                tv_btn_changedate.setVisibility(View.VISIBLE);
                tv_btn_cancel.setVisibility(View.VISIBLE);
            } else if (is_status.equalsIgnoreCase("2") && Is_Paid.equalsIgnoreCase("0")) {
                tv_btn_chechkout.setVisibility(View.VISIBLE);
            } else if ((!is_status.equalsIgnoreCase("3")) && Is_Paid.equalsIgnoreCase("1")) {
                tv_btn_complete.setVisibility(View.VISIBLE);
            } else if ((is_status.equalsIgnoreCase("3")) && Is_Paid.equalsIgnoreCase("1")) {

                if (rating_booking.equalsIgnoreCase("") || rating_booking.equalsIgnoreCase("null")) {
                    tv_btn_WriteReview.setVisibility(View.VISIBLE);
                }


            }

        } else {   // stylist


            tv_btn_changedate.setVisibility(View.GONE);
            tv_btn_confirm.setVisibility(View.GONE);
            tv_btn_cancel.setVisibility(View.GONE);
            tv_btn_chechkout.setVisibility(View.GONE);
            tv_btn_complete.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);


            if (is_status.equalsIgnoreCase("1")) {
                tv_btn_changedate.setVisibility(View.VISIBLE);
                tv_btn_confirm.setVisibility(View.VISIBLE);
                tv_btn_cancel.setVisibility(View.VISIBLE);
            }

        }


        tv_btn_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_complete();
            }
        });
        tv_btn_WriteReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_review_dialog();
            }
        });

        tv_btn_chechkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_mycard();
            }
        });
        return rootView;
    }


    private void refresh() {
        if (mChannel == null) {
            GroupChannel.getChannel(mChannelUrl, new GroupChannel.GroupChannelGetHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChannel = groupChannel;
                    mChatAdapter.setChannel(mChannel);
                    mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        } else {
            mChannel.refresh(new GroupChannel.GroupChannelRefreshHandler() {
                @Override
                public void onResult(SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            ConnectionManager.addConnectionManagementHandler(context, CONNECTION_HANDLER_ID, new ConnectionManager.ConnectionManagementHandler() {
                @Override
                public void onConnected(boolean reconnect) {
                    refresh();
                }
            });

            mChatAdapter.setContext(getActivity()); // Glide bug fix (java.lang.IllegalArgumentException: You cannot start a load for a destroyed activity)

            // Gets channel from URL user requested

            Log.d(LOG_TAG, mChannelUrl);

            SendBird.addChannelHandler(CHANNEL_HANDLER_ID, new SendBird.ChannelHandler() {
                @Override
                public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                    if (baseChannel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.markAllMessagesAsRead();
                        // Add new message to view
                        mChatAdapter.addFirst(baseMessage);
                    }
                }

                @Override
                public void onMessageDeleted(BaseChannel baseChannel, long msgId) {
                    super.onMessageDeleted(baseChannel, msgId);
                    if (baseChannel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.delete(msgId);
                    }
                }

                @Override
                public void onMessageUpdated(BaseChannel channel, BaseMessage message) {
                    super.onMessageUpdated(channel, message);
                    if (channel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.update(message);
                    }
                }

                @Override
                public void onReadReceiptUpdated(GroupChannel channel) {
                    if (channel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onTypingStatusUpdated(GroupChannel channel) {
                    if (channel.getUrl().equals(mChannelUrl)) {
                        List<Member> typingUsers = channel.getTypingMembers();
                        displayTyping(typingUsers);
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {
            setTypingStatus(false);

            ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID);
            SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Save messages to cache.
        mChatAdapter.save();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_CHANNEL_URL, mChannelUrl);

        super.onSaveInstanceState(outState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Set this as true to restore background connection management.
        SendBird.setAutoBackgroundDetection(true);

        if (requestCode == INTENT_REQUEST_CHOOSE_MEDIA && resultCode == Activity.RESULT_OK) {
            // If user has successfully chosen the image, show a dialog to confirm upload.
            if (data == null) {
                Log.d(LOG_TAG, "data is null!");
                return;
            }

            sendFileWithThumbnail(data.getData());
        }
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mChatAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (mLayoutManager.findLastVisibleItemPosition() == mChatAdapter.getItemCount() - 1) {
                    mChatAdapter.loadPreviousMessages(CHANNEL_LIST_LIMIT, null);
                }
            }
        });
    }

    private void setUpChatListAdapter() {
        mChatAdapter.setItemClickListener(new BokingGroupChatAdapter.OnItemClickListener() {
            @Override
            public void onUserMessageItemClick(UserMessage message) {
                // Restore failed message and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {
                    return;
                }


                if (message.getCustomType().equals(BokingGroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE)) {
                    try {
                        UrlPreviewInfo info = new UrlPreviewInfo(message.getData());
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(info.getUrl()));
                        startActivity(browserIntent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFileMessageItemClick(FileMessage message) {
                // Load media chooser and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {
                    return;
                }


                onFileMessageClicked(message);
            }
        });

        mChatAdapter.setItemLongClickListener(new BokingGroupChatAdapter.OnItemLongClickListener() {
            @Override
            public void onUserMessageItemLongClick(UserMessage message, int position) {
                if (message.getSender().getUserId().equals(preferences.getPRE_SendBirdUserId())) {
                    showMessageOptionsDialog(message, position);
                }
            }

            @Override
            public void onFileMessageItemLongClick(FileMessage message) {
            }

            @Override
            public void onAdminMessageItemLongClick(AdminMessage message) {
            }
        });
    }

    private void showMessageOptionsDialog(final BaseMessage message, final int position) {
        String[] options = new String[]{"Edit message", "Delete message"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    setState(STATE_EDIT, message, position);
                } else if (which == 1) {
                    deleteMessage(message);
                }
            }
        });
        builder.create().show();
    }

    private void setState(int state, BaseMessage editingMessage, final int position) {
        switch (state) {
            case STATE_NORMAL:
                mCurrentState = STATE_NORMAL;
                mEditingMessage = null;

                mUploadFileButton.setVisibility(View.VISIBLE);
                //   mMessageSendButton.setText("SEND");
                mMessageEditText.setText("");
                break;

            case STATE_EDIT:
                mCurrentState = STATE_EDIT;
                mEditingMessage = editingMessage;

                mUploadFileButton.setVisibility(View.GONE);
                //  mMessageSendButton.setText("SAVE");
                String messageString = ((UserMessage) editingMessage).getMessage();
                if (messageString == null) {
                    messageString = "";
                }
                mMessageEditText.setText(messageString);
                if (messageString.length() > 0) {
                    mMessageEditText.setSelection(0, messageString.length());
                }

                mMessageEditText.requestFocus();
                mMessageEditText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIMM.showSoftInput(mMessageEditText, 0);

                        mRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollToPosition(position);
                            }
                        }, 500);
                    }
                }, 100);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void retryFailedMessage(final BaseMessage message) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Retry?")
                .setPositiveButton(R.string.resend_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            if (message instanceof UserMessage) {
                                String userInput = ((UserMessage) message).getMessage();
                                sendUserMessage(userInput);
                            } else if (message instanceof FileMessage) {
                                Uri uri = mChatAdapter.getTempFileMessageUri(message);
                                sendFileWithThumbnail(uri);
                            }
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                })
                .setNegativeButton(R.string.delete_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                }).show();
    }

    /**
     * Display which users are typing.
     * If more than two users are currently typing, this will state that "multiple users" are typing.
     *
     * @param typingUsers The list of currently typing users.
     */
    private void displayTyping(List<Member> typingUsers) {

        if (typingUsers.size() > 0) {
            mCurrentEventLayout.setVisibility(View.VISIBLE);
            String string;

            if (typingUsers.size() == 1) {
                string = String.format(getString(R.string.user_typing), typingUsers.get(0).getNickname());
            } else if (typingUsers.size() == 2) {
                string = String.format(getString(R.string.two_users_typing), typingUsers.get(0).getNickname(), typingUsers.get(1).getNickname());
            } else {
                string = getString(R.string.users_typing);
            }
            mCurrentEventText.setText(string);
        } else {
            mCurrentEventLayout.setVisibility(View.GONE);
        }
    }

    private void requestMedia() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {
            Intent intent = new Intent();

            intent.setType("*/*");

            intent.setAction(Intent.ACTION_GET_CONTENT);

            // Always show the chooser (if there are multiple options available)
            startActivityForResult(Intent.createChooser(intent, "Select Media"), INTENT_REQUEST_CHOOSE_MEDIA);

            // Set this as false to maintain connection
            // even when an external Activity is started.
            SendBird.setAutoBackgroundDetection(false);
        }
    }

    private void requestStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Snackbar.make(mRootLayout, "Storage access permissions are required to upload/download files.",
                    Snackbar.LENGTH_LONG)
                    .setAction("Okay", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    PERMISSION_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();
        } else {
            // Permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    private void onFileMessageClicked(FileMessage message) {
        String type = message.getType().toLowerCase();
        if (type.startsWith("image")) {
            Intent i = new Intent(getActivity(), PhotoViewerActivity.class);
            i.putExtra("url", message.getUrl());
            i.putExtra("type", message.getType());
            startActivity(i);
        } else if (type.startsWith("video")) {
            Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
            intent.putExtra("url", message.getUrl());
            startActivity(intent);
        } else {
            showDownloadConfirmDialog(message);
        }
    }

    private void showDownloadConfirmDialog(final FileMessage message) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setMessage("Download file?")
                    .setPositiveButton(R.string.download, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                FileUtils.downloadFile(getActivity(), message.getUrl(), message.getName());
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null).show();
        }

    }

    private void updateActionBarTitle() {
        String title = "";

        if (mChannel != null) {
            title = TextUtils.getGroupChannelTitle(mChannel);
        }

    }

    private void sendUserMessageWithUrl(final String text, String url) {
        if (mChannel == null) {
            return;
        }

        new WebUtils.UrlPreviewAsyncTask() {
            @Override
            protected void onPostExecute(UrlPreviewInfo info) {
                if (mChannel == null) {
                    return;
                }

                UserMessage tempUserMessage = null;
                BaseChannel.SendUserMessageHandler handler = new BaseChannel.SendUserMessageHandler() {
                    @Override
                    public void onSent(UserMessage userMessage, SendBirdException e) {
                        if (e != null) {
                            // Error!
                            Log.e(LOG_TAG, e.toString());
                            if (getActivity() != null) {
                                Toast.makeText(
                                        getActivity(),
                                        "Send failed with error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                                        .show();
                            }
                            mChatAdapter.markMessageFailed(userMessage.getRequestId());
                            return;
                        }

                        // Update a sent message to RecyclerView
                        mChatAdapter.markMessageSent(userMessage);
                    }
                };

                try {
                    // Sending a message with URL preview information and custom type.
                    String jsonString = info.toJsonString();
                    tempUserMessage = mChannel.sendUserMessage(text, jsonString, BokingGroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE, handler);
                } catch (Exception e) {
                    // Sending a message without URL preview information.
                    tempUserMessage = mChannel.sendUserMessage(text, handler);
                }


                // Display a user message to RecyclerView
                mChatAdapter.addFirst(tempUserMessage);
            }
        }.execute(url);
    }


    private void sendUserMessage(String text) {

        if (mChannel == null) {
            return;
        }

        List<String> urls = WebUtils.extractUrls(text);
        if (urls.size() > 0) {
            sendUserMessageWithUrl(text, urls.get(0));
            return;
        }
//
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("image", "https://mobile.style/uploads/images/582d035c1cdb0b3d862015447844fd4dheadphones-2.png");
//            jsonObject.put("name", "demo test");
//            jsonObject.put("price", "$12.00");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        UserMessage tempUserMessage = mChannel.sendUserMessage(text, new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Log.e(LOG_TAG, e.toString());
                    if (getActivity() != null) {
                        Toast.makeText(
                                getActivity(),
                                "Send failed with error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                                .show();
                    }
                    mChatAdapter.markMessageFailed(userMessage.getRequestId());
                    return;
                }

                // Update a sent message to RecyclerView
                mChatAdapter.markMessageSent(userMessage);
            }
        });

        // Display a user message to RecyclerView
        mChatAdapter.addFirst(tempUserMessage);
        if (mLayoutManager != null) {
            mLayoutManager.scrollToPosition(0);
        }

    }

    /**
     * Notify other users whether the current user is typing.
     *
     * @param typing Whether the user is currently typing.
     */
    private void setTypingStatus(boolean typing) {
        if (mChannel == null) {
            return;
        }

        if (typing) {
            mIsTyping = true;
            mChannel.startTyping();
        } else {
            mIsTyping = false;
            mChannel.endTyping();
        }
    }

    /**
     * Sends a File Message containing an image file.
     * Also requests thumbnails to be generated in specified sizes.
     *
     * @param uri The URI of the image, which in this case is received through an Intent request.
     */
    private void sendFileWithThumbnail(Uri uri) {
        if (mChannel == null) {
            return;
        }

        // Specify two dimensions of thumbnails to generate
        List<FileMessage.ThumbnailSize> thumbnailSizes = new ArrayList<>();
        thumbnailSizes.add(new FileMessage.ThumbnailSize(240, 240));
        thumbnailSizes.add(new FileMessage.ThumbnailSize(320, 320));

        Hashtable<String, Object> info = FileUtils.getFileInfo(getActivity(), uri);

        if (info == null || info.isEmpty()) {
            Toast.makeText(getActivity(), "Extracting file information failed.", Toast.LENGTH_LONG).show();
            return;
        }

        final String name;
        if (info.containsKey("name")) {
            name = (String) info.get("name");
        } else {
            name = "Sendbird File";
        }
        final String path = (String) info.get("path");
        final File file = new File(path);
        final String mime = (String) info.get("mime");

        int size = 0;
        if ((Integer) info.get("  size") != null) {
            size = (Integer) info.get("  size");
        }


        if (path == null || path.equals("")) {
            Toast.makeText(getActivity(), "File must be located in local storage.", Toast.LENGTH_LONG).show();
        } else {
            BaseChannel.SendFileMessageWithProgressHandler progressHandler = new BaseChannel.SendFileMessageWithProgressHandler() {
                @Override
                public void onProgress(int bytesSent, int totalBytesSent, int totalBytesToSend) {
                    FileMessage fileMessage = mFileProgressHandlerMap.get(this);
                    if (fileMessage != null && totalBytesToSend > 0) {
                        int percent = (int) (((double) totalBytesSent / totalBytesToSend) * 100);
                        mChatAdapter.setFileProgressPercent(fileMessage, percent);
                    }
                }

                @Override
                public void onSent(FileMessage fileMessage, SendBirdException e) {
                    if (e != null) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        mChatAdapter.markMessageFailed(fileMessage.getRequestId());
                        return;
                    }

                    mChatAdapter.markMessageSent(fileMessage);
                }
            };

            // Send image with thumbnails in the specified dimensions
            FileMessage tempFileMessage = mChannel.sendFileMessage(file, name, mime, size, "", null, thumbnailSizes, progressHandler);

            mFileProgressHandlerMap.put(progressHandler, tempFileMessage);

            mChatAdapter.addTempFileMessageInfo(tempFileMessage, uri);
            mChatAdapter.addFirst(tempFileMessage);
        }
    }

    private void editMessage(final BaseMessage message, String editedMessage) {
        if (mChannel == null) {
            return;
        }

        mChannel.updateUserMessage(message.getMessageId(), editedMessage, null, null, new BaseChannel.UpdateUserMessageHandler() {
            @Override
            public void onUpdated(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                    @Override
                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                        mChatAdapter.markAllMessagesAsRead();
                    }
                });
            }
        });
    }

    /**
     * Deletes a message within the channel.
     * Note that users can only delete messages sent by oneself.
     *
     * @param message The message to delete.
     */
    private void deleteMessage(final BaseMessage message) {
        if (mChannel == null) {
            return;
        }

        mChannel.deleteMessage(message, new BaseChannel.DeleteMessageHandler() {
            @Override
            public void onResult(SendBirdException e) {
                if (e != null) {
                    // Error!
                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                    @Override
                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                        mChatAdapter.markAllMessagesAsRead();
                    }
                });
            }
        });
    }


    private void method_cancel() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_booking_status + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "0")

                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_btn_changedate.setVisibility(View.GONE);
                                    tv_btn_confirm.setVisibility(View.GONE);
                                    tv_btn_cancel.setVisibility(View.GONE);
                                    tv_btn_chechkout.setVisibility(View.GONE);
                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_modify_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_modify);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        final TextView tv_bookingdate = dialog_card.findViewById(R.id.tv_bookingdate);
        final ImageView iv_cancel = dialog_card.findViewById(R.id.iv_cancel);
        CardView card_continue = dialog_card.findViewById(R.id.card_continue);

        final Spinner spiner_bookingtime = dialog_card.findViewById(R.id.spiner_bookingtime);
        ImageView iv_spiner_bookingtime = dialog_card.findViewById(R.id.iv_spiner_bookingtime);
        iv_spiner_bookingtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_bookingtime.performClick();
            }
        });

        getTimes();

        SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(context, Model_Time_list);
        spiner_bookingtime.setAdapter((SpinnerAdapter) customAdapter_state);


        spiner_bookingtime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                visit_starttime = "" + Model_Time_list.get(position).Final_Model_time;
                Log.e("visit_starttime", visit_starttime);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        dialog_card.show();


        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        card_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                method_change_date(dialog_card, "" + tv_bookingdate.getText().toString(), "" + visit_starttime);

                dialog_card.dismiss();
            }
        });


        if (BookingTime != null) {
            try {
                String timme = "" + BookingTime;
                String[] time = timme.split(":");
                String hour1 = time[0].trim();
                String min1 = time[1].trim();

                String time_final = "" + hour1 + ":" + min1;


                visit_starttime = time_final;
                Log.e("visit_starttime", visit_starttime);
                for (int i = 0; i < Model_Time_list.size(); i++) {
                    if (visit_starttime.equalsIgnoreCase("" + Model_Time_list.get(i).Final_Model_time)) {
                        spiner_bookingtime.setSelection(i);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        newCalendar_date = Calendar.getInstance();
        if (BookingDate != null) {
            try {

                DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date msgdate = sdf.parse(BookingDate);

                SimpleDateFormat format_day = new SimpleDateFormat("d");
                String date = format_day.format(msgdate);

                SimpleDateFormat format_year = new SimpleDateFormat("yyyy");
                String year = format_year.format(msgdate);

                SimpleDateFormat format_month = new SimpleDateFormat("M");
                String month = format_month.format(msgdate);


                String month_final = "";
                String day_final = "";

                if (Integer.parseInt(month) < 10) {
                    month_final = "0" + month;
                } else {
                    month_final = "" + month;
                }
                if (Integer.parseInt(date) < 10) {
                    day_final = "0" + date;
                } else {
                    day_final = "" + date;
                }


                tv_bookingdate.setText("" + year + "-" + month_final + "-" + day_final);

                newCalendar_date.set(Calendar.MONTH, Integer.parseInt(month) - 1);
                newCalendar_date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date));
                newCalendar_date.set(Calendar.YEAR, Integer.parseInt(year));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        tv_bookingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DatePickerDialog timePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        String myFormat = "YYYY-MM-DD"; //In which you need put here


                        int month = monthOfYear + 1;

                        String month_final = "";
                        String day_final = "";

                        if (month < 10) {
                            month_final = "0" + month;
                        } else {
                            month_final = "" + month;
                        }
                        if (dayOfMonth < 10) {
                            day_final = "0" + dayOfMonth;
                        } else {
                            day_final = "" + dayOfMonth;
                        }


                        tv_bookingdate.setText("" + year + "-" + month_final + "-" + day_final);

                    }

                }, newCalendar_date.get(Calendar.YEAR), newCalendar_date.get(Calendar.MONTH), newCalendar_date.get(Calendar.DAY_OF_MONTH));
                timePickerDialog.show();
                timePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            }
        });


    }

    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<TEMP_MOdelTime> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<TEMP_MOdelTime> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText("" + _list_documnetlist.get(i).Model_time);
            return view;
        }
    }


    class TEMP_MOdelTime {
        String Model_time = "";
        String Final_Model_time = "";

        public TEMP_MOdelTime(String s_time, String s1_final_time) {
            Model_time = s_time;
            Final_Model_time = s1_final_time;
        }
    }

    List<TEMP_MOdelTime> Model_Time_list = new ArrayList<>();

    private void getTimes() {

        Model_Time_list = new ArrayList<>();

        TEMP_MOdelTime m_1 = new TEMP_MOdelTime("06:00 am", "06:00");
        TEMP_MOdelTime m_2 = new TEMP_MOdelTime("06:30 am", "06:30");

        TEMP_MOdelTime m_3 = new TEMP_MOdelTime("07:00 am", "07:00");
        TEMP_MOdelTime m_4 = new TEMP_MOdelTime("07:30 am", "07:30");

        TEMP_MOdelTime m_5 = new TEMP_MOdelTime("08:00 am", "08:00");
        TEMP_MOdelTime m_6 = new TEMP_MOdelTime("08:30 am", "08:30");

        TEMP_MOdelTime m_7 = new TEMP_MOdelTime("09:00 am", "09:00");
        TEMP_MOdelTime m_8 = new TEMP_MOdelTime("09:30 am", "09:30");

        TEMP_MOdelTime m_9 = new TEMP_MOdelTime("10:00 am", "10:00");
        TEMP_MOdelTime m_10 = new TEMP_MOdelTime("10:30 am", "10:30");

        TEMP_MOdelTime m_11 = new TEMP_MOdelTime("11:00 am", "11:00");
        TEMP_MOdelTime m_12 = new TEMP_MOdelTime("11:30 am", "11:30");


        TEMP_MOdelTime m_13 = new TEMP_MOdelTime("12:00 am", "12:00");
        TEMP_MOdelTime m_14 = new TEMP_MOdelTime("12:30 pm", "12:30");


        TEMP_MOdelTime m_15 = new TEMP_MOdelTime("01:00 pm", "13:00");
        TEMP_MOdelTime m_16 = new TEMP_MOdelTime("01:30 pm", "13:30");

        TEMP_MOdelTime m_17 = new TEMP_MOdelTime("02:00 pm", "14:00");
        TEMP_MOdelTime m_18 = new TEMP_MOdelTime("02:30 pm", "14:30");

        TEMP_MOdelTime m_19 = new TEMP_MOdelTime("03:00 pm", "15:00");
        TEMP_MOdelTime m_20 = new TEMP_MOdelTime("03:30 pm", "15:30");

        TEMP_MOdelTime m_21 = new TEMP_MOdelTime("04:00 pm", "16:00");
        TEMP_MOdelTime m_22 = new TEMP_MOdelTime("04:30 pm", "16:30");


        TEMP_MOdelTime m_23 = new TEMP_MOdelTime("05:00 pm", "17:00");
        TEMP_MOdelTime m_24 = new TEMP_MOdelTime("05:30 pm", "17:30");

        TEMP_MOdelTime m_25 = new TEMP_MOdelTime("06:00 pm", "18:00");
        TEMP_MOdelTime m_26 = new TEMP_MOdelTime("06:30 pm", "18:30");


        TEMP_MOdelTime m_27 = new TEMP_MOdelTime("07:00 pm", "19:00");
        TEMP_MOdelTime m_28 = new TEMP_MOdelTime("07:30 pm", "19:30");


        TEMP_MOdelTime m_29 = new TEMP_MOdelTime("08:00 pm", "20:00");
        TEMP_MOdelTime m_30 = new TEMP_MOdelTime("08:30 pm", "20:30");


        TEMP_MOdelTime m_31 = new TEMP_MOdelTime("09:00 pm", "21:00");


        Model_Time_list.add(m_1);
        Model_Time_list.add(m_2);
        Model_Time_list.add(m_3);
        Model_Time_list.add(m_4);
        Model_Time_list.add(m_5);
        Model_Time_list.add(m_6);
        Model_Time_list.add(m_7);
        Model_Time_list.add(m_8);
        Model_Time_list.add(m_9);
        Model_Time_list.add(m_10);
        Model_Time_list.add(m_11);
        Model_Time_list.add(m_12);
        Model_Time_list.add(m_13);
        Model_Time_list.add(m_14);
        Model_Time_list.add(m_15);
        Model_Time_list.add(m_16);
        Model_Time_list.add(m_17);
        Model_Time_list.add(m_18);
        Model_Time_list.add(m_19);
        Model_Time_list.add(m_20);
        Model_Time_list.add(m_21);
        Model_Time_list.add(m_22);
        Model_Time_list.add(m_23);
        Model_Time_list.add(m_24);
        Model_Time_list.add(m_25);
        Model_Time_list.add(m_26);
        Model_Time_list.add(m_27);
        Model_Time_list.add(m_28);
        Model_Time_list.add(m_29);
        Model_Time_list.add(m_30);
        Model_Time_list.add(m_31);


    }


    String visit_starttime = "";
    Calendar newCalendar_date = Calendar.getInstance();

    private void method_change_date(final Dialog dialog_card, String booking_date, String booking_time) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_booking_details + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("booking_date", "" + booking_date)
                    .addBodyParameter("booking_time", "" + booking_time)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }

                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_confirm() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_booking_status + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "2")

                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_btn_changedate.setVisibility(View.GONE);
                                    tv_btn_confirm.setVisibility(View.GONE);
                                    tv_btn_cancel.setVisibility(View.GONE);

                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    List<MyCard_Data> data_order_list = new ArrayList<>();

    NonScrollListView listview_order;


    private void method_mycard() {

        final Dialog dialog_listcard = new Dialog(context);
        dialog_listcard.setContentView(R.layout.dialog_my_card);
        dialog_listcard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_listcard.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        listview_order = dialog_listcard.findViewById(R.id.listview_order);
        TextView tv_add_method = dialog_listcard.findViewById(R.id.tv_add_method);
        TextView tv_next = dialog_listcard.findViewById(R.id.tv_next);

        dialog_listcard.show();

        if (data_order_list != null) {
            Adapter_order adapter_review = new Adapter_order(context, data_order_list);
            listview_order.setAdapter(adapter_review);
        }

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                card_id = "";

                if (data_order_list != null) {
                    Adapter_order adapter_review = new Adapter_order(context, data_order_list);
                    listview_order.setAdapter(adapter_review);
                }


                method_change_card_dialog();
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_booking_checkout(dialog_listcard);
            }
        });

    }


    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_order_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }


    private void method_get_mycard1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_order_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();


                                        if (listview_order != null) {
                                            Adapter_order adapter_review = new Adapter_order(context, data_order_list);
                                            listview_order.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }
    }

    public class Adapter_order extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;

        int pos = -1;

        public Adapter_order(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_mystripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }


            String brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                viewHolder.iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.et_change_cardnumber.setText("" + brand + " / " + listState.get(position).getLast4());


            if (pos == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    pos = position;
                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
                    card_id = "" + listState.get(position).getId();


                    notifyDataSetChanged();

                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon, iv_checkbox;
            TextView et_change_cardnumber;
            CardView card_mylist;

        }

    }


    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);


        final EditText et_lastname = dialog_card.findViewById(R.id.et_lastname);
        final EditText et_firstname = dialog_card.findViewById(R.id.et_firstname);
        final EditText et_door_number = dialog_card.findViewById(R.id.et_door_number);
        final EditText et_pincode = dialog_card.findViewById(R.id.et_pincode);


        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {


                    CardBrand brand = CardUtils.getPossibleCardBrand(s.toString());

                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString(),
                            "" + et_firstname.getText().toString(),
                            "" + et_lastname.getText().toString(),
                            "" + et_door_number.getText().toString(),
                            "" + et_pincode.getText().toString()
                    )) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }

    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum,
                                String s_first_name, String s_last_name, String s_number, String s_pincode) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        // card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);

        Card.Builder cardbuild = new Card.Builder(cardNumber, cardExpMonth, cardExpYear, cardCVC)
                .name("" + s_first_name + "" + s_last_name)
                .addressLine1("" + s_number)
                .addressZip("" + s_pincode);
        card_stripe = cardbuild.build();


        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;


    }

    Card card_stripe;

    private void getUserKey(final Dialog dialog_card) {

        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, "" + preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());

                        Token_card = token.getId();


                        method_save_card(dialog_card);


                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


    }

    String Token_card;
    String card_id = "";

    private void method_save_card(final Dialog dialog_card) {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token", "" + Token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }


                                    method_get_mycard1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }
    }

    private void method_booking_checkout(final Dialog dialog_listcard) {

        if (card_id.equalsIgnoreCase("")) {
            Toast.makeText(context,
                    "Please add a payment card",
                    Toast.LENGTH_LONG).show();
        } else {

            if (utills.isOnline(context)) {
                progressDialog = utills.startLoader(context);

                AndroidNetworking.post(Global_Service_Api.API_booking_checkout)
                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                        .addBodyParameter("card_id", "" + card_id)
                        .addBodyParameter("booking_id", "" + order_id_booking)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String result) {
                                if (result == null || result == "") return;
                                Log.e("card", result);
                                try {
                                    JSONObject jsonObject = new JSONObject(result);

                                    String flag = jsonObject.getString("flag");
                                    String message = jsonObject.getString("message");

                                    if (flag.equalsIgnoreCase("true")) {

                                        if (dialog_listcard != null) {
                                            dialog_listcard.dismiss();
                                        }


                                        try {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            chechkout_InvoiceId = "" + jsonObject1.get("stripe_invoice_number");
                                            chechkout_Last4 = "" + jsonObject1.get("last4");
                                            chechkout_date = "" + jsonObject1.get("created_at");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                        sendUserMessage("PAID_");


                                        tv_btn_changedate.setVisibility(View.GONE);
                                        tv_btn_confirm.setVisibility(View.GONE);
                                        tv_btn_cancel.setVisibility(View.GONE);
                                        tv_btn_chechkout.setVisibility(View.GONE);

                                    }
                                    Toast.makeText(context,
                                            message,
                                            Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                utills.stopLoader(progressDialog);
                            }

                            @Override
                            public void onError(ANError e) {
                                Toast.makeText(context,
                                        e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                utills.stopLoader(progressDialog);
                                Log.d("API", e.toString());
                            }
                        });


            }
        }
    }

    private void method_complete() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_booking_status + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "3")

                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_btn_changedate.setVisibility(View.GONE);
                                    tv_btn_confirm.setVisibility(View.GONE);
                                    tv_btn_cancel.setVisibility(View.GONE);
                                    tv_btn_chechkout.setVisibility(View.GONE);
                                    tv_btn_complete.setVisibility(View.GONE);
                                    tv_btn_WriteReview.setVisibility(View.VISIBLE);
                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_review_dialog() {

        final Dialog dialog_review = new Dialog(context);
        dialog_review.setContentView(R.layout.dialog_review);
        dialog_review.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_review.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dialog_review.show();

        TextView tv_cancel11 = dialog_review.findViewById(R.id.tv_cancel11);
        CardView card_add_review = dialog_review.findViewById(R.id.card_add_review);
        ScaleRatingBar simpleRatingBar_rate = dialog_review.findViewById(R.id.simpleRatingBar_rate);
        EditText et_comment = dialog_review.findViewById(R.id.et_comment);

        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_review.dismiss();
            }
        });

        card_add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                float rate_num = simpleRatingBar_rate.getRating();
                if (rate_num > 0) {
                    method_review(dialog_review, et_comment.getText().toString(), rate_num);
                } else {
                    Toast.makeText(context, "Please add Review", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    private void method_review(Dialog dialog_review, String comments, float rate_num) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_booking_review_submit)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("booking_id", "" + order_id_booking)
                    .addBodyParameter("user_id", "" + is_stylist_id)
                    .addBodyParameter("rating", "" + rate_num)
                    .addBodyParameter("comments", "" + comments)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("booking_review_submit", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_btn_changedate.setVisibility(View.GONE);
                                    tv_btn_confirm.setVisibility(View.GONE);
                                    tv_btn_cancel.setVisibility(View.GONE);
                                    tv_btn_chechkout.setVisibility(View.GONE);
                                    tv_btn_complete.setVisibility(View.GONE);
                                    tv_btn_WriteReview.setVisibility(View.GONE);

                                    if (dialog_review != null) {
                                        dialog_review.dismiss();
                                    }

                                    JSONObject jsonObject2 = new JSONObject();
                                    jsonObject2.put("comments", "" + comments);
                                    jsonObject2.put("rating", "" + rate_num);
                                    rating_booking = "" + jsonObject2.toString();

                                    sendUserMessage("REVIEW_");


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    public static void method_get_details(final TextView tv_transaction, final TextView tv_date_prize) {

        if (utills.isOnline(context)) {
            final Preferences preferences = new Preferences(context);

            AndroidNetworking.get(Global_Service_Api.API_get_booking_details + "" + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card123", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_BookingDetails_Response Response = new Gson().fromJson(result.toString(), Model_BookingDetails_Response.class);

                                    if (Response.getData() != null) {

                                        Model_BookingDetails_Data details_data = new Model_BookingDetails_Data();
                                        details_data = Response.getData();


                                        if (details_data.getPayment() != null) {
                                            chechkout_InvoiceId = "" + details_data.getPayment().getStripeInvoiceNumber();
                                            chechkout_Last4 = "" + details_data.getPayment().getLast4();
                                            chechkout_date = "" + details_data.getPayment().getCreatedAt();
                                        }


                                        tv_transaction.setText("transaction id: " + chechkout_InvoiceId);

                                        if (is_user_id.equalsIgnoreCase(preferences.getPRE_SendBirdUserId())) {
                                            tv_date_prize.setText("You've paid £" + utills.roundTwoDecimals(Double.parseDouble(chechkout_price)) + " on " + utills.getTimeAccordingTime_Date(chechkout_date) + " using card x" + chechkout_Last4);
                                        } else {
                                            tv_date_prize.setText("Payment has been received £" + chechkout_price + " on " + utills.getTimeAccordingTime_Date(chechkout_date) + " using card x" + chechkout_Last4);
                                        }

                                        method_set_data(details_data);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            Log.d("API", e.toString());
                        }
                    });


        }

    }

    public static void method_api_call_Review(final TextView tv_comment, final ScaleRatingBar simpleRatingBar_) {
        if (utills.isOnline(context)) {
            final Preferences preferences = new Preferences(context);
            AndroidNetworking.get(Global_Service_Api.API_get_booking_details + "" + order_id_booking)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card123", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    Model_BookingDetails_Response Response = new Gson().fromJson(result.toString(), Model_BookingDetails_Response.class);
                                    if (Response.getData() != null) {
                                        Model_BookingDetails_Data details_data = new Model_BookingDetails_Data();
                                        details_data = Response.getData();
                                        if (details_data.getbooking_rating() != null) {
                                            tv_comment.setText("" + details_data.getbooking_rating().getComments());
                                            simpleRatingBar_.setRating( details_data.getbooking_rating().getRating());
                                        }
                                        method_set_data(details_data);

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }

    private static void method_set_data(Model_BookingDetails_Data details_data) {


        Is_Paid = "" +details_data.getIsPaid();
        is_status = "" +details_data.getStatus();

        if (is_user_id.equalsIgnoreCase(preferences.getPRE_SendBirdUserId())) {  // customer

            tv_btn_changedate.setVisibility(View.GONE);
            tv_btn_confirm.setVisibility(View.GONE);
            tv_btn_cancel.setVisibility(View.GONE);
            tv_btn_chechkout.setVisibility(View.GONE);
            tv_btn_complete.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);


            if (is_status.equalsIgnoreCase("1")) {
                tv_btn_changedate.setVisibility(View.VISIBLE);
                tv_btn_cancel.setVisibility(View.VISIBLE);
            } else if (is_status.equalsIgnoreCase("2") && Is_Paid.equalsIgnoreCase("0")) {
                tv_btn_chechkout.setVisibility(View.VISIBLE);
            } else if ((!is_status.equalsIgnoreCase("3")) && Is_Paid.equalsIgnoreCase("1")) {
                tv_btn_complete.setVisibility(View.VISIBLE);
            } else if ((is_status.equalsIgnoreCase("3")) && Is_Paid.equalsIgnoreCase("1")) {

                if (rating_booking.equalsIgnoreCase("") || rating_booking.equalsIgnoreCase("null")) {
                    tv_btn_WriteReview.setVisibility(View.VISIBLE);
                }


            }

        } else {   // stylist


            tv_btn_changedate.setVisibility(View.GONE);
            tv_btn_confirm.setVisibility(View.GONE);
            tv_btn_cancel.setVisibility(View.GONE);
            tv_btn_chechkout.setVisibility(View.GONE);
            tv_btn_complete.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);


            if (is_status.equalsIgnoreCase("1")) {
                tv_btn_changedate.setVisibility(View.VISIBLE);
                tv_btn_confirm.setVisibility(View.VISIBLE);
                tv_btn_cancel.setVisibility(View.VISIBLE);
            }

        }


    }


}
