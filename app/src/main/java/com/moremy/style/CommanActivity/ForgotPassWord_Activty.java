package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.R;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;


public class ForgotPassWord_Activty extends Base1_Activity implements View.OnClickListener {


    LinearLayout ll_email_verify, ll_password_verify;
    TextView tv_reset_next;

    EditText et_confirm_password, et_new_password, et_email, et_otp;
    TextView tv_email;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        context = ForgotPassWord_Activty.this;
        findview();


    }

    private void findview() {

        ll_password_verify = findViewById(R.id.ll_password_verify);
        ll_email_verify = findViewById(R.id.ll_email_verify);
        tv_reset_next = findViewById(R.id.tv_reset_next);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        et_new_password = findViewById(R.id.et_new_password);
        et_email = findViewById(R.id.et_email);
        tv_email = findViewById(R.id.tv_email);
        et_otp = findViewById(R.id.et_otp);

        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(this);


        et_confirm_password.setTransformationMethod(new MyPasswordTransformationMethod());
        et_new_password.setTransformationMethod(new MyPasswordTransformationMethod());

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_next:


                if (ll_password_verify.getVisibility() == View.GONE) {


                    Forgot_Password_Api_otp();


                } else {


                    if (et_new_password.getText().toString().equalsIgnoreCase("")) {
                      Toast toast = Toast.makeText(context,"The password field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                    } else if (et_confirm_password.getText().toString().equalsIgnoreCase("")) {
                        Toast toast = Toast.makeText(context,"The confirm password field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                    } else if (!et_confirm_password.getText().toString().equals(et_new_password.getText().toString())) {
                       Toast toast = Toast.makeText(context,"The password field and confirm password field should be match.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                    } else {

                        Forgot_Password_Api();

                    }


                }


                break;

        }
    }


    @Override
    public void onBackPressed() {

        if (ll_password_verify.getVisibility() == View.VISIBLE) {
            ll_password_verify.setVisibility(View.GONE);
            ll_email_verify.setVisibility(View.VISIBLE);
            tv_reset_next.setText("send reset email");
            return;
        } else {
            super.onBackPressed();
        }

    }


    public class MyPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    Dialog progressDialog = null;

    public void Forgot_Password_Api_otp() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_forgotpassword_Email)
                    .addBodyParameter("email", "" + et_email.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {
                                    ll_password_verify.setVisibility(View.VISIBLE);
                                    ll_email_verify.setVisibility(View.GONE);
                                    tv_reset_next.setText("reset");
                                    tv_email.setText("" + et_email.getText().toString());
                                }
                               Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }

    public void Forgot_Password_Api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.post(Global_Service_Api.API_resetPassword)
                    .addBodyParameter("email", "" + et_email.getText().toString())
                    .addBodyParameter("password", "" + et_new_password.getText().toString())
                    .addBodyParameter("otp", "" + et_otp.getText().toString())
                    .addBodyParameter("password_confirmation", "" + et_confirm_password.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {
                                    finish();
                                }
                               Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }


}
