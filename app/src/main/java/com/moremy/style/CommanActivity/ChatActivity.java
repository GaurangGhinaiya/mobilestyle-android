package com.moremy.style.CommanActivity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Booking_Fragment.Model.Model_BookingDetails_Review;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.moremy.style.chat.GroupChatFragment;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelParams;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends Base1_Activity {


    Handler mHandler;
    Fragment currentFragment;
    FragmentManager fragmentManager;

    FragmentTransaction fragmentTransaction;

    String ProductId;
    String ProductName;
    String ProductImage;
    String ProductPrize;
    String SellerImage;
    String SellerName;
    String SellerId;
    String ProductDeliveryCharge;
    String USER_ID;
    String buy_now_is = "";
    String total_quantity = "";

    ImageView iv_back;
    Preferences preferences;
    Dialog progressDialog = null;

    Context context;

    float TotalCharge = 0;

    ScaleRatingBar simpleRatingBar_quality;
    TextView tv_ProductDelivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        context = ChatActivity.this;
        preferences = new Preferences(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        mHandler = new Handler();


        iv_back = findViewById(R.id.iv_back);
        ImageView iv_profile_image = findViewById(R.id.iv_profile_image);
        ImageView img_sellerprofile = findViewById(R.id.img_sellerprofile);
        TextView tv_ProductName = findViewById(R.id.tv_ProductName);
        TextView tv_ProductPrize = findViewById(R.id.tv_ProductPrize);
        TextView tv_seller_name = findViewById(R.id.tv_seller_name);
         tv_ProductDelivery = findViewById(R.id.tv_ProductDelivery);
        simpleRatingBar_quality = findViewById(R.id.simpleRatingBar_quality);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Intent intent = getIntent();
        ProductId = "" + intent.getStringExtra("ProductId");
        ProductName = "" + intent.getStringExtra("ProductName");
        ProductImage = "" + intent.getStringExtra("ProductImage");
        ProductPrize = "" + intent.getStringExtra("ProductPrize");
        SellerImage = "" + intent.getStringExtra("SellerImage");
        SellerName = "" + intent.getStringExtra("SellerName");
        SellerId = "" + intent.getStringExtra("SellerId");
        ProductDeliveryCharge = "" + intent.getStringExtra("ProductDeliveryCharge");
        buy_now_is = "" + intent.getStringExtra("buy_now");
        total_quantity = "" + intent.getStringExtra("total_quantity");

        Log.e("Seller", "" + SellerName);

        tv_ProductName.setText("" + ProductName);
        tv_ProductPrize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(ProductPrize)));
        tv_seller_name.setText("" + SellerName);



        if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
            TotalCharge = Float.parseFloat(ProductPrize) + Float.parseFloat(ProductDeliveryCharge);
        } else {
            TotalCharge = Float.parseFloat(ProductPrize);
        }


        Log.e("Seller", "" + ProductPrize);
        Log.e("Seller", "" + ProductDeliveryCharge);
        Log.e("Seller", "" + TotalCharge);


        if (!ProductImage.equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(Global_Service_Api.IMAGE_URL + ProductImage)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }else {
            Glide.with(context)
                    .load(R.drawable.app_logo)
                    .into(iv_profile_image);
        }

        if (!SellerImage.equalsIgnoreCase("") && !SellerImage.equalsIgnoreCase("null")) {
            Glide.with(this)
                    .load(Global_Service_Api.IMAGE_URL + SellerImage)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(img_sellerprofile);
        }

        USER_ID = "" + preferences.getPRE_SendBirdUserId();

        SendBird.connect(USER_ID, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {    // Error.
                    Toast.makeText(ChatActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                method_create_channel1();

            }
        });


    }


    private void method_create_channel1() {


        List<String> users = new ArrayList<>();
        users.add("" + USER_ID);
        users.add("" + SellerId);

        GroupChannelParams params = new GroupChannelParams()
                .setPublic(false)
                .setEphemeral(false)
                .setDistinct(false)
                .addUserIds(users)
                .setName("" + ProductName);  // In a group channel, you can create a new channel by specifying its unique channel URL in a 'GroupChannelParams' object.;

        GroupChannel.createChannel(params, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {       // Error.

                    return;
                } else {

                    String channelUrl = groupChannel.getUrl();
                    if (channelUrl != null) {
                        // If started from notification


                        method_api_call(channelUrl);

                    }
                }
            }
        });


    }

    private void method_api_call(final String channelUrl) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_submit_order)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("sendgrid_order_id", "" + channelUrl)
                    .addBodyParameter("product_id", "" + ProductId)
                    .addBodyParameter("product_qty", "" + total_quantity)
                    .addBodyParameter("buy_now", "" + buy_now_is)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", "" + result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    method_api_call2("" + jsonObject1.get("id"), "" + jsonObject1.get("order_date"), channelUrl);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                    finish();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }

    }

    private void method_api_call2(final String id, final String order_date, final String channelUrl) {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_get_order_detail + id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_order_detail", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    float TotalCharge = 0;

                                    String Shipp_Type = "1";
                                    int quantity = 1;
                                    try {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject("product");
                                        Shipp_Type = "" + jsonObject2.get("shipping_type");
                                        String quantity2 = "" + jsonObject1.get("product_qty");
                                        if (!quantity2.equalsIgnoreCase("null")) {
                                            quantity = Integer.parseInt(quantity2);
                                        }


                                        String stype = "";
                                        if (Shipp_Type.equalsIgnoreCase("1")) {
                                            stype = " (per order)";
                                        } else {
                                            stype = " (per item)";
                                        }


                                        if (ProductDeliveryCharge != null) {

                                            if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) &&
                                                    (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                                                tv_ProductDelivery.setText("+ £" + utills.roundTwoDecimals(Double.parseDouble(ProductDeliveryCharge)) + " delivery"+stype);
                                            } else {
                                                tv_ProductDelivery.setText("+ £0 delivery"+stype);
                                            }


                                        } else {
                                            tv_ProductDelivery.setText("+ £" + "0 delivery"+stype);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (Shipp_Type.equalsIgnoreCase("1")) {
                                        if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                                            TotalCharge = (Float.parseFloat(ProductPrize) * quantity) + Float.parseFloat(ProductDeliveryCharge);
                                        } else {
                                            TotalCharge = Float.parseFloat(ProductPrize) * quantity;
                                        }
                                    } else {
                                        if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                                            TotalCharge = (Float.parseFloat(ProductPrize) + Float.parseFloat(ProductDeliveryCharge)) * quantity;
                                        } else {
                                            TotalCharge = Float.parseFloat(ProductPrize) * quantity;
                                        }
                                    }

                                    try {
                                        JSONObject jsonObject_seller = jsonObject1.getJSONObject("seller");
                                        if (jsonObject_seller.has("seller_rating") && !jsonObject_seller.isNull("seller_rating")) {
                                            JSONObject jsonObject_seller_rating = jsonObject_seller.getJSONObject("seller_rating");
                                            simpleRatingBar_quality.setRating(jsonObject_seller_rating.getLong("avg_rating"));
                                        } else {
                                            simpleRatingBar_quality.setRating(0);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }



                                    currentFragment = new GroupChatFragment(channelUrl, TotalCharge, 0);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("quantity", "" + quantity);
                                    bundle.putString("ProductId", "" + ProductId);
                                    bundle.putString("ProductName", "" + ProductName);
                                    bundle.putString("ProductImage", "" + ProductImage);
                                    bundle.putString("ProductPrize", "" + ProductPrize);
                                    bundle.putString("ProductDeliveryCharge", "" + ProductDeliveryCharge);
                                    bundle.putString("order_id", "" + id);
                                    bundle.putString("InvoiceId", "");
                                    bundle.putString("is_paid", "0");
                                    bundle.putString("status", ""+jsonObject1.getString("status"));


                                    bundle.putString("getShippingDate", "");
                                    bundle.putString("getDeliveryDate", "");
                                    bundle.putString("getOrderDate", "" + order_date);
                                    bundle.putString("getCancelDate", "");
                                    bundle.putString("card_paid", "");
                                    bundle.putString("Shipp_Type", "" + Shipp_Type);


                                    bundle.putString("text_trackingnumber", "" + jsonObject1.get("tracking_number"));
                                    bundle.putString("text_shippingcompany", "" + jsonObject1.get("tracking_name"));
                                    bundle.putString("tracking_url", "" + jsonObject1.get("tracking_url"));
                                    bundle.putString("is_seller_id", "" + jsonObject1.get("seller_id"));



                                    String rating = ""+ "" + jsonObject1.get("order_rating");
                                    bundle.putString("rating", "" + rating);


                                    currentFragment.setArguments(bundle);
                                    LoadFragment(currentFragment);

                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                    finish();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });


        }

    }


    public void LoadFragment(final Fragment fragment) {

        Runnable mPendingRunnable = new Runnable() {
            public void run() {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_group_channel, fragment);
                fragmentTransaction.commit();

            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }


    }

    @Override
    public void onBackPressed() {


        Log.e("onBackPressed", "" + fragmentManager.getBackStackEntryCount());

        if (fragmentManager.getBackStackEntryCount() > 0) {
            try {
                try {
                    fragmentManager.popBackStack();

                    FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                            0);
                    getSupportFragmentManager().popBackStack(entry.getId(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().executePendingTransactions();

                    super.onBackPressed();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            super.onBackPressed();

//            try {
//
//                if (ll_discover_cust != null) {
//
//                  Fragment  currentFragment_CUSTOMER = new Fragment_MyActivity();
//                    fragmentTransaction_CUSTOMER = fragmentManager_CUSTOMER.beginTransaction();
//                    fragmentTransaction_CUSTOMER.replace(R.id.fmFragment, currentFragment_CUSTOMER);
//                    fragmentTransaction_CUSTOMER.commit();
//
//                    ll_discover_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_cart_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_search_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_profile_cust.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_My_Activity_cust.setBackgroundColor(getResources().getColor(R.color.gray_trans1));
//
//
//                }
//
//                if (ll_discover_salon != null) {
//                    Fragment   currentFragment_SALON = new Fragment_MyActivity();
//                    fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
//                    fragmentTransaction_SALON.replace(R.id.fmFragment, currentFragment_SALON);
//                    fragmentTransaction_SALON.commit();
//
//
//                    ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_cart_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_search_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_profile_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_My_Activity_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));
//
//
//                }
//
//                if (ll_discover != null) {
//                    Fragment   currentFragment_Stylist_seller = new Fragment_MyActivity();
//                    fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
//                    fragmentTransaction_Stylist_seller.replace(R.id.fmFragment, currentFragment_Stylist_seller);
//                    fragmentTransaction_Stylist_seller.addToBackStack(null);
//                    fragmentTransaction_Stylist_seller.commit();
//
//                    ll_discover.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_cart.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_search.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_profile.setBackgroundColor(getResources().getColor(R.color.transparent));
//                    ll_My_Activity.setBackgroundColor(getResources().getColor(R.color.gray_trans1));
//
//
//                }
//
//            } catch (Resources.NotFoundException e) {
//                e.printStackTrace();
//            }catch (Exception e) {
//                e.printStackTrace();
//            }
//
//

        }


    }

}
