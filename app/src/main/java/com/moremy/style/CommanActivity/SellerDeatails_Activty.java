package com.moremy.style.CommanActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Activity.Base_Activity;
import com.moremy.style.CommanActivity.Product_Fragment.Fragment_Shop_Seller;
import com.moremy.style.CommanActivity.Product_Fragment.Fragment_review_Seller;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.StylistSeller.model.Profile.Profile_Data;
import com.moremy.style.StylistSeller.model.Profile.Profile_Response;
import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;



public class SellerDeatails_Activty extends Base_Activity {

    Dialog progressDialog = null;
    Context context;
    Preferences preferences;

    TabLayout tabs;
    CustomViewPager viewPager;

    ScaleRatingBar simpleRatingBar_quality;
    TextView tv_oneliner, tv_name, tv_servies;
    public static Profile_Data profile_data_SellerOnly_Seller;

    ImageView img_profile, iv_companylogo;

    ViewPager viewPager_images;
    TabLayout indicator;

    ProgressBar progress_bar_profile, progress_bar_companylogo;


    int id = 0;
    String Seller_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_details);
        context = SellerDeatails_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);


        Intent intent = getIntent();
         Seller_id = "" + intent.getStringExtra("Seller_id");

        simpleRatingBar_quality = (ScaleRatingBar) findViewById(R.id.simpleRatingBar_quality);
        progress_bar_profile = (ProgressBar) findViewById(R.id.progress_bar_profile);
        progress_bar_companylogo = (ProgressBar) findViewById(R.id.progress_bar_companylogo);
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        tv_oneliner = (TextView) findViewById(R.id.tv_oneliner);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_servies = (TextView) findViewById(R.id.tv_servies);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        iv_companylogo = (ImageView) findViewById(R.id.iv_companylogo);

        viewPager_images = findViewById(R.id.viewPager_images);
        indicator = findViewById(R.id.indicator);


        tabs.addTab(tabs.newTab().setText("Products"));
        tabs.addTab(tabs.newTab().setText("Reviews"));


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        method_set_tab_font();

    }

    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }

    private void method_get_api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            AndroidNetworking.get(Global_Service_Api.API_get_seller_details+""+Seller_id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            Log.e("onResponse: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    Profile_Response Response = new Gson().fromJson(result.toString(), Profile_Response.class);

                                    if (Response.getData() != null) {
                                        profile_data_SellerOnly_Seller = new Profile_Data();
                                        profile_data_SellerOnly_Seller = Response.getData();
                                        method_setdata();

                                    }


                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }

    private void method_setdata() {

        if (profile_data_SellerOnly_Seller != null) {
            tv_oneliner.setText("" + profile_data_SellerOnly_Seller.getOneLine());
            tv_name.setText("" + profile_data_SellerOnly_Seller.getName() + " " + profile_data_SellerOnly_Seller.getLastname());
            tv_servies.setText("" + profile_data_SellerOnly_Seller.getServiceName());


            if (profile_data_SellerOnly_Seller.getId() != null) {
                id = profile_data_SellerOnly_Seller.getId();
            }

            if (profile_data_SellerOnly_Seller.getseller_rating() != null) {
                simpleRatingBar_quality.setRating(profile_data_SellerOnly_Seller.getseller_rating().getavg_rating());
            } else {
                simpleRatingBar_quality.setRating(0);
            }


            try {
                if (profile_data_SellerOnly_Seller.getCompanyLogo() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_SellerOnly_Seller.getCompanyLogo())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .addListener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    iv_companylogo.setImageResource(R.drawable.app_logo);
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progress_bar_companylogo.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(iv_companylogo);
                } else {
                    iv_companylogo.setImageResource(R.drawable.app_logo);
                    progress_bar_companylogo.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                iv_companylogo.setImageResource(R.drawable.app_logo);
                progress_bar_companylogo.setVisibility(View.GONE);

                e.printStackTrace();
            }

            try {
                if (profile_data_SellerOnly_Seller.getProfilePic() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + profile_data_SellerOnly_Seller.getProfilePic())
                            .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            img_profile.setImageResource(R.drawable.app_logo);
                            progress_bar_profile.setVisibility(View.GONE);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_profile.setVisibility(View.GONE);
                            return false;
                        }
                    })
                            .into(img_profile);
                } else {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                img_profile.setImageResource(R.drawable.app_logo);
                progress_bar_profile.setVisibility(View.GONE);
                e.printStackTrace();
            }


            try {
                Pager adapter = new Pager(getSupportFragmentManager(), tabs.getTabCount());
                viewPager.setAdapter(adapter);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    Fragment_review_Seller fragment_review2;
    Fragment_Shop_Seller fragment_shop;

    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);

            fragment_review2 = new Fragment_review_Seller(profile_data_SellerOnly_Seller);
            fragment_shop = new Fragment_Shop_Seller();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_shop;
                case 1:
                    return fragment_review2;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }


    @Override
    public void onResume() {

        method_get_api();

        super.onResume();
    }


}
