package com.moremy.style.API;

public class Global_Service_Api {


   public static final String BASE_URL = "https://mobile.style";                      // Live
 // public static final String BASE_URL = "http://192.168.29.3:1122";                      // Live

    //  public static final String BASE_URL = "http://192.168.0.207:5678";                 // Local

    // public static final String BASE_URL = "http://192.168.0.3:1122";                     // Local


    public static final String MAIN_URL = BASE_URL + "/Api/v1/";
    public static final String IMAGE_URL = BASE_URL + "/";
    public static final String HOST_URL = BASE_URL + ":3333";


    public static final String API_services = MAIN_URL + "services";                         // GET
    public static final String API_Sub_services = MAIN_URL + "sub_services/";                // post

    public static final String API_Stylist_Signup = MAIN_URL + "stylist_signup";             // post
    public static final String API_Seller_Signup = MAIN_URL + "seller_signup";             // post


    public static final String API_logginuser = MAIN_URL + "logginuser";                     // post

    public static final String API_get_stylist_profile = MAIN_URL + "get_stylist_profile";   // post
    public static final String API_get_seller_profile = MAIN_URL + "get_seller_profile";   // post
    public static final String API_forgotpassword_Email = MAIN_URL + "forgotpassword";       // post
    public static final String API_resetPassword = MAIN_URL + "resetPassword";               // post
    public static final String API_Login = MAIN_URL + "login";                               // post
    public static final String API_check_email = MAIN_URL + "check_email";                               // post

    public static final String API_cities = MAIN_URL + "cities";                               // post
    public static final String API_stylistlogout = MAIN_URL + "stylistlogout";                               // post

    // post
    public static final String API_update_stylist_selfie_verify = MAIN_URL + "update_stylist_selfie_verify";                               // post
    public static final String API_get_product_category = MAIN_URL + "product_category";                               // post
    public static final String API_product_sub_categories_by_product_categories = MAIN_URL + "product_sub_categories_by_product_categories";                               // post
    public static final String API_lifestyles = MAIN_URL + "lifestyles";                               // post
    public static final String API_get_product_details = MAIN_URL + "get_product_details/";                               // post


    /*...............................................*/


    public static final String API_get_salon_services = MAIN_URL + "sub_services_by_services";     // post
    public static final String API_Salon_signup = MAIN_URL + "salon_signup";        // post
    public static final String API_get_salon_profile = MAIN_URL + "get_salon_profile";        // post


    public static final String API_customer_signup = MAIN_URL + "customer_signup";        // post
    public static final String API_get_customer_profile = MAIN_URL + "get_customer_profile";        // post
    public static final String API_get_faqs = MAIN_URL + "faqs";        // get
    public static final String API_support_categories = MAIN_URL + "support_categories";        // get
    public static final String API_ssubmit_support = MAIN_URL + "submit_support";        // get
    public static final String API_add_product = MAIN_URL + "add_product";        // get
    public static final String API_update_product = MAIN_URL + "update_product/";        // get

    public static final String API_Search = MAIN_URL + "search";        // post
    public static final String API_Search_by_type = MAIN_URL + "search_by_type";        // post
    public static final String API_Search_products = MAIN_URL + "search_products";        // post
    public static final String API_Search_products_by_category = MAIN_URL + "search_products_by_category";        // post


    public static final String API_Get_product_details = MAIN_URL + "get_product_details/";        // post
    public static final String API_Get_salon_details = MAIN_URL + "get_salon_details/";        // post
    public static final String API_Get_stylist_details = MAIN_URL + "get_stylist_details/";        // post
    public static final String API_home = MAIN_URL + "home";        // post


    /*...........................*/

    public static final String API_submit_order = MAIN_URL + "submit_order";        // post
    public static final String API_update_customer_location = MAIN_URL + "update_customer_location";        // post
    public static final String API_update_customer_profile = MAIN_URL + "update_customer_profile";        // post


    public static final String API_update_stylist_profile = MAIN_URL + "update_stylist_profile";        // post
    public static final String API_update_stylist_location = MAIN_URL + "update_stylist_location";        // post

    public static final String API_update_seller_profile = MAIN_URL + "update_seller_profile";        // post
    public static final String API_update_seller_location = MAIN_URL + "update_seller_location";        // post


    public static final String API_get_orders = MAIN_URL + "get_orders";        // post
    public static final String API_update_company_logo = MAIN_URL + "update_company_logo";        // post
    public static final String API_update_profile_picture = MAIN_URL + "update_profile_picture";        // post
    public static final String API_update_stylist_portfolio = MAIN_URL + "update_stylist_portfolio";        // post
    public static final String API_delete_stylist_portfolio = MAIN_URL + "delete_stylist_portfolio";        // post
    public static final String API_get_default_card = MAIN_URL + "get_default_card";        // post
    public static final String API_set_default_card = MAIN_URL + "set_default_card";        // post
    public static final String API_my_cards = MAIN_URL + "my_cards";        // post
    public static final String API_add_card = MAIN_URL + "add_card";        // post
    public static final String API_subscribe_premium = MAIN_URL + "subscribe_premium";        // post


    public static final String API_customer_orders = MAIN_URL + "customer_orders";        // post
    public static final String API_stripe_plans = MAIN_URL + "stripe_plans";        // post
    public static final String API_update_stylist_services = MAIN_URL + "update_stylist_services";        // post
    public static final String API_get_stylist_services = MAIN_URL + "get_stylist_services";        // post
    public static final String API_update_salon_profile = MAIN_URL + "update_salon_profile";        // post
    public static final String API_update_salon_portfolio = MAIN_URL + "update_salon_portfolio";        // post
    public static final String API_delete_salon_portfolio = MAIN_URL + "delete_salon_portfolio";        // post
    public static final String API_update_salon_location = MAIN_URL + "update_salon_location";        // post
    public static final String API_order_checkout = MAIN_URL + "order_checkout";        // post
    public static final String API_submit_booking = MAIN_URL + "submit_booking";        // post


    public static final String API_get_customer_booking = MAIN_URL + "get_customer_booking";        // post
    public static final String API_get_booking = MAIN_URL + "get_booking";        // post
    public static final String API_update_order_status = MAIN_URL + "update_order_status/";        // post


    public static final String API_update_booking_status = MAIN_URL + "update_booking_status/";        // post
    public static final String API_update_booking_details = MAIN_URL + "update_booking_details/";        // post
    public static final String API_booking_checkout = MAIN_URL + "booking_checkout";        // post
    public static final String API_get_booking_details = MAIN_URL + "get_booking_details/";        // post
    public static final String API_get_order_detail = MAIN_URL + "get_order_detail/";        // post
    public static final String API_my_expenditure = MAIN_URL + "my_expenditure";        // post
    public static final String API_my_income = MAIN_URL + "my_income";        // post
    public static final String API_submit_salon_booking = MAIN_URL + "submit_salon_booking";        // post


    public static final String API_get_Selected_salon_services = MAIN_URL + "get_salon_services/";        // post
    public static final String API_get_salon_subservices = MAIN_URL + "get_salon_subservices";        // post
    public static final String API_update_salon_services = MAIN_URL + "update_salon_services";        // post
    public static final String API_my_activity = MAIN_URL + "my_activity";        // post
    public static final String APIget_send_bird_detail = MAIN_URL + "get_send_bird_detail/";        // post
    public static final String API_add_address = MAIN_URL + "add_address";        // post
    public static final String API_get_address_book = MAIN_URL + "get_address_book";        // post
    public static final String API_get_setting_keys = MAIN_URL + "get_setting_keys";        // get


    public static final String API_coming_up_activity = MAIN_URL + "coming_up_activity";        // post
    public static final String API_stripe_connect_account = MAIN_URL + "stripe_connect_account";        // get


    public static final String API_booking_review_submit = MAIN_URL + "booking_review_submit";        // get
    public static final String API_order_review_submit = MAIN_URL + "order_review_submit";        // get
    public static final String API_my_rating = MAIN_URL + "my_rating";        // get
    public static final String API_get_seller_details = MAIN_URL + "get_seller_details/";        // get


//    USER_TYPE_CUSTOMER = 2);
//    USER_TYPE_STYLIST = 3);
//    USER_TYPE_SALON = 4);

}
