package com.moremy.style.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;

import com.moremy.style.Customer.Model.Model_SapportQue_Data;
import com.moremy.style.Customer.Model.Model_SapportQue_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Fragment_Comman_Sapport extends Fragment {

    private Context context;


    public Fragment_Comman_Sapport() {
    }

    Preferences preferences;
    Dialog progressDialog = null;


    EditText et_answer_support;
    ListView listview_question;
    CardView Card_listview_que;
    TextView tv_services_name_que, tv_user_name;
    LinearLayout ll_botom_answer, ll_done_question, ll_main_question;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_sapport1, container, false);
        context = getActivity();
        preferences = new Preferences(context);


        RelativeLayout LL_customer_frag_Main = view.findViewById(R.id.LL_customer_frag_Main);
        LL_customer_frag_Main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        ll_main_question = view.findViewById(R.id.ll_main_question);
        ll_done_question = view.findViewById(R.id.ll_done_question);
        et_answer_support = view.findViewById(R.id.et_answer_support);
        ll_botom_answer = view.findViewById(R.id.ll_botom_answer);
        tv_user_name = view.findViewById(R.id.tv_user_name);
        listview_question = view.findViewById(R.id.listview_question);
        tv_services_name_que = view.findViewById(R.id.tv_services_name_que);
        Card_listview_que = view.findViewById(R.id.Card_listview_que);
        CardView card_select = view.findViewById(R.id.card_select);
        LinearLayout ll_List_select = view.findViewById(R.id.ll_List_select);


        card_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview_que.setVisibility(View.VISIBLE);
            }
        });

        ll_List_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview_que.setVisibility(View.GONE);
            }
        });


        try {
            String str = "" + preferences.getPRE_SendBirdUserName();
            String[] splitStr = str.split("\\s+");
            tv_user_name.setText("" + splitStr[0] + ", we need to talk...");
        } catch (Exception e) {
            tv_user_name.setText("we need to talk...");
            e.printStackTrace();
        }


        method_get_faq();


        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setVisibility(View.GONE);


        CardView card_submit = view.findViewById(R.id.card_submit);
        card_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_submit();
            }
        });


        TextView tv_homescreen = view.findViewById(R.id.tv_homescreen);
        tv_homescreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FragmentManager fm = getFragmentManager();
                    for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }


    private void method_submit() {

        progressDialog = utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_ssubmit_support)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addBodyParameter("category_id", "" + service_id)
                .addBodyParameter("description", "" + et_answer_support.getText().toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;
                        Log.e("sapport", result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");


                            if (flag.equalsIgnoreCase("true")) {
                                ll_main_question.setVisibility(View.GONE);
                                ll_done_question.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }

    private void method_get_faq() {

        progressDialog = utills.startLoader(context);

        AndroidNetworking.get(Global_Service_Api.API_support_categories)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;
                        Log.e("sapport", result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");


                            List<Model_SapportQue_Data> list_services = new ArrayList<>();

                            if (flag.equalsIgnoreCase("true")) {

                                Model_SapportQue_Response Response = new Gson().fromJson(result.toString(), Model_SapportQue_Response.class);

                                if (Response.getData() != null) {
                                    list_services = Response.getData();
                                    GridAdapter adapter_counties = new GridAdapter(context, list_services);
                                    listview_question.setAdapter(adapter_counties);

                                }


                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }


    String service_name = "";
    String service_id = "";

    public class GridAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        List<Model_SapportQue_Data> countries_list = new ArrayList<>();

        GridAdapter(Context context, List<Model_SapportQue_Data> data_countries_list1) {
            this.context = context;
            countries_list = data_countries_list1;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return countries_list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GridAdapter.ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new GridAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.datalist_que, parent, false);
                viewHolder.tv_textcountry = (TextView) convertView.findViewById(R.id.tv_textcountry);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (GridAdapter.ViewHolder) convertView.getTag();
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ll_botom_answer.setVisibility(View.VISIBLE);
                    Card_listview_que.setVisibility(View.GONE);
                    tv_services_name_que.setText(countries_list.get(position).getName());

                    service_name = "" + countries_list.get(position).getName();
                    service_id = "" + countries_list.get(position).getId();

                }
            });
            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());

            return convertView;
        }

        private class ViewHolder {
            TextView tv_textcountry;
        }


    }


}
