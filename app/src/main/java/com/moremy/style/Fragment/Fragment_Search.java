package com.moremy.style.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.ProductDeatails_Activty;
import com.moremy.style.CommanActivity.SalonProfile_Activty;
import com.moremy.style.CommanActivity.SellerDeatails_Activty;
import com.moremy.style.CommanActivity.StylistDeatails_Activty;
import com.moremy.style.Fragment.Model.ModelSearch_ByProduct_Response;
import com.moremy.style.Fragment.Model.ModelSearch_BySalon_Response;
import com.moremy.style.Fragment.Model.ModelSearch_ByStylist_Response;
import com.moremy.style.Fragment.Model.Model_Serch_Data;
import com.moremy.style.Fragment.Model.Model_Serch_Response;
import com.moremy.style.Fragment.Model.Model_Serch_Salon;
import com.moremy.style.Fragment.Model.Model_serch_Product;
import com.moremy.style.Fragment.Model.Model_serch_Stylist;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.PaginationScrollListener;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentManager_CUSTOMER;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentTransaction_CUSTOMER;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentManager_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentTransaction_Stylist_seller;


public class Fragment_Search extends Fragment {

    private Context context;


    public Fragment_Search() {
    }

    LinearLayout ll_Serch_By_Item, ll_Serch_All, ll_NoData_layout, ll_default_layout;

    TextView tv_showing_by_item;
    TextView tv_by_product, tv_by_stylists, tv_by_salons, tv_by_seller;
    EditText et_serch_text;
    RecyclerView rv_showing_by_item, rv_by_products, rv_by_stylists, rv_by_salons, rv_by_seller;

    Dialog progressDialog = null;
    Preferences preferences;


    List<Model_Serch_Salon> datalist_Salon = new ArrayList<>();
    List<Model_serch_Product> datalist_Product = new ArrayList<>();
    List<Model_serch_Stylist> datalist_Stylist = new ArrayList<>();
    List<Model_serch_Stylist> datalist_Seller = new ArrayList<>();


    TextView tv_no_by_salons, tv_no_by_stylists, tv_no_by_products, tv_no_by_seller;


    String type_serch = "";


    double getLongitude = 0;
    double getLatitude = 0;


    public Fragment_Search(String type) {
        type_serch = type;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_serch, container, false);
        context = getActivity();
        preferences = new Preferences(context);


        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                GPSTracker gpsTracker = new GPSTracker(context);
                if (gpsTracker != null) {
                    if (gpsTracker.getIsGPSTrackingEnabled()) {
                        getLongitude = gpsTracker.getLongitude();
                        getLatitude = gpsTracker.getLatitude();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        rv_showing_by_item = view.findViewById(R.id.rv_showing_by_item);

        rv_by_products = view.findViewById(R.id.rv_by_products);
        rv_by_stylists = view.findViewById(R.id.rv_by_stylists);
        rv_by_salons = view.findViewById(R.id.rv_by_salons);
        rv_by_seller = view.findViewById(R.id.rv_by_seller);

        rv_by_products.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        rv_by_stylists.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        rv_by_salons.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        rv_by_seller.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));

        ll_Serch_By_Item = view.findViewById(R.id.ll_Serch_By_Item);
        ll_Serch_All = view.findViewById(R.id.ll_Serch_All);
        ll_NoData_layout = view.findViewById(R.id.ll_NoData_layout);
        ll_default_layout = view.findViewById(R.id.ll_default_layout);

        et_serch_text = view.findViewById(R.id.et_serch_text);

        tv_by_product = view.findViewById(R.id.tv_by_product);
        tv_by_stylists = view.findViewById(R.id.tv_by_stylists);
        tv_by_salons = view.findViewById(R.id.tv_by_salons);
        tv_by_seller = view.findViewById(R.id.tv_by_seller);


        tv_no_by_salons = view.findViewById(R.id.tv_no_by_salons);
        tv_no_by_stylists = view.findViewById(R.id.tv_no_by_stylists);
        tv_no_by_products = view.findViewById(R.id.tv_no_by_products);
        tv_no_by_seller = view.findViewById(R.id.tv_no_by_seller);
        tv_showing_by_item = view.findViewById(R.id.tv_showing_by_item);


        datalist_Salon = new ArrayList<>();
        datalist_Product = new ArrayList<>();
        datalist_Stylist = new ArrayList<>();
        datalist_Seller = new ArrayList<>();


        et_serch_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (et_serch_text.getText().toString().length() > 3) {
                    if (type_serch.equalsIgnoreCase("product")) {
                        tv_by_product.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                        type_serch = "product";

                        datalist_Seller_only = new ArrayList<>();
                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();


                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("stylist")) {
                        tv_by_stylists.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                        type_serch = "stylist";

                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("salon")) {
                        tv_by_salons.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));

                        type_serch = "salon";


                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        //  scrollListener.resetState();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("seller")) {
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface_Bold(context));

                        type_serch = "seller";


                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        //  scrollListener.resetState();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();

                    }
                    else {
                        method_serch_all();
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                    }
                }

            }
        });


        et_serch_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    et_serch_text.clearFocus();
                    InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(et_serch_text.getWindowToken(), 0);


                    if (type_serch.equalsIgnoreCase("product")) {
                        tv_by_product.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                        type_serch = "product";

                        datalist_Seller_only = new ArrayList<>();
                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();


                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("stylist")) {
                        tv_by_stylists.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                        type_serch = "stylist";

                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("salon")) {
                        tv_by_salons.setTypeface(utills.customTypeface_Bold(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));

                        type_serch = "salon";


                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        //  scrollListener.resetState();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();
                    }
                    else if (type_serch.equalsIgnoreCase("seller")) {
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface_Bold(context));

                        type_serch = "seller";


                        datalist_Stylist_only = new ArrayList<>();
                        datalist_Seller_only = new ArrayList<>();
                        datalist_Salon_only = new ArrayList<>();
                        datalist_Product_only = new ArrayList<>();

                        //  scrollListener.resetState();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item();

                    }
                    else {
                        method_serch_all();
                        tv_by_product.setTypeface(utills.customTypeface(context));
                        tv_by_stylists.setTypeface(utills.customTypeface(context));
                        tv_by_salons.setTypeface(utills.customTypeface(context));
                        tv_by_seller.setTypeface(utills.customTypeface(context));
                    }


                    return true;
                }
                return false;
            }
        });


        tv_by_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_by_product.setTypeface(utills.customTypeface_Bold(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));
                type_serch = "product";

                datalist_Seller_only = new ArrayList<>();
                datalist_Stylist_only = new ArrayList<>();
                datalist_Salon_only = new ArrayList<>();
                datalist_Product_only = new ArrayList<>();


                isLastPage = false;
                currentPage = 1;
                method_serch_by_Item();


            }
        });
        tv_by_stylists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_by_stylists.setTypeface(utills.customTypeface_Bold(context));
                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));
                type_serch = "stylist";

                datalist_Stylist_only = new ArrayList<>();
                datalist_Seller_only = new ArrayList<>();
                datalist_Salon_only = new ArrayList<>();
                datalist_Product_only = new ArrayList<>();

                isLastPage = false;
                currentPage = 1;
                method_serch_by_Item();
            }
        });
        tv_by_salons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_by_salons.setTypeface(utills.customTypeface_Bold(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));

                type_serch = "salon";


                datalist_Stylist_only = new ArrayList<>();
                datalist_Seller_only = new ArrayList<>();
                datalist_Salon_only = new ArrayList<>();
                datalist_Product_only = new ArrayList<>();

                //  scrollListener.resetState();
                isLastPage = false;
                currentPage = 1;
                method_serch_by_Item();

            }
        });
        tv_by_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface_Bold(context));

                type_serch = "seller";


                datalist_Stylist_only = new ArrayList<>();
                datalist_Seller_only = new ArrayList<>();
                datalist_Salon_only = new ArrayList<>();
                datalist_Product_only = new ArrayList<>();

                //  scrollListener.resetState();
                isLastPage = false;
                currentPage = 1;
                method_serch_by_Item();

            }
        });

        datalist_Stylist_only = new ArrayList<>();
        datalist_Seller_only = new ArrayList<>();
        datalist_Salon_only = new ArrayList<>();
        datalist_Product_only = new ArrayList<>();


        GridLayoutManager linearLayoutManager = new GridLayoutManager(context, 2);
        rv_showing_by_item.setLayoutManager(linearLayoutManager);


        rv_showing_by_item.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                method_serch_by_Item_scroll();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        if (!type_serch.equalsIgnoreCase("")) {


            datalist_Seller_only = new ArrayList<>();
            datalist_Stylist_only = new ArrayList<>();
            datalist_Salon_only = new ArrayList<>();
            datalist_Product_only = new ArrayList<>();


            isLastPage = false;
            currentPage = 1;
            method_serch_by_Item();

            if (type_serch.equalsIgnoreCase("product")) {
                if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                    tv_showing_by_item.setText("showing in products");
                } else {
                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' in products");
                }

                tv_by_product.setTypeface(utills.customTypeface_Bold(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));

            } else if (type_serch.equalsIgnoreCase("stylist")) {

                if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                    tv_showing_by_item.setText("showing by stylists");
                } else {
                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by stylists");
                }

                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_stylists.setTypeface(utills.customTypeface_Bold(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));

            } else if (type_serch.equalsIgnoreCase("salon")) {
                if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                    tv_showing_by_item.setText("showing by locations");
                } else {
                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by locations");
                }


                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface_Bold(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));

            } else if (type_serch.equalsIgnoreCase("seller")) {
                if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                    tv_showing_by_item.setText("showing by seller");
                } else {
                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by seller");
                }


                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface_Bold(context));

            }

        }


        TextView tv_feedback_share = view.findViewById(R.id.tv_feedback_share);
        tv_feedback_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Fragment currentFragment = new Fragment_Comman_Sapport();
                LoadFragment(currentFragment);


            }
        });


        ImageView iv_cancel_serch = view.findViewById(R.id.iv_cancel_serch);
        iv_cancel_serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_serch_text.setText("");
                method_serch_all();
                tv_by_product.setTypeface(utills.customTypeface(context));
                tv_by_stylists.setTypeface(utills.customTypeface(context));
                tv_by_salons.setTypeface(utills.customTypeface(context));
                tv_by_seller.setTypeface(utills.customTypeface(context));
                type_serch="";
            }
        });


        return view;


    }


    private void LoadFragment(Fragment currentFragment) {

        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (preferences.getPRE_Typee().equalsIgnoreCase("3")) {

            fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
            fragmentTransaction_Stylist_seller.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_Stylist_seller.addToBackStack(null);
            fragmentTransaction_Stylist_seller.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("4")) {

            fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
            fragmentTransaction_SALON.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_SALON.addToBackStack(null);
            fragmentTransaction_SALON.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("2")) {
            fragmentTransaction_CUSTOMER = fragmentManager_CUSTOMER.beginTransaction();
            fragmentTransaction_CUSTOMER.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_CUSTOMER.addToBackStack(null);
            fragmentTransaction_CUSTOMER.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("5")) {

            fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
            fragmentTransaction_Stylist_seller.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_Stylist_seller.addToBackStack(null);
            fragmentTransaction_Stylist_seller.commit();

        }

    }


    int currentPage = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;


    List<Model_serch_Stylist> datalist_Seller_only = new ArrayList<>();
    List<Model_serch_Stylist> datalist_Stylist_only = new ArrayList<>();
    List<Model_Serch_Salon> datalist_Salon_only = new ArrayList<>();
    List<Model_serch_Product> datalist_Product_only = new ArrayList<>();

    Adapter_Product_Only adapter_product;
    Adapter_Stylists_Only adapter_Stylists;
    Adapter_Salons_Only adapter_Salons;
    Adapter_Seller_Only adapter_Seller;

    private void method_serch_all() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            // progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_Search)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    //   .addBodyParameter("query", "")
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                datalist_Salon = new ArrayList<>();
                                datalist_Product = new ArrayList<>();
                                datalist_Stylist = new ArrayList<>();
                                datalist_Seller = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);
                                    ll_Serch_All.setVisibility(View.VISIBLE);


                                    Model_Serch_Response Response = new Gson().fromJson(result.toString(), Model_Serch_Response.class);

                                    if (Response.getData() != null) {
                                        Model_Serch_Data model_serch_data = new Model_Serch_Data();

                                        model_serch_data = Response.getData();


                                        if (model_serch_data.getSalons() != null) {

                                            datalist_Salon.addAll(Response.getData().getSalons());

                                            if (datalist_Salon.size() > 0) {
                                                tv_no_by_salons.setVisibility(View.GONE);
                                                Adapter_Salons adapter_salons = new Adapter_Salons(context, datalist_Salon);
                                                rv_by_salons.setAdapter(adapter_salons);
                                            }


                                        } else {
                                            rv_by_salons.setVisibility(View.GONE);
                                            tv_no_by_salons.setVisibility(View.VISIBLE);
                                        }

                                        if (model_serch_data.getProducts() != null) {

                                            datalist_Product.addAll(Response.getData().getProducts());

                                            if (datalist_Product.size() > 0) {
                                                tv_no_by_products.setVisibility(View.GONE);
                                                Adapter_Product adapter_product = new Adapter_Product(context, datalist_Product);
                                                rv_by_products.setAdapter(adapter_product);
                                            }


                                        } else {
                                            rv_by_products.setVisibility(View.GONE);
                                            tv_no_by_products.setVisibility(View.VISIBLE);
                                        }

                                        if (model_serch_data.getStylists() != null) {

                                            datalist_Stylist.addAll(Response.getData().getStylists());

                                            if (datalist_Stylist.size() > 0) {
                                                tv_no_by_stylists.setVisibility(View.GONE);
                                                Adapter_Stylists adapter_stylists = new Adapter_Stylists(context, datalist_Stylist);
                                                rv_by_stylists.setAdapter(adapter_stylists);

                                            }

                                        } else {
                                            rv_by_stylists.setVisibility(View.GONE);
                                            tv_no_by_stylists.setVisibility(View.VISIBLE);
                                        }

                                        if (model_serch_data.getSellers() != null) {

                                            datalist_Seller.addAll(Response.getData().getSellers());

                                            if (datalist_Seller.size() > 0) {
                                                tv_no_by_seller.setVisibility(View.GONE);
                                                Adapter_Seller adapter_stylists = new Adapter_Seller(context, datalist_Seller);
                                                rv_by_seller.setAdapter(adapter_stylists);

                                            }

                                        } else {
                                            rv_by_seller.setVisibility(View.GONE);
                                            tv_no_by_seller.setVisibility(View.VISIBLE);
                                        }


                                    }

                                } else {


                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);
                                    ll_Serch_All.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.VISIBLE);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    private void method_serch_by_Item() {

        if (utills.isOnline(context)) {

            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_Search_by_type)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("type", "" + type_serch)
                    .addBodyParameter("latitude", "" + getLatitude)
                    // .addBodyParameter("latitude", "51.5203176" )
                    // .addBodyParameter("longitude", "-0.106265" )
                    .addBodyParameter("longitude", "" + getLongitude)
                    .addBodyParameter("page", "" + currentPage)
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);
                            isLoading = false;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_All.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.VISIBLE);


                                    if (type_serch.equalsIgnoreCase("product")) {


                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing in products");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' in products");
                                        }


                                        ModelSearch_ByProduct_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByProduct_Response.class);
                                        if (Response.getData() != null) {

                                            datalist_Product_only = Response.getData();
                                            adapter_product = new Adapter_Product_Only(context, datalist_Product_only);
                                            rv_showing_by_item.setAdapter(adapter_product);
                                        }


                                    } else if (type_serch.equalsIgnoreCase("stylist")) {

                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by stylists");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by stylists");
                                        }


                                        ModelSearch_ByStylist_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByStylist_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Stylist_only = Response.getData();
                                            adapter_Stylists = new Adapter_Stylists_Only(context, datalist_Stylist_only);
                                            rv_showing_by_item.setAdapter(adapter_Stylists);
                                        }


                                    } else if (type_serch.equalsIgnoreCase("salon")) {

                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by locations");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by locations");
                                        }

                                        ModelSearch_BySalon_Response Response = new Gson().fromJson(result.toString(), ModelSearch_BySalon_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Salon_only = Response.getData();
                                            adapter_Salons = new Adapter_Salons_Only(context, datalist_Salon_only);
                                            rv_showing_by_item.setAdapter(adapter_Salons);
                                        }


                                    } else if (type_serch.equalsIgnoreCase("seller")) {

                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by seller");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by seller");
                                        }


                                        ModelSearch_ByStylist_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByStylist_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Seller_only = Response.getData();
                                            adapter_Seller = new Adapter_Seller_Only(context, datalist_Seller_only);
                                            rv_showing_by_item.setAdapter(adapter_Seller);
                                        }


                                    }


                                } else {
                                    isLastPage = true;

                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);
                                    ll_Serch_All.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.VISIBLE);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    private void method_serch_by_Item_scroll() {

        if (utills.isOnline(context)) {

            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader_cancel(context);

            AndroidNetworking.post(Global_Service_Api.API_Search_by_type)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("type", "" + type_serch)
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    .addBodyParameter("page", "" + currentPage)
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    // .addBodyParameter("query", "")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);

                            isLoading = false;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_All.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.VISIBLE);


                                    if (type_serch.equalsIgnoreCase("product")) {

                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing in products");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' in products");
                                        }


                                        ModelSearch_ByProduct_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByProduct_Response.class);
                                        if (Response.getData() != null) {

                                            datalist_Product_only.addAll(Response.getData());
                                            adapter_product.notifyDataSetChanged();
                                        }


                                    } else if (type_serch.equalsIgnoreCase("stylist")) {
                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by stylists");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by stylists");
                                        }

                                        ModelSearch_ByStylist_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByStylist_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Stylist_only.addAll(Response.getData());
                                            adapter_Stylists.notifyDataSetChanged();

                                        }


                                    } else if (type_serch.equalsIgnoreCase("salon")) {

                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by locations");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by locations");
                                        }

                                        ModelSearch_BySalon_Response Response = new Gson().fromJson(result.toString(), ModelSearch_BySalon_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Salon_only.addAll(Response.getData());
                                            adapter_Salons.notifyDataSetChanged();
                                        }


                                    } else if (type_serch.equalsIgnoreCase("seller")) {
                                        if (et_serch_text.getText().toString().equalsIgnoreCase("")) {
                                            tv_showing_by_item.setText("showing by seller");
                                        } else {
                                            tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' by seller");
                                        }

                                        ModelSearch_ByStylist_Response Response = new Gson().fromJson(result.toString(), ModelSearch_ByStylist_Response.class);
                                        if (Response.getData() != null) {
                                            datalist_Seller_only.addAll(Response.getData());
                                            adapter_Seller.notifyDataSetChanged();

                                        }


                                    }


                                } else {

                                    isLastPage = true;


//                                    ll_default_layout.setVisibility(View.GONE);
//                                    ll_Serch_By_Item.setVisibility(View.GONE);
//                                    ll_Serch_All.setVisibility(View.GONE);
//                                    ll_NoData_layout.setVisibility(View.VISIBLE);
//
//                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    public class Adapter_Product extends RecyclerView.Adapter<Adapter_Product.MyViewHolder> {
        private List<Model_serch_Product> arrayList;

        Context mcontext;


        public Adapter_Product(Context context, List<Model_serch_Product> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_product_prize, tv_product_name;
            ImageView iv_product_image;
            LinearLayout ll_product;


            public MyViewHolder(View view) {
                super(view);
                iv_product_image = view.findViewById(R.id.iv_product_image);
                tv_product_prize = view.findViewById(R.id.tv_product_prize);
                tv_product_name = view.findViewById(R.id.tv_product_name);
                ll_product = view.findViewById(R.id.ll_product);

            }
        }


        @Override
        public Adapter_Product.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_only, parent, false);

            return new Adapter_Product.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Product.MyViewHolder holder, final int position) {


            if (position == 0) {
                holder.ll_product.setPadding(40, 0, 0, 0);
            } else {
                holder.ll_product.setPadding(0, 0, 0, 0);
            }


            if (arrayList.get(position).getPicture() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getPicture())
                        .into(holder.iv_product_image);

            }


            holder.tv_product_name.setText("" + arrayList.get(position).getName());


            holder.tv_product_prize.setText("£" + utills.roundTwoDecimals(arrayList.get(position).getPrice()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ProductDeatails_Activty.class);
                    i.putExtra("product_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Salons extends RecyclerView.Adapter<Adapter_Salons.MyViewHolder> {
        private List<Model_Serch_Salon> arrayList;

        Context mcontext;

        public Adapter_Salons(Context context, List<Model_Serch_Salon> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_salon_name;
            TextView tv_miles;
            ImageView iv_salon_image, iv_logo;
            LinearLayout ll_salon;

            public MyViewHolder(View view) {
                super(view);
                iv_salon_image = view.findViewById(R.id.iv_salon_image);
                tv_salon_name = view.findViewById(R.id.tv_salon_name);
                ll_salon = view.findViewById(R.id.ll_salon);
                iv_logo = view.findViewById(R.id.iv_logo);
                tv_miles = view.findViewById(R.id.tv_miles);
            }
        }


        @Override
        public Adapter_Salons.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_salon_only, parent, false);

            return new Adapter_Salons.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Salons.MyViewHolder holder, final int position) {

            if (position == 0) {
                holder.ll_salon.setPadding(40, 0, 0, 0);
            } else {
                holder.ll_salon.setPadding(0, 0, 0, 0);
            }

            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_salon_image);

            }
            if (arrayList.get(position).getCompanyLogo() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getCompanyLogo())
                        .into(holder.iv_logo);

            }

            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_salon_name.setText("" + arrayList.get(position).getusername());
                }
            }

            if (arrayList.get(position).getDistance() != null) {
                Double distance = arrayList.get(position).getDistance();
                String mile = String.format("%.2f", distance);
                holder.tv_miles.setText("" + mile + "mi away");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, SalonProfile_Activty.class);
                    i.putExtra("salon_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Stylists extends RecyclerView.Adapter<Adapter_Stylists.MyViewHolder> {
        private List<Model_serch_Stylist> arrayList;
        Context mcontext;

        public Adapter_Stylists(Context context, List<Model_serch_Stylist> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_stylist_name;
            TextView tv_miles;
            ImageView iv_stylist_image;
            LinearLayout ll_stylist;

            public MyViewHolder(View view) {
                super(view);
                iv_stylist_image = view.findViewById(R.id.iv_stylist_image);
                tv_stylist_name = view.findViewById(R.id.tv_stylist_name);
                tv_miles = view.findViewById(R.id.tv_miles);
                ll_stylist = view.findViewById(R.id.ll_stylist);
            }
        }


        @Override
        public Adapter_Stylists.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_stylist_only, parent, false);

            return new Adapter_Stylists.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Stylists.MyViewHolder holder, final int position) {


            if (position == 0) {
                holder.ll_stylist.setPadding(40, 0, 0, 0);
            } else {
                holder.ll_stylist.setPadding(0, 0, 0, 0);
            }

            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_stylist_image);

            }


            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_stylist_name.setText("" + arrayList.get(position).getusername());
                }
            }

            if (arrayList.get(position).getDistance() != null) {
                Double distance = arrayList.get(position).getDistance();
                String mile = String.format("%.2f", distance);
                holder.tv_miles.setText("" + mile + "mi away");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, StylistDeatails_Activty.class);
                    i.putExtra("Stylist_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Seller extends RecyclerView.Adapter<Adapter_Seller.MyViewHolder> {
        private List<Model_serch_Stylist> arrayList;
        Context mcontext;

        public Adapter_Seller(Context context, List<Model_serch_Stylist> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_stylist_name;
            TextView tv_miles;
            ImageView iv_stylist_image;
            LinearLayout ll_stylist;

            public MyViewHolder(View view) {
                super(view);
                iv_stylist_image = view.findViewById(R.id.iv_stylist_image);
                tv_stylist_name = view.findViewById(R.id.tv_stylist_name);
                tv_miles = view.findViewById(R.id.tv_miles);
                ll_stylist = view.findViewById(R.id.ll_stylist);
            }
        }


        @Override
        public Adapter_Seller.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_stylist_only, parent, false);

            return new Adapter_Seller.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Seller.MyViewHolder holder, final int position) {


            if (position == 0) {
                holder.ll_stylist.setPadding(40, 0, 0, 0);
            } else {
                holder.ll_stylist.setPadding(0, 0, 0, 0);
            }

            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_stylist_image);

            }


            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_stylist_name.setText("" + arrayList.get(position).getusername());
                }
            }

           /* if (arrayList.get(position).getDistance() != null) {
                Double distance = arrayList.get(position).getDistance();
                String mile = String.format("%.2f", distance);
                holder.tv_miles.setText("" + mile + "mi away");
            }
*/
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, SellerDeatails_Activty.class);
                    i.putExtra("Seller_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });
            holder.tv_miles.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


    public class Adapter_Product_Only extends RecyclerView.Adapter<Adapter_Product_Only.MyViewHolder> {
        private List<Model_serch_Product> arrayList;

        Context mcontext;


        public Adapter_Product_Only(Context context, List<Model_serch_Product> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_product_prize, tv_product_name;
            ImageView iv_product_image;
            LinearLayout ll_product;


            public MyViewHolder(View view) {
                super(view);
                iv_product_image = view.findViewById(R.id.iv_product_image);
                tv_product_prize = view.findViewById(R.id.tv_product_prize);
                tv_product_name = view.findViewById(R.id.tv_product_name);
                ll_product = view.findViewById(R.id.ll_product);

            }
        }


        @Override
        public Adapter_Product_Only.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_only_byitem, parent, false);

            return new Adapter_Product_Only.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Product_Only.MyViewHolder holder, final int position) {


            if (arrayList.get(position).getPicture() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getPicture())
                        .into(holder.iv_product_image);

            }


            holder.tv_product_name.setText("" + arrayList.get(position).getName());
            holder.tv_product_prize.setText("£" + utills.roundTwoDecimals(arrayList.get(position).getPrice()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ProductDeatails_Activty.class);
                    i.putExtra("product_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Stylists_Only extends RecyclerView.Adapter<Adapter_Stylists_Only.MyViewHolder> {
        private List<Model_serch_Stylist> arrayList;
        Context mcontext;

        public Adapter_Stylists_Only(Context context, List<Model_serch_Stylist> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_stylist_name;
            TextView tv_miles;
            ImageView iv_stylist_image;
            LinearLayout ll_stylist;
            ScaleRatingBar simpleRatingBar_quality;

            public MyViewHolder(View view) {
                super(view);
                iv_stylist_image = view.findViewById(R.id.iv_stylist_image);
                tv_stylist_name = view.findViewById(R.id.tv_stylist_name);
                ll_stylist = view.findViewById(R.id.ll_stylist);
                tv_miles = view.findViewById(R.id.tv_miles);
                simpleRatingBar_quality = view.findViewById(R.id.simpleRatingBar_quality);
            }
        }


        @Override
        public Adapter_Stylists_Only.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_stylist_only_byitem, parent, false);

            return new Adapter_Stylists_Only.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Stylists_Only.MyViewHolder holder, final int position) {

            if (arrayList.get(position).getstylist_rating() != null) {
                holder.simpleRatingBar_quality.setRating(arrayList.get(position).getstylist_rating().getavg_rating());
            } else {
                holder.simpleRatingBar_quality.setRating(0);
            }


            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_stylist_image);

            }

            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_stylist_name.setText("" + arrayList.get(position).getusername());
                }
            }
            if (arrayList.get(position).getDistance() != null) {
                Double distance = arrayList.get(position).getDistance();
                String mile = String.format("%.2f", distance);
                holder.tv_miles.setText("" + mile + "mi away");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, StylistDeatails_Activty.class);
                    i.putExtra("Stylist_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Salons_Only extends RecyclerView.Adapter<Adapter_Salons_Only.MyViewHolder> {
        private List<Model_Serch_Salon> arrayList;

        Context mcontext;

        public Adapter_Salons_Only(Context context, List<Model_Serch_Salon> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_salon_name;
            ImageView iv_salon_image, iv_logo;
            LinearLayout ll_salon;
            TextView tv_miles;

            public MyViewHolder(View view) {
                super(view);
                iv_salon_image = view.findViewById(R.id.iv_salon_image);
                tv_salon_name = view.findViewById(R.id.tv_salon_name);
                ll_salon = view.findViewById(R.id.ll_salon);
                iv_logo = view.findViewById(R.id.iv_logo);
                tv_miles = view.findViewById(R.id.tv_miles);
            }
        }


        @Override
        public Adapter_Salons_Only.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_salon_only_byitem, parent, false);

            return new Adapter_Salons_Only.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Salons_Only.MyViewHolder holder, final int position) {


            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_salon_image);

            }
            if (arrayList.get(position).getCompanyLogo() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getCompanyLogo())
                        .into(holder.iv_logo);

            }


            //holder.tv_salon_name.setText("" + arrayList.get(position).getSalonName());
            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_salon_name.setText("" + arrayList.get(position).getusername());
                }
            }


            if (arrayList.get(position).getDistance() != null) {
                Double distance = arrayList.get(position).getDistance();
                String mile = String.format("%.2f", distance);
                holder.tv_miles.setText("" + mile + "mi away");
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, SalonProfile_Activty.class);
                    i.putExtra("salon_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Seller_Only extends RecyclerView.Adapter<Adapter_Seller_Only.MyViewHolder> {
        private List<Model_serch_Stylist> arrayList;
        Context mcontext;

        public Adapter_Seller_Only(Context context, List<Model_serch_Stylist> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_stylist_name;
            TextView tv_miles;
            ImageView iv_stylist_image;
            LinearLayout ll_stylist;
            ScaleRatingBar simpleRatingBar_quality;

            public MyViewHolder(View view) {
                super(view);
                iv_stylist_image = view.findViewById(R.id.iv_stylist_image);
                tv_stylist_name = view.findViewById(R.id.tv_stylist_name);
                ll_stylist = view.findViewById(R.id.ll_stylist);
                tv_miles = view.findViewById(R.id.tv_miles);
                simpleRatingBar_quality = view.findViewById(R.id.simpleRatingBar_quality);
            }
        }


        @Override
        public Adapter_Seller_Only.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_stylist_only_byitem, parent, false);

            return new Adapter_Seller_Only.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Seller_Only.MyViewHolder holder, final int position) {

            if (arrayList.get(position).getstylist_rating() != null) {
                holder.simpleRatingBar_quality.setRating(arrayList.get(position).getstylist_rating().getavg_rating());
            } else {
                holder.simpleRatingBar_quality.setRating(0);
            }


            if (arrayList.get(position).getProfilePic() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getProfilePic())
                        .into(holder.iv_stylist_image);

            }

            if (arrayList.get(position).getusername() != null) {
                if (!arrayList.get(position).getusername().equalsIgnoreCase("null")) {
                    holder.tv_stylist_name.setText("" + arrayList.get(position).getusername());
                }
            }
//            if (arrayList.get(position).getDistance() != null) {
//                Double distance = arrayList.get(position).getDistance();
//                String mile = String.format("%.2f", distance);
//                holder.tv_miles.setText("" + mile + "mi away");
//            }
            holder.tv_miles.setVisibility(View.GONE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, SellerDeatails_Activty.class);
                    i.putExtra("Seller_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


    // EndlessRecyclerViewScrollListener scrollListener;

}
