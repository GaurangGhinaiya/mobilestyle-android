
package com.moremy.style.Fragment.Model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Customer.Fragment.EditAccountFragment.Model_Order.ModelOrder_Payment;
import com.moremy.style.Model_MyIncome.Model_MyIncome_BookingServiceItem;
import com.moremy.style.Model_MyIncome.Model_MyIncome_User;

public class MyActivity_Data {


    public String is_date = "";

    public MyActivity_Data(String key) {
        is_date = key;
    }


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("stylist_id")
    @Expose
    private Integer stylistId;
    @SerializedName("service_price")
    @Expose
    private float servicePrice;
    @SerializedName("send_bird_id")
    @Expose
    private String sendBirdId;
    @SerializedName("booking_date")
    @Expose
    private String bookingDate;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("is_paid")
    @Expose
    private Integer isPaid;
    @SerializedName("confirm_date")
    @Expose
    private Object confirmDate;
    @SerializedName("complete_date")
    @Expose
    private String completeDate;


    @SerializedName("cancel_by")
    @Expose
    private Integer cancelBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("activity_type")
    @Expose
    private String activityType="";
    @SerializedName("user")
    @Expose
    private Model_MyIncome_User user;
    @SerializedName("stylist")
    @Expose
    private Model_serch_Stylist stylist;
    @SerializedName("booking_service_item")
    @Expose
    private List<Model_MyIncome_BookingServiceItem> bookingServiceItem = null;

    @SerializedName("payment")
    @Expose
    private ModelOrder_Payment payment;

    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("sendgrid_order_id")
    @Expose
    private String sendgridOrderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_qty")
    @Expose
    private Integer productQty;
    @SerializedName("product_price")
    @Expose
    private float productPrice;
    @SerializedName("shipping_price")
    @Expose
    private float shippingPrice;
    @SerializedName("shipping_type")
    @Expose
    private Integer shippingType;
    @SerializedName("tracking_name")
    @Expose
    private Object trackingName;
    @SerializedName("tracking_number")
    @Expose
    private Object trackingNumber;
    @SerializedName("tracking_url")
    @Expose
    private Object trackingUrl;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("shipping_date")
    @Expose
    private String shippingDate;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("seller")
    @Expose
    private MyActivity_Seller seller;
    @SerializedName("product")
    @Expose
    private Model_serch_Product product;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStylistId() {
        return stylistId;
    }

    public void setStylistId(Integer stylistId) {
        this.stylistId = stylistId;
    }

    public float getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(float servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getSendBirdId() {
        return sendBirdId;
    }

    public void setSendBirdId(String sendBirdId) {
        this.sendBirdId = sendBirdId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public Object getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Object confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }


    public Integer getCancelBy() {
        return cancelBy;
    }

    public void setCancelBy(Integer cancelBy) {
        this.cancelBy = cancelBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public Model_MyIncome_User getUser() {
        return user;
    }

    public void setUser(Model_MyIncome_User user) {
        this.user = user;
    }

    public Model_serch_Stylist getStylist() {
        return stylist;
    }

    public void setStylist(Model_serch_Stylist stylist) {
        this.stylist = stylist;
    }

    public List<Model_MyIncome_BookingServiceItem> getBookingServiceItem() {
        return bookingServiceItem;
    }

    public void setBookingServiceItem(List<Model_MyIncome_BookingServiceItem> bookingServiceItem) {
        this.bookingServiceItem = bookingServiceItem;
    }

    public ModelOrder_Payment getPayment() {
        return payment;
    }

    public void setPayment(ModelOrder_Payment payment) {
        this.payment = payment;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSendgridOrderId() {
        return sendgridOrderId;
    }

    public void setSendgridOrderId(String sendgridOrderId) {
        this.sendgridOrderId = sendgridOrderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public float getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(float shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Integer getShippingType() {
        return shippingType;
    }

    public void setShippingType(Integer shippingType) {
        this.shippingType = shippingType;
    }

    public Object getTrackingName() {
        return trackingName;
    }

    public void setTrackingName(Object trackingName) {
        this.trackingName = trackingName;
    }

    public Object getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(Object trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public Object getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(Object trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public MyActivity_Seller getSeller() {
        return seller;
    }

    public void setSeller(MyActivity_Seller seller) {
        this.seller = seller;
    }

    public Model_serch_Product getProduct() {
        return product;
    }

    public void setProduct(Model_serch_Product product) {
        this.product = product;
    }


    @SerializedName("cancel_date")
    @Expose
    private String cancel_date;

    public String getCancel_date() {
        return cancel_date;
    }

    public void setCancel_date(String cancel_date) {
        this.cancel_date = deliveryDate;
    }


}
