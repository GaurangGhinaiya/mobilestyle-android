
package com.moremy.style.Fragment.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Comingg_Data {

    @SerializedName("completed")
    @Expose
    private List<MyActivity_Data> completed = null;

    public List<MyActivity_Data> getCompleted() {
        return completed;
    }

    public void setCompleted(List<MyActivity_Data> completed) {
        this.completed = completed;
    }

}
