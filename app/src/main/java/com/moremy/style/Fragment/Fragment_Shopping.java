package com.moremy.style.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.ProductDeatails_Activty;

import com.moremy.style.Fragment.Model_Shop.Model_Shopping_ByItem_Response;
import com.moremy.style.Fragment.Model_Shop.Model_Shopping_Data;
import com.moremy.style.Fragment.Model_Shop.Model_Shopping_Product;
import com.moremy.style.Fragment.Model_Shop.Model_Shopping_Response;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model.Services_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.PaginationScrollListener;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentManager_CUSTOMER;
import static com.moremy.style.Customer.Customer_MainProfile_Activty.fragmentTransaction_CUSTOMER;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentManager_Stylist_seller;
import static com.moremy.style.StylistSeller.StylistSeller_MainProfile_Activty.fragmentTransaction_Stylist_seller;


public class Fragment_Shopping extends Fragment {

    private Context context;


    public Fragment_Shopping() {
    }


    Dialog progressDialog = null;
    Preferences preferences;
    List<Services_Data> Salon_list_services;

    RecyclerView rv_serch_product_type, rv_serch_all, rv_showing_by_item;

    EditText et_serch_text;

    TextView tv_showing_by_item;
    LinearLayout ll_default_layout, ll_NoData_layout, ll_Serch_ALL, ll_Serch_By_Item;

    double getLongitude = 0;
    double getLatitude = 0;


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping, container, false);
        context = getActivity();


        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                GPSTracker gpsTracker = new GPSTracker(context);
                if (gpsTracker != null) {
                    if (gpsTracker.getIsGPSTrackingEnabled()) {
                        getLongitude = gpsTracker.getLongitude();
                        getLatitude = gpsTracker.getLatitude();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        preferences = new Preferences(context);

        tv_showing_by_item = view.findViewById(R.id.tv_showing_by_item);

        ll_default_layout = view.findViewById(R.id.ll_default_layout);
        ll_NoData_layout = view.findViewById(R.id.ll_NoData_layout);
        ll_Serch_ALL = view.findViewById(R.id.ll_Serch_ALL);
        ll_Serch_By_Item = view.findViewById(R.id.ll_Serch_By_Item);


        rv_serch_product_type = view.findViewById(R.id.rv_serch_product_type);
        rv_serch_all = view.findViewById(R.id.rv_serch_all);
        rv_serch_product_type.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        rv_serch_all.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));

        et_serch_text = view.findViewById(R.id.et_serch_text);


        Salon_list_services = new ArrayList<>();
        get_list();


        et_serch_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (et_serch_text.getText().toString().length() > 3) {

                    if(!serch_id.equalsIgnoreCase("")){
                        datalist_Product_only = new ArrayList<>();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item(serch_id , serch_name);

                    }else {
                        method_serch_all();
                        GridAdapter adapter_cat = new GridAdapter(context, Salon_list_services);
                        rv_serch_product_type.setAdapter(adapter_cat);
                    }

                }

            }
        });


        et_serch_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    et_serch_text.clearFocus();
                    InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(et_serch_text.getWindowToken(), 0);


                    if(!serch_id.equalsIgnoreCase("")){
                        datalist_Product_only = new ArrayList<>();
                        isLastPage = false;
                        currentPage = 1;
                        method_serch_by_Item(serch_id , serch_name);

                    }else {
                        method_serch_all();
                        GridAdapter adapter_cat = new GridAdapter(context, Salon_list_services);
                        rv_serch_product_type.setAdapter(adapter_cat);
                    }

                    return true;
                }
                return false;
            }
        });


        rv_showing_by_item = view.findViewById(R.id.rv_showing_by_item);

        GridLayoutManager linearLayoutManager = new GridLayoutManager(context, 2);
        rv_showing_by_item.setLayoutManager(linearLayoutManager);

        rv_showing_by_item.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                method_serch_by_Item_scroll(serch_id, serch_name);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        TextView tv_feedback_share = view.findViewById(R.id.tv_feedback_share);
        tv_feedback_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Fragment currentFragment = new Fragment_Comman_Sapport();
                LoadFragment(currentFragment);


            }
        });


        ImageView iv_cancel_serch=view.findViewById(R.id.iv_cancel_serch);
        iv_cancel_serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_serch_text.setText("");
                method_serch_all();
                GridAdapter adapter_cat = new GridAdapter(context, Salon_list_services);
                rv_serch_product_type.setAdapter(adapter_cat);
                serch_id = "";
                serch_name = "";
            }
        });


        return view;
    }

    private void LoadFragment(Fragment currentFragment) {

        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (preferences.getPRE_Typee().equalsIgnoreCase("3")) {

            fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
            fragmentTransaction_Stylist_seller.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_Stylist_seller.addToBackStack(null);
            fragmentTransaction_Stylist_seller.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("4")) {

            fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
            fragmentTransaction_SALON.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_SALON.addToBackStack(null);
            fragmentTransaction_SALON.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("2")) {
            fragmentTransaction_CUSTOMER = fragmentManager_CUSTOMER.beginTransaction();
            fragmentTransaction_CUSTOMER.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_CUSTOMER.addToBackStack(null);
            fragmentTransaction_CUSTOMER.commit();

        } else if (preferences.getPRE_Typee().equalsIgnoreCase("5")) {

            fragmentTransaction_Stylist_seller = fragmentManager_Stylist_seller.beginTransaction();
            fragmentTransaction_Stylist_seller.add(R.id.fmFragment, currentFragment);
            fragmentTransaction_Stylist_seller.addToBackStack(null);
            fragmentTransaction_Stylist_seller.commit();

        }

    }


    int currentPage = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    String serch_id = "", serch_name = "";
    List<Model_Shopping_Product> datalist_Product_only = new ArrayList<>();
    Adapter_Product_Only adapter_product;


    public void get_list() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_get_product_category)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("product_sub_categories", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Salon_list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {
                                        Salon_list_services.addAll(Response.getData());


                                        GridAdapter adapter_cat = new GridAdapter(context, Salon_list_services);
                                        rv_serch_product_type.setAdapter(adapter_cat);


                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    private void method_serch_all() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
          //  progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_Search_products)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    // .addBodyParameter("query", "")
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                List<Model_Shopping_Data> datalist_product = new ArrayList<>();
                                datalist_product = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_ALL.setVisibility(View.VISIBLE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);

                                    Model_Shopping_Response Response = new Gson().fromJson(result.toString(), Model_Shopping_Response.class);

                                    if (Response.getData() != null) {
                                        datalist_product.addAll(Response.getData());
                                        Adapter_Product adapter_product = new Adapter_Product(context, datalist_product);
                                        rv_serch_all.setAdapter(adapter_product);
                                    }

                                } else {

                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_ALL.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.VISIBLE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    private void method_serch_by_Item(String id, final String name) {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_Search_products_by_category)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("category_id", "" + id)
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    .addBodyParameter("page", "" + currentPage)
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    // .addBodyParameter("query", "")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("sub: ", result);
                            isLoading = false;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_ALL.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.VISIBLE);


                                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' in " + name);


                                    Model_Shopping_ByItem_Response Response = new Gson().fromJson(result.toString(), Model_Shopping_ByItem_Response.class);
                                    if (Response.getData() != null) {
                                        datalist_Product_only = new ArrayList<>();
                                        datalist_Product_only.addAll(Response.getData());
                                        adapter_product = new Adapter_Product_Only(context, datalist_Product_only);
                                        rv_showing_by_item.setAdapter(adapter_product);
                                    }


                                } else {

                                    isLastPage = true;

                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.GONE);
                                    ll_Serch_ALL.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.VISIBLE);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    private void method_serch_by_Item_scroll(String id, final String name) {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_Search_products_by_category)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("category_id", "" + id)
                    .addBodyParameter("latitude", "" + getLatitude)
                    .addBodyParameter("longitude", "" + getLongitude)
                    .addBodyParameter("page", "" + currentPage)
                    .addBodyParameter("query", "" + et_serch_text.getText().toString())
                    // .addBodyParameter("query", "")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("sub: ", result);
                            isLoading = false;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    ll_default_layout.setVisibility(View.GONE);
                                    ll_Serch_ALL.setVisibility(View.GONE);
                                    ll_NoData_layout.setVisibility(View.GONE);
                                    ll_Serch_By_Item.setVisibility(View.VISIBLE);


                                    tv_showing_by_item.setText("showing '" + et_serch_text.getText().toString() + "' in " + name);


                                    Model_Shopping_ByItem_Response Response = new Gson().fromJson(result.toString(), Model_Shopping_ByItem_Response.class);
                                    if (Response.getData() != null) {

                                        datalist_Product_only.addAll(Response.getData());
                                        adapter_product.notifyDataSetChanged();
                                    }


                                } else {

                                    isLastPage = true;


//                                    ll_default_layout.setVisibility(View.GONE);
//                                    ll_Serch_By_Item.setVisibility(View.GONE);
//                                    ll_Serch_ALL.setVisibility(View.GONE);
//                                    ll_NoData_layout.setVisibility(View.VISIBLE);
//
//                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                              Log.d("API", anError.getErrorDetail());
                        }
                    });

        }
    }


    public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Services_Data> arrayList;
        int lastPosition = -1;
        Context mcontext;

        public GridAdapter(Context context, List<Services_Data> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_textcountry;
            LinearLayout ll_serch_item;

            public MyViewHolder(View view) {
                super(view);

                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
                ll_serch_item = (LinearLayout) view.findViewById(R.id.ll_serch_item);
            }
        }

        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_service_shoping, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {


            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;


            if (arrayList.size() == 1) {
                viewHolder.ll_serch_item.setLayoutParams(new LinearLayout.LayoutParams(width,
                        RecyclerView.LayoutParams.MATCH_PARENT));
            }


            if (arrayList.size() == 2) {
                viewHolder.ll_serch_item.setLayoutParams(new LinearLayout.LayoutParams(width / 2,
                        RecyclerView.LayoutParams.MATCH_PARENT));
            }


            if (arrayList.size() == 3) {
                viewHolder.ll_serch_item.setLayoutParams(new LinearLayout.LayoutParams(width / 3,
                        RecyclerView.LayoutParams.MATCH_PARENT));
            }


            if (lastPosition == position) {
                //  viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_gray_light));
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
            } else {
                // viewHolder.tv_textcountry.setBackgroundColor(getResources().getColor(R.color.transparent));
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_medium(context));
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    lastPosition = position;
                    //  viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_gray_light));
                    viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));

                    serch_id = "" + arrayList.get(position).getId();
                    serch_name = "" + arrayList.get(position).getName();

                    datalist_Product_only = new ArrayList<>();
                    isLastPage = false;
                    currentPage = 1;

                    method_serch_by_Item("" + arrayList.get(position).getId(), "" + arrayList.get(position).getName());

                    notifyDataSetChanged();


                }
            });
            viewHolder.tv_textcountry.setText("by " + arrayList.get(position).getName());

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


    public class Adapter_Product extends RecyclerView.Adapter<Adapter_Product.MyViewHolder> {
        private List<Model_Shopping_Data> arrayList;

        Context mcontext;


        public Adapter_Product(Context context, List<Model_Shopping_Data> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_product_name_shop;
            RecyclerView rv_shop_products;


            public MyViewHolder(View view) {
                super(view);

                tv_product_name_shop = view.findViewById(R.id.tv_product_name_shop);
                rv_shop_products = view.findViewById(R.id.rv_shop_products);

            }
        }


        @Override
        public Adapter_Product.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_shopping_product, parent, false);

            return new Adapter_Product.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Product.MyViewHolder holder, final int position) {

            holder.tv_product_name_shop.setText("" + arrayList.get(position).getName());

            holder.rv_shop_products.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));


            if (arrayList.get(position).getProducts() != null) {
                List<Model_Shopping_Product> datalist_Product = new ArrayList<>();
                datalist_Product.addAll(arrayList.get(position).getProducts());
                Adapter_Product_inner adapter_product = new Adapter_Product_inner(context, datalist_Product);
                holder.rv_shop_products.setAdapter(adapter_product);
            }


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Product_inner extends RecyclerView.Adapter<Adapter_Product_inner.MyViewHolder> {
        private List<Model_Shopping_Product> arrayList;

        Context mcontext;


        public Adapter_Product_inner(Context context, List<Model_Shopping_Product> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_product_prize, tv_product_name;
            ImageView iv_product_image;
            LinearLayout ll_product;


            public MyViewHolder(View view) {
                super(view);
                iv_product_image = view.findViewById(R.id.iv_product_image);
                tv_product_prize = view.findViewById(R.id.tv_product_prize);
                tv_product_name = view.findViewById(R.id.tv_product_name);
                ll_product = view.findViewById(R.id.ll_product);

            }
        }


        @Override
        public Adapter_Product_inner.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_shop_product_only, parent, false);

            return new Adapter_Product_inner.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Product_inner.MyViewHolder holder, final int position) {


            if (position == 0) {
                holder.ll_product.setPadding(40, 0, 0, 0);
            } else {
                holder.ll_product.setPadding(0, 0, 0, 0);
            }


            if (arrayList.get(position).getPicture() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getPicture())
                        .into(holder.iv_product_image);

            }


            holder.tv_product_name.setText("" + arrayList.get(position).getName());
            holder.tv_product_prize.setText("£" + utills.roundTwoDecimals(arrayList.get(position).getPrice()));


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ProductDeatails_Activty.class);
                    i.putExtra("product_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

    public class Adapter_Product_Only extends RecyclerView.Adapter<Adapter_Product_Only.MyViewHolder> {
        private List<Model_Shopping_Product> arrayList;

        Context mcontext;


        public Adapter_Product_Only(Context context, List<Model_Shopping_Product> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_product_prize, tv_product_name;
            ImageView iv_product_image;
            LinearLayout ll_product;


            public MyViewHolder(View view) {
                super(view);
                iv_product_image = view.findViewById(R.id.iv_product_image);
                tv_product_prize = view.findViewById(R.id.tv_product_prize);
                tv_product_name = view.findViewById(R.id.tv_product_name);
                ll_product = view.findViewById(R.id.ll_product);

            }
        }


        @Override
        public Adapter_Product_Only.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_product_only_byitem, parent, false);

            return new Adapter_Product_Only.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final Adapter_Product_Only.MyViewHolder holder, final int position) {


            if (arrayList.get(position).getPicture() != null) {
                Glide.with(context)
                        .load(Global_Service_Api.IMAGE_URL + arrayList.get(position).getPicture())
                        .into(holder.iv_product_image);

            }


            holder.tv_product_name.setText("" + arrayList.get(position).getName());
            holder.tv_product_prize.setText("£" + utills.roundTwoDecimals(arrayList.get(position).getPrice()));


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ProductDeatails_Activty.class);
                    i.putExtra("product_id", "" + arrayList.get(position).getId());
                    startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }

}
