
package com.moremy.style.Fragment.Model_Shop;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Model.Model_Rating;
import com.moremy.style.Model.SendbirdOrder;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model_MyIncome.Model_MyIncome_Order;
import com.moremy.style.StylistSeller.model.ModelLifeStyle_Data;

public class ModelShopDetails_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("stylist_user")
    @Expose
    private Integer stylistUser;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_category_id")
    @Expose
    private String subCategoryId;
    @SerializedName("lifestyle_id")
    @Expose
    private String lifestyleId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;
    @SerializedName("shipping_lead_time")
    @Expose
    private String shippingLeadTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("stylist")
    @Expose
    private ModelShopDetails_Stylist stylist;
    @SerializedName("images")
    @Expose
    private List<Object> images = null;
    @SerializedName("category")
    @Expose
    private ModelShopDetails_DataCategory category;

    @SerializedName("subcategory")
    @Expose
    private ModelShopDetails_Subcategory subcategory;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStylistUser() {
        return stylistUser;
    }

    public void setStylistUser(Integer stylistUser) {
        this.stylistUser = stylistUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getLifestyleId() {
        return lifestyleId;
    }

    public void setLifestyleId(String lifestyleId) {
        this.lifestyleId = lifestyleId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingLeadTime() {
        return shippingLeadTime;
    }

    public void setShippingLeadTime(String shippingLeadTime) {
        this.shippingLeadTime = shippingLeadTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ModelShopDetails_Stylist getStylist() {
        return stylist;
    }

    public void setStylist(ModelShopDetails_Stylist stylist) {
        this.stylist = stylist;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public ModelShopDetails_DataCategory getCategory() {
        return category;
    }

    public void setCategory(ModelShopDetails_DataCategory category) {
        this.category = category;
    }



    public ModelShopDetails_Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(ModelShopDetails_Subcategory subcategory) {
        this.subcategory = subcategory;
    }



    @SerializedName("sendbird_order")
    @Expose
    private SendbirdOrder sendbird_order;


    public SendbirdOrder getsendbird_order() {
        return sendbird_order;
    }

    public void setsendbird_order(SendbirdOrder sendbird_order) {
        this.sendbird_order = sendbird_order;
    }


    @SerializedName("lifestyle")
    @Expose
    private List<ModelLifeStyle_Data> lifestyle = null;

    public List<ModelLifeStyle_Data> getlifestyle() {
        return lifestyle;
    }

    public void setlifestyle(List<ModelLifeStyle_Data> lifestyle) {
        this.lifestyle = lifestyle;
    }




   @SerializedName("selected_category")
    @Expose
    private List<Services_Data> category_list = null;


    public List<Services_Data> getcategory_list() {
        return category_list;
    }

    public void setcategory_list(List<Services_Data> category_list) {
        this.category_list = category_list;
    }




    @SerializedName("selected_subcategory")
    @Expose
    private List<Services_Data> subcategory_list = null;


    public List<Services_Data> getSubcategory_list() {
        return subcategory_list;
    }

    public void setSubcategory_list(List<Services_Data> subcategory_list) {
        this.subcategory_list = subcategory_list;
    }


}
