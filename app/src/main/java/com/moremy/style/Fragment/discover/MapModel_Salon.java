
package com.moremy.style.Fragment.discover;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapModel_Salon {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("googe_id")
    @Expose
    private Object googeId;
    @SerializedName("apple_id")
    @Expose
    private Object appleId;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("name")
    @Expose
    private Object name;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("salon_name")
    @Expose
    private String salonName;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("passbase_id")
    @Expose
    private Object passbaseId;
    @SerializedName("contact")
    @Expose
    private Object contact;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("one_line")
    @Expose
    private Object oneLine;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("date_of_birth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("user_image")
    @Expose
    private Object userImage;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("keywords")
    @Expose
    private String keywords;
    @SerializedName("address_line_1")
    @Expose
    private String addressLine1;
    @SerializedName("street_name")
    @Expose
    private String streetName;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("selling_status")
    @Expose
    private String sellingStatus;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
//    @SerializedName("radius_from_postal_code")
//    @Expose
//    private Object radiusFromPostalCode;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("is_selfie_verify")
    @Expose
    private Integer isSelfieVerify;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("fcm_token")
    @Expose
    private Object fcmToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Object getGoogeId() {
        return googeId;
    }

    public void setGoogeId(Object googeId) {
        this.googeId = googeId;
    }

    public Object getAppleId() {
        return appleId;
    }

    public void setAppleId(Object appleId) {
        this.appleId = appleId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Object getPassbaseId() {
        return passbaseId;
    }

    public void setPassbaseId(Object passbaseId) {
        this.passbaseId = passbaseId;
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getOneLine() {
        return oneLine;
    }

    public void setOneLine(Object oneLine) {
        this.oneLine = oneLine;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(String sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

//    public Object getRadiusFromPostalCode() {
//        return radiusFromPostalCode;
//    }
//
//    public void setRadiusFromPostalCode(Object radiusFromPostalCode) {
//        this.radiusFromPostalCode = radiusFromPostalCode;
//    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getIsSelfieVerify() {
        return isSelfieVerify;
    }

    public void setIsSelfieVerify(Integer isSelfieVerify) {
        this.isSelfieVerify = isSelfieVerify;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Object getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(Object fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
