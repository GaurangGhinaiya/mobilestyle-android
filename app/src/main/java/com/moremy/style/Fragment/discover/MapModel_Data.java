
package com.moremy.style.Fragment.discover;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapModel_Data {

    @SerializedName("stylists")
    @Expose
    private List<MapModel_Stylist> stylists = null;
    @SerializedName("salons")
    @Expose
    private List<MapModel_Salon> salons = null;
    @SerializedName("products")
    @Expose
    private List<MapModel_Product> products = null;

    public List<MapModel_Stylist> getStylists() {
        return stylists;
    }

    public void setStylists(List<MapModel_Stylist> stylists) {
        this.stylists = stylists;
    }

    public List<MapModel_Salon> getSalons() {
        return salons;
    }

    public void setSalons(List<MapModel_Salon> salons) {
        this.salons = salons;
    }

    public List<MapModel_Product> getProducts() {
        return products;
    }

    public void setProducts(List<MapModel_Product> products) {
        this.products = products;
    }

}
