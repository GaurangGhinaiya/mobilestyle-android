package com.moremy.style.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.BubbleLayout;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.CommanActivity.SalonProfile_Activty;
import com.moremy.style.CommanActivity.StylistDeatails_Activty;
import com.moremy.style.Fragment.discover.MapModel_Data;
import com.moremy.style.Fragment.discover.MapModel_Response;
import com.moremy.style.Fragment.discover.MapModel_Salon;
import com.moremy.style.Fragment.discover.MapModel_Stylist;
import com.moremy.style.R;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_EditAccount;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_FAQ;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_Sapport;
import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.PushUtils;
import com.moremy.style.Utills.utills;
import com.moremy.style.fcm.MyFirebaseMessagingService;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.os.Looper.getMainLooper;
import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.exponential;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.match;
import static com.mapbox.mapboxsdk.style.expressions.Expression.rgba;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.expressions.Expression.zoom;
import static com.mapbox.mapboxsdk.style.layers.Property.ICON_ANCHOR_BOTTOM;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_My_Activity_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_search_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_profile_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_discover_salon;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.ll_cart_salon;



public class Fragment_DiscoverSalon extends Fragment implements
        OnMapReadyCallback, PermissionsListener, LocationEngineCallback<LocationEngineResult> {

    private Context context;


    public Fragment_DiscoverSalon() {
    }

    Dialog progressDialog = null;
    Preferences preferences;

    TextView tv_name;
    ImageView img_profile;
    ImageView iv_companylogo;


    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;


    String token = "pk.eyJ1IjoiZm9yZXNpZ2h0dGVjaG5vbG9naWVzIiwiYSI6ImNqa3phMXBiNDBydjczcXFwNWQzbWlqNnkifQ.fQQWorOl5PVt4s4M0pwYaA";

    double getLongitude=0;
    double getLatitude=0;
    NestedScrollView scrollview;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        Mapbox.getInstance(context, token);
        View view = inflater.inflate(R.layout.fragment_discover, container, false);



        preferences = new Preferences(context);
        tv_name = view.findViewById(R.id.tv_name);
        img_profile = view.findViewById(R.id.img_profile);
        iv_companylogo = view.findViewById(R.id.iv_companylogo);

        get_name();


        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        scrollview = view.findViewById(R.id.scrollview);


        mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {  // & MotionEvent.ACTION_MASK
                    case MotionEvent.ACTION_DOWN:
                        scrollview.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        scrollview.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                mapView.onTouchEvent(event);
                return true;
            }

        });



        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    method_moresalon();

            }
        });


        CardView card_product = view.findViewById(R.id.card_product);
        CardView card_stylist = view.findViewById(R.id.card_stylist);
        CardView card_salon = view.findViewById(R.id.card_salon);
        card_stylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_set_serch("stylist");
            }
        });
        card_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_set_serch("salon");
            }
        });
        card_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_set_serch("product");
            }
        });
        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                GPSTracker gpsTracker = new GPSTracker(context);
                if (gpsTracker != null) {
                    if (gpsTracker.getIsGPSTrackingEnabled()) {
                        getLongitude = gpsTracker.getLongitude();
                        getLatitude = gpsTracker.getLatitude();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    private void method_set_serch(String type) {

        ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_cart_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_search_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_profile_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_My_Activity_salon.setBackgroundColor(getResources().getColor(R.color.transparent));

        try {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ll_search_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

        Fragment currentFragment = new Fragment_Search(type);
        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
        fragmentTransaction_SALON.replace(R.id.fmFragment, currentFragment);
        fragmentTransaction_SALON.commit();

    }



    List<Feature> symbolLayerIconFeatureList = new ArrayList<>();

    public void get_list() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.post(Global_Service_Api.API_home)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("latitude", ""+getLatitude)
                    .addBodyParameter("longitude", ""+getLongitude)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("home", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    MapModel_Response Response = new Gson().fromJson(result.toString(), MapModel_Response.class);

                                    if (Response.getData() != null) {

                                        MapModel_Data mapModel_data = new MapModel_Data();
                                        mapModel_data = Response.getData();

                                        symbolLayerIconFeatureList = new ArrayList<>();

                                        if (mapModel_data.getSalons() != null) {
                                            List<MapModel_Salon> datalist_Salon = new ArrayList<>();
                                            datalist_Salon = mapModel_data.getSalons();

                                            if (datalist_Salon.size() > 0) {
                                                for (int i = 0; i < datalist_Salon.size(); i++) {

                                                    Feature Feature_salon = Feature.fromGeometry(
                                                            Point.fromLngLat(Double.parseDouble(datalist_Salon.get(i).getLongitude()),
                                                                    Double.parseDouble(datalist_Salon.get(i).getLatitude())));
                                                    Feature_salon.addStringProperty("Title", "Salon");
                                                    Feature_salon.addStringProperty("Name", "" + datalist_Salon.get(i).getSalonName());
                                                    Feature_salon.addStringProperty("bgcolor", "a1");
                                                    Feature_salon.addStringProperty("Id", "" + datalist_Salon.get(i).getId());
                                                    Feature_salon.addBooleanProperty(PROPERTY_SELECTED, false);

                                                    symbolLayerIconFeatureList.add(Feature_salon);

                                                }

                                            }

                                        }


                                        if (mapModel_data.getStylists() != null) {
                                            List<MapModel_Stylist> datalist_Stylists = new ArrayList<>();
                                            datalist_Stylists = mapModel_data.getStylists();

                                            if (datalist_Stylists.size() > 0) {
                                                for (int i = 0; i < datalist_Stylists.size(); i++) {

                                                    Feature Feature_Stylist = Feature.fromGeometry(
                                                            Point.fromLngLat(Double.parseDouble(datalist_Stylists.get(i).getLongitude()),
                                                                    Double.parseDouble(datalist_Stylists.get(i).getLatitude())));
                                                    Feature_Stylist.addStringProperty("Title", "Stylist");
                                                    Feature_Stylist.addStringProperty("Name", "" + datalist_Stylists.get(i).getName() + " " + datalist_Stylists.get(i).getLastname());
                                                    Feature_Stylist.addStringProperty("bgcolor", "a2");
                                                    Feature_Stylist.addStringProperty("Id", "" + datalist_Stylists.get(i).getId());
                                                    Feature_Stylist.addBooleanProperty(PROPERTY_SELECTED, false);

                                                    symbolLayerIconFeatureList.add(Feature_Stylist);

                                                }

                                            }

                                        }


                                        if (mapboxMap.getStyle() != null) {

                                            source = new GeoJsonSource("map_show", FeatureCollection.fromFeatures(symbolLayerIconFeatureList));

                                            mapboxMap.getStyle().addSource(source);

                                            CircleLayer circleLayer = new CircleLayer("layer-id", "map_show");
                                            circleLayer.setSourceLayer("sf2010");
                                            circleLayer.withProperties(
                                                    circleRadius(
                                                            interpolate(
                                                                    exponential(1f),
                                                                    zoom(),
                                                                    stop(10f, 10f),
                                                                    stop(11f, 11f),
                                                                    stop(12f, 12f),
                                                                    stop(13f, 13f),
                                                                    stop(14f, 14f),
                                                                    stop(15f, 15f),
                                                                    stop(16f, 16f),
                                                                    stop(17f, 17f)


                                                            )),
                                                    circleColor(
                                                            match(get("bgcolor"), rgba(0, 0, 0, 0),
                                                                    stop("a1", "#F5CDD6"),
                                                                    stop("a2", "#CDF5D1")
                                                            )
                                                    ));


                                            mapboxMap.getStyle().addLayer(circleLayer);

                                        }

                                    }


//                               match(new Expression[]{get("bgcolor"), Expression.literal(rgb(0, 0, 0)),
//                                Expression.literal(stop("a1", "#e55e5e")),
//                                 Expression.literal(stop("a2", "#3bb2d0")),}


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap1) {
        mapboxMap = mapboxMap1;

        mapboxMap.setStyle(Style.MAPBOX_STREETS,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                        setUpInfoWindowLayer(style);

                        get_list();

                        mapboxMap.addOnMapClickListener(new MapboxMap.OnMapClickListener() {
                            @Override
                            public boolean onMapClick(@NonNull LatLng point) {
                                handleClickIcon(mapboxMap.getProjection().toScreenLocation(point));
                                return false;
                            }
                        });
                    }
                });


    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(context)) {


            LocationComponentOptions customLocationComponentOptions = LocationComponentOptions.builder(context)
                    .accuracyAlpha(.8f)
                    .accuracyAnimationEnabled(false)
                    .accuracyColor(R.color.black)
                    .backgroundTintColor(R.color.black)
                    .foregroundTintColor(R.color.black)
                    .build();
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(context, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.NORMAL);

            initLocationEngine();
//            LocationComponent locationComponent = mapboxMap.getLocationComponent();
//            locationComponent.activateLocationComponent(
//                    LocationComponentActivationOptions.builder(context, loadedMapStyle).build());
//            locationComponent.setLocationComponentEnabled(true);
//            locationComponent.setCameraMode(CameraMode.TRACKING);
//            locationComponent.setRenderMode(RenderMode.NORMAL);


        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }


    LocationEngine locationEngine;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;

    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(context);

        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationEngine.requestLocationUpdates(request, this, getMainLooper());
            locationEngine.getLastLocation(this);
            return;
        }

    }

    @Override
    public void onSuccess(LocationEngineResult result) {
        Location location = result.getLastLocation();
        if (location == null) {
            return;
        }
        if (mapboxMap != null && result.getLastLocation() != null) {
            mapboxMap.getLocationComponent().forceLocationUpdate(result.getLastLocation());
        }
    }

    @Override
    public void onFailure(@NonNull Exception exception) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(this);
        }
        mapView.onDestroy();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        // Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (GPSStatus()) {
            Log.e("onResume: ", "Your Location Services Is Enabled");
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Location Permission");
            builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(android.R.string.no, null);
            builder.show();


        }
        mapView.onResume();
    }

    public boolean GPSStatus() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private static final String PROPERTY_NAME = "Name";

    public boolean handleClickIcon(PointF screenPoint) {
        List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, "layer-id");
        if (!features.isEmpty()) {
            Feature feature = features.get(0);
            if (feature.properties() != null) {


                String Title = features.get(0).getStringProperty("Title");
                String Name = features.get(0).getStringProperty("Name");
                String Id = features.get(0).getStringProperty("Id");


                if (Title.equalsIgnoreCase("Stylist")) {
                    Intent i = new Intent(context, StylistDeatails_Activty.class);
                    i.putExtra("Stylist_id", "" + Id);
                    startActivity(i);
                } else if (Title.equalsIgnoreCase("Salon")) {
                    Intent i = new Intent(context, SalonProfile_Activty.class);
                    i.putExtra("salon_id", "" + Id);
                    startActivity(i);
                }


//                if (symbolLayerIconFeatureList != null) {
//                    for (int i = 0; i < symbolLayerIconFeatureList.size(); i++) {
//                        if (symbolLayerIconFeatureList.get(i).getStringProperty(PROPERTY_NAME).equalsIgnoreCase("" + name)) {
//                            if (featureSelectStatus(i)) {
//                                setFeatureSelectState(symbolLayerIconFeatureList.get(i), false);
//                            } else {
//                                setSelected(i);
//                            }
//                        } else {
//                            setFeatureSelectState(symbolLayerIconFeatureList.get(i), false);
//                        }
//                    }
//
//
//                    new GenerateViewIconTask(context).execute(FeatureCollection.fromFeature(feature));
//                }
            }
        } else {
            Toast.makeText(context, "no properties found", Toast.LENGTH_SHORT).show();
        }
        return true;


    }


    private void refreshSource() {
        if (source != null && symbolLayerIconFeatureList != null) {
            source.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList));
        }
    }

    private void setSelected(int index) {
        if (symbolLayerIconFeatureList != null) {
            Feature feature = symbolLayerIconFeatureList.get(index);
            setFeatureSelectState(feature, true);
            refreshSource();
        }
    }

    private void setFeatureSelectState(Feature feature, boolean selectedState) {
        if (feature.properties() != null) {
            feature.properties().addProperty(PROPERTY_SELECTED, selectedState);
            refreshSource();
        }
    }


    private boolean featureSelectStatus(int index) {
        if (symbolLayerIconFeatureList == null) {
            return false;
        }
        return symbolLayerIconFeatureList.get(index).getBooleanProperty(PROPERTY_SELECTED);
    }


//    public void handleClickIcon(PointF screenPoint) {
//        List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, "layer-id");
//        if (!features.isEmpty()) {
//
//            BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(context).inflate(R.layout.symbol_layer_info_window_layout_callout, null);
//
//            String name = features.get(0).getStringProperty("Title");
//            TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
//            titleTextView.setText(name);
//
//            String style = features.get(0).getStringProperty("Name");
//            TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
//            descriptionTextView.setText(style);
//            int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//            bubbleLayout.measure(measureSpec, measureSpec);
//
//            float measuredWidth = bubbleLayout.getMeasuredWidth();
//
//            bubbleLayout.setArrowPosition(measuredWidth / 2 - 5);
//
//            Bitmap bitmap = SymbolGenerator.generate(bubbleLayout);
//
//
//            HashMap<String, Bitmap> imagesMap = new HashMap<>();
//            imagesMap.put("popup_Images", bitmap);
//
//            mapboxMap.getStyle().addImages(imagesMap);
//        }
//    }


    public class GenerateViewIconTask extends AsyncTask<FeatureCollection, Void, HashMap<String, Bitmap>> {

        Context context;
        public Feature featureAtMapClickPoint;

        boolean refreshSource;

        GenerateViewIconTask(Context context1, boolean refreshSource) {
            context = context1;
            this.refreshSource = refreshSource;
        }

        GenerateViewIconTask(Context context1) {
            context = context1;

        }

        @SuppressWarnings("WrongThread")
        @Override
        protected HashMap<String, Bitmap> doInBackground(FeatureCollection... params) {

            HashMap<String, Bitmap> imagesMap = new HashMap<>();
            if (context != null) {
                LayoutInflater inflater = LayoutInflater.from(context);

                if (params[0].features() != null) {
                    featureAtMapClickPoint = params[0].features().get(0);


                    BubbleLayout bubbleLayout = (BubbleLayout) inflater.inflate(
                            R.layout.symbol_layer_info_window_layout_callout, null);


                    if (featureAtMapClickPoint.properties() != null) {


                        String name = featureAtMapClickPoint.getStringProperty("Title");
                        TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
                        titleTextView.setText(name);

                        String style = featureAtMapClickPoint.getStringProperty("Name");
                        TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
                        descriptionTextView.setText(style);


                        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        bubbleLayout.measure(measureSpec, measureSpec);

                        float measuredWidth = bubbleLayout.getMeasuredWidth();

                        bubbleLayout.setArrowPosition(measuredWidth / 2 - 5);

                        Bitmap bitmap = SymbolGenerator.generate(bubbleLayout);
                        imagesMap.put("popup_Images", bitmap);
                    }
                }
            }

            return imagesMap;
        }

        @Override
        public void onPostExecute(HashMap<String, Bitmap> bitmapHashMap) {
            super.onPostExecute(bitmapHashMap);

            if (bitmapHashMap != null) {
                mapboxMap.getStyle().addImages(bitmapHashMap);
                if (refreshSource) {
                    refreshSource();
                }
            }
        }

    }


    private static class SymbolGenerator {

        /**
         * Generate a Bitmap from an Android SDK View.
         *
         * @param view the View to be drawn to a Bitmap
         * @return the generated bitmap
         */
        static Bitmap generate(@NonNull View view) {
            int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            view.measure(measureSpec, measureSpec);

            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();

            view.layout(0, 0, measuredWidth, measuredHeight);
            Bitmap bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.TRANSPARENT);
            Canvas canvas = new Canvas(bitmap);
            view.draw(canvas);
            return bitmap;
        }
    }

    private static final String CALLOUT_LAYER_ID = "CALLOUT_LAYER_ID";
    private GeoJsonSource source;

    private void setUpInfoWindowLayer(@NonNull Style loadedStyle) {


        loadedStyle.addLayer(new SymbolLayer(CALLOUT_LAYER_ID, "map_show")
                .withProperties(
                        iconImage("popup_Images"),
                        iconAnchor(ICON_ANCHOR_BOTTOM),
                        iconAllowOverlap(true),
                        iconIgnorePlacement(true),
                        iconOffset(new Float[]{-2f, -28f})
                )
                /* add a filter to show only when selected feature property is true */
                .withFilter(eq((get(PROPERTY_SELECTED)), literal(true))));
    }


    private static final String PROPERTY_SELECTED = "selected";

    String profile_picsendbird = "";
    public void get_name() {
        if (utills.isOnline(context)) {

            AndroidNetworking.get(Global_Service_Api.API_logginuser)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);

                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String flag = jsonObject.getString("flag");
                                if (flag.equalsIgnoreCase("true")) {
                                    try {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");


                                        String id = "" + jsonObject1.getString("id");
                                        String name1 = "" + jsonObject1.getString("name");
                                        String lastname = "" + jsonObject1.getString("lastname");
                                        String user_type = "" + jsonObject1.getString("user_type");


                                        if (lastname.equalsIgnoreCase("null")) {
                                            lastname = "";
                                        }

                                        preferences.setPRE_Typee(""+user_type);
                                        preferences.setPRE_SendBirdUserName("" + name1 + " " + lastname);
                                        preferences.setPRE_SendBirdUserId("" + id);


                                        String name = "" + jsonObject1.getString("name");
                                        String profile_pic = "" + jsonObject1.getString("profile_pic");
                                        String company_logo = "" + jsonObject1.getString("company_logo");

                                        tv_name.setText("" + name + ", hiii!");

                                        if ((!profile_pic.equalsIgnoreCase("")) && (!profile_pic.equalsIgnoreCase("null"))) {
                                            Glide.with(context)
                                                    .load(Global_Service_Api.IMAGE_URL + profile_pic)
                                                    .into(img_profile);
                                            profile_picsendbird = Global_Service_Api.IMAGE_URL + profile_pic;
                                        }else {
                                            Glide.with(context)
                                                    .load(R.drawable.app_logo)
                                                    .into(img_profile);
                                        }

                                        if ((!company_logo.equalsIgnoreCase("")) && (!company_logo.equalsIgnoreCase("null"))) {
                                            Glide.with(context)
                                                    .load(Global_Service_Api.IMAGE_URL + company_logo)
                                                    .into(iv_companylogo);
                                        }else {
                                            Glide.with(context)
                                                    .load(R.drawable.app_logo)
                                                    .into(iv_companylogo);
                                        }

                                        SendBird.connect("" + preferences.getPRE_SendBirdUserId(), new SendBird.ConnectHandler() {
                                            @Override
                                            public void onConnected(User user, SendBirdException e) {
                                                if (e != null) {    // Error.
                                                } else {
                                                    String userNickname = "" + preferences.getPRE_SendBirdUserName();
                                                    SendBird.updateCurrentUserInfo(userNickname, profile_picsendbird, new SendBird.UserInfoUpdateHandler() {
                                                        @Override
                                                        public void onUpdated(SendBirdException e) {
                                                            if (e != null) {

                                                            }
                                                        }
                                                    });

                                                    SendBird.registerPushTokenForCurrentUser(preferences.getPRE_FCMtoken(), new SendBird.RegisterPushTokenWithStatusHandler() {
                                                        @Override
                                                        public void onRegistered(SendBird.PushTokenRegistrationStatus ptrs, SendBirdException e) {
                                                            if (e != null) {
                                                                return;
                                                            }

                                                            if (ptrs == SendBird.PushTokenRegistrationStatus.PENDING) {
                                                                // A token registration is pending.
                                                                // Retry the registration after a connection has been successfully established.
                                                            }

                                                    PushUtils.registerPushHandler(new MyFirebaseMessagingService());


                                                        }
                                                    });

                                                }

                                            }
                                        });


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.toString());
                        }
                    });

        }
    }





    /*.........*/

    private void method_moresalon() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_salon);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);

        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver "+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        final TextView  tv_MyProductsOrders = dialog.findViewById(R.id.tv_MyProductsOrders);
        tv_MyProductsOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(2);
                LoadFragment_salon(currentFragment);
            }
        });

        final TextView  tv_MyServicesBookings = dialog.findViewById(R.id.tv_MyServicesBookings);
        tv_MyServicesBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(3);
                LoadFragment_salon(currentFragment);
            }
        });




        tv_name_title.setText("" + preferences.getPRE_SendBirdUserName());

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });
            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_Sapport();
                LoadFragment_salon(currentFragment);
            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_FAQ();
                LoadFragment_salon(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(0);
                LoadFragment_salon(currentFragment);
            }
        });


        dialog.show();


    }

    public void LoadFragment_salon(final Fragment fragment) {



        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_SALON.add(R.id.fmFragment, fragment);
        fragmentTransaction_SALON.addToBackStack(null);
        fragmentTransaction_SALON.commit();



    }



    /*.........*/

    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                                preferences.setPRE_TOKEN("");


                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }





}
