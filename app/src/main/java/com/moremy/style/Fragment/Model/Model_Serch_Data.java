
package com.moremy.style.Fragment.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Serch_Data {



    @SerializedName("stylists")
    @Expose
    private List<Model_serch_Stylist> stylists = null;

    @SerializedName("sellers")
    @Expose
    private List<Model_serch_Stylist> sellers = null;



    @SerializedName("salons")
    @Expose
    private List<Model_Serch_Salon> salons = null;
    @SerializedName("products")
    @Expose
    private List<Model_serch_Product> products = null;

    public List<Model_serch_Stylist> getStylists() {
        return stylists;
    }

    public void setStylists(List<Model_serch_Stylist> stylists) {
        this.stylists = stylists;
    }

    public List<Model_serch_Stylist> getSellers() {
        return sellers;
    }

    public void setSellers(List<Model_serch_Stylist> sellers) {
        this.sellers = sellers;
    }

    public List<Model_Serch_Salon> getSalons() {
        return salons;
    }

    public void setSalons(List<Model_Serch_Salon> salons) {
        this.salons = salons;
    }

    public List<Model_serch_Product > getProducts() {
        return products;
    }

    public void setProducts(List<Model_serch_Product> products) {
        this.products = products;
    }

}
