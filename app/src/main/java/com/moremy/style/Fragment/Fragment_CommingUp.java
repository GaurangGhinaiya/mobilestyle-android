package com.moremy.style.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Booking_Fragment.ChatBookingDirect_Activity;
import com.moremy.style.CommanActivity.ChatOrderActivity;
import com.moremy.style.Fragment.Model.Model_Comingg_Response;
import com.moremy.style.Fragment.Model.MyActivity_Data;
import com.moremy.style.Model_MyIncome.Model_MyIncome_BookingServiceItem;
import com.moremy.style.R;
import com.moremy.style.Utills.PaginationScrollListener;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.moremy.style.chat.utils.ConnectionManager;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.moremy.style.Utills.utills.CONNECTION_HANDLER_ID;


public class Fragment_CommingUp extends Fragment {

    private Context context;


    public Fragment_CommingUp() {
    }


    Dialog progressDialog = null;
    Preferences preferences;

    RecyclerView rv_all_activity;
    MessageAdapter_rv adapter_cat;
    List<MyActivity_Data> data_list = new ArrayList<>();

    TextView tv_no_data;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comingup, container, false);
        context = getActivity();

        preferences = new Preferences(context);
        rv_all_activity = view.findViewById(R.id.rv_all_activity);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        tv_no_data.setVisibility(View.GONE);
        data_list = new ArrayList<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rv_all_activity.setLayoutManager(linearLayoutManager);


        rv_all_activity.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                get_list_scroll();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        get_list();

    }

    int currentPage = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;


    public void get_list() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.post(Global_Service_Api.API_coming_up_activity)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("page", "1")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            isLoading = false;


                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("coming_up", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                data_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_Comingg_Response Response = new Gson().fromJson(result.toString(), Model_Comingg_Response.class);

                                    if (Response.getData() != null) {

                                        rv_all_activity.setVisibility(View.VISIBLE);
                                        tv_no_data.setVisibility(View.GONE);



                                        /*.................*/

                                        String json = new Gson().toJson(Response.getData());
                                        JSONObject jsonObject_key = new JSONObject(json);

                                        Iterator<String> iter = jsonObject_key.keys();
                                        while (iter.hasNext()) {
                                            String key = iter.next();


                                            if (!key.equalsIgnoreCase("json_data")) {

                                                if (key.equalsIgnoreCase("completed")) {
                                                    is_completed_key = true;
                                                }

                                                MyActivity_Data data = new MyActivity_Data(key);
                                                data_list.add(data);

                                                JSONArray jsonArray = jsonObject_key.getJSONArray(key);

                                                Type listType = new TypeToken<List<MyActivity_Data>>() {
                                                }.getType();
                                                List<MyActivity_Data> list_temp = new ArrayList<>();
                                                list_temp = new Gson().fromJson(jsonArray.toString(), listType);

                                                data_list.addAll(list_temp);

                                            }


                                        }


//                                        /*.................*/
//
//                                        MyActivity_Data data = new MyActivity_Data("Completed");
//                                        data_list.add(data);
//
//                                        data_list.addAll(comingg_data.getCompleted());

                                        adapter_cat = new MessageAdapter_rv(context, data_list);
                                        rv_all_activity.setAdapter(adapter_cat);


                                    } else {
                                        tv_no_data.setVisibility(View.VISIBLE);
                                        rv_all_activity.setVisibility(View.GONE);
                                    }


                                } else {
                                    isLastPage = true;
                                    tv_no_data.setVisibility(View.VISIBLE);
                                    rv_all_activity.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    public void get_list_scroll() {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.post(Global_Service_Api.API_coming_up_activity)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("page", "" + currentPage)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            isLoading = false;


                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("coming_up", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    Model_Comingg_Response Response = new Gson().fromJson(result.toString(), Model_Comingg_Response.class);

                                    if (Response.getData() != null) {

                                        String json = new Gson().toJson(Response.getData());
                                        JSONObject jsonObject_key = new JSONObject(json);

                                        if (!is_completed_key) {

                                            Iterator<String> iter = jsonObject_key.keys();
                                            while (iter.hasNext()) {
                                                String key = iter.next();

                                                if (!key.equalsIgnoreCase("json_data")) {

                                                    if (key.equalsIgnoreCase("completed")) {
                                                        is_completed_key = true;
                                                    }

                                                    MyActivity_Data data = new MyActivity_Data(key);
                                                    data_list.add(data);

                                                    JSONArray jsonArray = jsonObject_key.getJSONArray(key);

                                                    Type listType = new TypeToken<List<MyActivity_Data>>() {
                                                    }.getType();
                                                    List<MyActivity_Data> list_temp = new ArrayList<>();
                                                    list_temp = new Gson().fromJson(jsonArray.toString(), listType);

                                                    data_list.addAll(list_temp);


                                                }


                                            }
                                        } else {
                                            Iterator<String> iter = jsonObject_key.keys();
                                            while (iter.hasNext()) {
                                                String key = iter.next();
                                                if (key.equalsIgnoreCase("completed")) {
                                                    is_completed_key = true;
                                                }

                                                JSONArray jsonArray = jsonObject_key.getJSONArray(key);
                                                Type listType = new TypeToken<List<MyActivity_Data>>() {
                                                }.getType();
                                                List<MyActivity_Data> list_temp = new ArrayList<>();
                                                list_temp = new Gson().fromJson(jsonArray.toString(), listType);

                                                data_list.addAll(list_temp);

                                            }
                                        }


                                        adapter_cat.notifyDataSetChanged();

                                    }


                                } else {
                                    isLastPage = true;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }


    }

    boolean is_completed_key = false;


    public class MessageAdapter_rv extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<MyActivity_Data> listState;
        Context context;

        public MessageAdapter_rv(Context context, List<MyActivity_Data> verticalList) {

            this.listState = verticalList;
            this.context = context;

        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;

            String item = listState.get(viewType).getActivityType();

            if (item.equalsIgnoreCase("my_bookings")) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.datalist_bookinglist_activity, parent, false);
                viewHolder = new MyViewHolder_Booking(view);
            } else if (item.equalsIgnoreCase("customer_bookings")) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.datalist_bookinglist_activity, parent, false);
                viewHolder = new MyViewHolder_Booking(view);
            } else if (item.equalsIgnoreCase("my_orders")) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.datalist_orderlist_activity, parent, false);
                viewHolder = new MyViewHolder_Product(view);
            } else if (item.equalsIgnoreCase("customer_orders")) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.datalist_orderlist_activity, parent, false);
                viewHolder = new MyViewHolder_Product(view);
            } else if (item.equalsIgnoreCase("")) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.datalist_date, parent, false);
                viewHolder = new MyViewHolder_Date(view);
            }

            return viewHolder;


        }


        protected class MyViewHolder_Date extends RecyclerView.ViewHolder {
            TextView tv_date_name;

            public MyViewHolder_Date(View view) {
                super(view);
                tv_date_name = view.findViewById(R.id.tv_date_name);

            }
        }


        protected class MyViewHolder_Product extends RecyclerView.ViewHolder {
            TextView tv_product_name_o;
            TextView tv_product_date_o;
            TextView tv_count;
            TextView tv_product_chat;
            TextView tv_product_time;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
            ProgressBar progress_bar;

            public MyViewHolder_Product(View view) {
                super(view);

                tv_product_name_o = view.findViewById(R.id.tv_product_name_o);
                tv_product_date_o = view.findViewById(R.id.tv_product_date_o);
                tv_product_chat = view.findViewById(R.id.tv_product_chat);
                iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                ll_main_cell = view.findViewById(R.id.ll_main_cell);
                progress_bar = view.findViewById(R.id.progress_bar);
                tv_product_time = view.findViewById(R.id.tv_product_time);
                tv_count = view.findViewById(R.id.tv_count);


            }
        }


        protected class MyViewHolder_Booking extends RecyclerView.ViewHolder {
            TextView tv_service_name_b;
            TextView tv_chat_details;
            TextView tv_count;

            TextView tv_booking_time;
            ImageView iv_product_image_o;
            LinearLayout ll_main_cell;
            ProgressBar progress_bar;
            TextView tv_date_b;

            public MyViewHolder_Booking(View view) {
                super(view);

                tv_service_name_b = view.findViewById(R.id.tv_service_name_b);
                tv_chat_details = view.findViewById(R.id.tv_chat_details);
                tv_count = view.findViewById(R.id.tv_count);


                iv_product_image_o = view.findViewById(R.id.iv_product_image_o);
                ll_main_cell = view.findViewById(R.id.ll_main_cell);
                progress_bar = view.findViewById(R.id.progress_bar);
                tv_booking_time = view.findViewById(R.id.tv_booking_time);
                tv_date_b = view.findViewById(R.id.tv_date_b);


            }
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

            if (holder instanceof MyViewHolder_Date) {

                final MyViewHolder_Date viewHolder_date = (MyViewHolder_Date) holder;


                if (!listState.get(position).is_date.equalsIgnoreCase("")) {

                    if (listState.get(position).is_date.equalsIgnoreCase("completed")) {
                        viewHolder_date.tv_date_name.setText("Completed");
                    } else {

                        viewHolder_date.tv_date_name.setText("" + utills.getTimeAccordingDate_Activity(listState.get(position).is_date));
                    }

                }


            } else if (holder instanceof MyViewHolder_Product) {

                final MyViewHolder_Product viewHolder_product = (MyViewHolder_Product) holder;

                ConnectionManager.addConnectionManagementHandler(context, CONNECTION_HANDLER_ID, new ConnectionManager.ConnectionManagementHandler() {
                    @Override
                    public void onConnected(boolean reconnect) {
                        GroupChannel.getChannel("" + listState.get(position).getSendgridOrderId(), new GroupChannel.GroupChannelGetHandler() {
                            @Override
                            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                                if (e != null) {
                                    // Error!
                                    e.printStackTrace();
                                    return;
                                }
                                //  Log.e("getSendgridOrderId","" + listState.get(position).getSendgridOrderId() );
                                Log.e("getSendgridOrderId", "" + groupChannel.getUnreadMessageCount());
                                int i = groupChannel.getUnreadMessageCount();

                                if (groupChannel.getLastMessage() != null) {
                                    String msg = "" + groupChannel.getLastMessage().getMessage();

                                    if (msg.equalsIgnoreCase("PAID_")) {
                                        viewHolder_product.tv_product_chat.setText("Paid");
                                    } else if (msg.equalsIgnoreCase("SHIPPED_")) {
                                        viewHolder_product.tv_product_chat.setText("Dispatched");
                                    } else if (msg.equalsIgnoreCase("REVIEW_")) {
                                        viewHolder_product.tv_product_chat.setText("Review");
                                    } else {
                                        viewHolder_product.tv_product_chat.setText("" + msg);
                                    }
                                }


                                if (i != 0) {
                                    viewHolder_product.tv_count.setVisibility(View.VISIBLE);
                                    viewHolder_product.tv_count.setText("" + i);
                                } else {
                                    viewHolder_product.tv_count.setVisibility(View.GONE);
                                }

                            }
                        });

                    }
                });


                if (listState.get(position).getOrderDate() != null) {
                    if (!listState.get(position).getOrderDate().equalsIgnoreCase("") && !listState.get(position).getOrderDate().equalsIgnoreCase("null")) {
                        viewHolder_product.progress_bar.setProgress(33);
                        viewHolder_product.tv_product_date_o.setText("ordered on " + utills.getTimeAccordingCuntry1(listState.get(position).getOrderDate()));
                    }
                }

                if (listState.get(position).getShippingDate() != null) {
                    if (!listState.get(position).getShippingDate().equalsIgnoreCase("") && !listState.get(position).getShippingDate().equalsIgnoreCase("null")) {
                        viewHolder_product.progress_bar.setProgress(60);
                        viewHolder_product.tv_product_date_o.setText("dispatched on " + utills.getTimeAccordingCuntry1(listState.get(position).getShippingDate()));
                    }
                }

                if (listState.get(position).getDeliveryDate() != null) {
                    if (!listState.get(position).getDeliveryDate().equalsIgnoreCase("") && !listState.get(position).getDeliveryDate().equalsIgnoreCase("null")) {
                        viewHolder_product.progress_bar.setProgress(100);
                        viewHolder_product.tv_product_date_o.setText("completed on " + utills.getTimeAccordingCuntry1(listState.get(position).getDeliveryDate()));
                    }
                }


                if (listState.get(position).getCancel_date() != null) {
                    if (!listState.get(position).getCancel_date().equalsIgnoreCase("") && !listState.get(position).getCancel_date().equalsIgnoreCase("null")) {
                        viewHolder_product.progress_bar.setProgress(0);
                        viewHolder_product.tv_product_date_o.setText("cancelled on " + utills.getTimeAccordingCuntry1(listState.get(position).getCancel_date()));
                    }
                }

                viewHolder_product.tv_product_name_o.setText("" + listState.get(position).getProduct().getName());
                viewHolder_product.tv_product_time.setText("" + utills.getTimeAccordingTime(listState.get(position).getOrderDate()));


                if (listState.get(position).getProduct().getPicture() != null) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + listState.get(position).getProduct().getPicture())
                            .into(viewHolder_product.iv_product_image_o);

                }


                viewHolder_product.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, ChatOrderActivity.class);
                        i.putExtra("channelUrl", "" + listState.get(position).getSendgridOrderId());
                        i.putExtra("ProductId", "" + listState.get(position).getId());
                        i.putExtra("ProductName", "" + listState.get(position).getProduct().getName());
                        i.putExtra("ProductImage", "" + listState.get(position).getProduct().getPicture());
                        i.putExtra("ProductPrize", "" + listState.get(position).getProductPrice());
                        i.putExtra("SellerImage", "" + listState.get(position).getSeller().getProfilePic());
                        i.putExtra("SellerId", "" + listState.get(position).getSeller().getId());
                        i.putExtra("SellerName", "" + listState.get(position).getSeller().getName() + " " + listState.get(position).getSeller().getLastname());
                        i.putExtra("ProductDeliveryCharge", "" + listState.get(position).getShippingPrice());
                        i.putExtra("order_id", "" + listState.get(position).getId());
                        if (listState.get(position).getPayment() != null) {
                            i.putExtra("InvoiceId", "" + listState.get(position).getPayment().getStripeInvoiceNumber());
                        } else {
                            i.putExtra("InvoiceId", "");
                        }
                        i.putExtra("is_paid", "" + listState.get(position).getIsPaid());

                        i.putExtra("getShippingDate", "" + listState.get(position).getShippingDate());
                        i.putExtra("getDeliveryDate", "" + listState.get(position).getDeliveryDate());
                        i.putExtra("getOrderDate", "" + listState.get(position).getOrderDate());
                        i.putExtra("status", "" + listState.get(position).getStatus());


                        startActivity(i);
                    }
                });


            } else if (holder instanceof MyViewHolder_Booking) {

                final MyViewHolder_Booking viewHolder = (MyViewHolder_Booking) holder;
                /*???//....................*/


                ConnectionManager.addConnectionManagementHandler(context, CONNECTION_HANDLER_ID, new ConnectionManager.ConnectionManagementHandler() {
                    @Override
                    public void onConnected(boolean reconnect) {
                        GroupChannel.getChannel("" + listState.get(position).getSendBirdId(), new GroupChannel.GroupChannelGetHandler() {
                            @Override
                            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                                if (e != null) {
                                    // Error!
                                    e.printStackTrace();
                                    return;
                                }
                                //  Log.e("getSendBirdId","" + listState.get(position).getSendBirdId() );
                                Log.e("getSendBirdId", "" + groupChannel.getUnreadMessageCount());

                                int i = groupChannel.getUnreadMessageCount();

                                if (groupChannel.getLastMessage() != null) {
                                    String msg = "" + groupChannel.getLastMessage().getMessage();

                                    if (msg.equalsIgnoreCase("PAID_")) {
                                        viewHolder.tv_chat_details.setText("Paid");
                                    } else if (msg.equalsIgnoreCase("SHIPPED_")) {
                                        viewHolder.tv_chat_details.setText("Dispatched");
                                    } else if (msg.equalsIgnoreCase("REVIEW_")) {
                                        viewHolder.tv_chat_details.setText("Review");
                                    } else {
                                        viewHolder.tv_chat_details.setText("" + msg);
                                    }

                                }

                                if (i != 0) {
                                    viewHolder.tv_count.setVisibility(View.VISIBLE);
                                    viewHolder.tv_count.setText("" + i);
                                } else {
                                    viewHolder.tv_count.setVisibility(View.GONE);
                                }


                            }
                        });

                    }
                });


                String pic = "";
                if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + listState.get(position).getUserId())) {
                    if (listState.get(position).getStylist() != null) {
                        pic = "" + listState.get(position).getStylist().getProfilePic();
                    }
                } else {
                    if (listState.get(position).getUser() != null) {
                        pic = "" + listState.get(position).getUser().getProfilePic();
                    }
                }

                if (!pic.equalsIgnoreCase("") && !pic.equalsIgnoreCase("null")) {
                    Glide.with(context)
                            .load(Global_Service_Api.IMAGE_URL + pic)
                            .into(viewHolder.iv_product_image_o);
                }


                viewHolder.tv_booking_time.setText("" + listState.get(position).getBookingTime());
                final List<Model_MyIncome_BookingServiceItem> data__list = new ArrayList<>();
                data__list.addAll(listState.get(position).getBookingServiceItem());


                StringBuilder stringBuilder_ids = new StringBuilder();
                for (int i = 0; i < data__list.size(); i++) {
                    if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                        stringBuilder_ids.append(", ");
                    }
                    stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                }


                viewHolder.tv_service_name_b.setText("" + stringBuilder_ids);


                String is_wait = "";
                if (listState.get(position).getStatus() == 0) {
                    is_wait = "cancelled for ";
                    viewHolder.progress_bar.setProgress(0);
                } else if (listState.get(position).getStatus() == 1) {
                    is_wait = "awaiting confirmation for ";
                    viewHolder.progress_bar.setProgress(30);
                } else if (listState.get(position).getStatus() == 2) {
                    is_wait = "confirmed for ";
                    viewHolder.progress_bar.setProgress(60);
                } else if (listState.get(position).getStatus() == 3) {
                    is_wait = "completed for ";
                    viewHolder.progress_bar.setProgress(100);
                }
                String date = utills.getTimeAccordingBooking(listState.get(position).getBookingDate());
                viewHolder.tv_date_b.setText(is_wait + "" + listState.get(position).getBookingTime() + " on " + date);


                viewHolder.ll_main_cell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        double price = 0;

                        StringBuilder stringBuilder_ids = new StringBuilder();
                        for (int i = 0; i < data__list.size(); i++) {
                            if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                stringBuilder_ids.append(", ");
                            }
                            price = price + data__list.get(i).getServicePrice();
                            stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                        }


                        Intent i = new Intent(context, ChatBookingDirect_Activity.class);
                        i.putExtra("grup_Name", "" + stringBuilder_ids);
                        i.putExtra("grup_price", "" + price);

                        if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + listState.get(position).getUserId())) {


                            if (listState.get(position).getStylist() != null) {
                                i.putExtra("profile_pic", "" + listState.get(position).getStylist().getProfilePic());
                            }
                        } else {
                            if (listState.get(position).getUser() != null) {
                                i.putExtra("profile_pic", "" + listState.get(position).getUser().getProfilePic());
                            }
                        }

                        i.putExtra("channelUrl", "" + listState.get(position).getSendBirdId());
                        i.putExtra("order_id", "" + listState.get(position).getId());

                        startActivity(i);

                    }
                });


            }

        }


        @Override
        public int getItemCount() {
            return listState.size();
        }


    }


    @Override
    public void onDestroy() {
        ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID);
        super.onDestroy();
    }


}
