
package com.moremy.style.Model_MyIncome;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_MyIncome_Order {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("sendgrid_order_id")
    @Expose
    private String sendgridOrderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_qty")
    @Expose
    private Integer productQty;
    @SerializedName("product_price")
    @Expose
    private float productPrice;
    @SerializedName("shipping_price")
    @Expose
    private float shippingPrice;
    @SerializedName("shipping_type")
    @Expose
    private Integer shippingType;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("tracking_name")
    @Expose
    private String trackingName;
    @SerializedName("tracking_number")
    @Expose
    private String trackingNumber;
    @SerializedName("tracking_url")
    @Expose
    private String trackingUrl;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("is_paid")
    @Expose
    private Integer isPaid;
    @SerializedName("shipping_date")
    @Expose
    private String shippingDate;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("cancel_date")
    @Expose
    private String cancelDate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("product")
    @Expose
    private Model_MyIncome_Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSendgridOrderId() {
        return sendgridOrderId;
    }

    public void setSendgridOrderId(String sendgridOrderId) {
        this.sendgridOrderId = sendgridOrderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public float getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(float shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public Integer getShippingType() {
        return shippingType;
    }

    public void setShippingType(Integer shippingType) {
        this.shippingType = shippingType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTrackingName() {
        return trackingName;
    }

    public void setTrackingName(String trackingName) {
        this.trackingName = trackingName;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Model_MyIncome_Product getProduct() {
        return product;
    }

    public void setProduct(Model_MyIncome_Product product) {
        this.product = product;
    }

}
