
package com.moremy.style.Model_MyIncome;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_MyIncome_Data {

    @SerializedName("total_income")
    @Expose
    private float totalIncome;

    @SerializedName("fees_percentage")
    @Expose
    private String fees_percentage;

    @SerializedName("income_since_date")
    @Expose
    private String income_since_date;

    @SerializedName("income_after_fees_cut")
    @Expose
    private Double incomeAfterFeesCut;
    @SerializedName("transactions")
    @Expose
    private List<Model_MyIncome_Transaction> transactions = null;

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }


    public String getfees_percentage() {
        return fees_percentage;
    }

    public void setfees_percentage(String fees_percentage) {
        this.fees_percentage = fees_percentage;
    }

    public String getincome_since_date() {
        return income_since_date;
    }

    public void setincome_since_date(String income_since_date) {
        this.income_since_date = income_since_date;
    }

    public Double getIncomeAfterFeesCut() {
        return incomeAfterFeesCut;
    }

    public void setIncomeAfterFeesCut(Double incomeAfterFeesCut) {
        this.incomeAfterFeesCut = incomeAfterFeesCut;
    }

    public List<Model_MyIncome_Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Model_MyIncome_Transaction> transactions) {
        this.transactions = transactions;
    }




}
