package com.moremy.style;

import android.app.Application;
import android.os.StrictMode;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.PushUtils;
import com.moremy.style.fcm.MyFirebaseMessagingService;
import com.sendbird.android.SendBird;
import com.stripe.android.PaymentConfiguration;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import static com.moremy.style.Utills.utills.APP_ID;
import static com.moremy.style.Utills.utills.stripe_key;


public class   AnalyticsApplication extends MultiDexApplication {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    synchronized public Tracker getDefaultTracker() {
        sAnalytics = GoogleAnalytics.getInstance(this);

        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }


    @Override
    public void onCreate() {
        super.onCreate();


        MultiDex.install(this);

        StrictMode.ThreadPolicy policy =
                new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        AndroidNetworking.initialize(getApplicationContext());


        preferences = new Preferences(getApplicationContext());
        GetToken();
        if (preferences.getPRE_sb_application_id().equalsIgnoreCase("")) {
            SendBird.init(APP_ID, getApplicationContext());
        } else {
            SendBird.init(preferences.getPRE_sb_application_id(), getApplicationContext());
        }


        if (preferences.getPRE_stripe_id().equalsIgnoreCase("")) {
            PaymentConfiguration.init(
                    getApplicationContext(),
                    "" +stripe_key
            );
        } else {
            PaymentConfiguration.init(
                    getApplicationContext(),
                    "" + preferences.getPRE_stripe_id()
            );
        }


        PushUtils.registerPushHandler(new MyFirebaseMessagingService());

    }

 public static    Preferences preferences;


    public void GetToken() {
//        if (preferencesUtility.getTokenFCM().equalsIgnoreCase("")) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String Token = instanceIdResult.getToken();
                preferences.setPRE_FCMtoken(Token);
                Log.e("token1", Token);

            }
        });
//        }
    }


}