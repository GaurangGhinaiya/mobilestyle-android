
package com.moremy.style.CommanModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Salonn_SalonImage {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("salon_id")
    @Expose
    private Integer salonId;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSalonId() {
        return salonId;
    }

    public void setSalonId(Integer salonId) {
        this.salonId = salonId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
