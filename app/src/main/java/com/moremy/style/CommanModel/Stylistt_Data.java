
package com.moremy.style.CommanModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Model.Model_Rating;
import com.moremy.style.Model.Review.ModelReview_Data;

public class Stylistt_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("googe_id")
    @Expose
    private Object googeId;
    @SerializedName("apple_id")
    @Expose
    private Object appleId;
    @SerializedName("login_type")
    @Expose
    private String loginType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private Object username;
    @SerializedName("is_premium")
    @Expose
    private Integer isPremium;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("salon_name")
    @Expose
    private Object salonName;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("passbase_id")
    @Expose
    private Object passbaseId;
    @SerializedName("contact")
    @Expose
    private Object contact;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("one_line")
    @Expose
    private String oneLine;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("date_of_birth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("user_image")
    @Expose
    private Object userImage;
    @SerializedName("company_logo")
    @Expose
    private Object companyLogo;
    @SerializedName("website_url")
    @Expose
    private Object websiteUrl;
    @SerializedName("instagram_url")
    @Expose
    private Object instagramUrl;
    @SerializedName("telephone")
    @Expose
    private Object telephone;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("keywords")
    @Expose
    private Object keywords;
    @SerializedName("address_line_1")
    @Expose
    private String addressLine1;
    @SerializedName("street_name")
    @Expose
    private Object streetName;
    @SerializedName("county")
    @Expose
    private Object county;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("selling_status")
    @Expose
    private String sellingStatus;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("radius_from_postal_code")
    @Expose
    private String radiusFromPostalCode;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("is_selfie_verify")
    @Expose
    private Integer isSelfieVerify;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("fcm_token")
    @Expose
    private Object fcmToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("stylist_subservices")
    @Expose
    private List<Stylistt_StylistSubservice> stylistSubservices = null;
    @SerializedName("stylist_portfolio")
    @Expose
    private List<Stylistt_StylistPortfolio> stylistPortfolio = null;
    @SerializedName("service")
    @Expose
    private Stylistt_Service_ service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Object getGoogeId() {
        return googeId;
    }

    public void setGoogeId(Object googeId) {
        this.googeId = googeId;
    }

    public Object getAppleId() {
        return appleId;
    }

    public void setAppleId(Object appleId) {
        this.appleId = appleId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getUsername() {
        return username;
    }

    public void setUsername(Object username) {
        this.username = username;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public Object getSalonName() {
        return salonName;
    }

    public void setSalonName(Object salonName) {
        this.salonName = salonName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Object getPassbaseId() {
        return passbaseId;
    }

    public void setPassbaseId(Object passbaseId) {
        this.passbaseId = passbaseId;
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOneLine() {
        return oneLine;
    }

    public void setOneLine(String oneLine) {
        this.oneLine = oneLine;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public Object getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(Object companyLogo) {
        this.companyLogo = companyLogo;
    }

    public Object getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(Object websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public Object getInstagramUrl() {
        return instagramUrl;
    }

    public void setInstagramUrl(Object instagramUrl) {
        this.instagramUrl = instagramUrl;
    }

    public Object getTelephone() {
        return telephone;
    }

    public void setTelephone(Object telephone) {
        this.telephone = telephone;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getKeywords() {
        return keywords;
    }

    public void setKeywords(Object keywords) {
        this.keywords = keywords;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public Object getStreetName() {
        return streetName;
    }

    public void setStreetName(Object streetName) {
        this.streetName = streetName;
    }

    public Object getCounty() {
        return county;
    }

    public void setCounty(Object county) {
        this.county = county;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public String getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(String sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRadiusFromPostalCode() {
        return radiusFromPostalCode;
    }

    public void setRadiusFromPostalCode(String radiusFromPostalCode) {
        this.radiusFromPostalCode = radiusFromPostalCode;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getIsSelfieVerify() {
        return isSelfieVerify;
    }

    public void setIsSelfieVerify(Integer isSelfieVerify) {
        this.isSelfieVerify = isSelfieVerify;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Object getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(Object fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Stylistt_StylistSubservice> getStylistSubservices() {
        return stylistSubservices;
    }

    public void setStylistSubservices(List<Stylistt_StylistSubservice> stylistSubservices) {
        this.stylistSubservices = stylistSubservices;
    }

    public List<Stylistt_StylistPortfolio> getStylistPortfolio() {
        return stylistPortfolio;
    }

    public void setStylistPortfolio(List<Stylistt_StylistPortfolio> stylistPortfolio) {
        this.stylistPortfolio = stylistPortfolio;
    }

    public Stylistt_Service_ getService() {
        return service;
    }

    public void setService(Stylistt_Service_ service) {
        this.service = service;
    }

    @SerializedName("stylist_rating")
    @Expose
    private Model_Rating stylist_rating;

    public Model_Rating getstylist_rating() {
        return stylist_rating;
    }

    public void setstylist_rating(Model_Rating stylist_rating) {
        this.stylist_rating = stylist_rating;
    }

    @SerializedName("stylist_rating_list")
    @Expose
    private List<ModelComman_Review_Data> stylist_rating_list = null;

    public List<ModelComman_Review_Data> getReview() {
        return stylist_rating_list;
    }

    public void setReview(List<ModelComman_Review_Data> stylist_rating_list) {
        this.stylist_rating_list = stylist_rating_list;
    }




}
