
package com.moremy.style.CommanModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Model.Model_Rating;

public class Salonn_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("salon_name")
    @Expose
    private String salonName;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("keywords")
    @Expose
    private List<String> keywords = null;
    @SerializedName("address_line_1")
    @Expose
    private String addressLine1;
    @SerializedName("street_name")
    @Expose
    private String streetName;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("one_line")
    @Expose
    private Object oneLine;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("service")
    @Expose
    private List<Salonn_Service> service = null;
    @SerializedName("salon_images")
    @Expose
    private List<Salonn_SalonImage> salonImages = null;
    @SerializedName("salon_portfolio")
    @Expose
    private List<Stylistt_StylistPortfolio> salonPortfolio = null;
    @SerializedName("salon_opening_days")
    @Expose
    private List<Object> salonOpeningDays = null;
    @SerializedName("salon_subservices")
    @Expose
    private List<Salonn_SalonSubservice> salonSubservices = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Object getOneLine() {
        return oneLine;
    }

    public void setOneLine(Object oneLine) {
        this.oneLine = oneLine;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Salonn_Service> getService() {
        return service;
    }

    public void setService(List<Salonn_Service> service) {
        this.service = service;
    }

    public List<Salonn_SalonImage> getSalonImages() {
        return salonImages;
    }

    public void setSalonImages(List<Salonn_SalonImage> salonImages) {
        this.salonImages = salonImages;
    }

    public List<Stylistt_StylistPortfolio> getSalonPortfolio() {
        return salonPortfolio;
    }

    public void setSalonPortfolio(List<Stylistt_StylistPortfolio> salonPortfolio) {
        this.salonPortfolio = salonPortfolio;
    }

    public List<Object> getSalonOpeningDays() {
        return salonOpeningDays;
    }

    public void setSalonOpeningDays(List<Object> salonOpeningDays) {
        this.salonOpeningDays = salonOpeningDays;
    }

    public List<Salonn_SalonSubservice> getSalonSubservices() {
        return salonSubservices;
    }

    public void setSalonSubservices(List<Salonn_SalonSubservice> salonSubservices) {
        this.salonSubservices = salonSubservices;
    }

    @SerializedName("salon_rating")
    @Expose
    private Model_Rating stylist_rating;

    public Model_Rating getstylist_rating() {
        return stylist_rating;
    }

    public void setstylist_rating(Model_Rating stylist_rating) {
        this.stylist_rating = stylist_rating;
    }

    @SerializedName("salon_rating_list")
    @Expose
    private List<ModelComman_Review_Data> stylist_rating_list = null;

    public List<ModelComman_Review_Data> getReview() {
        return stylist_rating_list;
    }

    public void setReview(List<ModelComman_Review_Data> stylist_rating_list) {
        this.stylist_rating_list = stylist_rating_list;
    }



}
