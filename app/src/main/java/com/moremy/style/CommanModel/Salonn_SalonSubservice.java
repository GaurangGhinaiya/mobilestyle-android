
package com.moremy.style.CommanModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Salonn_SalonSubservice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("salon_id")
    @Expose
    private Integer salonId;
    @SerializedName("subservice_id")
    @Expose
    private Integer subserviceId;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("service")
    @Expose
    private Stylistt_Service service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSalonId() {
        return salonId;
    }

    public void setSalonId(Integer salonId) {
        this.salonId = salonId;
    }

    public Integer getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(Integer subserviceId) {
        this.subserviceId = subserviceId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Stylistt_Service getService() {
        return service;
    }

    public void setService(Stylistt_Service service) {
        this.service = service;
    }

    public boolean is_chechkbox = false;
}
