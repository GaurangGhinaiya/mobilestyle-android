package com.moremy.style.chat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Model.MyCard_Data;
import com.moremy.style.Model.MyCard_Response;
import com.moremy.style.R;

import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Utills.NonScrollListView;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.moremy.style.chat.utils.ConnectionManager;
import com.moremy.style.chat.utils.MediaPlayerActivity;
import com.moremy.style.chat.utils.PhotoViewerActivity;

import com.moremy.style.chat.utils.WebUtils;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;


import com.moremy.style.chat.utils.FileUtils;
import com.moremy.style.chat.utils.TextUtils;
import com.moremy.style.chat.utils.UrlPreviewInfo;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CardUtils;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.CardParams;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import static com.moremy.style.Utills.utills.CONNECTION_HANDLER_ID;


public class GroupChatFragment extends Fragment {

    private static final String LOG_TAG = GroupChatFragment.class.getSimpleName();

    private static final int CHANNEL_LIST_LIMIT = 30;

    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_CHAT";

    private static final int STATE_NORMAL = 0;
    private static final int STATE_EDIT = 1;

    private static final String STATE_CHANNEL_URL = "STATE_CHANNEL_URL";
    private static final int INTENT_REQUEST_CHOOSE_MEDIA = 301;
    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 13;
    static final String EXTRA_CHANNEL_URL = "EXTRA_CHANNEL_URL";

    private InputMethodManager mIMM;
    private HashMap<BaseChannel.SendFileMessageWithProgressHandler, FileMessage> mFileProgressHandlerMap;

    private RelativeLayout mRootLayout;
    private RecyclerView mRecyclerView;
    private GroupChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;
    private EditText mMessageEditText;
    private ImageView mMessageSendButton;
    private ImageView mUploadFileButton;
    private View mCurrentEventLayout;
    private TextView mCurrentEventText;

    private GroupChannel mChannel;
    private String mChannelUrl;
    private PreviousMessageListQuery mPrevMessageListQuery;

    private boolean mIsTyping;

    private int mCurrentState = STATE_NORMAL;
    private BaseMessage mEditingMessage = null;

    /**
     * To create an instance of this fragment, a Channel URL should be required.
     *
     * @param channelUrl
     */

    static float totalCharge;

    public GroupChatFragment() {
    }

    public GroupChatFragment(String channelUrl, float totalCharge1, int i) {
        mChannelUrl = channelUrl;
        totalCharge = totalCharge1;
        is_boolen_user_or_seller = i;
    }

    static TextView tv_chechk_out_prize, tv_bynow_prize;
    static Context context;
    String ProductId = "";
    String ProductName = "";
    String ProductImage = "";
    String ProductPrize = "";
    String ProductDeliveryCharge = "";
    public static String order_id_product = "";
    static String InvoiceId = "";
    static String is_paid = "";
    static String status = "";
    String Shipp_Type = "";
    String is_seller_id = "";


    static String card_paid = "";

    static LinearLayout ll_by_now;

    List<MyCard_Data> data_order_list = new ArrayList<>();

    TextView tv_cardname, tv_minus, tv_plus, tv_quantity;
    ImageView iv_changecard_icon;

    int total_quantity = 1;


    static TextView tv_order_now, tv_dispatched_now, tv_completed_item, tv_Dispatch_item_final, tv_Delivered_item_final, tv_btn_WriteReview;

    static ProgressBar progress_bar;

    static int is_boolen_user_or_seller = 0; //0 customer // 1 seller


    static String getShippingDate = "";
    static String getDeliveryDate = "";
    static String getOrderDate = "";
    static String getCancelDate = "";

    public static String rating_product = "";


    static TextView tv_cancel_item, tv_confirm_item;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        preferences = new Preferences(context);


        if (getArguments() != null) {

            ProductId = "" + getArguments().getString("ProductId");
            ProductName = "" + getArguments().getString("ProductName");
            ProductImage = "" + getArguments().getString("ProductImage");
            ProductPrize = "" + getArguments().getString("ProductPrize");
            ProductDeliveryCharge = "" + getArguments().getString("ProductDeliveryCharge");
            order_id_product = "" + getArguments().getString("order_id");

            InvoiceId = "" + getArguments().getString("InvoiceId");
            is_paid = "" + getArguments().getString("is_paid");


            getShippingDate = "" + getArguments().getString("getShippingDate");
            getDeliveryDate = "" + getArguments().getString("getDeliveryDate");
            getOrderDate = "" + getArguments().getString("getOrderDate");
            getCancelDate = "" + getArguments().getString("getCancelDate");
            status = "" + getArguments().getString("status");
            card_paid = "" + getArguments().getString("card_paid");
            Shipp_Type = "" + getArguments().getString("Shipp_Type");
            is_seller_id = "" + getArguments().getString("is_seller_id");


            text_trackingnumber = "" + getArguments().getString("text_trackingnumber");
            text_shippingcompany = "" + getArguments().getString("text_shippingcompany");
            tracking_url = "" + getArguments().getString("tracking_url");
            total_quantity = Integer.parseInt("" + getArguments().getString("quantity"));

            rating_product = "" + getArguments().getString("rating");


        }


        mIMM = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mFileProgressHandlerMap = new HashMap<>();


        // Log.d(LOG_TAG, mChannelUrl);

        mChatAdapter = new GroupChatAdapter(getActivity(), ProductName,
                ProductImage, InvoiceId, totalCharge, card_paid, getOrderDate, is_boolen_user_or_seller);
        setUpChatListAdapter();

        // Load messages from cache.
        mChatAdapter.load(mChannelUrl);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);

        setRetainInstance(true);


        tv_cancel_item = rootView.findViewById(R.id.tv_cancel_item);
        tv_confirm_item = rootView.findViewById(R.id.tv_confirm_item);
        progress_bar = rootView.findViewById(R.id.progress_bar);
        tv_order_now = rootView.findViewById(R.id.tv_order_now);
        tv_dispatched_now = rootView.findViewById(R.id.tv_dispatched_now);
        tv_completed_item = rootView.findViewById(R.id.tv_completed_item);
        tv_Dispatch_item_final = rootView.findViewById(R.id.tv_Dispatch_item_final);
        tv_Delivered_item_final = rootView.findViewById(R.id.tv_Delivered_item_final);
        tv_btn_WriteReview = rootView.findViewById(R.id.tv_btn_WriteReview);


        tv_minus = rootView.findViewById(R.id.tv_minus);
        tv_quantity = rootView.findViewById(R.id.tv_quantity);
        tv_plus = rootView.findViewById(R.id.tv_plus);
        tv_cardname = rootView.findViewById(R.id.tv_cardname);
        iv_changecard_icon = rootView.findViewById(R.id.iv_changecard_icon);

        tv_bynow_prize = rootView.findViewById(R.id.tv_bynow_prize);
        ll_by_now = rootView.findViewById(R.id.ll_by_now);
        tv_chechk_out_prize = rootView.findViewById(R.id.tv_chechk_out_prize);
        tv_chechk_out_prize.setText("checkout (£" + utills.roundTwoDecimals(totalCharge) + ")");


        ImageView iv_profile_image = rootView.findViewById(R.id.iv_profile_image);
        TextView tv_ProductName = rootView.findViewById(R.id.tv_ProductName);
        TextView tv_ProductPrize = rootView.findViewById(R.id.tv_ProductPrize);
        TextView tv_ProductDelivery = rootView.findViewById(R.id.tv_ProductDelivery);

        tv_ProductName.setText("" + ProductName);
        tv_ProductPrize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(ProductPrize)));


        String stype = "";
        if (Shipp_Type.equalsIgnoreCase("1")) {
            stype = " (per order)";
        } else {
            stype = " (per item)";
        }

        if (ProductDeliveryCharge != null) {
            if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) &&
                    (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                tv_ProductDelivery.setText("+ £" + utills.roundTwoDecimals(Double.parseDouble(ProductDeliveryCharge)) + " delivery" + stype);
            } else {
                tv_ProductDelivery.setText("+ £0 delivery" + stype);
            }
        } else {
            tv_ProductDelivery.setText("+ £" + "0 delivery" + stype);
        }

        if (!ProductImage.equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(Global_Service_Api.IMAGE_URL + ProductImage)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }


        if (is_boolen_user_or_seller == 0) {  // customer

            tv_Dispatch_item_final.setVisibility(View.GONE);
            tv_chechk_out_prize.setVisibility(View.GONE);
            ll_by_now.setVisibility(View.GONE);
            tv_Delivered_item_final.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);

            if (status.equalsIgnoreCase("0")) {
                tv_chechk_out_prize.setVisibility(View.GONE);
                ll_by_now.setVisibility(View.GONE);
            } /*else if (is_paid.equalsIgnoreCase("1")) {
                tv_chechk_out_prize.setVisibility(View.GONE);
                ll_by_now.setVisibility(View.GONE);
            }*/ else if (is_paid.equalsIgnoreCase("0") && status.equalsIgnoreCase("4")) {
                tv_chechk_out_prize.setVisibility(View.VISIBLE);
                ll_by_now.setVisibility(View.GONE);
            } else if (is_paid.equalsIgnoreCase("1") && status.equalsIgnoreCase("2")) {
                tv_Delivered_item_final.setVisibility(View.VISIBLE);
            } else if ((status.equalsIgnoreCase("3")) && is_paid.equalsIgnoreCase("1")) {

                if (rating_product.equalsIgnoreCase("") || rating_product.equalsIgnoreCase("null")) {
                    tv_btn_WriteReview.setVisibility(View.VISIBLE);
                }


            }


        } else {  // seller

            tv_Dispatch_item_final.setVisibility(View.GONE);
            tv_chechk_out_prize.setVisibility(View.GONE);
            ll_by_now.setVisibility(View.GONE);
            tv_Delivered_item_final.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);

            if (status.equalsIgnoreCase("0")) {
                tv_Dispatch_item_final.setVisibility(View.GONE);
                tv_confirm_item.setVisibility(View.GONE);
                tv_cancel_item.setVisibility(View.GONE);
                tv_Delivered_item_final.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("1")) {

                tv_confirm_item.setVisibility(View.VISIBLE);
                tv_cancel_item.setVisibility(View.VISIBLE);
                // cancel and confirm visible
            } else if (is_paid.equalsIgnoreCase("1") && status.equalsIgnoreCase("4")) {
                tv_Dispatch_item_final.setVisibility(View.VISIBLE);
            }

        }


        // status == 0 button gone then
        //  customer
        // customer  status == 4 , is_paid=0 &  payment button gone  && is_paid=1 then gone
        // is paid== 1 then


        //  seller
// cancel and confirm
        // status 1 then   if cancel then 0 send api
        // status 1 then  confirm stuts 4 send api
        // is paid==1 and status== 4  then ship button and
        //status ==2 then ispaid==1 then deliver button  then send api status==3 api


        if (!getOrderDate.equalsIgnoreCase("") && !getOrderDate.equalsIgnoreCase("null")) {
            tv_order_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(33);
            tv_order_now.setText("ordered (" + utills.getTimeAccordingDate(getOrderDate) + ")");
        }

        if (!getShippingDate.equalsIgnoreCase("") && !getShippingDate.equalsIgnoreCase("null")) {
            tv_dispatched_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(60);
            tv_dispatched_now.setText("dispatched (" + utills.getTimeAccordingDate(getShippingDate) + ")");
        }

        if (!getDeliveryDate.equalsIgnoreCase("") && !getDeliveryDate.equalsIgnoreCase("null")) {
            tv_completed_item.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(100);
            tv_completed_item.setText("completed (" + utills.getTimeAccordingDate(getDeliveryDate) + ")");
        }

        if (!getCancelDate.equalsIgnoreCase("") && !getCancelDate.equalsIgnoreCase("null")) {
            tv_order_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(0);
            tv_order_now.setText("cancelled (" + utills.getTimeAccordingDate(getCancelDate) + ")");
        }


        data_order_list = new ArrayList<>();
        method_get_mycard();

        mRootLayout = (RelativeLayout) rootView.findViewById(R.id.layout_group_chat_root);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_group_chat);

        mCurrentEventLayout = rootView.findViewById(R.id.layout_group_chat_current_event);
        mCurrentEventText = (TextView) rootView.findViewById(R.id.text_group_chat_current_event);

        mMessageEditText = (EditText) rootView.findViewById(R.id.edittext_group_chat_message);
        mMessageSendButton = (ImageView) rootView.findViewById(R.id.button_group_chat_send);
        mUploadFileButton = (ImageView) rootView.findViewById(R.id.button_group_chat_upload);

        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mMessageSendButton.setEnabled(true);
                } else {
                    mMessageSendButton.setEnabled(false);
                }
            }
        });

        mMessageSendButton.setEnabled(false);
        mMessageSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentState == STATE_EDIT) {
                    String userInput = mMessageEditText.getText().toString().trim();
                    if (userInput.length() > 0) {
                        if (mEditingMessage != null) {
                            editMessage(mEditingMessage, userInput);
                        }
                    }
                    setState(STATE_NORMAL, null, -1);
                } else {
                    String userInput = mMessageEditText.getText().toString().trim();
                    if (userInput.length() > 0) {
                        sendUserMessage(userInput);
                        mMessageEditText.setText("");
                    }
                }
            }
        });

        mUploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMedia();
            }
        });

        mIsTyping = false;
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mIsTyping) {
                    setTypingStatus(true);
                }

                if (s.length() == 0) {
                    setTypingStatus(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        setUpRecyclerView();
        setHasOptionsMenu(true);

        tv_chechk_out_prize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_mycard();
            }
        });


        tv_bynow_prize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_order_checkout();
            }
        });


        tv_quantity.setText("" + total_quantity);


        tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (total_quantity > 1) {
                    total_quantity = total_quantity - 1;
                }
                tv_quantity.setText("" + total_quantity);


                if (Shipp_Type.equalsIgnoreCase("1")) {
                    if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                        totalCharge = (Float.parseFloat(ProductPrize) * total_quantity) + Float.parseFloat(ProductDeliveryCharge);
                    } else {
                        totalCharge = Float.parseFloat(ProductPrize) * total_quantity;
                    }
                } else {
                    if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                        totalCharge = (Float.parseFloat(ProductPrize) + Float.parseFloat(ProductDeliveryCharge)) * total_quantity;
                    } else {
                        totalCharge = Float.parseFloat(ProductPrize) * total_quantity;
                    }
                }

                tv_bynow_prize.setText("buy now (£" + utills.roundTwoDecimals(totalCharge) + ")");

            }
        });


        tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                total_quantity = total_quantity + 1;
                tv_quantity.setText("" + total_quantity);


                if (Shipp_Type.equalsIgnoreCase("1")) {
                    if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                        totalCharge = (Float.parseFloat(ProductPrize) * total_quantity) + Float.parseFloat(ProductDeliveryCharge);
                    } else {
                        totalCharge = Float.parseFloat(ProductPrize) * total_quantity;
                    }
                } else {
                    if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                        totalCharge = (Float.parseFloat(ProductPrize) + Float.parseFloat(ProductDeliveryCharge)) * total_quantity;
                    } else {
                        totalCharge = Float.parseFloat(ProductPrize) * total_quantity;
                    }
                }

                tv_bynow_prize.setText("buy now (£" + utills.roundTwoDecimals(totalCharge) + ")");

            }
        });


        tv_Dispatch_item_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_dispatch_dialog();
            }
        });

        tv_Delivered_item_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_order_diliverd();
            }
        });
        tv_btn_WriteReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_review_dialog();
            }
        });

        tv_cancel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_order_cancel_confirm("0");
            }
        });

        tv_confirm_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_order_cancel_confirm("4");
            }
        });

        return rootView;
    }

    private void refresh() {
        if (mChannel == null) {
            GroupChannel.getChannel(mChannelUrl, new GroupChannel.GroupChannelGetHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChannel = groupChannel;
                    mChatAdapter.setChannel(mChannel);
                    mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        } else {
            mChannel.refresh(new GroupChannel.GroupChannelRefreshHandler() {
                @Override
                public void onResult(SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            ConnectionManager.addConnectionManagementHandler(context, CONNECTION_HANDLER_ID, new ConnectionManager.ConnectionManagementHandler() {
                @Override
                public void onConnected(boolean reconnect) {
                    refresh();
                }
            });

            mChatAdapter.setContext(getActivity()); // Glide bug fix (java.lang.IllegalArgumentException: You cannot start a load for a destroyed activity)

            // Gets channel from URL user requested

            Log.d(LOG_TAG, mChannelUrl);

            SendBird.addChannelHandler(CHANNEL_HANDLER_ID, new SendBird.ChannelHandler() {
                @Override
                public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                    if (baseChannel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.markAllMessagesAsRead();
                        // Add new message to view
                        mChatAdapter.addFirst(baseMessage);
                    }
                }

                @Override
                public void onMessageDeleted(BaseChannel baseChannel, long msgId) {
                    super.onMessageDeleted(baseChannel, msgId);
                    if (baseChannel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.delete(msgId);
                    }
                }

                @Override
                public void onMessageUpdated(BaseChannel channel, BaseMessage message) {
                    super.onMessageUpdated(channel, message);
                    if (channel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.update(message);
                    }
                }

                @Override
                public void onReadReceiptUpdated(GroupChannel channel) {
                    if (channel.getUrl().equals(mChannelUrl)) {
                        mChatAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onTypingStatusUpdated(GroupChannel channel) {
                    if (channel.getUrl().equals(mChannelUrl)) {
                        List<Member> typingUsers = channel.getTypingMembers();
                        displayTyping(typingUsers);
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {
            setTypingStatus(false);

            ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID);
            SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Save messages to cache.
        mChatAdapter.save();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_CHANNEL_URL, mChannelUrl);

        super.onSaveInstanceState(outState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Set this as true to restore background connection management.
        SendBird.setAutoBackgroundDetection(true);

        if (requestCode == INTENT_REQUEST_CHOOSE_MEDIA && resultCode == Activity.RESULT_OK) {
            // If user has successfully chosen the image, show a dialog to confirm upload.
            if (data == null) {
                Log.d(LOG_TAG, "data is null!");
                return;
            }

            sendFileWithThumbnail(data.getData());
        }
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mChatAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (mLayoutManager.findLastVisibleItemPosition() == mChatAdapter.getItemCount() - 1) {
                    mChatAdapter.loadPreviousMessages(CHANNEL_LIST_LIMIT, null);
                }
            }
        });
    }

    private void setUpChatListAdapter() {
        mChatAdapter.setItemClickListener(new GroupChatAdapter.OnItemClickListener() {
            @Override
            public void onUserMessageItemClick(UserMessage message) {
                // Restore failed message and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {
                    return;
                }


                if (message.getCustomType().equals(GroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE)) {
                    try {
                        UrlPreviewInfo info = new UrlPreviewInfo(message.getData());
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(info.getUrl()));
                        startActivity(browserIntent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFileMessageItemClick(FileMessage message) {
                // Load media chooser and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {
                    return;
                }


                onFileMessageClicked(message);
            }
        });

        mChatAdapter.setItemLongClickListener(new GroupChatAdapter.OnItemLongClickListener() {
            @Override
            public void onUserMessageItemLongClick(UserMessage message, int position) {
                if (message.getSender().getUserId().equals(preferences.getPRE_SendBirdUserId())) {
                    showMessageOptionsDialog(message, position);
                }
            }

            @Override
            public void onFileMessageItemLongClick(FileMessage message) {
            }

            @Override
            public void onAdminMessageItemLongClick(AdminMessage message) {
            }
        });
    }

    private void showMessageOptionsDialog(final BaseMessage message, final int position) {
        String[] options = new String[]{"Edit message", "Delete message"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    setState(STATE_EDIT, message, position);
                } else if (which == 1) {
                    deleteMessage(message);
                }
            }
        });
        builder.create().show();
    }

    private void setState(int state, BaseMessage editingMessage, final int position) {
        switch (state) {
            case STATE_NORMAL:
                mCurrentState = STATE_NORMAL;
                mEditingMessage = null;

                mUploadFileButton.setVisibility(View.VISIBLE);
                //   mMessageSendButton.setText("SEND");
                mMessageEditText.setText("");
                break;

            case STATE_EDIT:
                mCurrentState = STATE_EDIT;
                mEditingMessage = editingMessage;

                mUploadFileButton.setVisibility(View.GONE);
                //  mMessageSendButton.setText("SAVE");
                String messageString = ((UserMessage) editingMessage).getMessage();
                if (messageString == null) {
                    messageString = "";
                }
                mMessageEditText.setText(messageString);
                if (messageString.length() > 0) {
                    mMessageEditText.setSelection(0, messageString.length());
                }

                mMessageEditText.requestFocus();
                mMessageEditText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIMM.showSoftInput(mMessageEditText, 0);

                        mRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollToPosition(position);
                            }
                        }, 500);
                    }
                }, 100);


                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void retryFailedMessage(final BaseMessage message) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Retry?")
                .setPositiveButton(R.string.resend_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            if (message instanceof UserMessage) {
                                String userInput = ((UserMessage) message).getMessage();
                                sendUserMessage(userInput);
                            } else if (message instanceof FileMessage) {
                                Uri uri = mChatAdapter.getTempFileMessageUri(message);
                                sendFileWithThumbnail(uri);
                            }
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                })
                .setNegativeButton(R.string.delete_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                }).show();
    }

    /**
     * Display which users are typing.
     * If more than two users are currently typing, this will state that "multiple users" are typing.
     *
     * @param typingUsers The list of currently typing users.
     */
    private void displayTyping(List<Member> typingUsers) {

        if (typingUsers.size() > 0) {
            mCurrentEventLayout.setVisibility(View.VISIBLE);
            String string;

            if (typingUsers.size() == 1) {
                string = String.format(getString(R.string.user_typing), typingUsers.get(0).getNickname());
            } else if (typingUsers.size() == 2) {
                string = String.format(getString(R.string.two_users_typing), typingUsers.get(0).getNickname(), typingUsers.get(1).getNickname());
            } else {
                string = getString(R.string.users_typing);
            }
            mCurrentEventText.setText(string);
        } else {
            mCurrentEventLayout.setVisibility(View.GONE);
        }
    }

    private void requestMedia() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {
            Intent intent = new Intent();

            intent.setType("*/*");

            intent.setAction(Intent.ACTION_GET_CONTENT);

            // Always show the chooser (if there are multiple options available)
            startActivityForResult(Intent.createChooser(intent, "Select Media"), INTENT_REQUEST_CHOOSE_MEDIA);

            // Set this as false to maintain connection
            // even when an external Activity is started.
            SendBird.setAutoBackgroundDetection(false);
        }
    }

    private void requestStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Snackbar.make(mRootLayout, "Storage access permissions are required to upload/download files.",
                    Snackbar.LENGTH_LONG)
                    .setAction("Okay", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    PERMISSION_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();
        } else {
            // Permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    private void onFileMessageClicked(FileMessage message) {
        String type = message.getType().toLowerCase();
        if (type.startsWith("image")) {
            Intent i = new Intent(getActivity(), PhotoViewerActivity.class);
            i.putExtra("url", message.getUrl());
            i.putExtra("type", message.getType());
            startActivity(i);
        } else if (type.startsWith("video")) {
            Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
            intent.putExtra("url", message.getUrl());
            startActivity(intent);
        } else {
            showDownloadConfirmDialog(message);
        }
    }

    private void showDownloadConfirmDialog(final FileMessage message) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setMessage("Download file?")
                    .setPositiveButton(R.string.download, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                FileUtils.downloadFile(getActivity(), message.getUrl(), message.getName());
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null).show();
        }

    }

    private void updateActionBarTitle() {
        String title = "";

        if (mChannel != null) {
            title = TextUtils.getGroupChannelTitle(mChannel);
        }

    }

    private void sendUserMessageWithUrl(final String text, String url) {
        if (mChannel == null) {
            return;
        }

        new WebUtils.UrlPreviewAsyncTask() {
            @Override
            protected void onPostExecute(UrlPreviewInfo info) {
                if (mChannel == null) {
                    return;
                }

                UserMessage tempUserMessage = null;
                BaseChannel.SendUserMessageHandler handler = new BaseChannel.SendUserMessageHandler() {
                    @Override
                    public void onSent(UserMessage userMessage, SendBirdException e) {
                        if (e != null) {
                            // Error!
                            Log.e(LOG_TAG, e.toString());
                            if (getActivity() != null) {
                                Toast.makeText(
                                        getActivity(),
                                        "Send failed with error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                                        .show();
                            }
                            mChatAdapter.markMessageFailed(userMessage.getRequestId());
                            return;
                        }

                        // Update a sent message to RecyclerView
                        mChatAdapter.markMessageSent(userMessage);
                    }
                };

                try {
                    // Sending a message with URL preview information and custom type.
                    String jsonString = info.toJsonString();
                    tempUserMessage = mChannel.sendUserMessage(text, jsonString, GroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE, handler);
                } catch (Exception e) {
                    // Sending a message without URL preview information.
                    tempUserMessage = mChannel.sendUserMessage(text, handler);
                }


                // Display a user message to RecyclerView
                mChatAdapter.addFirst(tempUserMessage);
            }
        }.execute(url);
    }

    private void sendUserMessage(String text) {
        if (mChannel == null) {
            return;
        }

        List<String> urls = WebUtils.extractUrls(text);
        if (urls.size() > 0) {
            sendUserMessageWithUrl(text, urls.get(0));
            return;
        }

//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("image", "https://mobile.style/uploads/images/582d035c1cdb0b3d862015447844fd4dheadphones-2.png");
//            jsonObject.put("name", "demo test");
//            jsonObject.put("price", "$12.00");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        UserMessage tempUserMessage = mChannel.sendUserMessage(text, new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Log.e(LOG_TAG, e.toString());
                    if (getActivity() != null) {
                        Toast.makeText(
                                getActivity(),
                                "Send failed with error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                                .show();
                    }
                    mChatAdapter.markMessageFailed(userMessage.getRequestId());
                    return;
                }

                // Update a sent message to RecyclerView
                mChatAdapter.markMessageSent(userMessage);
            }
        });

        // Display a user message to RecyclerView
        mChatAdapter.addFirst(tempUserMessage);

        if (mLayoutManager != null) {
            mLayoutManager.scrollToPosition(0);
        }

    }

    /**
     * Notify other users whether the current user is typing.
     *
     * @param typing Whether the user is currently typing.
     */
    private void setTypingStatus(boolean typing) {
        if (mChannel == null) {
            return;
        }

        if (typing) {
            mIsTyping = true;
            mChannel.startTyping();
        } else {
            mIsTyping = false;
            mChannel.endTyping();
        }
    }

    /**
     * Sends a File Message containing an image file.
     * Also requests thumbnails to be generated in specified sizes.
     *
     * @param uri The URI of the image, which in this case is received through an Intent request.
     */
    private void sendFileWithThumbnail(Uri uri) {
        if (mChannel == null) {
            return;
        }

        // Specify two dimensions of thumbnails to generate
        List<FileMessage.ThumbnailSize> thumbnailSizes = new ArrayList<>();
        thumbnailSizes.add(new FileMessage.ThumbnailSize(240, 240));
        thumbnailSizes.add(new FileMessage.ThumbnailSize(320, 320));

        Hashtable<String, Object> info = FileUtils.getFileInfo(getActivity(), uri);

        if (info == null || info.isEmpty()) {
            Toast.makeText(getActivity(), "Extracting file information failed.", Toast.LENGTH_LONG).show();
            return;
        }

        final String name;
        if (info.containsKey("name")) {
            name = (String) info.get("name");
        } else {
            name = "Sendbird File";
        }
        final String path = (String) info.get("path");
        final File file = new File(path);
        final String mime = (String) info.get("mime");

        int size = 0;
        if ((Integer) info.get("  size") != null) {
            size = (Integer) info.get("  size");
        }

        if (path == null || path.equals("")) {
            Toast.makeText(getActivity(), "File must be located in local storage.", Toast.LENGTH_LONG).show();
        } else {
            BaseChannel.SendFileMessageWithProgressHandler progressHandler = new BaseChannel.SendFileMessageWithProgressHandler() {
                @Override
                public void onProgress(int bytesSent, int totalBytesSent, int totalBytesToSend) {
                    FileMessage fileMessage = mFileProgressHandlerMap.get(this);
                    if (fileMessage != null && totalBytesToSend > 0) {
                        int percent = (int) (((double) totalBytesSent / totalBytesToSend) * 100);
                        mChatAdapter.setFileProgressPercent(fileMessage, percent);
                    }
                }

                @Override
                public void onSent(FileMessage fileMessage, SendBirdException e) {
                    if (e != null) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        mChatAdapter.markMessageFailed(fileMessage.getRequestId());
                        return;
                    }

                    mChatAdapter.markMessageSent(fileMessage);
                }
            };

            // Send image with thumbnails in the specified dimensions
            FileMessage tempFileMessage = mChannel.sendFileMessage(file, name, mime, size, "", null, thumbnailSizes, progressHandler);

            mFileProgressHandlerMap.put(progressHandler, tempFileMessage);

            mChatAdapter.addTempFileMessageInfo(tempFileMessage, uri);
            mChatAdapter.addFirst(tempFileMessage);
        }
    }

    private void editMessage(final BaseMessage message, String editedMessage) {
        if (mChannel == null) {
            return;
        }

        mChannel.updateUserMessage(message.getMessageId(), editedMessage, null, null, new BaseChannel.UpdateUserMessageHandler() {
            @Override
            public void onUpdated(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                    @Override
                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                        mChatAdapter.markAllMessagesAsRead();
                    }
                });
            }
        });
    }

    /**
     * Deletes a message within the channel.
     * Note that users can only delete messages sent by oneself.
     *
     * @param message The message to delete.
     */
    private void deleteMessage(final BaseMessage message) {
        if (mChannel == null) {
            return;
        }

        mChannel.deleteMessage(message, new BaseChannel.DeleteMessageHandler() {
            @Override
            public void onResult(SendBirdException e) {
                if (e != null) {
                    // Error!
                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mChatAdapter.loadLatestMessages(CHANNEL_LIST_LIMIT, new BaseChannel.GetMessagesHandler() {
                    @Override
                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                        mChatAdapter.markAllMessagesAsRead();
                    }
                });
            }
        });
    }


    NonScrollListView listview_order;

    String Brand_name = "";
    String Last4_digit = "";


    private void method_mycard() {


        final Dialog dialog_listcard = new Dialog(context);
        dialog_listcard.setContentView(R.layout.dialog_my_card);
        dialog_listcard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_listcard.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        listview_order = dialog_listcard.findViewById(R.id.listview_order);

        TextView tv_add_method = dialog_listcard.findViewById(R.id.tv_add_method);
        final TextView tv_next = dialog_listcard.findViewById(R.id.tv_next);

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (card_id.equalsIgnoreCase("")) {
                    Toast.makeText(context,
                            "Please Select payment card",
                            Toast.LENGTH_LONG).show();
                } else {

                    if (dialog_listcard != null) {
                        dialog_listcard.dismiss();
                    }


                    tv_chechk_out_prize.setVisibility(View.GONE);
                    ll_by_now.setVisibility(View.VISIBLE);


                    String brand = "" + Brand_name;

                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                    tv_bynow_prize.setText("buy now (£" + utills.roundTwoDecimals(totalCharge) + ")");
                    tv_cardname.setText("" + brand + " / " + Last4_digit);
                }
            }
        });


        dialog_listcard.show();

        if (data_order_list != null) {
            Adapter_order adapter_review = new Adapter_order(context, data_order_list);
            listview_order.setAdapter(adapter_review);
        }

        tv_add_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                card_id = "";

                if (data_order_list != null) {
                    Adapter_order adapter_review = new Adapter_order(context, data_order_list);
                    listview_order.setAdapter(adapter_review);
                }


                method_change_card_dialog();
            }
        });

    }


    Dialog progressDialog = null;

    private void method_get_mycard() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_order_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private void method_get_mycard1() {

        if (utills.isOnline(context)) {
            utills.stopLoader(progressDialog);
            progressDialog = utills.startLoader(context);

            AndroidNetworking.get(Global_Service_Api.API_my_cards)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                data_order_list = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    MyCard_Response Response = new Gson().fromJson(result.toString(), MyCard_Response.class);

                                    if (Response.getData() != null) {
                                        data_order_list = Response.getData();


                                        if (listview_order != null) {
                                            Adapter_order adapter_review = new Adapter_order(context, data_order_list);
                                            listview_order.setAdapter(adapter_review);
                                        }


                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    public class Adapter_order extends BaseAdapter {

        Context context;
        List<MyCard_Data> listState;
        LayoutInflater inflater;

        int pos_selected = -1;

        public Adapter_order(Context applicationContext, List<MyCard_Data> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_order.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_order.ViewHolder();
                view = inflater.inflate(R.layout.datalist_mystripelist, viewGroup, false);
                viewHolder.iv_changecard_icon = view.findViewById(R.id.iv_changecard_icon);
                viewHolder.et_change_cardnumber = view.findViewById(R.id.et_change_cardnumber);
                viewHolder.card_mylist = view.findViewById(R.id.card_mylist);
                viewHolder.iv_checkbox = view.findViewById(R.id.iv_checkbox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_order.ViewHolder) view.getTag();
            }


            String brand = listState.get(position).getBrand();

            if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
            } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
            } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
            } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
            } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
            } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
            } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
            } else {
                iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
            }


            viewHolder.et_change_cardnumber.setText("" + brand + " / " + listState.get(position).getLast4());


            if (pos_selected == position) {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
            } else {
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_plain_chechk);
            }


            final ViewHolder finalViewHolder = viewHolder;
            viewHolder.card_mylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    pos_selected = position;


                    finalViewHolder.iv_checkbox.setImageResource(R.drawable.ic_fill_chechk);
                    card_id = "" + listState.get(position).getId();
                    Last4_digit = "" + listState.get(position).getLast4();
                    Brand_name = "" + listState.get(position).getBrand();


                    notifyDataSetChanged();
                }
            });


            return view;
        }

        private class ViewHolder {
            ImageView iv_changecard_icon, iv_checkbox;
            TextView et_change_cardnumber;
            CardView card_mylist;

        }

    }


    private void method_change_card_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_change_stipe_card);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardNumberEditText et_change_cardnumber = dialog_card.findViewById(R.id.et_change_cardnumber);
        final ImageView iv_changecard_icon = dialog_card.findViewById(R.id.iv_changecard_icon);
        final CardView card_add = dialog_card.findViewById(R.id.card_add);
        final ExpiryDateEditText et_Cardchnage_expiry_date = dialog_card.findViewById(R.id.et_Cardchnage_expiry_date);
        final StripeEditText et_cardchnage_cvc = dialog_card.findViewById(R.id.et_cardchnage_cvc);
        final TextView tv_cancel11 = dialog_card.findViewById(R.id.tv_cancel11);
        card_add.setEnabled(true);

        dialog_card.show();


        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        et_change_cardnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


                if (start < 4) {

                    CardBrand brand = CardUtils.getPossibleCardBrand(s.toString());


                    if (CardBrand.AmericanExpress.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_amex_template_32);
                    } else if (CardBrand.Discover.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_discover_template_32);
                    } else if (CardBrand.JCB.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_jcb_template_32);
                    } else if (CardBrand.DinersClub.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_diners_template_32);
                    } else if (CardBrand.Visa.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_visa_template_32);
                    } else if (CardBrand.MasterCard.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_mastercard_template_32);
                    } else if (CardBrand.UnionPay.getDisplayName().equals(brand)) {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unionpay_template_32);
                    } else {
                        iv_changecard_icon.setImageResource(R.drawable.stripe_ic_unknown);
                    }


                }


            }
        });

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utills.animationPopUp(card_add);


                if (et_change_cardnumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Number", Toast.LENGTH_SHORT).show();
                } else if (et_cardchnage_cvc.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter CVC", Toast.LENGTH_SHORT).show();
                } else if (et_Cardchnage_expiry_date.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Date", Toast.LENGTH_SHORT).show();
                } else {
                    if (isCardValid(et_change_cardnumber.getText().toString(),
                            et_Cardchnage_expiry_date.getText().toString(),
                            et_cardchnage_cvc.getText().toString())) {
                        card_add.setEnabled(false);
                        getUserKey(dialog_card);
                    }
                }


            }
        });


    }

    private boolean isCardValid(String cardNumber_data, String card_date, String card_cvcnum) {


        String cardNumber = cardNumber_data.replace(" ", "");
        String cardCVC = card_cvcnum;
        StringTokenizer tokens = new StringTokenizer(card_date, "/");
        int cardExpMonth = Integer.parseInt(tokens.nextToken());
        int cardExpYear = Integer.parseInt(tokens.nextToken());


        card_stripe = Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC);

        boolean validation = card_stripe.validateCard();
        if (validation) {
            return true;
        } else if (!card_stripe.validateNumber()) {
            Toast.makeText(context, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateExpiryDate()) {
            Toast.makeText(context, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card_stripe.validateCVC()) {
            Toast.makeText(context, "The CVC code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    Card card_stripe;

    private void getUserKey(final Dialog dialog_card) {


        CardParams cardParams = new CardParams(card_stripe.getNumber(), card_stripe.getExpMonth(), card_stripe.getExpYear(), card_stripe.getCvc());

        Stripe stripe = new Stripe(context, "" + preferences.getPRE_stripe_id());
        stripe.createCardToken(
                cardParams,
                new ApiResultCallback<Token>() {
                    public void onSuccess(@NonNull Token token) {

                        Log.e("token_stripe", "" + token.getId());
                        Token_card = token.getId();
                        method_save_card(dialog_card);

                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context,
                                e.getLocalizedMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        );


    }

    String Token_card;
    String card_id = "";
    static Preferences preferences;


    private void method_save_card(final Dialog dialog_card) {


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_add_card)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("stripe_token", "" + Token_card)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }


                                    method_get_mycard1();


                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }

    private void method_order_checkout() {

        if (card_id.equalsIgnoreCase("")) {
            Toast.makeText(context,
                    "Please add a payment card",
                    Toast.LENGTH_LONG).show();
        } else {

            if (utills.isOnline(context)) {
                utills.stopLoader(progressDialog);
                progressDialog = utills.startLoader(context);

                AndroidNetworking.post(Global_Service_Api.API_order_checkout)
                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                        .addBodyParameter("card_id", "" + card_id)
                        .addBodyParameter("order_id", "" + order_id_product)
                        .addBodyParameter("payment_type", "order")
                        .addBodyParameter("product_qty", "" + total_quantity)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String result) {
                                if (result == null || result == "") return;
                                Log.e("card", result);
                                try {
                                    JSONObject jsonObject = new JSONObject(result);

                                    String flag = jsonObject.getString("flag");
                                    String message = jsonObject.getString("message");

                                    if (flag.equalsIgnoreCase("true")) {


                                        try {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            InvoiceId = "" + jsonObject1.get("stripe_invoice_number");
                                            card_paid = "" + jsonObject1.get("last4");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        sendUserMessage("PAID_");

                                        ll_by_now.setVisibility(View.GONE);

                                    }

                                    Toast.makeText(context,
                                            message,
                                            Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                utills.stopLoader(progressDialog);
                            }

                            @Override
                            public void onError(ANError e) {
                                Toast.makeText(context,
                                        e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                utills.stopLoader(progressDialog);
                                Log.d("API", e.toString());
                            }
                        });


            }
        }
    }


    private void method_dispatch_dialog() {


        final Dialog dialog_card = new Dialog(context);
        dialog_card.setContentView(R.layout.dialog_dispatch);
        dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_card.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText et_shippingcompany = dialog_card.findViewById(R.id.et_shippingcompany);
        final EditText et_trackingnumber = dialog_card.findViewById(R.id.et_trackingnumber);
        final EditText et_trackingurl = dialog_card.findViewById(R.id.et_trackingurl);
        final CardView card_continue = dialog_card.findViewById(R.id.card_continue);
        final ImageView iv_cancel = dialog_card.findViewById(R.id.iv_cancel);


        dialog_card.show();


        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_card.dismiss();
            }
        });

        card_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                text_shippingcompany = et_shippingcompany.getText().toString();
                text_trackingnumber = et_trackingnumber.getText().toString();
                tracking_url = et_trackingurl.getText().toString();


                method_order_dispatch(dialog_card, et_shippingcompany.getText().toString(),
                        et_trackingnumber.getText().toString(), et_trackingurl.getText().toString());
            }
        });


    }

    public static String text_shippingcompany = "";
    public static String text_trackingnumber = "";
    public static String tracking_url = "";


    private void method_order_dispatch(final Dialog dialog_card, String et_shippingcompany, String et_trackingnumber, String et_trackingurl) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_order_status + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "2")
                    .addBodyParameter("tracking_name", "" + et_shippingcompany)
                    .addBodyParameter("tracking_number", "" + et_trackingnumber)
                    .addBodyParameter("tracking_url", "" + et_trackingurl)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    tv_Dispatch_item_final.setVisibility(View.GONE);

                                    if (dialog_card != null) {
                                        dialog_card.dismiss();
                                    }


                                    sendUserMessage("SHIPPED_");

                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_order_diliverd() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_order_status + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "3")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_Delivered_item_final.setVisibility(View.GONE);
                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_order_cancel_confirm(String status) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_order_status + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("status", "" + status)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("card", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    tv_confirm_item.setVisibility(View.GONE);
                                    tv_cancel_item.setVisibility(View.GONE);
                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    private void method_review_dialog() {

        final Dialog dialog_review = new Dialog(context);
        dialog_review.setContentView(R.layout.dialog_review);
        dialog_review.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog_review.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dialog_review.show();

        TextView tv_cancel11 = dialog_review.findViewById(R.id.tv_cancel11);
        CardView card_add_review = dialog_review.findViewById(R.id.card_add_review);
        ScaleRatingBar simpleRatingBar_rate = dialog_review.findViewById(R.id.simpleRatingBar_rate);
        EditText et_comment = dialog_review.findViewById(R.id.et_comment);

        tv_cancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_review.dismiss();
            }
        });

        card_add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                float rate_num = simpleRatingBar_rate.getRating();
                if (rate_num > 0) {
                    method_review(dialog_review, et_comment.getText().toString(), rate_num);
                } else {
                    Toast.makeText(context, "Please add Review", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    private void method_review(Dialog dialog_review, String comments, float rate_num) {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_order_review_submit)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("order_id", "" + order_id_product)
                    .addBodyParameter("user_id", "" + is_seller_id)
                    .addBodyParameter("rating", "" + rate_num)
                    .addBodyParameter("comments", "" + comments)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("order_review_submit", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    tv_btn_WriteReview.setVisibility(View.GONE);
                                    if (dialog_review != null) {
                                        dialog_review.dismiss();
                                    }


                                    JSONObject jsonObject2 = new JSONObject();
                                    jsonObject2.put("comments", "" + comments);
                                    jsonObject2.put("rating", "" + rate_num);
                                    rating_product = "" + jsonObject2.toString();

                                    sendUserMessage("REVIEW_");

                                }

                                Toast.makeText(context,
                                        message,
                                        Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError e) {
                            Toast.makeText(context,
                                    e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            utills.stopLoader(progressDialog);
                            Log.d("API", e.toString());
                        }
                    });


        }

    }


    public static void method_api_call(final TextView tv_transaction, final TextView tv_date_prize) {
        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_get_order_detail + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_order_detail", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    String date_paid = "";
                                    String card_paid1 = "";
                                    String stripe_invoice_number = "";
                                    try {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject("payment");
                                        date_paid = "" + jsonObject1.get("order_date");


                                        card_paid1 = "" + jsonObject2.get("last4");
                                        stripe_invoice_number = "" + jsonObject2.get("stripe_invoice_number");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    InvoiceId = stripe_invoice_number;
                                    tv_transaction.setText("transaction id: " + stripe_invoice_number);


                                    float TotalCharge = 0;

                                    String Shipp_Type = "1";
                                    int quantity = 1;
                                    try {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject("product");
                                        Shipp_Type = "" + jsonObject2.get("shipping_type");
                                        String quantity2 = "" + jsonObject1.get("product_qty");
                                        if (!quantity2.equalsIgnoreCase("null")) {
                                            quantity = Integer.parseInt(quantity2);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    String ProductPrize = "" + jsonObject1.get("product_price");
                                    String ProductDeliveryCharge = "" + jsonObject1.get("shipping_price");


                                    if (Shipp_Type.equalsIgnoreCase("1")) {
                                        if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                                            TotalCharge = (Float.parseFloat(ProductPrize) * quantity) + Float.parseFloat(ProductDeliveryCharge);
                                        } else {
                                            TotalCharge = Float.parseFloat(ProductPrize) * quantity;
                                        }
                                    } else {
                                        if ((!ProductDeliveryCharge.equalsIgnoreCase("null")) && (!ProductDeliveryCharge.equalsIgnoreCase(""))) {
                                            TotalCharge = (Float.parseFloat(ProductPrize) + Float.parseFloat(ProductDeliveryCharge)) * quantity;
                                        } else {
                                            TotalCharge = Float.parseFloat(ProductPrize) * quantity;
                                        }
                                    }


                                    totalCharge = TotalCharge;
                                    card_paid = card_paid1;


                                    if (is_boolen_user_or_seller == 0) {
                                        tv_date_prize.setText("You've paid £" + TotalCharge + " on " + utills.getTimeAccordingTime_Date(date_paid) + " using card x" + card_paid1);
                                    } else {
                                        tv_date_prize.setText("Payment has been received £" + TotalCharge + " on " + utills.getTimeAccordingTime_Date(date_paid) + " using card x" + card_paid1);
                                    }

                                    method_change_data(jsonObject1);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }

    public static void method_api_call_shipped(final TextView tv_transaction, final TextView tv_date_prize, final TextView tv_tracking_url) {
        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_get_order_detail + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_order_detail", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");


                                    String stripe_invoice_number = "";
                                    try {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject("payment");
                                        stripe_invoice_number = "" + jsonObject2.get("stripe_invoice_number");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    text_shippingcompany = "" + jsonObject1.get("tracking_name");
                                    text_trackingnumber = "" + jsonObject1.get("tracking_number");
                                    tracking_url = "" + jsonObject1.get("tracking_url");


                                    tv_transaction.setText("transaction id: " + InvoiceId);

                                    if (tracking_url.equalsIgnoreCase("") || tracking_url.equalsIgnoreCase("null")) {
                                        tv_tracking_url.setVisibility(View.GONE);
                                    } else {
                                        tv_tracking_url.setVisibility(View.VISIBLE);
                                        tv_tracking_url.setText("Tracking url : " + tracking_url);
                                    }

                                    tv_tracking_url.setText("Tracking url : " + tracking_url);
                                    tv_transaction.setText("transaction id: " + stripe_invoice_number);
                                    tv_date_prize.setText("Via " + text_shippingcompany + " Tracking Ref " + text_trackingnumber);

                                    method_change_data(jsonObject1);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }


    public static void method_api_call_Review(final TextView tv_comment, final ScaleRatingBar simpleRatingBar_) {
        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_get_order_detail + order_id_product)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;
                            Log.e("API_get_order_detail", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    String rating = "" + jsonObject1.get("order_rating");
                                    JSONObject jsonObject2 = new JSONObject("" + rating);
                                    tv_comment.setText("" + jsonObject2.getString("comments"));
                                    simpleRatingBar_.setRating(jsonObject2.getLong("rating"));

                                    method_change_data(jsonObject1);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("API", anError.toString());
                        }
                    });


        }

    }

    private static void method_change_data(JSONObject jsonObject1) {


        try {
           is_paid="" + jsonObject1.getString("is_paid");
           status= "" + jsonObject1.getString("status");
           getShippingDate= "" + jsonObject1.getString("shipping_date");
           getDeliveryDate= "" + jsonObject1.getString("delivery_date");
           getOrderDate= "" + jsonObject1.getString("order_date");
            getCancelDate= "" + jsonObject1.getString("cancel_date");
        } catch (JSONException e) {
            e.printStackTrace();
        }



        if (is_boolen_user_or_seller == 0) {  // customer

            tv_Dispatch_item_final.setVisibility(View.GONE);
            tv_chechk_out_prize.setVisibility(View.GONE);
            ll_by_now.setVisibility(View.GONE);
            tv_Delivered_item_final.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);

            if (status.equalsIgnoreCase("0")) {
                tv_chechk_out_prize.setVisibility(View.GONE);
                ll_by_now.setVisibility(View.GONE);
            } /*else if (is_paid.equalsIgnoreCase("1")) {
                tv_chechk_out_prize.setVisibility(View.GONE);
                ll_by_now.setVisibility(View.GONE);
            }*/ else if (is_paid.equalsIgnoreCase("0") && status.equalsIgnoreCase("4")) {
                tv_chechk_out_prize.setVisibility(View.VISIBLE);
                ll_by_now.setVisibility(View.GONE);
            } else if (is_paid.equalsIgnoreCase("1") && status.equalsIgnoreCase("2")) {
                tv_Delivered_item_final.setVisibility(View.VISIBLE);
            } else if ((status.equalsIgnoreCase("3")) && is_paid.equalsIgnoreCase("1")) {

                if (rating_product.equalsIgnoreCase("") || rating_product.equalsIgnoreCase("null")) {
                    tv_btn_WriteReview.setVisibility(View.VISIBLE);
                }


            }


        } else {  // seller

            tv_Dispatch_item_final.setVisibility(View.GONE);
            tv_chechk_out_prize.setVisibility(View.GONE);
            ll_by_now.setVisibility(View.GONE);
            tv_Delivered_item_final.setVisibility(View.GONE);
            tv_btn_WriteReview.setVisibility(View.GONE);

            if (status.equalsIgnoreCase("0")) {
                tv_Dispatch_item_final.setVisibility(View.GONE);
                tv_confirm_item.setVisibility(View.GONE);
                tv_cancel_item.setVisibility(View.GONE);
                tv_Delivered_item_final.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("1")) {

                tv_confirm_item.setVisibility(View.VISIBLE);
                tv_cancel_item.setVisibility(View.VISIBLE);
                // cancel and confirm visible
            } else if (is_paid.equalsIgnoreCase("1") && status.equalsIgnoreCase("4")) {
                tv_Dispatch_item_final.setVisibility(View.VISIBLE);
            }

        }


        // status == 0 button gone then
        //  customer
        // customer  status == 4 , is_paid=0 &  payment button gone  && is_paid=1 then gone
        // is paid== 1 then


        //  seller
// cancel and confirm
        // status 1 then   if cancel then 0 send api
        // status 1 then  confirm stuts 4 send api
        // is paid==1 and status== 4  then ship button and
        //status ==2 then ispaid==1 then deliver button  then send api status==3 api


        if (!getOrderDate.equalsIgnoreCase("") && !getOrderDate.equalsIgnoreCase("null")) {
            tv_order_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(33);
            tv_order_now.setText("ordered (" + utills.getTimeAccordingDate(getOrderDate) + ")");
        }

        if (!getShippingDate.equalsIgnoreCase("") && !getShippingDate.equalsIgnoreCase("null")) {
            tv_dispatched_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(60);
            tv_dispatched_now.setText("dispatched (" + utills.getTimeAccordingDate(getShippingDate) + ")");
        }

        if (!getDeliveryDate.equalsIgnoreCase("") && !getDeliveryDate.equalsIgnoreCase("null")) {
            tv_completed_item.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(100);
            tv_completed_item.setText("completed (" + utills.getTimeAccordingDate(getDeliveryDate) + ")");
        }

        if (!getCancelDate.equalsIgnoreCase("") && !getCancelDate.equalsIgnoreCase("null")) {
            tv_order_now.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            progress_bar.setProgress(0);
            tv_order_now.setText("cancelled (" + utills.getTimeAccordingDate(getCancelDate) + ")");
        }


    }


}
