package com.moremy.style.Salon.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.R;
import com.moremy.style.Ratingbar.ScaleRatingBar;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_EditAccount;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_FAQ;
import com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_Sapport;

import com.moremy.style.Salon.Model.Salon_Data;
import com.moremy.style.Salon.Model.Salon_Response;
import com.moremy.style.Salon.Model.Salon_SalonImage;
import com.moremy.style.Salon.Model.Salon_SalonPortfolio;

import com.moremy.style.Utills.CustomViewPager;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;


public class Fragment_Account_Salon extends Fragment {

    private Context context;


    public Fragment_Account_Salon() {
    }

    ScaleRatingBar simpleRatingBar_quality;
    TextView tv_oneliner, tv_name, tv_servies, tv_address;
    public static Salon_Data salon_data;

    ImageView img_profile, iv_companylogo;

    List<Salon_SalonPortfolio> arrayList1_random = new ArrayList<>();
    List<Salon_SalonImage> Salon_imageslist = new ArrayList<>();
    ViewPager viewPager_images;
    TabLayout indicator;

    ProgressBar progress_bar_profile, progress_bar_companylogo;

    Dialog progressDialog = null;


    TabLayout tabs;
    CustomViewPager viewPager;

    Preferences preferences;
    RecyclerView Rv_images;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_salon, container, false);
        context = getActivity();

        preferences = new Preferences(context);


        progress_bar_profile = (ProgressBar) view.findViewById(R.id.progress_bar_profile);
        progress_bar_companylogo = (ProgressBar) view.findViewById(R.id.progress_bar_companylogo);
        tabs = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (CustomViewPager) view.findViewById(R.id.viewPager);
        tv_oneliner = (TextView) view.findViewById(R.id.tv_oneliner);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_servies = (TextView) view.findViewById(R.id.tv_servies);
        tv_address = (TextView) view.findViewById(R.id.tv_address);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);
        iv_companylogo = (ImageView) view.findViewById(R.id.iv_companylogo);
        simpleRatingBar_quality = (ScaleRatingBar) view.findViewById(R.id.simpleRatingBar_quality);

        viewPager_images = view.findViewById(R.id.viewPager_images);
        indicator = view.findViewById(R.id.indicator);


        Rv_images = view.findViewById(R.id.Rv_images);
        Rv_images.setNestedScrollingEnabled(false);
        Rv_images.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));


        tabs.addTab(tabs.newTab().setText("Services"));
        tabs.addTab(tabs.newTab().setText("Reviews"));



        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        method_set_tab_font();


        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_more();
            }
        });





        return view;
    }



    private void method_more() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_salon);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);


        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver "+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }


        final TextView  tv_MyProductsOrders = dialog.findViewById(R.id.tv_MyProductsOrders);
        tv_MyProductsOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(2);
                LoadFragment(currentFragment);
            }
        });

        final TextView  tv_MyServicesBookings = dialog.findViewById(R.id.tv_MyServicesBookings);
        tv_MyServicesBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(3);
                LoadFragment(currentFragment);
            }
        });



        if (salon_data != null) {
            tv_name_title.setText("" + salon_data.getName());
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });
            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_Sapport();
                LoadFragment(currentFragment);
            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_FAQ();
                LoadFragment(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(0);
                LoadFragment(currentFragment);
            }
        });



        dialog.show();


    }


    public void LoadFragment(final Fragment fragment) {

        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_SALON.add(R.id.fmFragment, fragment);
        fragmentTransaction_SALON.addToBackStack(null);
        fragmentTransaction_SALON.commit();



        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {

                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                preferences.setPRE_TOKEN("");
                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }



    private void method_set_tab_font() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utills.customTypeface_medium(context));

                }
            }
        }
    }

    private void method_get_api() {


        AndroidNetworking.get(Global_Service_Api.API_get_salon_profile)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {


                                Salon_Response Response = new Gson().fromJson(result.toString(), Salon_Response.class);

                                if (Response.getData() != null) {
                                    salon_data = new Salon_Data();
                                    salon_data = Response.getData();
                                    method_setdata();

                                }


                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                          Log.d("API", anError.getErrorDetail());
                    }
                });

    }

    private void method_setdata() {

        try {
            if (salon_data != null) {
                tv_oneliner.setText("" + salon_data.getOneLine());
                tv_name.setText("" + salon_data.getName()+" " + salon_data.getLastname());

                if (salon_data.getService() != null) {
                    if (salon_data.getService().size() > 0) {
                        tv_servies.setText("" + salon_data.getService().get(0).getName());
                    }
                }

                tv_address.setText("" + salon_data.getPostalCode() + ", " + salon_data.getCity());


                if (salon_data.getstylist_rating() != null) {
                    simpleRatingBar_quality.setRating(salon_data.getstylist_rating().getavg_rating());
                }else {
                    simpleRatingBar_quality.setRating(0);
                }

                try {
                    if (salon_data.getCompanyLogo() != null) {
                        Glide.with(context)
                                .load(Global_Service_Api.IMAGE_URL + salon_data.getCompanyLogo())
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .addListener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        iv_companylogo.setImageResource(R.drawable.app_logo);
                                        progress_bar_companylogo.setVisibility(View.GONE);
                                        return true;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        progress_bar_companylogo.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(iv_companylogo);
                    } else {
                        iv_companylogo.setImageResource(R.drawable.app_logo);
                        progress_bar_companylogo.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    iv_companylogo.setImageResource(R.drawable.app_logo);
                    progress_bar_companylogo.setVisibility(View.GONE);

                    e.printStackTrace();
                }

                try {
                    if (salon_data.getProfilePic() != null) {
                        Glide.with(context)
                                .load(Global_Service_Api.IMAGE_URL + salon_data.getProfilePic())
                                .diskCacheStrategy(DiskCacheStrategy.NONE).addListener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                img_profile.setImageResource(R.drawable.app_logo);
                                progress_bar_profile.setVisibility(View.GONE);
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progress_bar_profile.setVisibility(View.GONE);
                                return false;
                            }
                        })
                                .into(img_profile);
                    } else {
                        img_profile.setImageResource(R.drawable.app_logo);
                        progress_bar_profile.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    img_profile.setImageResource(R.drawable.app_logo);
                    progress_bar_profile.setVisibility(View.GONE);
                    e.printStackTrace();
                }


                if (salon_data.getSalonPortfolio() != null) {
                    arrayList1_random = new ArrayList<>();
                    arrayList1_random.addAll(salon_data.getSalonPortfolio());
                    viewPager_images.setAdapter(new SliderAdapter(context, arrayList1_random));
                    indicator.setupWithViewPager(viewPager_images, true);
                    Timer timer = new Timer();
                    timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
                }


                if (salon_data.getSalonImages() != null) {
                    Salon_imageslist = new ArrayList<>();
                    Salon_imageslist = salon_data.getSalonImages();

                    GridAdapter adapter_counties = new GridAdapter(context, Salon_imageslist);
                    Rv_images.setAdapter(adapter_counties);

                }


                try {
                    Pager
                    adapter = new Pager(getChildFragmentManager(), tabs.getTabCount());
                    viewPager.setAdapter(adapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SliderTimer extends TimerTask {
        @Override
        public void run() {

            try {

                if(getActivity()!= null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (arrayList1_random.size() > 0) {
                                if (viewPager_images.getCurrentItem() < arrayList1_random.size() - 1) {
                                    viewPager_images.setCurrentItem(viewPager_images.getCurrentItem() + 1);
                                } else {
                                    viewPager_images.setCurrentItem(0);
                                }
                            }

                        }
                    });

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class SliderAdapter extends PagerAdapter {

        private Context context;
        private List<Salon_SalonPortfolio> color;

        public SliderAdapter(Context context, List<Salon_SalonPortfolio> color) {
            this.context = context;
            this.color = color;
        }

        @Override
        public int getCount() {
            return color.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_slider, null);


            ImageView iv_appicon__pager = (ImageView) view.findViewById(R.id.iv_appicon__pager);
            final   ProgressBar progress_bar_slider = (ProgressBar) view.findViewById(R.id.progress_bar_slider);


            Glide.with(context)
                    .load(Global_Service_Api.IMAGE_URL + color.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(iv_appicon__pager);


            ViewPager viewPager = (ViewPager) container;
            viewPager.addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPager viewPager = (ViewPager) container;
            View view = (View) object;
            viewPager.removeView(view);
        }
    }

//    public void UpdateProfile_Image() {
//        if (utills.isOnline(context)) {
//
//            if (SelectedFile_profile != null) {
//
//                AndroidNetworking.upload(Global_Service_Api.API_update_profile_img)
//                        .addMultipartFile("profile_pic", SelectedFile_profile)
//                        .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
//                        .build()
//                        .getAsString(new StringRequestListener() {
//                            @Override
//                            public void onResponse(String result) {
//                                utills.stopLoader(progressDialog);
//                                if (result == null || result == "") return;
//
//                                try {
//                                    JSONObject jsonObject = new JSONObject(result);
//                                    String msg = jsonObject.getString("message");
//                                    String flag = jsonObject.getString("flag");
//                                    String code = jsonObject.getString("code");
//
//                                    if (flag.equalsIgnoreCase("true")) {
//                                        SelectedFile_profile = null;
//                                    }
//
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//
//                            @Override
//                            public void onError(ANError anError) {
//                                utills.stopLoader(progressDialog);
//                                Toast.makeText(context, "Internal Server Error", Toast.LENGTH_SHORT).show();
//                                  Log.d("API", anError.getErrorDetail());
//                            }
//                        });
//            }
//
//        }
//    }


    Fragment_Pricelist fragment_pricelist;
    Fragment_review_Salon fragment_review;


    class Pager extends FragmentStatePagerAdapter {

        int tabCount;


        public Pager(FragmentManager fm, int tabCount) {
            super(fm);

            fragment_pricelist = new Fragment_Pricelist();
            fragment_review = new Fragment_review_Salon();

            this.tabCount = tabCount;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_pricelist;
                case 1:
                    return fragment_review;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

    }

    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Salon_SalonImage> countries_list;

        Context mcontext;

        public GridAdapter(Context context, List<Salon_SalonImage> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView iv_appicon__pager;
            ProgressBar progress_bar_slider;

            public MyViewHolder(View view) {
                super(view);

                iv_appicon__pager = (ImageView) view.findViewById(R.id.iv_appicon__pager);
                progress_bar_slider = (ProgressBar) view.findViewById(R.id.progress_bar_slider);
            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_images, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {


            Glide.with(context)
                    .load(Global_Service_Api.IMAGE_URL + countries_list.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.progress_bar_slider.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(viewHolder.iv_appicon__pager);


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    @Override
    public void onResume() {

        method_get_api();

        super.onResume();
    }
}
