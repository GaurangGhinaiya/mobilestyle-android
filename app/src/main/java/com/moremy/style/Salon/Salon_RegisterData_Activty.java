package com.moremy.style.Salon;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.R;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class Salon_RegisterData_Activty extends Base1_Activity implements View.OnClickListener {


    String is_type;

    Preferences preferences;
    EditText et_firstname, et_lastname, et_emailname, et_password, et_confirmpassword, et_username;

    String is_gender = "";
    Context context;

    ImageView iv_show_hide_confirmpassword, iv_show_hide_password;

    LinearLayout ll_username_chechk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_data_main);
        context = Salon_RegisterData_Activty.this;
        preferences = new Preferences(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        findview();


        ll_username_chechk = findViewById(R.id.ll_username_chechk);

        TextView tv_gender = findViewById(R.id.tv_gender);
        tv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_gendere);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final ImageView iv_cancel = dialog.findViewById(R.id.iv_cancel);

                dialog.show();


                iv_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });


    }

    private void findview() {


        et_firstname = findViewById(R.id.et_firstname);
        et_lastname = findViewById(R.id.et_lastname);
        et_emailname = findViewById(R.id.et_emailname);
        et_password = findViewById(R.id.et_password);
        et_confirmpassword = findViewById(R.id.et_confirmpassword);
        et_username = findViewById(R.id.et_username);

        et_confirmpassword.setTransformationMethod(new MyPasswordTransformationMethod());
        et_password.setTransformationMethod(new MyPasswordTransformationMethod());
        iv_show_hide_password = findViewById(R.id.iv_show_hide_password);
        iv_show_hide_confirmpassword = findViewById(R.id.iv_show_hide_confirmpassword);

        iv_show_hide_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });
        iv_show_hide_confirmpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide_unhide_password();
            }
        });


        Intent intent = getIntent();
        is_type = intent.getStringExtra("is_type");


        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(this);


        final Spinner spiner_gender = findViewById(R.id.spiner_gender);
        ImageView iv_spinner = findViewById(R.id.iv_spinner);
        iv_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_gender.performClick();
            }
        });




        final List<String> list_gender = new ArrayList<>();
        list_gender.add("Male");
        list_gender.add("Female");
        list_gender.add("Other");
        list_gender.add("Rather not say");
        list_gender.add("Please select");

        final SpinnerdocumentAdapter customAdapter_state = new SpinnerdocumentAdapter(this, list_gender);
        spiner_gender.setAdapter((SpinnerAdapter) customAdapter_state);
        spiner_gender.setSelection(customAdapter_state.getCount());


        spiner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(customAdapter_state.getCount()  != position){
                    is_gender = "" + list_gender.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        et_firstname.setText("" + preferences.getPRE_FirstName());
        et_lastname.setText("" + preferences.getPRE_LastName());
        et_emailname.setText("" + preferences.getPRE_Email());
        et_password.setText("" + preferences.getPRE_Password());
        et_username.setText("" + preferences.getPRE_UserName());
        // et_confirmpassword.setText("" + preferences.getPRE_Password());
        String Genderr = "" + preferences.getPRE_Gender();



        et_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!et_username.getText().toString().equalsIgnoreCase("")) {
                        method_call_chechk_username();
                    }
                }
            }
        });
        et_emailname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!et_emailname.getText().toString().equalsIgnoreCase("")) {
                        method_call_chechk_email();
                    }
                }
            }
        });

    }


    private void method_call_chechk_email() {

        if (utills.isOnline(context)) {


            AndroidNetworking.post(Global_Service_Api.API_check_email)
                    .addBodyParameter("email", "" + et_emailname.getText().toString())


                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                } else {

                                    Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private void method_call_chechk_username() {

        if (utills.isOnline(context)) {


            AndroidNetworking.post(Global_Service_Api.API_check_email)
                    .addBodyParameter("username", "" + et_username.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                   //  ll_username_chechk.setBackground(null);
                                } else {
                                    Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();
                                  //   ll_username_chechk.setBackground(getDrawable(R.drawable.bg_borderr_red2));
                                    et_username.setFocusable(true);
                                    et_username.requestFocus();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }


    class SpinnerdocumentAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size()-1 ;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_next:


                if (et_username.getText().toString().equalsIgnoreCase("")) {
                    Toast toast = Toast.makeText(context, "The user name field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                } else if (et_firstname.getText().toString().equalsIgnoreCase("")) {

                  Toast toast = Toast.makeText(context,"The first name field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                } else if (et_lastname.getText().toString().equalsIgnoreCase("")) {

                     Toast toast = Toast.makeText(context,"The last name field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                } else if (et_emailname.getText().toString().equalsIgnoreCase("")) {

                    Toast toast = Toast.makeText(context,"The email address field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                } else if (et_password.getText().toString().equalsIgnoreCase("")) {
                  Toast toast = Toast.makeText(context,"The password field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                } else if (et_confirmpassword.getText().toString().equalsIgnoreCase("")) {
                    Toast toast = Toast.makeText(context,"The confirm password field is required.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                } else if (!et_confirmpassword.getText().toString().equals(et_password.getText().toString())) {

                   Toast toast = Toast.makeText(context,"The password field and confirm password field should be match.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                } else {

                    method_call_email_chechk_api();

                }

                break;


        }
    }

    Dialog progressDialog = null;

    private void method_call_email_chechk_api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_check_email)
                    .addBodyParameter("email", "" + et_emailname.getText().toString())
                    .addBodyParameter("password", "" + et_password.getText().toString())
                    .addBodyParameter("username", "" + et_username.getText().toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {

                                    preferences.setPRE_FirstName("" + et_firstname.getText().toString());
                                    preferences.setPRE_LastName("" + et_lastname.getText().toString());
                                    preferences.setPRE_Email("" + et_emailname.getText().toString());
                                    preferences.setPRE_Password("" + et_password.getText().toString());
                                    preferences.setPRE_UserName("" + et_username.getText().toString());
                                    preferences.setPRE_Gender("" + is_gender);


                                    Intent i = new Intent(context, Salon_RegisterDataProfile_Activty.class);
                                    i.putExtra("is_type", "" + is_type);
                                    startActivity(i);

                                } else {
                                    Toast toast = Toast.makeText(context,message, Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.TOP, 0, 0);
                                    toast.show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    boolean is_show_pwd = false;

    public void hide_unhide_password() {

        if (is_show_pwd == false) {
            iv_show_hide_password.setImageResource(R.drawable.ic_eye);
            et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_password.setSelection(et_password.getText().length());
            is_show_pwd = true;


            iv_show_hide_confirmpassword.setImageResource(R.drawable.ic_eye);
            et_confirmpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            et_confirmpassword.setSelection(et_confirmpassword.getText().length());


        } else if (is_show_pwd == true) {
            iv_show_hide_password.setImageResource(R.drawable.ic_hide);
            et_password.setTransformationMethod(new MyPasswordTransformationMethod());
            et_password.setSelection(et_password.getText().length());

            is_show_pwd = false;

            iv_show_hide_confirmpassword.setImageResource(R.drawable.ic_hide);
            et_confirmpassword.setTransformationMethod(new MyPasswordTransformationMethod());
            et_confirmpassword.setSelection(et_confirmpassword.getText().length());


        }
    }


    public class MyPasswordTransformationMethod extends HideReturnsTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new MyPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }


}
