package com.moremy.style.Salon;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.R;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Salon.Model.Services_SubModel_SubService;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Salon.Salon_RegisterPortPholio_Activty.Salon_Image_List;
import static com.moremy.style.Salon.Salon_RegisterSalonPicture_Activty.Image_List_salon_picture;
import static com.moremy.style.Salon.Salon_RegisterSubServicesFinal_Activty.salon_list_services_final;


public class Salon_RegisterFinal_Ready_Activty extends Base1_Activity {

    Preferences preferences;
    Context context;
    Dialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_ready);
        context = Salon_RegisterFinal_Ready_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);


        TextView tv_final_name = findViewById(R.id.tv_final_name);
        tv_final_name.setText("Now that your salon is listed, customers will be able to learn more about your services and book with you");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateBussinessInfo();

            }
        });


    }

    JSONArray jsonArray = new JSONArray();
    JSONArray jsonArray_premium = new JSONArray();
    List<File> FOTO = new ArrayList<File>();
    List<File> FOTO_picture = new ArrayList<File>();

    public void UpdateBussinessInfo() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);


            String logo = preferences.getPRE_SelectedFile_logo();
            String profile = preferences.getPRE_SelectedFile_profile();

            /*...............premium.....*/


            jsonArray_premium = new JSONArray();
            for (int i = 0; i < salon_list_services_final.size(); i++) {
                try {
                    if (salon_list_services_final.get(i).getServices_Primium() != null) {
                        if (salon_list_services_final.get(i).getServices_Primium().size() > 0) {

                            List<Services_SubModel_SubService> services_temp = new ArrayList<>();
                            services_temp.addAll(salon_list_services_final.get(i).getServices_Primium());

                            for (int J_temp = 0; J_temp < services_temp.size(); J_temp++) {

                                JSONObject jsonObject = new JSONObject();

                                jsonObject.put("service_id", salon_list_services_final.get(i).getId());


                                jsonObject.put("name", "" + services_temp.get(J_temp).getName());
                                jsonObject.put("price", "" + services_temp.get(J_temp).price);
                                jsonObject.put("time", "" + services_temp.get(J_temp).time);
                                jsonObject.put("description", "" + services_temp.get(J_temp).is_Services_description);
                                jsonObject.put("id", "" + services_temp.get(J_temp).getId());


                                jsonArray_premium.put(jsonObject);

                            }

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            jsonArray = new JSONArray();
            for (int i = 0; i < salon_list_services_final.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("service_id", salon_list_services_final.get(i).getId());
                    Gson gson = new Gson();
                    String jsonString = gson.toJson(salon_list_services_final.get(i).getSubServices());
                    jsonObject.put("sub_service", jsonString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jsonObject);
            }


            FOTO = new ArrayList<File>();
            for (int i = 0; i < Salon_Image_List.size(); i++) {
                FOTO.add(new File(Salon_Image_List.get(i).toString()));
            }

            FOTO_picture = new ArrayList<File>();
            for (int i = 0; i < Image_List_salon_picture.size(); i++) {
                FOTO_picture.add(new File(Image_List_salon_picture.get(i).toString()));
            }


            if (logo.equalsIgnoreCase("") && profile.equalsIgnoreCase("")) {
                method_api_both_null();
            } else if (profile.equalsIgnoreCase("") && !logo.equalsIgnoreCase("")) {
                File file_logo = new File(preferences.getPRE_SelectedFile_logo());
                method_api_profile_null(file_logo);
            } else if (logo.equalsIgnoreCase("") && !profile.equalsIgnoreCase("")) {
                File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                method_api_logo_null(file_profile_pic);
            } else {

                File file_profile_pic = new File(preferences.getPRE_SelectedFile_profile());
                File file_logo = new File(preferences.getPRE_SelectedFile_logo());

                method_api_not_null(file_profile_pic, file_logo);
            }


        }

    }

    private void method_api_both_null() {


        AndroidNetworking.upload(Global_Service_Api.API_Salon_signup)
                .addMultipartFileList("salon_images[]", FOTO)
                .addMultipartFileList("salon_portfolio[]", FOTO_picture)
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("salon_name", "" + preferences.getPRE_SalonName())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                        Toast.makeText(context, "" + anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_api_not_null(File file_profile_pic, File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_Salon_signup)
                .addMultipartFileList("salon_images[]", FOTO)
                .addMultipartFileList("salon_portfolio[]", FOTO_picture)
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("salon_name", "" + preferences.getPRE_SalonName())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartFile("company_logo", file_logo)

                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                        Toast.makeText(context, "" + anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }


    private void method_api_profile_null(File file_logo) {


        AndroidNetworking.upload(Global_Service_Api.API_Salon_signup)
                .addMultipartFileList("salon_images[]", FOTO)
                .addMultipartFileList("salon_portfolio[]", FOTO_picture)
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("salon_name", "" + preferences.getPRE_SalonName())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())


                .addMultipartFile("company_logo", file_logo)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                        Toast.makeText(context, "" + anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_api_logo_null(File file_profile_pic) {


        AndroidNetworking.upload(Global_Service_Api.API_Salon_signup)
                .addMultipartFileList("salon_images[]", FOTO)
                .addMultipartFileList("salon_portfolio[]", FOTO_picture)
                .addMultipartParameter("username", "" + preferences.getPRE_UserName())
                .addMultipartParameter("name", "" + preferences.getPRE_FirstName())
                .addMultipartParameter("lastname", "" + preferences.getPRE_LastName())
                .addMultipartParameter("email", "" + preferences.getPRE_Email())
                .addMultipartParameter("salon_name", "" + preferences.getPRE_SalonName())
                .addMultipartParameter("password", "" + preferences.getPRE_Password())
                .addMultipartParameter("gender", "" + preferences.getPRE_Gender())
                .addMultipartParameter("one_line", "" + preferences.getPRE_Main_Oneline())
                .addMultipartParameter("city", "" + preferences.getPRE_City())
                .addMultipartParameter("postal_code", "" + preferences.getPRE_Code())
                .addMultipartParameter("address_line_1", "" + preferences.getPRE_address())
                .addMultipartParameter("device", "1")
                .addMultipartParameter("device_token", "" + preferences.getPRE_FCMtoken())
                .addMultipartParameter("sub_services", "" + jsonArray.toString())
                .addMultipartParameter("other_sub_services", "" + jsonArray_premium.toString())


                .addMultipartFile("profile_pic", file_profile_pic)
                .addMultipartParameter("latitude", "" + preferences.getPRE_Latitude())
                .addMultipartParameter("longitude", "" + preferences.getPRE_Longitude())

                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {
                                method_set_data(result);
                            }
                            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                        Toast.makeText(context, "" + anError.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void method_set_data(String result) {


        preferences.setPRE_Email("");

        preferences.setPRE_Main_ServiceID("");
        preferences.setPRE_Mile("");
        preferences.setPRE_Code("");
        preferences.setPRE_address("");
        preferences.setPRE_City("");
        preferences.setPRE_SelectedFile_logo("");
        preferences.setPRE_SelectedFile_profile("");
        preferences.setPRE_Gender("");
        preferences.setPRE_Password("");
        preferences.setPRE_FirstName("");
        preferences.setPRE_LastName("");
        preferences.setPRE_Main_Oneline("");
        preferences.setPRE_UserName("");

        preferences.setPRE_Latitude("");
        preferences.setPRE_Longitude("");
        preferences.setPRE_SalonName("");
        preferences.setPRE_Is_Premium_Salon(false);


        try {
            JSONObject jsonObject = new JSONObject(result);

            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            String s_token = "" + jsonObject1.get("token");
            preferences.setPRE_TOKEN(s_token);

            Intent i = new Intent(this, Salon_MainProfile_Activty.class);
            startActivity(i);
            finish();
            finishAffinity();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
