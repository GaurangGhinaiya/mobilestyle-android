
package com.moremy.style.Salon.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonModifySelected_Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("service_id")
    @Expose
    private List<String> serviceId = null;
    @SerializedName("salon_subservices")
    @Expose
    private List<SalonModifySelected_SalonSubservice> salonSubservices = null;
    @SerializedName("other_sub_services")
    @Expose
    private List<SalonModifySelected_SalonSubservice> otherSubServices = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getServiceId() {
        return serviceId;
    }

    public void setServiceId(List<String> serviceId) {
        this.serviceId = serviceId;
    }

    public List<SalonModifySelected_SalonSubservice> getSalonSubservices() {
        return salonSubservices;
    }

    public void setSalonSubservices(List<SalonModifySelected_SalonSubservice> salonSubservices) {
        this.salonSubservices = salonSubservices;
    }

    public List<SalonModifySelected_SalonSubservice> getOtherSubServices() {
        return otherSubServices;
    }

    public void setOtherSubServices(List<SalonModifySelected_SalonSubservice> otherSubServices) {
        this.otherSubServices = otherSubServices;
    }


    @SerializedName("service")
    @Expose
    private List<Salon_Service> service = null;


    public List<Salon_Service> getService() {
        return service;
    }

    public void setService(List<Salon_Service> service) {
        this.service = service;
    }


}
