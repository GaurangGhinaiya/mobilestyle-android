package com.moremy.style.Salon.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.moremy.style.Model.Model_Rating;

public class Salon_Data {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("lastname")
@Expose
private String lastname;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("salon_name")
@Expose
private String salonName;
@SerializedName("email")
@Expose
private String email;
@SerializedName("profile_pic")
@Expose
private String profilePic;
@SerializedName("company_logo")
@Expose
private String companyLogo;
@SerializedName("status")
@Expose
private Integer status;
@SerializedName("service_id")
@Expose
private String serviceId;
@SerializedName("description")
@Expose
private Object description;
@SerializedName("keywords")
@Expose
private List<String> keywords = null;

@SerializedName("street_name")
@Expose
private Object streetName;
@SerializedName("city")
@Expose
private String city;
@SerializedName("postal_code")
@Expose
private String postalCode;
@SerializedName("county")
@Expose
private Object county;
@SerializedName("country")
@Expose
private String country;
@SerializedName("one_line")
@Expose
private String oneLine;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;
@SerializedName("service")
@Expose
private List<Salon_Service> service = null;
@SerializedName("salon_images")
@Expose
private List<Salon_SalonImage> salonImages = null;
@SerializedName("salon_portfolio")
@Expose
private List<Salon_SalonPortfolio> salonPortfolio = null;
@SerializedName("salon_subservices")
@Expose
private List<Salon_SalonSubservice> salonSubservices = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getLastname() {
return lastname;
}

public void setLastname(String lastname) {
this.lastname = lastname;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getSalonName() {
return salonName;
}

public void setSalonName(String salonName) {
this.salonName = salonName;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getProfilePic() {
return profilePic;
}

public void setProfilePic(String profilePic) {
this.profilePic = profilePic;
}

public String getCompanyLogo() {
return companyLogo;
}

public void setCompanyLogo(String companyLogo) {
this.companyLogo = companyLogo;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getServiceId() {
return serviceId;
}

public void setServiceId(String serviceId) {
this.serviceId = serviceId;
}

public Object getDescription() {
return description;
}

public void setDescription(Object description) {
this.description = description;
}

public List<String> getKeywords() {
return keywords;
}

public void setKeywords(List<String> keywords) {
this.keywords = keywords;
}


public Object getStreetName() {
return streetName;
}

public void setStreetName(Object streetName) {
this.streetName = streetName;
}

public String getCity() {
return city;
}

public void setCity(String city) {
this.city = city;
}

public String getPostalCode() {
return postalCode;
}

public void setPostalCode(String postalCode) {
this.postalCode = postalCode;
}

public Object getCounty() {
return county;
}

public void setCounty(Object county) {
this.county = county;
}

public String getCountry() {
return country;
}

public void setCountry(String country) {
this.country = country;
}

public String getOneLine() {
return oneLine;
}

public void setOneLine(String oneLine) {
this.oneLine = oneLine;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public List<Salon_Service> getService() {
return service;
}

public void setService(List<Salon_Service> service) {
this.service = service;
}

public List<Salon_SalonImage> getSalonImages() {
return salonImages;
}

public void setSalonImages(List<Salon_SalonImage> salonImages) {
this.salonImages = salonImages;
}

public List<Salon_SalonPortfolio> getSalonPortfolio() {
return salonPortfolio;
}

public void setSalonPortfolio(List<Salon_SalonPortfolio> salonPortfolio) {
this.salonPortfolio = salonPortfolio;
}

public List<Salon_SalonSubservice> getSalonSubservices() {
return salonSubservices;
}

public void setSalonSubservices(List<Salon_SalonSubservice> salonSubservices) {
this.salonSubservices = salonSubservices;
}




    @SerializedName("username")
    @Expose
    private String username;


    public String getusername() {
        return username;
    }
    public void setusername(String username) {
        this.username = username;
    }


    @SerializedName("address_line_1")
    @Expose
    private String address_line_1;


    public String getaddress_line_1() {
        return address_line_1;
    }
    public void setaddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }





    @SerializedName("is_premium")
    @Expose
    private String is_premium;

    public String getis_premium() {
        return is_premium;
    }
    public void setis_premium(String is_premium) {
        this.is_premium = is_premium;
    }


    @SerializedName("salon_rating")
    @Expose
    private Model_Rating stylist_rating;

    public Model_Rating getstylist_rating() {
        return stylist_rating;
    }

    public void setstylist_rating(Model_Rating stylist_rating) {
        this.stylist_rating = stylist_rating;
    }

}