package com.moremy.style.Salon.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Salon_SalonSubservice {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("salon_id")
@Expose
private Integer salonId;
@SerializedName("subservice_id")
@Expose
private Integer subserviceId;
@SerializedName("price")
@Expose
private float price;
@SerializedName("time")
@Expose
private Integer time;
@SerializedName("service")
@Expose
private Salon_Service_ service;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getSalonId() {
return salonId;
}

public void setSalonId(Integer salonId) {
this.salonId = salonId;
}

public Integer getSubserviceId() {
return subserviceId;
}

public void setSubserviceId(Integer subserviceId) {
this.subserviceId = subserviceId;
}

public float getPrice() {
return price;
}

public void setPrice(float price) {
this.price = price;
}

public Integer getTime() {
return time;
}

public void setTime(Integer time) {
this.time = time;
}

public Salon_Service_ getService() {
return service;
}

public void setService(Salon_Service_ service) {
this.service = service;
}

}