package com.moremy.style.Salon.Modify;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Model.Services_Data;
import com.moremy.style.Model.Services_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Salon_ModifyServices_Activty extends Base1_Activity {

    Dialog progressDialogs = null;


    Context context;

    List<Services_Data> Salon_list_services;
    List<Services_Data> list_services_final = new ArrayList<>();

    RecyclerView recycleview_services;
    Preferences preferences;
    String salon_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_services);
        context = Salon_ModifyServices_Activty.this;
        activity = Salon_ModifyServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(context);

        recycleview_services = findViewById(R.id.recycleview_services);
        recycleview_services.setNestedScrollingEnabled(false);


        ChipsLayoutManager spanLayoutManager = ChipsLayoutManager.newBuilder(this)
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .build();
        recycleview_services.setLayoutManager(spanLayoutManager);

        Intent intent = getIntent();
        salon_id = "" + intent.getStringExtra("salon_id");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                StringBuilder stringBuilder = new StringBuilder();
                if (list_services_final.size() > 0) {
                    for (int i = 0; i < list_services_final.size(); i++) {
                        if (list_services_final.get(i).getIs_selected()) {
                            if (!stringBuilder.toString().equalsIgnoreCase("")) {
                                stringBuilder.append(",");
                            }
                            stringBuilder.append(list_services_final.get(i).getId());
                        }
                    }
                }


                if (!stringBuilder.toString().equalsIgnoreCase("")) {
                    Intent i = new Intent(context, Salon_ModifySubServices_Activty.class);
                    i.putExtra("service_ids", "" + stringBuilder);

                    startActivity(i);
                } else {
                    Toast.makeText(context, "Please Select Service", Toast.LENGTH_SHORT).show();
                }


            }
        });

        Salon_list_services = new ArrayList<>();
        list_services_final = new ArrayList<>();

        get_list();


    }

    public void get_list() {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_services)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Salon_list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {
                                        Salon_list_services = Response.getData();

                                        get_list_selected();

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    public void get_list_selected() {
        if (utills.isOnline(this)) {
            utills.stopLoader(progressDialogs);
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_get_Selected_salon_services+salon_id)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                List<Services_Data> list_services_selected = new ArrayList<>();
                               list_services_final = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_Response Response = new Gson().fromJson(result.toString(), Services_Response.class);

                                    if (Response.getData() != null) {

                                        list_services_selected .addAll( Response.getData());


                                        for (int j = 0; j < Salon_list_services.size(); j++) {

                                            String i_id = "" + Salon_list_services.get(j).getId();

                                            boolean found = false;
                                            int poss = 0;

                                            for (int i_select = 0; i_select < list_services_selected.size(); i_select++) {
                                                if (i_id.equalsIgnoreCase("" + list_services_selected.get(i_select).getId())) {
                                                    found = true;
                                                    poss = i_select;
                                                }
                                            }

                                            if (found) {
                                                Services_Data services_data=new Services_Data();
                                                services_data.setId(list_services_selected.get(poss).getId());
                                                services_data.setIs_selected(true);
                                                services_data.setName(list_services_selected.get(poss).getName());
                                                list_services_final.add(services_data);
                                                poss = 0;

                                            } else {

                                                Services_Data services_data=new Services_Data();
                                                services_data.setId(Salon_list_services.get(j).getId());
                                                services_data.setIs_selected(false);
                                                services_data.setName(Salon_list_services.get(j).getName());
                                                list_services_final.add(services_data);

                                            }


                                        }


                                        GridAdapter adapter_cat = new GridAdapter(context, list_services_final);
                                        recycleview_services.setAdapter(adapter_cat);

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Services_Data> arrayList;
        int lastPosition = -1;
        Context mcontext;

        public GridAdapter(Context context, List<Services_Data> arrayList) {
            this.arrayList = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_textcountry;

            public MyViewHolder(View view) {
                super(view);

                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
            }
        }

        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_service, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {


            if (arrayList.get(position).getIs_selected()) {
                arrayList.get(position).setIs_selected(true);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
                viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_black));
            } else {
                arrayList.get(position).setIs_selected(false);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_white));
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (arrayList.get(position).getIs_selected()) {
                        arrayList.get(position).setIs_selected(false);
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                        viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_white));

                    } else {

                        arrayList.get(position).setIs_selected(true);
                        viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));
                        viewHolder.tv_textcountry.setBackground(getResources().getDrawable(R.drawable.bg_black));

                    }


                }
            });


            viewHolder.tv_textcountry.setText(arrayList.get(position).getName());


        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }

    }


    public static Activity activity = null;
    public static void finish_this() {
        if(activity != null){
            activity.finish();
        }
    }

}
