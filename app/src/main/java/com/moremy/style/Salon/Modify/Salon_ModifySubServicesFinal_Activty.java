package com.moremy.style.Salon.Modify;


import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.R;
import com.moremy.style.Salon.Model.Services_SubModel_SubService;
import com.moremy.style.Salon.Model.Services_SubModel_data;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class Salon_ModifySubServicesFinal_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    List<Services_SubModel_data> salon_list_services_final = new ArrayList<>();

    RecyclerView recycleview_cat;
    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_sub_services_final);
        context = Salon_ModifySubServicesFinal_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = new Preferences(this);

        recycleview_cat = findViewById(R.id.listview_country);
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));


        RelativeLayout rl_next = findViewById(R.id.rl_next);
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                method_modify_api();
            }
        });

        salon_list_services_final = new ArrayList<>();


        Bundle extras = getIntent().getExtras();
        String arraylist_a = extras.getString("arraylist");
        Type listType = new TypeToken<List<Services_SubModel_data>>() {
        }.getType();

        List<Services_SubModel_data> list_services_temp_move = new ArrayList<>();
        list_services_temp_move = new Gson().fromJson(arraylist_a, listType);


        if (list_services_temp_move != null) {
            if (list_services_temp_move.size() > 0) {

                for (int i = 0; i < list_services_temp_move.size(); i++) {
                    if (list_services_temp_move.get(i).getSubServices() != null) {
                        if (list_services_temp_move.get(i).getSubServices().size() > 0) {

                            List<Services_SubModel_SubService> subservices_final = new ArrayList<>();
                            List<Services_SubModel_SubService> subservices = new ArrayList<>();
                            subservices.addAll(list_services_temp_move.get(i).getSubServices());


                            for (int j = 0; j < subservices.size(); j++) {
                                if (subservices.get(j).is_chechkbox) {
                                    Services_SubModel_SubService servicem = new Services_SubModel_SubService();
                                    servicem.is_chechkbox = subservices.get(j).is_chechkbox;
                                    servicem.time = subservices.get(j).time;
                                    servicem.price = subservices.get(j).price;
                                    servicem.setId(subservices.get(j).getId());
                                    servicem.setName(subservices.get(j).getName());
                                    subservices_final.add(servicem);
                                }
                            }

                            salon_list_services_final.add(list_services_temp_move.get(i));
                            salon_list_services_final.get(i).setSubServices(subservices_final);
                        }
                    }
                }
            }
        }


        Log.e("onCreate: ", "" + salon_list_services_final.size());

        GridAdapter adapter_counties = new GridAdapter(context, salon_list_services_final);
        recycleview_cat.setAdapter(adapter_counties);


        TextView tv_add_more_back = findViewById(R.id.tv_add_more_back);
        tv_add_more_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    Dialog progressDialog = null;

    private void method_modify_api() {

        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArray_premium = new JSONArray();

        jsonArray_premium = new JSONArray();
        for (int i = 0; i < salon_list_services_final.size(); i++) {
            try {
                if (salon_list_services_final.get(i).getServices_Primium() != null) {
                    if (salon_list_services_final.get(i).getServices_Primium().size() > 0) {

                        List<Services_SubModel_SubService> services_temp = new ArrayList<>();
                        services_temp.addAll(salon_list_services_final.get(i).getServices_Primium());

                        for (int J_temp = 0; J_temp < services_temp.size(); J_temp++) {

                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("service_id", salon_list_services_final.get(i).getId());


                            jsonObject.put("name", "" + services_temp.get(J_temp).getName());
                            jsonObject.put("price", "" + services_temp.get(J_temp).price);
                            jsonObject.put("time", "" + services_temp.get(J_temp).time);
                            jsonObject.put("description", "" + services_temp.get(J_temp).is_Services_description);
                            jsonObject.put("id", "" + services_temp.get(J_temp).getId());


                            jsonArray_premium.put(jsonObject);

                        }

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        jsonArray = new JSONArray();
        for (int i = 0; i < salon_list_services_final.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("service_id", salon_list_services_final.get(i).getId());
                Gson gson = new Gson();
                String jsonString = gson.toJson(salon_list_services_final.get(i).getSubServices());
                jsonObject.put("sub_service", jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }


        StringBuilder stringBuilder_ids = new StringBuilder();
        for (int i = 0; i < salon_list_services_final.size(); i++) {
            if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                stringBuilder_ids.append(", ");
            }
            stringBuilder_ids.append("" + salon_list_services_final.get(i).getId());
        }


        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_salon_services)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("sub_services", "" + jsonArray.toString())
                    .addBodyParameter("other_sub_services", "" + jsonArray_premium.toString())
                    .addBodyParameter("service_id", "" + stringBuilder_ids.toString())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");
                                if (flag.equalsIgnoreCase("true")) {


                                    Salon_ModifySubServices_Activty.finish_this();
                                    Salon_ModifyServices_Activty.finish_this();  finish();

                                }
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                            Toast.makeText(context, "" + anError.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {

        private List<Services_SubModel_data> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<Services_SubModel_data> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_category_list;
            RecyclerView Rv_prizelist;
            RecyclerView Rv_prizelist_premium;

            public MyViewHolder(View view) {
                super(view);
                tv_category_list = (TextView) view.findViewById(R.id.tv_category_list);
                Rv_prizelist = (RecyclerView) view.findViewById(R.id.Rv_prizelist);
                Rv_prizelist_premium = (RecyclerView) view.findViewById(R.id.Rv_prizelist_premium);

            }
        }


        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_salon, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_category_list.setText(countries_list.get(position).getName());
            viewHolder.Rv_prizelist.setNestedScrollingEnabled(false);
            viewHolder.Rv_prizelist_premium.setNestedScrollingEnabled(false);
            viewHolder.Rv_prizelist.setLayoutManager(new GridLayoutManager(context, 1));
            viewHolder.Rv_prizelist_premium.setLayoutManager(new GridLayoutManager(context, 1));


            if (countries_list.get(position).getSubServices() != null) {

                List<Services_SubModel_SubService> salon_list_services_tempp = new ArrayList<>();
                salon_list_services_tempp.addAll(countries_list.get(position).getSubServices());
                GridAdapter_subprize adapter_counties = new GridAdapter_subprize(context, salon_list_services_tempp);
                viewHolder.Rv_prizelist.setAdapter(adapter_counties);

            }

            if (countries_list.get(position).getServices_Primium() != null) {

                List<Services_SubModel_SubService> salon_list_services_tempp = new ArrayList<>();
                salon_list_services_tempp.addAll(countries_list.get(position).getServices_Primium());
                GridAdapter_subprize adapter_counties = new GridAdapter_subprize(context, salon_list_services_tempp);
                viewHolder.Rv_prizelist_premium.setAdapter(adapter_counties);

            }

        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    class GridAdapter_subprize extends RecyclerView.Adapter<GridAdapter_subprize.MyViewHolder> {

        private List<Services_SubModel_SubService> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter_subprize(Context context, List<Services_SubModel_SubService> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry, tv_textPrize;

            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);
                tv_textPrize = (TextView) view.findViewById(R.id.tv_textPrize);

            }
        }


        @Override
        public GridAdapter_subprize.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_prize, parent, false);

            return new GridAdapter_subprize.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter_subprize.MyViewHolder viewHolder, final int position) {

            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());
            if (!countries_list.get(position).price.equalsIgnoreCase("")) {
                viewHolder.tv_textPrize.setText("£" + utills.roundTwoDecimals(Double.parseDouble(countries_list.get(position).price)));
            }
        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


}
