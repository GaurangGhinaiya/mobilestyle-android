package com.moremy.style.Salon;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.moremy.style.R;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Utills.Preferences;


public class Salon_RegisterBio_Activty extends Base1_Activity {


    Context context;
    Preferences preferences;
    EditText et_oneline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_bio);
        context = Salon_RegisterBio_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(this);

        RelativeLayout rl_next = findViewById(R.id.rl_next);
        et_oneline = findViewById(R.id.et_oneline);


        et_oneline.setText("" + preferences.getPRE_Main_Oneline());

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (et_oneline.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "plesae enter your bio", Toast.LENGTH_SHORT).show();
                } else {
                    preferences.setPRE_Main_Oneline("" + et_oneline.getText().toString());

                    Intent i = new Intent(context, Salon_RegisterFinal_Ready_Activty.class);
                    startActivity(i);
                }


            }
        });


    }


}
