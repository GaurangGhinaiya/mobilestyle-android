package com.moremy.style.Salon;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.moremy.style.BuildConfig;
import com.moremy.style.R;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.Utills.FileUtils;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import static com.theartofdev.edmodo.cropper.CropImage.getPickImageResultUri;

public class Salon_RegisterDataProfile_Activty extends Base1_Activity implements View.OnClickListener {


    TextView tv_title, tv_do_this_later;
    ImageView iv_profile_image, iv_logo;
    EditText et_salon_name;
    Preferences preferences;

    int is_logo_profile = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_data_profile_main);
        preferences = new Preferences(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        findview();

        SelectedFile_profile = null;
        SelectedFile_logo = null;


    }

    private void findview() {


        tv_title = findViewById(R.id.tv_title);
        et_salon_name = findViewById(R.id.et_salon_name);


        tv_do_this_later = findViewById(R.id.tv_do_this_later);
        iv_profile_image = findViewById(R.id.iv_profile_image);
        iv_logo = findViewById(R.id.iv_logo);


        Intent intent = getIntent();
        String type = intent.getStringExtra("is_type");

        if (type.equalsIgnoreCase("1")) {
            tv_title.setText("Customer");
        } else if (type.equalsIgnoreCase("2")) {
            tv_title.setText("Stylists & Sellers");
        }


        RelativeLayout rl_next_1 = findViewById(R.id.rl_next_1);
        rl_next_1.setOnClickListener(this);
        iv_profile_image.setOnClickListener(this);
        iv_logo.setOnClickListener(this);
        tv_do_this_later.setOnClickListener(this);


        if (!preferences.getPRE_SelectedFile_profile().equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(preferences.getPRE_SelectedFile_profile())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_profile_image);
        }

        et_salon_name.setText("" + preferences.getPRE_SalonName());


    }


    Integer PICKFILE_RESULT_CODE = 1001;

    void selectdocument() {

        if (!utills.Permissions_READ_EXTERNAL_STORAGE(this)) {
            utills.Request_READ_EXTERNAL_STORAGE(this);
        } else {


            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_chose_file);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            Window window = dialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final LinearLayout ll_title = dialog.findViewById(R.id.ll_title);
            final LinearLayout tv_choosefile = dialog.findViewById(R.id.tv_choosefile);
            final LinearLayout tv_tackepicture = dialog.findViewById(R.id.tv_tackepicture);
            final TextView tv_dialog_name = dialog.findViewById(R.id.tv_dialog_name);


            if (is_logo_profile == 2) {
                tv_dialog_name.setVisibility(View.GONE);
                ll_title.setVisibility(View.VISIBLE);
            } else {
                tv_dialog_name.setVisibility(View.VISIBLE);
                ll_title.setVisibility(View.GONE);
            }


            dialog.show();


            tv_tackepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectImage();
                    dialog.dismiss();
                }
            });

            tv_choosefile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                    dialog.dismiss();
                }
            });


        }
    }

    File files;
    Uri mCapturedImageURI;
    File SelectedFile_profile;
    File SelectedFile_logo;

    private void SelectImage() {

        files = null;
        try {
            files = utills.createImageFile();
        } catch (IOException ex) {
            Log.d("mylog", "Exception while creating file: " + ex.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (files != null) {
                Log.d("mylog", "Photofile not null");

                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".share",
                        files);

                mCapturedImageURI = Uri.parse(files.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        } else {
            try {
                files.createNewFile();
            } catch (IOException e) {
            }

            Uri outputFileUri = Uri.fromFile(files);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, 1);
        }
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            Uri content_describer = data.getData();

            String path = "";
            try {
                path = FileUtils.getPath(this, content_describer);
                if (path != null) {
                    File file = new File(path);


                    if (is_logo_profile == 1) {


                        try {
                            SelectedFile_profile = saveBitmapToFile(file);
                        } catch (Exception e) {
                            SelectedFile_profile = file;
                            e.printStackTrace();
                        }


                        Glide.with(this)
                                .load(SelectedFile_profile.getAbsolutePath())
                                .addListener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                        return false;
                                    }
                                })
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_profile_image);

                    } else {


                        try {
                            SelectedFile_logo = saveBitmapToFile(file);
                        } catch (Exception e) {
                            SelectedFile_logo = file;
                            e.printStackTrace();
                        }

                        Glide.with(this)
                                .load(SelectedFile_logo.getAbsolutePath())
                                .addListener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                        return false;
                                    }
                                })
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_logo);

                    }


                } else {

                    Toast.makeText(this, "Please select file from your directory", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            try {
                if (files != null) {
                    if (files.exists()) {

                        if (is_logo_profile == 1) {

                            try {
                                SelectedFile_profile = saveBitmapToFile(files);
                            } catch (Exception e) {
                                SelectedFile_profile = files;
                                e.printStackTrace();
                            }

                            Glide.with(this)
                                    .load(SelectedFile_profile.getAbsolutePath())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(iv_profile_image);

                        } else {


                            try {
                                SelectedFile_logo = saveBitmapToFile(files);
                            } catch (Exception e) {
                                SelectedFile_logo = files;
                                e.printStackTrace();
                            }

                            Glide.with(this)
                                    .load(SelectedFile_logo.getAbsolutePath())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(iv_logo);
                        }


                    } else {
                        Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "File is Null", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {

            Uri imageUri = getPickImageResultUri(this, data);
            CropImage.activity(imageUri)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(this);

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {


            if (files != null) {
                if (files.exists()) {
                    CropImage.activity(Uri.fromFile(files))
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .start(this);
                } else {
                    Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "File is Null", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                String path = FileUtils.getPath(this, resultUri);
                File file123 = new File(path);

                try {
                    if (file123 != null) {
                        if (file123.exists()) {

                            if (is_logo_profile == 1) {

                                try {
                                    SelectedFile_profile = utills.saveBitmapToFile(file123);
                                    try {
                                        long fileSizeInBytes = SelectedFile_profile.length();
                                        long fileSizeInKB = fileSizeInBytes / 1024;
                                        long fileSizeInMB = fileSizeInKB / 1024;
                                        if (fileSizeInMB > 5) {
                                            Toast.makeText(this,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                                            SelectedFile_profile=null;
                                            return;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } catch (Exception e) {
                                    SelectedFile_profile = file123;
                                    e.printStackTrace();
                                }

                                Glide.with(this)
                                        .load(SelectedFile_profile.getAbsolutePath())
                                        .addListener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                                return false;
                                            }
                                        })
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .into(iv_profile_image);

                            } else {


                                try {
                                    SelectedFile_logo = utills.saveBitmapToFile(file123);
                                    try {
                                        long fileSizeInBytes = SelectedFile_logo.length();
                                        long fileSizeInKB = fileSizeInBytes / 1024;
                                        long fileSizeInMB = fileSizeInKB / 1024;
                                        if (fileSizeInMB > 5) {
                                            Toast.makeText(this,"File must be less than 5MB",Toast.LENGTH_SHORT).show();
                                            SelectedFile_logo=null;
                                            return;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } catch (Exception e) {
                                    SelectedFile_logo = file123;
                                    e.printStackTrace();
                                }

                                Glide.with(this)
                                        .load(SelectedFile_logo.getAbsolutePath())
                                        .addListener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                                return false;
                                            }
                                        })
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .into(iv_logo);
                            }


                        } else {
                            Toast.makeText(this, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "File is Null", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_next_1:


                if (SelectedFile_profile != null) {
                    preferences.setPRE_SelectedFile_profile("" + SelectedFile_profile.getAbsolutePath());
                } else {
                    preferences.setPRE_SelectedFile_profile("");
                }

                if (SelectedFile_logo != null) {
                    preferences.setPRE_SelectedFile_logo("" + SelectedFile_logo.getAbsolutePath());
                } else {
                    preferences.setPRE_SelectedFile_logo("");
                }



                if ( preferences.getPRE_SelectedFile_profile().equalsIgnoreCase("")) {
                    Toast toast = Toast.makeText(this,"Please select profile picture", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                } else if ( preferences.getPRE_SelectedFile_logo().equalsIgnoreCase("")) {
                    Toast toast = Toast.makeText(this,"Please select logo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                } else {

                    if (SelectedFile_profile != null) {
                        preferences.setPRE_SelectedFile_profile("" + SelectedFile_profile.getAbsolutePath());
                    } else {
                        preferences.setPRE_SelectedFile_profile("" + preferences.getPRE_SelectedFile_profile());
                    }


                    if (SelectedFile_logo != null) {
                        preferences.setPRE_SelectedFile_logo("" + SelectedFile_logo.getAbsolutePath());
                    } else {
                        preferences.setPRE_SelectedFile_logo("");
                    }


                    preferences.setPRE_SalonName("" + et_salon_name.getText().toString());


                    if (!et_salon_name.getText().toString().equalsIgnoreCase("")) {
                        Intent i = new Intent(this, Salon_RegisterLocation_Activty.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(this, "Please enter Salon Name", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("onClick:profile ", preferences.getPRE_SelectedFile_profile());
                    Log.e("onClick:logo ", preferences.getPRE_SelectedFile_logo());

                }
                break;
            case R.id.iv_profile_image:

                is_logo_profile = 1;
                selectdocument();

                break;
            case R.id.iv_logo:

                is_logo_profile = 2;
                selectdocument();

                break;
            case R.id.tv_do_this_later:



                if (SelectedFile_profile != null) {
                    preferences.setPRE_SelectedFile_profile("" + SelectedFile_profile.getAbsolutePath());
                } else {
                    preferences.setPRE_SelectedFile_profile("");
                }

                if (SelectedFile_logo != null) {
                    preferences.setPRE_SelectedFile_logo("" + SelectedFile_logo.getAbsolutePath());
                } else {
                    preferences.setPRE_SelectedFile_logo("");
                }

                preferences.setPRE_SalonName("" + et_salon_name.getText().toString());


                if (!et_salon_name.getText().toString().equalsIgnoreCase("")) {
                    Intent i = new Intent(this, Salon_RegisterLocation_Activty.class);
                    startActivity(i);
                } else {
                    Toast.makeText(this, "Please enter Salon Name", Toast.LENGTH_SHORT).show();
                }



                Log.e("onClick:profile ", preferences.getPRE_SelectedFile_profile());
                Log.e("onClick:logo ", preferences.getPRE_SelectedFile_logo());

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectdocument();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
