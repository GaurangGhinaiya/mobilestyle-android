package com.moremy.style.Salon.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.moremy.style.R;
import com.moremy.style.Salon.Model.Salon_Data;
import com.moremy.style.Salon.Model.Salon_SalonSubservice;
import com.moremy.style.Salon.Modify.Salon_ModifyServices_Activty;
import com.moremy.style.Utills.utills;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Salon.Fragment.Fragment_Account_Salon.salon_data;


public class Fragment_Pricelist extends Fragment {

    private Context context;


    public Fragment_Pricelist() {
    }

    private List<Salon_SalonSubservice> data_prize_list = new ArrayList<>();
    private GridView listview_prize;


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylist_pricelist, container, false);
        context = getActivity();


        data_prize_list = new ArrayList<>();
        listview_prize = view.findViewById(R.id.listview_countiy);


        TextView tv_modify_services= view.findViewById(R.id.tv_modify_services);
        tv_modify_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (salon_data != null) {

                    Intent i = new Intent(context, Salon_ModifyServices_Activty.class);
                    i.putExtra("salon_id",""+salon_data.getId());
                    startActivity(i);

                }



            }
        });

        return view;
    }


    private boolean isVisible;
    private boolean isStarted;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            setData();
        } else {
            if (listview_prize != null) {
                listview_prize.setVisibility(View.GONE);
            }
        }

    }

    private void setData() {
        if (listview_prize != null) {
            listview_prize.setVisibility(View.VISIBLE);
            data_prize_list = new ArrayList<>();


            if (salon_data != null) {
                Salon_Data profile_data1 = new Salon_Data();
                profile_data1 = salon_data;
                data_prize_list.addAll(profile_data1.getSalonSubservices());
            }

            Adapter_Prize adapter_prize = new Adapter_Prize(context, data_prize_list);
            listview_prize.setAdapter(adapter_prize);
        }

    }

    class Adapter_Prize extends BaseAdapter {

        Context context;
        List<Salon_SalonSubservice> listState;
        LayoutInflater inflater;

        boolean is_selected = false;
        int pos_selected = -1;

        Adapter_Prize(Context applicationContext, List<Salon_SalonSubservice> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.datalist_pricelist, viewGroup, false);
                viewHolder.ll_mainprizelayout = view.findViewById(R.id.ll_mainprizelayout);
                viewHolder.tv_treatmentname = view.findViewById(R.id.tv_treatmentname);
                viewHolder.iv_right = view.findViewById(R.id.iv_right);
                viewHolder.tv_prize = view.findViewById(R.id.tv_prize);
                viewHolder.tv_time = view.findViewById(R.id.tv_time);
                viewHolder.view_line = view.findViewById(R.id.view_line);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }


            if (listState.size() - 1 == position) {
                viewHolder.view_line.setVisibility(View.GONE);
            } else {
                viewHolder.view_line.setVisibility(View.VISIBLE);
            }

            if (listState.get(position).getService() != null) {
                viewHolder.tv_treatmentname.setText("" + listState.get(position).getService().getName());
            }


            viewHolder.tv_prize.setText("£" + utills.roundTwoDecimals(listState.get(position).getPrice()));
            viewHolder.tv_time.setText("" + listState.get(position).getTime() + "m");


            if (is_selected) {

                if (pos_selected == position) {
                    viewHolder.tv_prize.setVisibility(View.GONE);
                    viewHolder.iv_right.setVisibility(View.VISIBLE);
                    viewHolder.tv_treatmentname.setTextColor(getResources().getColor(R.color.parrot));
                } else {
                    viewHolder.tv_prize.setVisibility(View.VISIBLE);
                    viewHolder.iv_right.setVisibility(View.GONE);
                    viewHolder.tv_treatmentname.setTextColor(getResources().getColor(R.color.black_light6));
                }


            }


//            viewHolder.ll_mainprizelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    pos_selected = position;
//                    is_selected = true;
//
//                    notifyDataSetChanged();
//                }
//            });


            return view;
        }

        public class ViewHolder {
            LinearLayout ll_mainprizelayout;
            TextView tv_treatmentname, tv_prize, tv_time;
            ImageView iv_right;
            View view_line;
        }

    }

}
