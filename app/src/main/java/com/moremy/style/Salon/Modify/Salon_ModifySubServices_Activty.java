package com.moremy.style.Salon.Modify;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Base1_Activity;
import com.moremy.style.CommanActivity.Premium_Activity;
import com.moremy.style.Model.StripePlan_Data;
import com.moremy.style.Model.StripePlan_Response;
import com.moremy.style.R;
import com.moremy.style.Salon.Model.SalonModifySelected_Data;
import com.moremy.style.Salon.Model.SalonModifySelected_Response;
import com.moremy.style.Salon.Model.SalonModifySelected_SalonSubservice;

import com.moremy.style.Salon.Model.Services_SubModel_Response;
import com.moremy.style.Salon.Model.Services_SubModel_SubService;
import com.moremy.style.Salon.Model.Services_SubModel_data;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Salon_ModifySubServices_Activty extends Base1_Activity {

    public static Dialog progressDialogs = null;


    Context context;
    List<Services_SubModel_data> salon_list_services = new ArrayList<>();
    List<Services_SubModel_data> Temp_salon_list_services = new ArrayList<>();


    TextView tv_main_categoryname;

    RecyclerView recycleview_cat, listview_Premium;

    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_register_sub_services);
        context = Salon_ModifySubServices_Activty.this;
        activity = Salon_ModifySubServices_Activty.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new Preferences(context);

        listview_Premium = findViewById(R.id.listview_Premium);
        listview_Premium.setNestedScrollingEnabled(false);
        listview_Premium.setLayoutManager(new GridLayoutManager(this, 1));

        recycleview_cat = findViewById(R.id.listview_country);
        tv_main_categoryname = findViewById(R.id.tv_main_categoryname);
        recycleview_cat.setNestedScrollingEnabled(false);
        recycleview_cat.setLayoutManager(new GridLayoutManager(this, 1));

        Intent intent = getIntent();
        String service_id = "" + intent.getStringExtra("service_ids");


        RelativeLayout rl_next = findViewById(R.id.rl_next);

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                method_final_next();


            }
        });

        salon_list_services = new ArrayList<>();
        Temp_salon_list_services = new ArrayList<>();
        get_list(service_id);

    }

    private void method_final_next() {

        try {
            List<Services_SubModel_data> list_services_test = new ArrayList<>();
            if (salon_list_services != null) {
                if (salon_list_services.size() > 0) {
                    try {
                        if (salon_list_services.get(page_count).getSubServices() != null) {
                            if (salon_list_services.get(page_count).getSubServices().size() > 0) {
                                for (int j = 0; j < salon_list_services.get(page_count).getSubServices().size(); j++) {
                                    if (salon_list_services.get(page_count).getSubServices().get(j).is_chechkbox) {
                                        list_services_test.add(salon_list_services.get(page_count));
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
            if (list_services_test.size() > 0) {

                int is_subservice_prize = 0;

                for (int i = 0; i < list_services_test.size(); i++) {
                    if (list_services_test.get(i).getSubServices() != null) {
                        if (list_services_test.get(i).getSubServices().size() > 0) {
                            for (int j = 0; j < list_services_test.get(i).getSubServices().size(); j++) {
                                if (list_services_test.get(i).getSubServices().get(j).is_chechkbox) {
                                    if (!list_services_test.get(i).getSubServices().get(j).price.equalsIgnoreCase("")) {
                                        float price = Float.parseFloat(list_services_test.get(i).getSubServices().get(j).price);
                                        if (price > 250) {
                                            is_subservice_prize = 1;
                                        }else if (price < 4) {
                                            is_subservice_prize = 3;
                                        }
                                    } else {
                                        is_subservice_prize = 2;
                                    }

                                }
                            }
                        }
                    }
                }


                if (is_subservice_prize == 1) {

                    Toast.makeText(context, "Price must be less than £250", Toast.LENGTH_SHORT).show();
                }else if (is_subservice_prize == 3) {
                    Toast.makeText(context, "Minimum price should be £4", Toast.LENGTH_SHORT).show();
                } else if (is_subservice_prize == 2) {

                    Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                } else {


                    List<Services_SubModel_SubService> list_services_premium = new ArrayList<>();
                    if (salon_list_services.get(page_count).getServices_Primium() != null) {
                        if (salon_list_services.get(page_count).getServices_Primium().size() > 0) {
                            list_services_premium.addAll(salon_list_services.get(page_count).getServices_Primium());
                        }
                    }

                    if (list_services_premium.size() > 0) {
                        int is_subservice_prize_premium = 0;

                        for (int i = 0; i < list_services_premium.size(); i++) {
                            if (!list_services_premium.get(i).price.equalsIgnoreCase("")) {
                                float price = Float.parseFloat(list_services_premium.get(i).price);
                                if (price > 250) {
                                    is_subservice_prize_premium = 1;
                                }else if (price < 4) {
                                    is_subservice_prize_premium = 3;
                                }
                            } else {
                                is_subservice_prize_premium = 2;
                            }
                        }

                        if (is_subservice_prize_premium == 1) {
                            Toast.makeText(context, "Price must be less than £250", Toast.LENGTH_SHORT).show();
                        }else if (is_subservice_prize_premium == 3) {
                            Toast.makeText(context, "Minimum price should be £4", Toast.LENGTH_SHORT).show();
                        } else if (is_subservice_prize_premium == 2) {
                            Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                        } else {
                            page_count++;
                            method_next();
                        }

                    } else {
                        page_count++;
                        method_next();
                    }

                }

            } else {
                Toast.makeText(context, "Please Select Atleast One SubService", Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    int page_count = 0;


    private void method_next() {
        try {
            if (salon_list_services != null) {
                if (salon_list_services.size() > 0) {

                    if (page_count != salon_list_services.size()) {

                        tv_main_categoryname.setText("" + salon_list_services.get(page_count).getName());

                        if (salon_list_services.get(page_count).getSubServices() != null) {
                            if (salon_list_services.get(page_count).getSubServices().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_services_temp = new ArrayList<>();
                                salon_list_services_temp.addAll(salon_list_services.get(page_count).getSubServices());

                                GridAdapter adapter_counties = new GridAdapter(context, salon_list_services_temp);
                                recycleview_cat.setAdapter(adapter_counties);

                            }
                        }


                        if (salon_list_services.get(page_count).getServices_Primium() != null) {
                            if (salon_list_services.get(page_count).getServices_Primium().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                                salon_list_premium_temp.addAll(salon_list_services.get(page_count).getServices_Primium());
                                GridAdapter_premium adapter_counties = new GridAdapter_premium(context, salon_list_premium_temp);
                                listview_Premium.setAdapter(adapter_counties);

                            }
                        }


                    } else {

                        page_count = page_count - 1;

                        Intent i = new Intent(context, Salon_ModifySubServicesFinal_Activty.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("arraylist", new Gson().toJson(salon_list_services));
                        i.putExtras(bundle);
                        startActivity(i);


                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void get_list(String service_id) {
        if (utills.isOnline(this)) {
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.post(Global_Service_Api.API_get_salon_services)
                    .addBodyParameter("service_id", service_id)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Temp_salon_list_services = new ArrayList<>();

                                if (flag.equalsIgnoreCase("true")) {

                                    Services_SubModel_Response Response = new Gson().fromJson(result.toString(), Services_SubModel_Response.class);

                                    if (Response.getData() != null) {

                                        Temp_salon_list_services.addAll(Response.getData());


                                        for (int i = 0; i < Temp_salon_list_services.size(); i++) {


                                            if (Temp_salon_list_services.get(i).getSubServices() != null) {
                                                if (Temp_salon_list_services.get(i).getSubServices().size() > 0) {

                                                    List<Services_SubModel_SubService> salon_list_services_temp = new ArrayList<>();
                                                    salon_list_services_temp.addAll(Temp_salon_list_services.get(i).getSubServices());


                                                    Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                                    suB_services_data.setName("Is_Premiummm");
                                                    suB_services_data.is_chechkbox = false;
                                                    suB_services_data.is_premium_box = false;
                                                    salon_list_services_temp.add(suB_services_data);

                                                    Temp_salon_list_services.get(i).setSubServices(salon_list_services_temp);


                                                }
                                            }

                                        }

                                        get_selected_list();


                                    }

                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    public void get_selected_list() {
        if (utills.isOnline(this)) {
            utills.stopLoader(progressDialogs);
            progressDialogs = utills.startLoader(this);
            AndroidNetworking.get(Global_Service_Api.API_get_salon_subservices)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialogs);
                            if (result == null || result == "") return;
                            Log.e("salon_subservices", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                List<SalonModifySelected_SalonSubservice> list_Subservices_selected_Temp = new ArrayList<>();
                                List<SalonModifySelected_SalonSubservice> list_Premium_selected_Temp = new ArrayList<>();


                                if (flag.equalsIgnoreCase("true")) {

                                    SalonModifySelected_Response Response = new Gson().fromJson(result.toString(), SalonModifySelected_Response.class);

                                    if (Response.getData() != null) {
                                        SalonModifySelected_Data Modify_Data = new SalonModifySelected_Data();
                                        Modify_Data = Response.getData();


                                        salon_list_services = new ArrayList<>();


                                        if (Modify_Data.getSalonSubservices() != null) {
                                            list_Subservices_selected_Temp.addAll(Modify_Data.getSalonSubservices());
                                        }

                                        if (Modify_Data.getOtherSubServices() != null) {
                                            list_Premium_selected_Temp.addAll(Modify_Data.getOtherSubServices());
                                        }


                                        for (int j = 0; j < Temp_salon_list_services.size(); j++) {

                                            List<Services_SubModel_SubService> list_Subservices_Final = new ArrayList<>();

                                            List<Services_SubModel_SubService> list_Subservices_Temp = new ArrayList<>();
                                            list_Subservices_Temp.addAll(Temp_salon_list_services.get(j).getSubServices());


                                            List<Services_SubModel_SubService> list_Premium_Final = new ArrayList<>();


                                            String ID = "" + Temp_salon_list_services.get(j).getId();


                                            for (int i_select = 0; i_select < list_Premium_selected_Temp.size(); i_select++) {
                                                if (ID.equalsIgnoreCase("" + list_Premium_selected_Temp.get(i_select).getService().getParentId())) {
                                                    Services_SubModel_SubService services_data = new Services_SubModel_SubService();
                                                    services_data.setId(list_Premium_selected_Temp.get(i_select).getService().getId());
                                                    services_data.is_chechkbox = true;
                                                    services_data.price = list_Premium_selected_Temp.get(i_select).getPrice();
                                                    services_data.time = list_Premium_selected_Temp.get(i_select).getTime();
                                                    services_data.is_Services_description = "" + list_Premium_selected_Temp.get(i_select).getService().getDescription();
                                                    services_data.setName("" + list_Premium_selected_Temp.get(i_select).getService().getName());
                                                    services_data.serviceid = ("" + list_Premium_selected_Temp.get(i_select).getService().getParentId());
                                                    list_Premium_Final.add(services_data);

                                                }
                                            }


                                            for (int i_main = 0; i_main < list_Subservices_Temp.size(); i_main++) {

                                                boolean found_1 = false;
                                                for (int i_select = 0; i_select < list_Subservices_selected_Temp.size(); i_select++) {
                                                    if (ID.equalsIgnoreCase("" + list_Subservices_selected_Temp.get(i_select).getService().getParentId())) {
                                                        found_1 = true;
                                                    }
                                                }

                                                if (found_1) {

                                                    String i_id = "" + list_Subservices_Temp.get(i_main).getId();

                                                    boolean found = false;
                                                    int poss = 0;

                                                    for (int i_select = 0; i_select < list_Subservices_selected_Temp.size(); i_select++) {
                                                        if (i_id.equalsIgnoreCase("" + list_Subservices_selected_Temp.get(i_select).getService().getId())) {
                                                            found = true;
                                                            poss = i_select;
                                                        }
                                                    }

                                                    if (found) {

                                                        Services_SubModel_SubService services_data = new Services_SubModel_SubService();
                                                        services_data.setId(list_Subservices_selected_Temp.get(poss).getService().getId());
                                                        services_data.is_chechkbox = true;
                                                        services_data.price = list_Subservices_selected_Temp.get(poss).getPrice();
                                                        services_data.time = list_Subservices_selected_Temp.get(poss).getTime();
                                                        services_data.is_Services_description = "" + list_Subservices_selected_Temp.get(poss).getService().getDescription();
                                                        services_data.setName("" + list_Subservices_selected_Temp.get(poss).getService().getName());
                                                        services_data.serviceid = ("" + list_Subservices_selected_Temp.get(poss).getService().getParentId());
                                                        list_Subservices_Final.add(services_data);
                                                        poss = 0;


                                                    } else {


                                                        if(list_Subservices_Temp.get(i_main).getName().equalsIgnoreCase("Is_Premiummm")){

                                                            if (list_Premium_Final.size() > 0) {
                                                                Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                                                suB_services_data.setName("Is_Premiummm");
                                                                suB_services_data.is_chechkbox = false;
                                                                suB_services_data.is_premium_box = true;
                                                                list_Subservices_Final.add(suB_services_data);
                                                            } else {
                                                                Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                                                suB_services_data.setName("Is_Premiummm");
                                                                suB_services_data.is_chechkbox = false;
                                                                suB_services_data.is_premium_box = false;
                                                                list_Subservices_Final.add(suB_services_data);
                                                            }

                                                        }else {

                                                            Services_SubModel_SubService services_data = new Services_SubModel_SubService();
                                                            services_data.setId(list_Subservices_Temp.get(i_main).getId());
                                                            services_data.is_chechkbox = false;
                                                            services_data.price = (list_Subservices_Temp.get(i_main).price);
                                                            services_data.time = (list_Subservices_Temp.get(i_main).time);
                                                            services_data.is_Services_description = (list_Subservices_Temp.get(i_main).is_Services_description);
                                                            services_data.setName(list_Subservices_Temp.get(i_main).getName());
                                                            services_data.serviceid = "" + (list_Subservices_Temp.get(i_main).getParentId());
                                                            list_Subservices_Final.add(services_data);
                                                        }
                                                    }


                                                }else {


                                                    if(list_Subservices_Temp.get(i_main).getName().equalsIgnoreCase("Is_Premiummm")){

                                                        if (list_Premium_Final.size() > 0) {
                                                            Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                                            suB_services_data.setName("Is_Premiummm");
                                                            suB_services_data.is_chechkbox = false;
                                                            suB_services_data.is_premium_box = true;
                                                            list_Subservices_Final.add(suB_services_data);
                                                        } else {
                                                            Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                                            suB_services_data.setName("Is_Premiummm");
                                                            suB_services_data.is_chechkbox = false;
                                                            suB_services_data.is_premium_box = false;
                                                            list_Subservices_Final.add(suB_services_data);
                                                        }

                                                    }else {

                                                        Services_SubModel_SubService services_data = new Services_SubModel_SubService();
                                                        services_data.setId(list_Subservices_Temp.get(i_main).getId());
                                                        services_data.is_chechkbox = false;
                                                        services_data.price = (list_Subservices_Temp.get(i_main).price);
                                                        services_data.time = (list_Subservices_Temp.get(i_main).time);
                                                        services_data.is_Services_description = (list_Subservices_Temp.get(i_main).is_Services_description);
                                                        services_data.setName(list_Subservices_Temp.get(i_main).getName());
                                                        services_data.serviceid = "" + (list_Subservices_Temp.get(i_main).getParentId());
                                                        list_Subservices_Final.add(services_data);
                                                    }
                                                }


                                            }


                                            Services_SubModel_data modelData = new Services_SubModel_data();
                                            modelData.setSubServices(list_Subservices_Final);
                                            modelData.setServices_Primium(list_Premium_Final);
                                            modelData.setId("" + Temp_salon_list_services.get(j).getId());
                                            modelData.setName("" + Temp_salon_list_services.get(j).getName());

                                            salon_list_services.add(j, modelData);


                                        }


                                        method_next();

                                    }

                                } else {

                                    salon_list_services.addAll(Temp_salon_list_services);

                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialogs);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {
        private List<Services_SubModel_SubService> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter(Context context, List<Services_SubModel_SubService> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_textcountry;
            EditText et_prize;
            ImageView iv_checkbox;
            LinearLayout ll_15, ll_30, ll_45, ll_60, ll_70, ll_chechkbox, ll_horizontal;


            LinearLayout ll_premium;


            public MyViewHolder(View view) {
                super(view);
                tv_textcountry = (TextView) view.findViewById(R.id.tv_textcountry);

                ll_15 = (LinearLayout) view.findViewById(R.id.ll_15);
                ll_30 = (LinearLayout) view.findViewById(R.id.ll_30);
                ll_45 = (LinearLayout) view.findViewById(R.id.ll_45);
                ll_60 = (LinearLayout) view.findViewById(R.id.ll_60);
                ll_70 = (LinearLayout) view.findViewById(R.id.ll_70);
                ll_horizontal = (LinearLayout) view.findViewById(R.id.ll_horizontal);
                ll_chechkbox = (LinearLayout) view.findViewById(R.id.ll_chechkbox);

                et_prize = (EditText) view.findViewById(R.id.et_prize);
                iv_checkbox = (ImageView) view.findViewById(R.id.iv_checkbox);


                ll_premium = (LinearLayout) view.findViewById(R.id.ll_premium);

            }
        }

        @Override
        public GridAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_with_premium, parent, false);

            return new GridAdapter.MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter.MyViewHolder viewHolder, final int position) {


            if (countries_list.get(position).getName().equals("Is_Premiummm")) {
                viewHolder.ll_premium.setVisibility(View.VISIBLE);
                viewHolder.tv_textcountry.setVisibility(View.GONE);
            } else {
                viewHolder.ll_premium.setVisibility(View.GONE);
                viewHolder.tv_textcountry.setVisibility(View.VISIBLE);
            }


            viewHolder.tv_textcountry.setText("" + countries_list.get(position).getName());
            viewHolder.et_prize.setText("" + countries_list.get(position).price);


            viewHolder.et_prize.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).price = "" + s.toString();
                }
            });


            if (countries_list.get(position).is_chechkbox) {

                viewHolder.ll_horizontal.setVisibility(View.VISIBLE);
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));


            } else {

                viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                viewHolder.ll_horizontal.setVisibility(View.GONE);
                viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);


                if (countries_list.get(position).is_premium_box) {
                    viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                    listview_Premium.setVisibility(View.VISIBLE);
                } else {
                    listview_Premium.setVisibility(View.GONE);
                }


            }


            viewHolder.ll_chechkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (countries_list.get(position).getName().equals("Is_Premiummm")) {

                        if (countries_list.get(position).is_premium_box) {

                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                            countries_list.get(position).is_premium_box = false;

                            listview_Premium.setVisibility(View.GONE);


                            List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                            salon_list_services.get(page_count).setServices_Primium(salon_list_premium_temp);


                        } else {

                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                            countries_list.get(position).is_premium_box = true;

                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));


                            if (preferences.getPRE_Is_Premium_Salon()) {
                                listview_Premium.setVisibility(View.VISIBLE);


                                List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                                Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                                salon_list_premium_temp.add(suB_services_data);


                                salon_list_services.get(page_count).setServices_Primium(salon_list_premium_temp);


                                GridAdapter_premium adapter_counties = new GridAdapter_premium(context, salon_list_premium_temp);
                                listview_Premium.setAdapter(adapter_counties);


                            } else {
                                method_dialog_isPremium();
                            }


                        }
                    } else {

                        if (countries_list.get(position).is_chechkbox) {
                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface(context));
                            viewHolder.ll_horizontal.setVisibility(View.GONE);
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_plain);
                            countries_list.get(position).is_chechkbox = false;
                            countries_list.get(position).time = "15";
                            countries_list.get(position).price = "";
                            viewHolder.et_prize.setText("");

                        } else {
                            viewHolder.ll_horizontal.setVisibility(View.VISIBLE);
                            viewHolder.iv_checkbox.setImageResource(R.drawable.ic_chechkbox_fill);
                            countries_list.get(position).is_chechkbox = true;
                            viewHolder.tv_textcountry.setTypeface(utills.customTypeface_Bold(context));

                        }

                    }


                }
            });

            viewHolder.ll_15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).time = "15";

                    viewHolder.ll_15.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_30.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "30";

                    viewHolder.ll_30.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });


            viewHolder.ll_45.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "45";
                    viewHolder.ll_45.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });


            viewHolder.ll_60.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "60";
                    viewHolder.ll_60.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_70.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "70";
                    viewHolder.ll_70.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    @Override
    public void onBackPressed() {

        try {
            if (salon_list_services != null) {
                if (salon_list_services.size() > 0) {


                    if (page_count > 0) {

                        page_count = page_count - 1;

                        tv_main_categoryname.setText("" + salon_list_services.get(page_count).getName());

                        if (salon_list_services.get(page_count).getSubServices() != null) {
                            if (salon_list_services.get(page_count).getSubServices().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_services_temp = new ArrayList<>();
                                salon_list_services_temp.addAll(salon_list_services.get(page_count).getSubServices());
                                GridAdapter adapter_counties = new GridAdapter(context, salon_list_services_temp);
                                recycleview_cat.setAdapter(adapter_counties);

                            }
                        }


                        if (salon_list_services.get(page_count).getServices_Primium() != null) {
                            if (salon_list_services.get(page_count).getServices_Primium().size() > 0) {

                                List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                                salon_list_premium_temp.addAll(salon_list_services.get(page_count).getServices_Primium());
                                GridAdapter_premium adapter_counties = new GridAdapter_premium(context, salon_list_premium_temp);
                                listview_Premium.setAdapter(adapter_counties);

                            }
                        }


                    } else {

                        super.onBackPressed();
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    /*.........premium....*/

    private void method_dialog_isPremium() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_is_premium);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final CardView card_premium = dialog.findViewById(R.id.card_premium);
        final TextView tv_nothanks = dialog.findViewById(R.id.tv_nothanks);

        final LinearLayout ll_3_plan = dialog.findViewById(R.id.ll_3_plan);
        final LinearLayout ll_2_plan = dialog.findViewById(R.id.ll_2_plan);
        final LinearLayout ll_1_plan = dialog.findViewById(R.id.ll_1_plan);
        ImageView iv_cancel_subscribe = dialog.findViewById(R.id.iv_cancel_subscribe);
        iv_cancel_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll_1_month = dialog.findViewById(R.id.ll_1_month);
        ll_1_month_no = dialog.findViewById(R.id.ll_1_month_no);
        ll_1_prize = dialog.findViewById(R.id.ll_1_prize);


        ll_2_month = dialog.findViewById(R.id.ll_2_month);
        ll_2_month_no = dialog.findViewById(R.id.ll_2_month_no);
        ll_2_prize = dialog.findViewById(R.id.ll_2_prize);

        ll_3_month = dialog.findViewById(R.id.ll_3_month);
        ll_3_month_no = dialog.findViewById(R.id.ll_3_month_no);
        ll_3_prize = dialog.findViewById(R.id.ll_3_prize);

        TextView tv_add_method = dialog.findViewById(R.id.tv_add_method);
        final TextView tv_final_prize = dialog.findViewById(R.id.tv_final_prize);


        datalist_StripePlan = new ArrayList<>();
        method_dialog_plan();


        dialog.show();

        ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
        ll_3_plan.setBackground(null);
        ll_1_plan.setBackground(null);


        card_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                preferences.setPRE_Is_Premium_Salon(true);
                listview_Premium.setVisibility(View.VISIBLE);


                List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                suB_services_data.is_chechkbox = false;
                salon_list_premium_temp.add(suB_services_data);


                salon_list_services.get(page_count).setServices_Primium(salon_list_premium_temp);


                GridAdapter_premium adapter_counties = new GridAdapter_premium(context, salon_list_premium_temp);
                listview_Premium.setAdapter(adapter_counties);


                Intent intent = new Intent(context, Premium_Activity.class);
                startActivity(intent);

                dialog.dismiss();
            }
        });


        tv_nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listview_Premium.setVisibility(View.GONE);

                List<Services_SubModel_SubService> salon_list_premium_temp = new ArrayList<>();
                salon_list_services.get(page_count).setServices_Primium(salon_list_premium_temp);


                preferences.setPRE_Is_Premium_Salon(false);

                dialog.dismiss();
            }
        });

        ll_3_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ll_3_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_2_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_3_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 2) {
                            plan_id = datalist_StripePlan.get(2).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        ll_2_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_1_plan.setBackground(null);

                tv_final_prize.setText("" + ll_2_prize.getText().toString());


                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 1) {
                            plan_id = datalist_StripePlan.get(1).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        ll_1_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_1_plan.setBackground(getResources().getDrawable(R.drawable.border_black_trans));
                ll_3_plan.setBackground(null);
                ll_2_plan.setBackground(null);

                tv_final_prize.setText("" + ll_1_prize.getText().toString());

                try {
                    if (datalist_StripePlan != null) {
                        if (datalist_StripePlan.size() > 0) {
                            plan_id = datalist_StripePlan.get(0).getId();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    List<StripePlan_Data> datalist_StripePlan = new ArrayList<>();
    TextView ll_1_month,
            ll_1_month_no,
            ll_1_prize,
            ll_2_month,
            ll_2_month_no,
            ll_2_prize,
            ll_3_month,
            ll_3_month_no,
            ll_3_prize;

    String plan_id;

    public void method_dialog_plan() {
        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);
            AndroidNetworking.get(Global_Service_Api.API_stripe_plans)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("home", result);
                            datalist_StripePlan = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    StripePlan_Response Response = new Gson().fromJson(result.toString(), StripePlan_Response.class);

                                    if (Response.getData() != null) {

                                        datalist_StripePlan.addAll(Response.getData());


                                        try {
                                            if (datalist_StripePlan != null) {

                                                if (datalist_StripePlan.size() > 0) {
                                                    ll_1_month.setText("" + datalist_StripePlan.get(0).getInterval());
                                                    ll_1_month_no.setText("" + datalist_StripePlan.get(0).getIntervalCount());
                                                    ll_1_prize.setText("£" + (datalist_StripePlan.get(0).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 1) {
                                                    ll_2_month.setText("" + datalist_StripePlan.get(1).getInterval());
                                                    ll_2_month_no.setText("" + datalist_StripePlan.get(1).getIntervalCount());
                                                    ll_2_prize.setText("£" + (datalist_StripePlan.get(1).getAmount() / 100));
                                                }

                                                if (datalist_StripePlan.size() > 2) {
                                                    ll_3_month.setText("" + datalist_StripePlan.get(2).getInterval());
                                                    ll_3_month_no.setText("" + datalist_StripePlan.get(2).getIntervalCount());
                                                    ll_3_prize.setText("£" + (datalist_StripePlan.get(2).getAmount() / 100));
                                                }


                                                try {
                                                    if (datalist_StripePlan != null) {
                                                        if (datalist_StripePlan.size() > 1) {
                                                            plan_id = datalist_StripePlan.get(1).getId();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.e("API", anError.toString());
                        }
                    });

        }
    }


    Dialog progressDialog = null;


    class GridAdapter_premium extends RecyclerView.Adapter<GridAdapter_premium.MyViewHolder> {
        private List<Services_SubModel_SubService> countries_list;

        Context mcontext;
        int lastPosition = -1;

        public GridAdapter_premium(Context context, List<Services_SubModel_SubService> arrayList) {
            this.countries_list = arrayList;
            mcontext = context;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            EditText et_prize, et_ServiceName, et_Description;
            LinearLayout ll_15, ll_30, ll_45, ll_60, ll_70, ll_add_another_servise;
            CardView card_cancel;

            public MyViewHolder(View view) {
                super(view);

                ll_15 = (LinearLayout) view.findViewById(R.id.ll_15);
                ll_30 = (LinearLayout) view.findViewById(R.id.ll_30);
                ll_45 = (LinearLayout) view.findViewById(R.id.ll_45);
                ll_60 = (LinearLayout) view.findViewById(R.id.ll_60);
                ll_70 = (LinearLayout) view.findViewById(R.id.ll_70);


                et_prize = (EditText) view.findViewById(R.id.et_prize);
                et_ServiceName = (EditText) view.findViewById(R.id.et_ServiceName);
                et_Description = (EditText) view.findViewById(R.id.et_Description);
                ll_add_another_servise = (LinearLayout) view.findViewById(R.id.ll_add_another_servise);
                card_cancel = (CardView) view.findViewById(R.id.card_cancel);


            }
        }


        @Override
        public GridAdapter_premium.MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.datalist_sub_service_with_premium_list, parent, false);

            return new GridAdapter_premium.MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final GridAdapter_premium.MyViewHolder viewHolder, final int position) {


            if (position == 0) {
                viewHolder.card_cancel.setVisibility(View.GONE);
            } else {
                viewHolder.card_cancel.setVisibility(View.VISIBLE);
            }

            if (position == countries_list.size() - 1) {
                viewHolder.ll_add_another_servise.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ll_add_another_servise.setVisibility(View.GONE);
            }

            if (countries_list.get(position).getName() != null) {
                if (!countries_list.get(position).getName().equalsIgnoreCase("null")) {
                    viewHolder.et_ServiceName.setText("" + countries_list.get(position).getName());
                }
            } else {
                viewHolder.et_ServiceName.setText("");
            }


            viewHolder.et_Description.setText("" + countries_list.get(position).is_Services_description);
            viewHolder.et_prize.setText("" + countries_list.get(position).price);


            viewHolder.card_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).is_Services_description = "";
                    countries_list.get(position).price = "";
                    countries_list.get(position).setName("");
                    countries_list.remove(position);
                    notifyDataSetChanged();

                    salon_list_services.get(page_count).setServices_Primium(countries_list);

                }
            });

            viewHolder.et_Description.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).is_Services_description = "" + s.toString();
                }
            });


            viewHolder.et_ServiceName.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).setName("" + s.toString());
                }
            });


            viewHolder.et_prize.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countries_list.get(position).price = "" + s.toString();
                }
            });


            viewHolder.ll_add_another_servise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Services_SubModel_SubService suB_services_data = new Services_SubModel_SubService();
                    countries_list.add(suB_services_data);

                    notifyDataSetChanged();


                    salon_list_services.get(page_count).setServices_Primium(countries_list);

                }
            });


            viewHolder.ll_15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    countries_list.get(position).time = "15";

                    viewHolder.ll_15.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_30.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "30";

                    viewHolder.ll_30.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });

            viewHolder.ll_45.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "45";
                    viewHolder.ll_45.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_60.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "60";
                    viewHolder.ll_60.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_70.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            });

            viewHolder.ll_70.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    countries_list.get(position).time = "70";
                    viewHolder.ll_70.setBackgroundResource(R.drawable.bg_gray_light);
                    /*.............................*/
                    viewHolder.ll_15.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_30.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_45.setBackgroundColor(getResources().getColor(R.color.transparent));
                    viewHolder.ll_60.setBackgroundColor(getResources().getColor(R.color.transparent));

                }
            });


        }

        @Override
        public int getItemCount() {
            return countries_list.size();
        }

    }


    public static Activity activity = null;
    public static void finish_this() {
        if(activity != null){
            activity.finish();
        }
    }


}
