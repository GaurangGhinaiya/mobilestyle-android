package com.moremy.style.Salon.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Salon_SalonPortfolio {

@SerializedName("id")
@Expose
private String id;
@SerializedName("salon_id")
@Expose
private Integer salonId;
@SerializedName("image")
@Expose
private String image;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Integer getSalonId() {
return salonId;
}

public void setSalonId(Integer salonId) {
this.salonId = salonId;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}


    public Salon_SalonPortfolio(String s_id, String s_image) {
        id = s_id;
        image = s_image;

    }

}