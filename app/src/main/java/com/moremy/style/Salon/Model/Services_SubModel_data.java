package com.moremy.style.Salon.Model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Services_SubModel_data {

    @SerializedName("id")
    @Expose
    private String id="0";
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sub_services")
    @Expose
    private List<Services_SubModel_SubService> subServices = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Services_SubModel_SubService> getSubServices() {
        return subServices;
    }

    public void setSubServices(List<Services_SubModel_SubService> subServices) {
        this.subServices = subServices;
    }




    private List<Services_SubModel_SubService> Services_Primium = null;

    public List<Services_SubModel_SubService> getServices_Primium() {
        return Services_Primium;
    }

    public void setServices_Primium(List<Services_SubModel_SubService> Services_Primium) {
        this.Services_Primium = Services_Primium;
    }



}