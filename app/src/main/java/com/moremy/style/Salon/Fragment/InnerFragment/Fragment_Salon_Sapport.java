package com.moremy.style.Salon.Fragment.InnerFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.Customer.Model.Model_SapportQue_Data;
import com.moremy.style.Customer.Model.Model_SapportQue_Response;
import com.moremy.style.R;

import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.moremy.style.Salon.Fragment.Fragment_Account_Salon.salon_data;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentManager_SALON;
import static com.moremy.style.Salon.Salon_MainProfile_Activty.fragmentTransaction_SALON;


public class Fragment_Salon_Sapport extends Fragment {

    private Context context;


    public Fragment_Salon_Sapport() {
    }

    Preferences preferences;
    Dialog progressDialog = null;


    EditText et_answer_support;
    ListView listview_question;
    CardView Card_listview_que;
    TextView tv_services_name_que, tv_user_name;
    LinearLayout ll_botom_answer, ll_done_question, ll_main_question;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_sapport, container, false);
        context = getActivity();
        preferences = new Preferences(context);


        RelativeLayout LL_customer_frag_Main = view.findViewById(R.id.LL_customer_frag_Main);
        LL_customer_frag_Main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        ll_main_question = view.findViewById(R.id.ll_main_question);
        ll_done_question = view.findViewById(R.id.ll_done_question);
        et_answer_support = view.findViewById(R.id.et_answer_support);
        ll_botom_answer = view.findViewById(R.id.ll_botom_answer);
        tv_user_name = view.findViewById(R.id.tv_user_name);
        listview_question = view.findViewById(R.id.listview_question);
        tv_services_name_que = view.findViewById(R.id.tv_services_name_que);
        Card_listview_que = view.findViewById(R.id.Card_listview_que);
        CardView card_select = view.findViewById(R.id.card_select);
        LinearLayout ll_List_select = view.findViewById(R.id.ll_List_select);


        card_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview_que.setVisibility(View.VISIBLE);
            }
        });

        ll_List_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card_listview_que.setVisibility(View.GONE);
            }
        });


        if (salon_data != null) {
            tv_user_name.setText("" + salon_data.getName() + ", we need to talk...");
        }


        method_get_faq();


        ImageView iv_more = view.findViewById(R.id.iv_more);
        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_more();
            }
        });


        CardView card_submit = view.findViewById(R.id.card_submit);
        card_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_submit();
            }
        });


        TextView tv_homescreen=view.findViewById(R.id.tv_homescreen);
        tv_homescreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FragmentManager fm = getFragmentManager();
                    for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        TextView tv_text_que=view.findViewById(R.id.tv_text_que);
        tv_text_que.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment currentFragment = new Fragment_Salon_FAQ();
                LoadFragment(currentFragment);
            }
        });



        return view;
    }


    private void method_submit() {

        progressDialog = utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_ssubmit_support)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .addBodyParameter("category_id", "" + service_id)
                .addBodyParameter("description", "" + et_answer_support.getText().toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;
                        Log.e("sapport", result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");


                            if (flag.equalsIgnoreCase("true")) {
                                ll_main_question.setVisibility(View.GONE);
                                ll_done_question.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }

    private void method_get_faq() {

        progressDialog = utills.startLoader(context);

        AndroidNetworking.get(Global_Service_Api.API_support_categories)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;
                        Log.e("sapport", result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");


                            List<Model_SapportQue_Data> list_services = new ArrayList<>();

                            if (flag.equalsIgnoreCase("true")) {

                                Model_SapportQue_Response Response = new Gson().fromJson(result.toString(), Model_SapportQue_Response.class);

                                if (Response.getData() != null) {
                                    list_services = Response.getData();
                                    GridAdapter adapter_counties = new GridAdapter(context, list_services);
                                    listview_question.setAdapter(adapter_counties);

                                }


                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }


    private void method_more() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_more_salon);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView tv_name_title = dialog.findViewById(R.id.tv_name_title);
        final TextView tv_account = dialog.findViewById(R.id.tv_account);
        final TextView tv_faq = dialog.findViewById(R.id.tv_faq);
        final TextView tv_sapport = dialog.findViewById(R.id.tv_sapport);
        final TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        final ImageView iv_close = dialog.findViewById(R.id.iv_close);

        final TextView tv_versionname = dialog.findViewById(R.id.tv_versionname);
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            tv_versionname.setText("ver " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final TextView  tv_MyProductsOrders = dialog.findViewById(R.id.tv_MyProductsOrders);
        tv_MyProductsOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(2);
                LoadFragment(currentFragment);
            }
        });

        final TextView  tv_MyServicesBookings = dialog.findViewById(R.id.tv_MyServicesBookings);
        tv_MyServicesBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(3);
                LoadFragment(currentFragment);
            }
        });


        if (salon_data != null) {
            tv_name_title.setText("" + salon_data.getName());
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog_card = new Dialog(context);
                dialog_card.setContentView(R.layout.dialog_chancel);
                dialog_card.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog_card.getWindow();
                window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                TextView tv_yes = dialog_card.findViewById(R.id.tv_yes);
                TextView tv_No = dialog_card.findViewById(R.id.tv_No);
                TextView tv_name_dialog = dialog_card.findViewById(R.id.tv_name_dialog);
                tv_name_dialog.setText("Are you sure you want to logout?");
                dialog_card.show();

                tv_No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                    }
                });

                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_card.dismiss();
                        method_logout();

                    }
                });
            }
        });


        tv_sapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_Sapport();
                LoadFragment(currentFragment);
            }
        });


        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_FAQ();
                LoadFragment(currentFragment);
            }
        });

        tv_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Fragment currentFragment = new Fragment_Salon_EditAccount(0);
                LoadFragment(currentFragment);
            }
        });



        dialog.show();


    }

    private void method_logout() {

        utills.startLoader(context);

        AndroidNetworking.post(Global_Service_Api.API_stylistlogout)
                .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        if (result == null || result == "") return;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            String flag = jsonObject.getString("flag");
                            String message = jsonObject.getString("message");
                            if (flag.equalsIgnoreCase("true")) {


                                try {
                                    SendBird.unregisterPushTokenForCurrentUser(preferences.getPRE_FCMtoken(),
                                            new SendBird.UnregisterPushTokenHandler() {
                                                @Override
                                                public void onUnregistered(SendBirdException e) {
                                                    if (e != null) {    // Error.
                                                        return;
                                                    }
                                                }
                                            }
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                preferences.setPRE_TOKEN("");
                                Intent i = new Intent(context, Login_activty.class);
                                startActivity(i);

                                if (getActivity() != null) {
                                    getActivity().finish();
                                }

                            }
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        utills.stopLoader(progressDialog);
                    }

                    @Override
                    public void onError(ANError anError) {
                        utills.stopLoader(progressDialog);
                        Log.d("API", anError.toString());
                    }
                });

    }

    public void LoadFragment(final Fragment fragment) {

        try {
            FragmentManager fm = getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
        // fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction_SALON.add(R.id.fmFragment, fragment);
        fragmentTransaction_SALON.addToBackStack(null);
        fragmentTransaction_SALON.commit();

    }


    String service_name = "";
    String service_id = "";

    public class GridAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        List<Model_SapportQue_Data> countries_list = new ArrayList<>();

        GridAdapter(Context context, List<Model_SapportQue_Data> data_countries_list1) {
            this.context = context;
            countries_list = data_countries_list1;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return countries_list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GridAdapter.ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new GridAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.datalist_que, parent, false);
                viewHolder.tv_textcountry = (TextView) convertView.findViewById(R.id.tv_textcountry);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (GridAdapter.ViewHolder) convertView.getTag();
            }


            viewHolder.tv_textcountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ll_botom_answer.setVisibility(View.VISIBLE);
                    Card_listview_que.setVisibility(View.GONE);
                    tv_services_name_que.setText(countries_list.get(position).getName());

                    service_name = "" + countries_list.get(position).getName();
                    service_id = "" + countries_list.get(position).getId();

                }
            });
            viewHolder.tv_textcountry.setText(countries_list.get(position).getName());

            return convertView;
        }

        private class ViewHolder {
            TextView tv_textcountry;
        }


    }


}
