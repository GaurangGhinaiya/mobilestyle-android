
package com.moremy.style.Salon.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonModifySelected_SalonSubservice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("salon_id")
    @Expose
    private Integer salonId;
    @SerializedName("subservice_id")
    @Expose
    private Integer subserviceId;
    @SerializedName("price")
    @Expose
    private String price="15";
    @SerializedName("time")
    @Expose
    private String time="";
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("service")
    @Expose
    private SalonModifySelected_Service service;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSalonId() {
        return salonId;
    }

    public void setSalonId(Integer salonId) {
        this.salonId = salonId;
    }

    public Integer getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(Integer subserviceId) {
        this.subserviceId = subserviceId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SalonModifySelected_Service getService() {
        return service;
    }

    public void setService(SalonModifySelected_Service service) {
        this.service = service;
    }





    public boolean is_chechkbox = false;



    public boolean is_premium_box = false;


    public String is_Services_description = "";
    public String serviceid = "";


}
