package com.moremy.style.Salon.Fragment.InnerFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.turf.TurfConstants;
import com.mapbox.turf.TurfMeta;
import com.mapbox.turf.TurfTransformation;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.R;
import com.moremy.style.Utills.AutoCompleteTextviewCustom;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;
import static com.moremy.style.Salon.Fragment.InnerFragment.Fragment_Salon_EditAccount.salon_data_Editprofile;


public class Fragment_salonLocation extends Fragment implements
        OnMapReadyCallback, PermissionsListener {
    Context context;


    public Fragment_salonLocation() {
    }

    Spinner spiner_city;
    ArrayList<String> search_list = new ArrayList<>();
    AutoCompleteTextviewCustom et_code;
    EditText  et_address;
    Preferences preferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_location, container, false);
        context = getActivity();
        preferences = new Preferences(context);
        search_list = new ArrayList<>();
        userIsInteracting = true;

       // scrollview = view.findViewById(R.id.scrollview);
        et_address = view.findViewById(R.id.et_address);
        et_code = view.findViewById(R.id.et_code);
        et_code.setThreshold(1);
        spiner_city = view.findViewById(R.id.spiner_city);
        ImageView iv_spinner_city = view.findViewById(R.id.iv_spinner_city);
        iv_spinner_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spiner_city.performClick();
            }
        });

        spiner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (userIsInteracting) {
                    userIsInteracting = false;
                } else {
                    City_Name = "" + search_list.get(position);
                    makeGeocodeSearch();

                    et_code.setText("");
                    search_list_postcode = new ArrayList<>();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (context, R.layout.autotextview_layout1, search_list_postcode);
                    et_code.setAdapter(adapter);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        method_get_city();

        et_code.setText("" + salon_data_Editprofile.getPostalCode());
        makeGeocodeSearch_postalcode();

        et_code.setDropDownWidth(getResources().getDisplayMetrics().widthPixels - 50);
        ArrayAdapter<String> adapter_code = new ArrayAdapter<String>
                (context, R.layout.autotextview_layout1, search_list_postcode);
        et_code.setAdapter(adapter_code);

        if(salon_data_Editprofile.getaddress_line_1() != null){
            if(!salon_data_Editprofile.getaddress_line_1().equalsIgnoreCase("null")){
                et_address.setText(""+salon_data_Editprofile.getaddress_line_1());

            }
        }



        CardView card_save = view.findViewById(R.id.card_save);
        card_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int is_valid = 0;
                for (int i = 0; i < search_list_postcode.size(); i++) {
                    String postal_code = "" + et_code.getText().toString().trim();
                    if (postal_code.equalsIgnoreCase(search_list_postcode.get(i).toString())) {
                        is_valid = 1;
                    }
                }
                if (is_valid == 1) {
                    method_api();
                } else {
                    Toast.makeText(context, "plesae enter valid postal code", Toast.LENGTH_LONG).show();
                }

            }
        });



        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {  // & MotionEvent.ACTION_MASK
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                mapView.onTouchEvent(event);
                return true;
            }

        });


        et_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                 if (et_code.getText().toString().length() >= 1) {
                    makeGeocodeSearch_postalcode();
                }

            }
        });


        et_code.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                utills.hideKeyboard(getActivity());
                makeGeocodeSearch_code();

            }
        });


        return view;
    }

    String City_Name = "";
    private boolean userIsInteracting;

    private void method_get_city() {
        if (utills.isOnline(context)) {
            AndroidNetworking.get(Global_Service_Api.API_cities)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {

                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                if (flag.equalsIgnoreCase("true")) {
                                    search_list = new ArrayList<>();


                                    JSONArray jsonObject2 = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonObject2.length(); i++) {
                                        JSONObject jsonObject1 = jsonObject2.getJSONObject(i);
                                        search_list.add("" + jsonObject1.getString("name"));
                                    }
                                    search_list.add("please select");

                                    SpinnerdocumentAdapter1 customAdapter_state = new SpinnerdocumentAdapter1(context, search_list);
                                    spiner_city.setAdapter((SpinnerAdapter) customAdapter_state);

                                    spiner_city.setSelection(customAdapter_state.getCount());

                                    City_Name = "" + salon_data_Editprofile.getCity();
                                    if (!City_Name.equalsIgnoreCase("")) {
                                        try {
                                            for (int i = 0; i < search_list.size(); i++) {
                                                if (City_Name.equalsIgnoreCase(search_list.get(i))) {
                                                    spiner_city.setSelection(i);
                                                    method_get_maps(City_Name);
                                                    break;
                                                }
                                            }
                                        } catch (Exception e) {
                                            spiner_city.setSelection(customAdapter_state.getCount());
                                            e.printStackTrace();
                                        }

                                    } else {
                                        spiner_city.setSelection(customAdapter_state.getCount());
                                    }


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.e("API", anError.toString());
                        }
                    });

        }
    }

    class SpinnerdocumentAdapter1 extends BaseAdapter {
        Context context;
        LayoutInflater inflter;
        List<String> _list_documnetlist;

        public SpinnerdocumentAdapter1(Context applicationContext, List<String> _list_documnetlist) {
            this.context = applicationContext;
            this._list_documnetlist = _list_documnetlist;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return _list_documnetlist.size() - 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.custom_spinner_items_type, null);
            AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.textView);
            names.setText(_list_documnetlist.get(i));
            return view;
        }
    }

    Dialog progressDialog = null;

    private void method_api() {

        if (utills.isOnline(context)) {
            progressDialog = utills.startLoader(context);

            AndroidNetworking.post(Global_Service_Api.API_update_salon_location)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .addBodyParameter("postal_code", "" + et_code.getText().toString())
                    .addBodyParameter("address_line_1", "" + et_address.getText().toString())
                    .addBodyParameter("city", "" + City_Name)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            if (result == null || result == "") return;

                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");

                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            utills.stopLoader(progressDialog);
                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });


        }
    }

    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;

    private static final String TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID
            = "TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID";
    private static final String TURF_CALCULATION_FILL_LAYER_ID = "TURF_CALCULATION_FILL_LAYER_ID";
    private static final String CIRCLE_CENTER_LAYER_ID = "CIRCLE_CENTER_LAYER_ID";

    private String circleUnit = TurfConstants.UNIT_MILES;
    private int circleSteps = 360;
    private double circleRadius = 0.5;


    String token = "pk.eyJ1IjoiZm9yZXNpZ2h0dGVjaG5vbG9naWVzIiwiYSI6ImNqa3phMXBiNDBydjczcXFwNWQzbWlqNnkifQ.fQQWorOl5PVt4s4M0pwYaA";


    String Longitude = "";
    String Latitude = "";
    ArrayList<String> search_list_postcode = new ArrayList<>();

    Point Final_point;
    boolean is_true = false;

    private void makeGeocodeSearch() {
        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + City_Name)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {

                                Point firstResultPoint = results.get(0).center();
                                Final_point = firstResultPoint;
                                LatLng cityLatLng = new LatLng(Final_point.latitude(), Final_point.longitude());
                                animateCameraToNewPosition(cityLatLng);
                                drawPolygonCircle(Final_point, 10);
                                Longitude = "" + Final_point.longitude();
                                Latitude = "" + Final_point.latitude();

                            } else {
                                Toast.makeText(context, "no result",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    // Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            //  Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }

    private void makeGeocodeSearch_postalcode() {

        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + et_code.getText().toString())
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {


                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {
                                search_list_postcode = new ArrayList<>();

                                for (int i = 0; i < results.size(); i++) {
                                    String name= ""+results.get(i).placeName();
                                    if(name.contains(City_Name)){
                                        search_list_postcode.add("" + results.get(i).text());
                                    }
                                }

                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                                    (context, R.layout.autotextview_layout1, search_list_postcode);

                                            et_code.setAdapter(adapter);
                                            adapter.notifyDataSetChanged();

                                        }
                                    });
                                }


                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.e("maps", "failed");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Log.e("maps", "failed");
                }
            });
        } catch (ServicesException servicesException) {
            Log.e("maps", "failed");
            servicesException.printStackTrace();
        }
    }


    private void makeGeocodeSearch_code() {
        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + et_code.getText().toString())
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {

                                Point firstResultPoint = results.get(0).center();
                                Final_point = firstResultPoint;
                                LatLng cityLatLng = new LatLng(Final_point.latitude(), Final_point.longitude());
                                animateCameraToNewPosition(cityLatLng);
                                drawPolygonCircle(Final_point, 11);
                                Longitude = "" + Final_point.longitude();
                                Latitude = "" + Final_point.latitude();

                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    // Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            //  Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }

    private void animateCameraToNewPosition(LatLng latLng) {

        mapboxMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(10)
                        .build()), 1500);


        if (mapboxMap.getStyle() != null) {
            GeoJsonSource spaceStationSource = mapboxMap.getStyle().getSourceAs("source-id");
            if (spaceStationSource != null) {
                spaceStationSource.setGeoJson(FeatureCollection.fromFeature(
                        Feature.fromGeometry(Point.fromLngLat(latLng.getLongitude(), latLng.getLatitude()))
                ));
            }
        }

    }



    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap1) {
        mapboxMap = mapboxMap1;

        mapboxMap.setStyle(Style.MAPBOX_STREETS,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);


                        style.addImage(("marker_icon"), getResources().getDrawable(R.drawable.ic_dot));
                        style.addSource(new GeoJsonSource("source-id"));
                        style.addLayer(new SymbolLayer("layer-id", "source-id")
                                .withProperties(
                                        PropertyFactory.iconImage("marker_icon"),
                                        PropertyFactory.iconIgnorePlacement(true),
                                        PropertyFactory.iconAllowOverlap(true)
                                ));


                        style.addSource(new GeoJsonSource(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID));

                        FillLayer fillLayer = new FillLayer(TURF_CALCULATION_FILL_LAYER_ID,
                                TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID);
                        fillLayer.setProperties(
                                fillColor(getResources().getColor(R.color.purpule)),
                                fillOpacity(.7f));
                        style.addLayerBelow(fillLayer, CIRCLE_CENTER_LAYER_ID);


                    }
                });


    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(context)) {


            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(context, loadedMapStyle).build());
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);


        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        // Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);


                }
            });
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        is_true = false;
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private void drawPolygonCircle(final Point circleCenter,final int i) {


        try {
            if (circleCenter != null) {
                mapboxMap.getStyle(new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        if (style.isFullyLoaded()) {

                            LatLng cityLatLng = new LatLng(circleCenter.latitude(), circleCenter.longitude());

                            mapboxMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(new CameraPosition.Builder()
                                            .target(cityLatLng)
                                            .zoom(i)
                                            .build()), 1500);


                            Polygon polygonArea = getTurfPolygon(circleCenter, circleRadius, circleSteps, circleUnit);
                            GeoJsonSource polygonCircleSource = style.getSourceAs(TURF_CALCULATION_FILL_LAYER_GEOJSON_SOURCE_ID);
                            if (polygonCircleSource != null) {
                                polygonCircleSource.setGeoJson(Polygon.fromOuterInner(
                                        LineString.fromLngLats(TurfMeta.coordAll(polygonArea, false))));
                            }
                        }
                    }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private Polygon getTurfPolygon(@NonNull Point centerPoint1, @NonNull double radius,
                                   @NonNull int steps, @NonNull String units) {
        return TurfTransformation.circle(centerPoint1, radius, steps, units);
    }



    private void method_get_maps(String city_name) {
        try {

            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(token)
                    .country("GB")
                    .query("" + city_name)
                    .mode(GeocodingCriteria.MODE_PLACES)
                    .build();
            client.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call,
                                       Response<GeocodingResponse> response) {
                    try {
                        if (response.body() != null) {
                            List<CarmenFeature> results = response.body().features();
                            if (results.size() > 0) {

                                Point firstResultPoint = results.get(0).center();
                                Final_point = firstResultPoint;
                                LatLng cityLatLng = new LatLng(Final_point.latitude(), Final_point.longitude());
                                animateCameraToNewPosition(cityLatLng);
                                drawPolygonCircle(Final_point, 11);
                                Longitude = "" + Final_point.longitude();
                                Latitude = "" + Final_point.latitude();

                            } else {
//                                Toast.makeText(RegisterLocation_Activty.this, "no result",
//                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    // Timber.e("Geocoding Failure: " + throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            //  Timber.e("Error geocoding: " + servicesException.toString());
            servicesException.printStackTrace();
        }
    }


  //  NestedScrollView scrollview;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.VISIBLE);
//            }
        } else {
//            if (scrollview != null) {
//                scrollview.setVisibility(View.GONE);
//            }
        }
    }

}