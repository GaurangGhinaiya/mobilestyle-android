package com.moremy.style.Salon.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import androidx.fragment.app.Fragment;

import com.moremy.style.R;

import java.util.ArrayList;
import java.util.List;


public class Fragment_Shop extends Fragment {
    Context context;


    public Fragment_Shop() {
    }

    List<String> data_Shop_list = new ArrayList<>();
    Adapter_Shop adapter_Shop;

    GridView listview_Shop;


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shoplist, container, false);
        context = getActivity();


        data_Shop_list = new ArrayList<>();


        listview_Shop = view.findViewById(R.id.listview_countiy);



        return view;
    }


    public class Adapter_Shop extends BaseAdapter {

        Context context;
        List<String> listState;
        LayoutInflater inflater;


        public Adapter_Shop(Context applicationContext, List<String> spinnerArray) {
            this.context = applicationContext;
            this.listState = spinnerArray;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return listState.size() + 10;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            Adapter_Shop.ViewHolder viewHolder = null;
            if (view == null) {
                viewHolder = new Adapter_Shop.ViewHolder();
                view = inflater.inflate(R.layout.datalist_shoplist, viewGroup, false);
             //  viewHolder.card_mainlayout = view.findViewById(R.id.card_mainlayout);
                view.setTag(viewHolder);
            } else {
                viewHolder = (Adapter_Shop.ViewHolder) view.getTag();
            }


            return view;
        }

        private class ViewHolder {
          //  CardView card_mainlayout;
        }

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);


        if (isVisibleToUser) {


            if(listview_Shop != null){

                listview_Shop.setVisibility(View.VISIBLE);
                data_Shop_list = new ArrayList<>();
                adapter_Shop = new Adapter_Shop(context, data_Shop_list);
                listview_Shop.setAdapter(adapter_Shop);

            }


        }else {
            if(listview_Shop != null){
                 listview_Shop.setVisibility(View.GONE);
            }


        }

    }

}