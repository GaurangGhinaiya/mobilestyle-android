package com.moremy.style.Salon;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.moremy.style.API.Global_Service_Api;
import com.moremy.style.Activity.Base_Activity;
import com.moremy.style.AnalyticsApplication;


import com.moremy.style.CommanActivity.Booking_Fragment.ChatBookingDirect_Activity;
import com.moremy.style.CommanActivity.ChatOrderActivity;
import com.moremy.style.CommanActivity.Login_activty;
import com.moremy.style.Fragment.Fragment_DiscoverSalon;
import com.moremy.style.Fragment.Fragment_MyActivity;
import com.moremy.style.Fragment.Fragment_Search;
import com.moremy.style.Fragment.Fragment_Shopping;
import com.moremy.style.Model_MyIncome.Model_MyIncome_BookingServiceItem;
import com.moremy.style.R;
import com.moremy.style.Salon.Fragment.Fragment_Account_Salon;
import com.moremy.style.Utills.GPSTracker;
import com.moremy.style.Utills.Preferences;
import com.moremy.style.Utills.utills;
import com.moremy.style.fcm.Model_FCM_Data;
import com.moremy.style.fcm.Model_FCM_Response;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Salon_MainProfile_Activty extends Base_Activity {


    Context context;

    Handler mHandler;


    public static Fragment currentFragment_SALON;
    public static FragmentManager fragmentManager_SALON;
    public static FragmentTransaction fragmentTransaction_SALON;


    public static LinearLayout ll_discover_salon, ll_cart_salon, ll_search_salon, ll_profile_salon, ll_My_Activity_salon;
    public static TextView tv_count_total_salon;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_main_profile);
        context = Salon_MainProfile_Activty.this;
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        preferences = new Preferences(context);


        Intent intent = getIntent();
        String channelUrl = intent.getStringExtra("channelUrl");
        if (channelUrl != null && !channelUrl.equalsIgnoreCase("")) {
            getChannel_Details(channelUrl);
        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        fragmentManager_SALON = getSupportFragmentManager();
        fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
        mHandler = new Handler();

        fragmentManager_SALON = getSupportFragmentManager();
        currentFragment_SALON = new Fragment_DiscoverSalon();
        LoadFragment(currentFragment_SALON);


        method_tab();

        tv_count_total_salon = findViewById(R.id.tv_count_total);


    }


    Dialog progressDialog = null;

    public void getChannel_Details(String channelUrl) {
        if (utills.isOnline(this)) {
            progressDialog = utills.startLoader(this);

            AndroidNetworking.get(Global_Service_Api.APIget_send_bird_detail + channelUrl)
                    .addHeaders("Authorization", "Bearer " + preferences.getPRE_TOKEN())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String result) {
                            utills.stopLoader(progressDialog);
                            if (result == null || result == "") return;
                            Log.e("onResponse: ", result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                String flag = jsonObject.getString("flag");
                                String message = jsonObject.getString("message");


                                if (flag.equalsIgnoreCase("true")) {

                                    Model_FCM_Response Response = new Gson().fromJson(result.toString(), Model_FCM_Response.class);

                                    if (Response.getData() != null) {

                                        Model_FCM_Data activity_data = new Model_FCM_Data();
                                        activity_data = Response.getData();


                                        if (activity_data.getActivityType().equalsIgnoreCase("booking")) {

                                            final List<Model_MyIncome_BookingServiceItem> data__list = new ArrayList<>();
                                            data__list.addAll(activity_data.getBookingServiceItem());

                                            double price = 0;

                                            StringBuilder stringBuilder_ids = new StringBuilder();
                                            for (int i = 0; i < data__list.size(); i++) {
                                                if (!stringBuilder_ids.toString().equalsIgnoreCase("")) {
                                                    stringBuilder_ids.append(", ");
                                                }
                                                price = price + data__list.get(i).getServicePrice();
                                                stringBuilder_ids.append("" + data__list.get(i).getService().getName() + " (£" + utills.roundTwoDecimals(data__list.get(i).getServicePrice()) + ")");
                                            }


                                            Intent i = new Intent(context, ChatBookingDirect_Activity.class);
                                            i.putExtra("grup_Name", "" + stringBuilder_ids);
                                            i.putExtra("grup_price", "" + price);

                                            if (preferences.getPRE_SendBirdUserId().equalsIgnoreCase("" + activity_data.getUserId())) {
                                                i.putExtra("profile_pic", "" + activity_data.getStylist().getProfilePic());
                                            } else {
                                                i.putExtra("profile_pic", "" + activity_data.getUser().getProfilePic());
                                            }

                                            i.putExtra("channelUrl", "" + activity_data.getSendBirdId());
                                            i.putExtra("order_id", "" + activity_data.getId());

                                            startActivity(i);

                                        } else if (activity_data.getActivityType().equalsIgnoreCase("order")) {


                                            Intent i = new Intent(context, ChatOrderActivity.class);
                                            i.putExtra("channelUrl", "" + activity_data.getSendgridOrderId());
                                            i.putExtra("ProductId", "" + activity_data.getId());
                                            i.putExtra("ProductName", "" + activity_data.getProduct().getName());
                                            i.putExtra("ProductImage", "" + activity_data.getProduct().getPicture());
                                            i.putExtra("ProductPrize", "" + activity_data.getProductPrice());
                                            i.putExtra("SellerImage", "" + activity_data.getSeller().getProfilePic());
                                            i.putExtra("SellerId", "" + activity_data.getSeller().getId());
                                            i.putExtra("SellerName", "" + activity_data.getSeller().getName() + " " + activity_data.getSeller().getLastname());
                                            i.putExtra("ProductDeliveryCharge", "" + activity_data.getShippingPrice());
                                            i.putExtra("order_id", "" + activity_data.getId());
                                            if (activity_data.getPayment() != null) {
                                                i.putExtra("InvoiceId", "" + activity_data.getPayment().getStripeInvoiceNumber());
                                            } else {
                                                i.putExtra("InvoiceId", "");
                                            }
                                            i.putExtra("is_paid", "" + activity_data.getIsPaid());

                                            i.putExtra("getShippingDate", "" + activity_data.getShippingDate());
                                            i.putExtra("getDeliveryDate", "" + activity_data.getDeliveryDate());
                                            i.putExtra("getOrderDate", "" + activity_data.getOrderDate());

                                            i.putExtra("status", "" + activity_data.getStatus());

                                            startActivity(i);
                                        }

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            utills.stopLoader(progressDialog);
                            Log.d("API", anError.toString());
                        }
                    });

        }
    }


    private void method_tab() {
        ll_discover_salon = findViewById(R.id.ll_discover);
        ll_cart_salon = findViewById(R.id.ll_cart);
        ll_search_salon = findViewById(R.id.ll_search);
        ll_profile_salon = findViewById(R.id.ll_profile);
        ll_My_Activity_salon = findViewById(R.id.ll_My_Activity);

        method_comman();
        ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));


        ll_discover_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_comman();

                ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

                currentFragment_SALON = new Fragment_DiscoverSalon();
                LoadFragment(currentFragment_SALON);

            }
        });
        ll_cart_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_comman();


                ll_cart_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

                currentFragment_SALON = new Fragment_Shopping();
                LoadFragment(currentFragment_SALON);

            }
        });

        ll_My_Activity_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_comman();


                ll_My_Activity_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

                currentFragment_SALON = new Fragment_MyActivity();
                LoadFragment(currentFragment_SALON);

            }
        });
        ll_search_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_comman();

                ll_search_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

                currentFragment_SALON = new Fragment_Search("");
                LoadFragment(currentFragment_SALON);

            }
        });
        ll_profile_salon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method_comman();

                ll_profile_salon.setBackgroundColor(getResources().getColor(R.color.gray_trans1));

                currentFragment_SALON = new Fragment_Account_Salon();
                LoadFragment(currentFragment_SALON);

            }
        });

    }

    private void method_comman() {
        ll_discover_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_cart_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_search_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_profile_salon.setBackgroundColor(getResources().getColor(R.color.transparent));
        ll_My_Activity_salon.setBackgroundColor(getResources().getColor(R.color.transparent));

        try {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void LoadFragment(final Fragment fragment) {

        Runnable mPendingRunnable = new Runnable() {
            public void run() {
                fragmentTransaction_SALON = fragmentManager_SALON.beginTransaction();
                fragmentTransaction_SALON.replace(R.id.fmFragment, fragment);
                fragmentTransaction_SALON.commit();

            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
    }


    Tracker mTracker;

    @Override
    protected void onResume() {
        super.onResume();

        //sending tracking information
        mTracker.setScreenName("Main Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        Preferences preferences = new Preferences(context);
        SendBird.connect("" + preferences.getPRE_SendBirdUserId(), new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {    // Error.
                } else {


                    SendBird.getTotalUnreadMessageCount(new GroupChannel.GroupChannelTotalUnreadMessageCountHandler() {
                        @Override
                        public void onResult(int totalUnreadMessageCount, SendBirdException e) {
                            if (e != null) {    // Error.
                                return;
                            }

                            Log.e("totalUnreadMessageCount", "" + totalUnreadMessageCount);

                            if (totalUnreadMessageCount != 0) {
                                tv_count_total_salon.setVisibility(View.VISIBLE);
                                tv_count_total_salon.setText("" + totalUnreadMessageCount);
                            } else {
                                tv_count_total_salon.setVisibility(View.GONE);
                            }


                        }
                    });


                }

            }
        });


        try {
            if (!utills.Permissions_READ_EXTERNAL_location(this)) {
                utills.Request_READ_EXTERNAL_Location(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /*....................................*/

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {


        Log.e("onBackPressed", "" + fragmentManager_SALON.getBackStackEntryCount());

        if (fragmentManager_SALON.getBackStackEntryCount() > 1) {
            try {
                fragmentManager_SALON.popBackStack();

                FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                        fragmentManager_SALON.getBackStackEntryCount() - 1);
                getSupportFragmentManager().popBackStack(entry.getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().executePendingTransactions();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (fragmentManager_SALON.getBackStackEntryCount() == 1) {
            try {
                try {
                    fragmentManager_SALON.popBackStack();

                    FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                            0);
                    getSupportFragmentManager().popBackStack(entry.getId(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getSupportFragmentManager().executePendingTransactions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
            }
            this.doubleBackToExitPressedOnce = true;
            getFragmentManager().popBackStack();
            Toast.makeText(context, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
            return;

        }


    }


}
